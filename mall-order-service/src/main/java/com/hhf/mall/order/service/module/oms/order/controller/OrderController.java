package com.hhf.mall.order.service.module.oms.order.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.order.service.module.oms.order.dto.OrderChartDTO;
import com.hhf.mall.order.service.module.oms.order.dto.OrderDetailDTO;
import com.hhf.mall.order.service.module.oms.order.dto.OrderVO;
import com.hhf.mall.order.service.module.oms.order.dto.OrderVOMapper;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrder;
import com.hhf.mall.order.service.module.oms.order.entity.OrderDeleteStatus;
import com.hhf.mall.order.service.module.oms.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 订单
 * @date 2022-01-16 21:05
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/order")
@Validated
public class OrderController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderVOMapper orderVOMapper;

    /**
     * 查询订单列表
     * @param orderSn
     * @param status
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ORDER)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"orderSn", "status"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("deleteStatus", String.valueOf(OrderDeleteStatus.NO.getKey()));
        List<OmsOrder> list = orderService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<OmsOrder> pageInfo = new PageInfo<>(list);
            List<OrderVO> voList = orderVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 删除，其实是更新删除状态
     * @param id
     * @return
     */
    @ApiLog(note = "删除订单")
    @RequirePermissions(PermissionConst.ADMIN_ORDER)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        orderService.delete(id, "后台管理员");
        return ApiResponse.success();
    }

    /**
     * 关闭订单
     * @param id
     * @param note
     * @return
     */
    @ApiLog(note = "关闭订单")
    @RequirePermissions(PermissionConst.ADMIN_ORDER)
    @PostMapping("/updateClose")
    public ResponseEntity<ApiResponse> updateClose(@RequestParam Long id, @RequestParam String note) {
        orderService.close(id, note);
        return ApiResponse.success();
    }

    /**
     * 订单发货
     * @param id
     * @param deliveryCompany
     * @param deliverySn
     * @return
     */
    @ApiLog(note = "订单发货")
    @RequirePermissions(PermissionConst.ADMIN_ORDER)
    @PostMapping("/updateDelivery")
    public ResponseEntity<ApiResponse> updateDelivery(@RequestParam Long id, @RequestParam String deliveryCompany,
                                                  @RequestParam String deliverySn) {
        orderService.delivery(id, deliveryCompany, deliverySn);
        return ApiResponse.success();
    }

    /**
     * 查看订单详情
     * @param id
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ORDER)
    @GetMapping("/detail")
    public ResponseEntity<ApiResponse> detail(@RequestParam Long id) {
        OrderDetailDTO dto = orderService.detail(id);
        return ApiResponse.success(dto);
    }

    /**
     * 修改备注
     * @param id
     * @param note
     * @param status
     * @return
     */
    @ApiLog(note = "修改订单备注")
    @RequirePermissions(PermissionConst.ADMIN_ORDER)
    @PostMapping("/updateNote")
    public ResponseEntity<ApiResponse> updateNote(@RequestParam Long id, @RequestParam String note, @RequestParam Integer status) {
        orderService.note(id, note, status);
        return ApiResponse.success();
    }

    /**
     *
     * @param start
     * @param end
     * @return
     */
    @GetMapping("/chart")
    public ResponseEntity<ApiResponse> getOrderChart(@RequestParam String start, @RequestParam String end) {
        List<OrderChartDTO> dtoList = orderService.getOrderChart(start, end);
        return ApiResponse.success(dtoList);
    }
}
