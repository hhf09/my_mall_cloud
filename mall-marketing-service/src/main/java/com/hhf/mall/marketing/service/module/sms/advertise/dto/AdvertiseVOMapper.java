package com.hhf.mall.marketing.service.module.sms.advertise.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.product.ProductDTO;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.AdvertiseStatus;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertise;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 9:35
 */
@Component
public class AdvertiseVOMapper extends BaseDTOMapper<SmsHomeAdvertise, AdvertiseVO> {
    @Autowired
    private ProductClient productClient;

    @Override
    public AdvertiseVO mapToDTO(SmsHomeAdvertise entity) {
        AdvertiseVO vo = new AdvertiseVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(AdvertiseStatus.instanceOf(entity.getStatus()).getText());
        ResponseEntity<ApiResponse> responseEntity = productClient.getById(entity.getProductId());
        ApiResponse response = responseEntity.getBody();
        ProductDTO productDTO = response.toObject(ProductDTO.class);
        vo.setBrandName(productDTO.getBrandName());
        vo.setProductName(productDTO.getName());
        vo.setProductSn(productDTO.getProductSn());
        return vo;
    }

    @Override
    public List<AdvertiseVO> mapToDTOList(List<SmsHomeAdvertise> entityList) {
        List<AdvertiseVO> voList = new ArrayList<>();
        for (SmsHomeAdvertise entity : entityList) {
            AdvertiseVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
