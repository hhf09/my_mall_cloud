package com.hhf.mall.user.service.module.ums.memberReceiveAddress.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddress;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddressExample;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.mapper.UmsMemberReceiveAddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @date 2022-01-24 21:07
 */
@Service
public class ReceiveAddrService extends BaseService<UmsMemberReceiveAddress, Long, UmsMemberReceiveAddressExample> {

    @Autowired
    private UmsMemberReceiveAddressMapper receiveAddrMapper;

    @Override
    protected void before(UmsMemberReceiveAddress entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(UmsMemberReceiveAddress entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<UmsMemberReceiveAddress, Long, UmsMemberReceiveAddressExample> getMapper() {
        return receiveAddrMapper;
    }

    public List<UmsMemberReceiveAddress> list(ListParam listParam) {
        UmsMemberReceiveAddressExample example = new UmsMemberReceiveAddressExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return receiveAddrMapper.selectByExample(example);
    }

    public void selectOrderRule(UmsMemberReceiveAddressExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(UmsMemberReceiveAddressExample example, ListParam listParam) {
        UmsMemberReceiveAddressExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case ListParam.KEYWORD:
                    criteria.andNameLike("%" + value + "%");
                    break;
                case "phone":
                    criteria.andPhoneEqualTo(value);
                    break;
                case "memberId":
                    criteria.andMemberIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
