package com.hhf.mall.user.service.module.ums.permission.mapper;


import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermissionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsPermissionMapper extends BaseMapper<UmsPermission, Long, UmsPermissionExample> {
    int countByExample(UmsPermissionExample example);

    int deleteByExample(UmsPermissionExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsPermission record);

    int insertSelective(UmsPermission record);

    List<UmsPermission> selectByExample(UmsPermissionExample example);

    UmsPermission selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsPermission record, @Param("example") UmsPermissionExample example);

    int updateByExample(@Param("record") UmsPermission record, @Param("example") UmsPermissionExample example);

    int updateByPrimaryKeySelective(UmsPermission record);

    int updateByPrimaryKey(UmsPermission record);

    //自定义start

    /**
     * 根据角色id查询权限列表
     * @param roleId
     * @return
     */
    List<UmsPermission> getMenuListByRoleId(Long roleId);

    /**
     * 根据用户id查询权限value列表
     * @param userId
     * @return
     */
    List<String> getPermissionValueListByUserId(Long userId);
}