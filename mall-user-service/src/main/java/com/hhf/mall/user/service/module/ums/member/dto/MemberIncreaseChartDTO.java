package com.hhf.mall.user.service.module.ums.member.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 用户增长图表
 * @date 2022-02-15 22:39
 */
@Data
public class MemberIncreaseChartDTO {
    //月份
    private String months;

    //用户增长数
    private int increaseCount;
}
