package com.hhf.mall.feign.product;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author hhf
 * @description: 运费模板feign接口
 * @date 2022/7/20 17:42
 */
@FeignClient(value = Const.PRODUCT_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "FeightTemplateClient")
public interface FeightTemplateClient {
    String URL_PREFIX = Const.PRODUCT_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/feignTemplate";

    /**
     * 通过id查找
     *
     * @param id
     * @return
     */
    @GetMapping(URL_PREFIX + "/getById")
    ResponseEntity<ApiResponse> getById(@RequestParam Long id);
}
