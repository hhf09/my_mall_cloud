package com.hhf.mall.feign.user;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import com.hhf.mall.dto.user.ApiLogDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author hhf
 * @description 接口日志feign接口
 * @date 2023-01-27 20:57
 */
@FeignClient(value = Const.USER_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "ApiLogClient")
public interface ApiLogClient {
    String URL_PREFIX = Const.USER_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/apiLog";

    /**
     * 添加日志
     * @param dto
     * @return
     */
    @PostMapping(URL_PREFIX + "/add")
    ResponseEntity<ApiResponse> add(@RequestBody ApiLogDTO dto);

    /**
     * 查询
     * @param listParam
     * @return
     */
    @GetMapping(URL_PREFIX + "/list")
    ResponseEntity<ApiResponse> list(ListParam listParam);
}
