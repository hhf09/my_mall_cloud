package com.hhf.mall.order.service.module.oms.order.entity;

/**
 * @author hhf
 * @description: 订单支付方式枚举
 * @date 2022/12/28 10:50
 */
public enum OrderPayType {
    NOT_PAY(0, "未支付"),
    ALI_PAY(1, "支付宝"),
    WECHAT_PAY(2, "微信");

    private final int key;
    private final String text;

    OrderPayType(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static OrderPayType instanceOf(int key) {
        for (OrderPayType e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
