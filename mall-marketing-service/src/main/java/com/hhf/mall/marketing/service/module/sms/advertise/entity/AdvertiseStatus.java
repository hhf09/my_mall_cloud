package com.hhf.mall.marketing.service.module.sms.advertise.entity;

/**
 * @author hhf
 * @description: 广告上下线状态枚举
 * @date 2022/12/28 10:50
 */
public enum AdvertiseStatus {
    STATUS_DISABLE(0, "下线"),
    STATUS_ENABLE(1, "上线");

    private final int key;
    private final String text;

    AdvertiseStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static AdvertiseStatus instanceOf(int key) {
        for (AdvertiseStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }

}
