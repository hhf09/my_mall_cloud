package com.hhf.mall.user.service.module.sys.errorLog.dto;

import com.hhf.mall.common.constant.UserType;
import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLog;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdmin;
import com.hhf.mall.user.service.module.ums.admin.service.AdminService;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import com.hhf.mall.user.service.module.ums.member.service.MemberService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author hhf
 * @description: 
 * @date 2022-07-12 21:56
 */
@Component
public class ErrorLogVOMapper extends BaseDTOMapper<SysErrorLog, ErrorLogVO> {
    @Autowired
    private AdminService adminService;
    @Autowired
    private MemberService memberService;

    @Override
    public ErrorLogVO mapToDTO(SysErrorLog entity) {
        ErrorLogVO vo = new ErrorLogVO();
        BeanUtils.copyProperties(entity, vo);
        if (Objects.equals(entity.getUserType(), UserType.MEMBER.getKey())) {
            UmsMember member = memberService.selectByPrimaryKey(entity.getUserId());
            vo.setUserName(Objects.nonNull(member) ? member.getUsername() : null);
        } else {
            UmsAdmin admin = adminService.selectByPrimaryKey(entity.getUserId());
            vo.setUserName(Objects.nonNull(admin) ? admin.getUsername() : null);
        }
        return vo;
    }

    @Override
    public List<ErrorLogVO> mapToDTOList(List<SysErrorLog> entityList) {
        List<ErrorLogVO> voList = new ArrayList<>();
        for (SysErrorLog entity : entityList) {
            ErrorLogVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
