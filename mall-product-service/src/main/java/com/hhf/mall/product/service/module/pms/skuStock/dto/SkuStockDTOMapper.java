package com.hhf.mall.product.service.module.pms.skuStock.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStock;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 10:36
 */
@Component
public class SkuStockDTOMapper extends BaseDTOMapper<PmsSkuStock, SkuStockDTO> {
    @Override
    public SkuStockDTO mapToDTO(PmsSkuStock entity) {
        SkuStockDTO dto = new SkuStockDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<SkuStockDTO> mapToDTOList(List<PmsSkuStock> entityList) {
        List<SkuStockDTO> dtoList = new ArrayList<>();
        for (PmsSkuStock entity : entityList) {
            SkuStockDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
