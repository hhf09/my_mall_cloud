package com.hhf.mall.product.service.module.pms.commentReply.controller;

import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.commentReply.dto.CommentReplyDTO;
import com.hhf.mall.product.service.module.pms.commentReply.dto.CommentReplyDTOMapper;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReply;
import com.hhf.mall.product.service.module.pms.commentReply.service.CommentReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description: 评论回复
 * @date 2022/12/29 9:19
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/reply")
@Validated
public class CommentReplyWebController {
    @Autowired
    private CommentReplyService commentReplyService;
    @Autowired
    private CommentReplyDTOMapper commentReplyDTOMapper;

    /**
     * 添加回复
     * @param reply
     * @return
     */
    @ApiLog(note = "添加回复")
    @RequirePermissions(PermissionConst.WEB_COMMENT_REPLY)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody PmsCommentReply reply) {
        reply.setDefault();
        commentReplyService.add(reply);
        return ApiResponse.success();
    }

    /**
     * 查找某个用户的所有回复
     * @param memberId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_COMMENT_REPLY)
    @GetMapping("/listByMemberId")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsCommentReply> list = commentReplyService.listByMemberId(listParam);
        List<CommentReplyDTO> dtoList = commentReplyDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }
}
