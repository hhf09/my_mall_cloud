package com.hhf.mall.user.service;

import com.hhf.mall.common.Const;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hhf
 * @description 用户服务
 * @date 2022-06-28 23:43
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = Const.USER_SERVICE_MODULE_PACKAGE + Const.MAPPER_SCAN_PACKAGE)
@EnableFeignClients(basePackages = Const.FEIGN_PACKAGE)
public class UserServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }
}
