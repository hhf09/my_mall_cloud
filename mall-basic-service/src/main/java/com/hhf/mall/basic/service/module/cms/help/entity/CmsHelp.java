package com.hhf.mall.basic.service.module.cms.help.entity;

import java.io.Serializable;
import java.util.Date;

public class CmsHelp implements Serializable {

    private Long id;

    private Long categoryId;

    /**
     * 标题
     *
     * @mbggenerated
     */
    private String title;

    /**
     * 启用状态：0->禁用；1->启用
     *
     * @mbggenerated
     */
    private Integer status;

    /**
     * 修改时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 内容
     *
     * @mbggenerated
     */
    private String content;

    /**
     * 更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", title=").append(title);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", content=").append(content);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public void setDefault() {
        this.setStatus(HelpStatus.STATUS_ENABLE.getKey());
        this.setCreateTime(new Date());
        this.setUpdateTime(new Date());
    }
}