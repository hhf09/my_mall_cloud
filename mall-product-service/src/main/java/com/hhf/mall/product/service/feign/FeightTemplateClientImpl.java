package com.hhf.mall.product.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.feign.product.FeightTemplateClient;
import com.hhf.mall.product.service.module.pms.feightTemplate.dto.FeightTemplateDTO;
import com.hhf.mall.product.service.module.pms.feightTemplate.dto.FeightTemplateDTOMapper;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplate;
import com.hhf.mall.product.service.module.pms.feightTemplate.service.FeightTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:46
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/feightTemplate")
public class FeightTemplateClientImpl implements FeightTemplateClient {
    @Autowired
    private FeightTemplateService feightTemplateService;
    @Autowired
    private FeightTemplateDTOMapper feightTemplateDTOMapper;

    @Override
    @GetMapping("/getById")
    public ResponseEntity<ApiResponse> getById(Long id) {
        PmsFeightTemplate feightTemplate = feightTemplateService.selectByPrimaryKey(id);
        FeightTemplateDTO dto = feightTemplateDTOMapper.mapToDTO(feightTemplate);
        return ApiResponse.success(dto);
    }
}
