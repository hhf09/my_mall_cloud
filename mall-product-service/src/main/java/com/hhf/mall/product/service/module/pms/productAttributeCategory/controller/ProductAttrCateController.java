package com.hhf.mall.product.service.module.pms.productAttributeCategory.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.dto.ProductAttributeCateItem;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.dto.ProductAttributeCategoryVO;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.dto.ProductAttributeCategoryVOMapper;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.entity.PmsProductAttributeCategory;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.service.ProductAttrCateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author hhf
 * @description 商品属性分类
 * @date 2022-01-09 19:44
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/productAttributeCategory")
@Validated
public class ProductAttrCateController {
    @Autowired
    private ProductAttrCateService productAttrCateService;
    @Autowired
    private ProductAttributeCategoryVOMapper productAttributeCategoryVOMapper;

    /**
     * 获取所有商品属性分类及其下属性
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE_CATEGORY)
    @GetMapping("/list/withAttr")
    public ResponseEntity<ApiResponse> getListWithAttr() {
        List<ProductAttributeCateItem> dtoList = productAttrCateService.getListWithAttr();
        return ApiResponse.success(dtoList);
    }

    /**
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE_CATEGORY)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsProductAttributeCategory> list = productAttrCateService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<PmsProductAttributeCategory> pageInfo = new PageInfo<>(list);
            List<ProductAttributeCategoryVO> voList = productAttributeCategoryVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @ApiLog(note = "添加商品属性分类")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE_CATEGORY)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestParam String name) {
        PmsProductAttributeCategory productAttributeCategory = new PmsProductAttributeCategory();
        productAttributeCategory.setName(name);
        productAttributeCategory.setDefault();
        productAttrCateService.add(productAttributeCategory);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新商品属性分类")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE_CATEGORY)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@RequestParam Long id, @RequestParam String name) {
        PmsProductAttributeCategory productAttributeCategory = new PmsProductAttributeCategory();
        productAttributeCategory.setName(name);
        productAttributeCategory.setId(id);
        productAttrCateService.updateByPrimaryKeySelective(productAttributeCategory);
        return ApiResponse.success();
    }

    @ApiLog(note = "删除商品属性分类")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE_CATEGORY)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        productAttrCateService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }
}
