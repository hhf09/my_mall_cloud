package com.hhf.mall.order.service.module.oms.orderReturnReason.entity;

/**
 * @author hhf
 * @description: 退货原因状态枚举
 * @date 2022/12/28 10:50
 */
public enum ReturnReasonStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    ReturnReasonStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static ReturnReasonStatus instanceOf(int key) {
        for (ReturnReasonStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
