package com.hhf.mall.auth.service.config;

import com.hhf.mall.auth.service.service.CustomUserDetailsService;
import com.hhf.mall.common.constant.AuthConst;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.KeyPair;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 认证服务器配置
 * @date 2023/2/7 15:58
 */
@Configuration
@EnableAuthorizationServer
@AllArgsConstructor
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    @Autowired
    private CertConfig certConfig;

    private final PasswordEncoder passwordEncoder;
    private final CustomUserDetailsService userDetailsService;
    private final AuthenticationManager authenticationManager;
    private final CustomTokenEnhancer tokenEnhancer;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                //后台用户client_id
                .withClient(AuthConst.ADMIN_CLIENT_ID)
                //后台用户client_secret
                .secret(passwordEncoder.encode(AuthConst.ADMIN_CLIENT_SECRET))
                .scopes(AuthConst.SCOPES)
                //授权模式
                .authorizedGrantTypes(AuthConst.PASSWORD_GRANT_TYPE, AuthConst.REFRESH_TOKEN_GRANT_TYPE)
                //token过期时间，单位秒
                .accessTokenValiditySeconds(AuthConst.token_timeout)
                //refresh_token过期时间，单位秒
                .refreshTokenValiditySeconds(AuthConst.refresh_token_timeout)
                .and()
                //前台用户client_id
                .withClient(AuthConst.MEMBER_CLIENT_ID)
                //前台用户client_secret
                .secret(passwordEncoder.encode(AuthConst.MEMBER_CLIENT_SECRET))
                .scopes(AuthConst.SCOPES)
                .authorizedGrantTypes(AuthConst.PASSWORD_GRANT_TYPE, AuthConst.REFRESH_TOKEN_GRANT_TYPE)
                .accessTokenValiditySeconds(AuthConst.token_timeout)
                .refreshTokenValiditySeconds(AuthConst.refresh_token_timeout);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        List<TokenEnhancer> enhancerList = new ArrayList<>();
        enhancerList.add(tokenEnhancer);
        enhancerList.add(accessTokenConverter());
        tokenEnhancerChain.setTokenEnhancers(enhancerList);
        endpoints.authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService)
                .accessTokenConverter(accessTokenConverter())
                .tokenEnhancer(tokenEnhancerChain);
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
        accessTokenConverter.setKeyPair(keyPair());
        return accessTokenConverter;
    }

    @Bean
    public KeyPair keyPair() {
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(new ClassPathResource(certConfig.getFileName()),
                certConfig.getPassword().toCharArray());
        return keyStoreKeyFactory.getKeyPair(certConfig.getAlias(), certConfig.getPassword().toCharArray());
    }
}
