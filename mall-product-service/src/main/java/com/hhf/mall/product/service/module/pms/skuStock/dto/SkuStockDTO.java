package com.hhf.mall.product.service.module.pms.skuStock.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 10:36
 */
@Data
public class SkuStockDTO {
    private Long id;
    private Long productId;
    private String skuCode;
    private BigDecimal price;
    private Integer stock;
    private Integer lowStock;
    private String sp;
    private String pic;
    private Integer sale;
    private Integer lockStock;
}
