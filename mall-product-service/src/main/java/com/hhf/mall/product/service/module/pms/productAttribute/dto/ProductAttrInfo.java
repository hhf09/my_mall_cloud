package com.hhf.mall.product.service.module.pms.productAttribute.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 商品分类对应属性信息
 * @date 2022-01-09 20:49
 */
@Data
public class ProductAttrInfo {
    private Long attributeId;
    private Long attributeCategoryId;
}
