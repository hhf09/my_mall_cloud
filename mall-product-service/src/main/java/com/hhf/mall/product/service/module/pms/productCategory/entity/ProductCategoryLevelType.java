package com.hhf.mall.product.service.module.pms.productCategory.entity;

/**
 * @author hhf
 * @description: 商品分类级别枚举
 * @date 2022/12/28 10:50
 */
public enum ProductCategoryLevelType {
    LEVEL_ONE(0, "1级分类"),
    LEVEL_TWO(1, "2级分类");

    private final int key;
    private final String text;

    ProductCategoryLevelType(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static ProductCategoryLevelType instanceOf(int key) {
        for (ProductCategoryLevelType e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
