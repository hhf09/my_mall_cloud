package com.hhf.mall.common.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolationException;

/**
 * 底层全局异常处理
 */
public class BaseExceptionHandler {

    /**
     * 处理controller方法的实体类参数异常
     *
     * @param e
     * @return
     */
    public ResponseEntity<ApiResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        StringBuffer stringBuffer = new StringBuffer();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = error.getDefaultMessage();
            stringBuffer.append(errorMessage).append(";");
        });
        return ApiResponse.error(ApiCode.REQUEST_VALIDATION_FAILED.getCode(), stringBuffer.toString());
    }

    /**
     * 处理controller方法的参数异常
     *
     * @param e
     * @return
     */
    public ResponseEntity<ApiResponse> handleConstraintViolationException(ConstraintViolationException e) {
        return ApiResponse.error(ApiCode.REQUEST_VALIDATION_FAILED.getCode(), ApiCode.REQUEST_VALIDATION_FAILED.getMessage());
    }

    /**
     * 处理业务逻辑异常
     * @param e
     * @return
     */
    public ResponseEntity<ApiResponse> handleCodeException(CodeException e) {
        return ApiResponse.error(e.getMessage());
    }
}

