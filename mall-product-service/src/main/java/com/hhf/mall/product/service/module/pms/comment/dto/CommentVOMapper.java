package com.hhf.mall.product.service.module.pms.comment.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.user.MemberDTO;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsComment;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.product.mapper.PmsProductMapper;
import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStock;
import com.hhf.mall.product.service.module.pms.skuStock.mapper.PmsSkuStockMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-07-13 21:37
 */
@Component
public class CommentVOMapper extends BaseDTOMapper<PmsComment, CommentVO> {
    @Autowired
    private MemberClient memberClient;
    @Autowired
    private PmsSkuStockMapper skuStockMapper;
    @Autowired
    private PmsProductMapper productMapper;

    @Override
    public CommentVO mapToDTO(PmsComment entity) {
        CommentVO vo = new CommentVO();
        BeanUtils.copyProperties(entity, vo);
        ResponseEntity<ApiResponse> responseEntity = memberClient.getById(entity.getMemberId());
        ApiResponse response = responseEntity.getBody();
        MemberDTO memberDTO = response.toObject(MemberDTO.class);
        vo.setMemberNickname(memberDTO.getNickname());
        vo.setMemberIcon(memberDTO.getIcon());
        PmsSkuStock skuStock = skuStockMapper.selectByPrimaryKey(entity.getSkuStockId());
        vo.setProductAttr(skuStock.getSp());
        PmsProduct product = productMapper.selectByPrimaryKey(skuStock.getProductId());
        vo.setProductName(product.getName());
        return vo;
    }

    @Override
    public List<CommentVO> mapToDTOList(List<PmsComment> entityList) {
        List<CommentVO> voList = new ArrayList<>();
        for (PmsComment entity : entityList) {
            CommentVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
