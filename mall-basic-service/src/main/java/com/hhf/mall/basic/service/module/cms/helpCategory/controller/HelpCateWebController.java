package com.hhf.mall.basic.service.module.cms.helpCategory.controller;

import com.hhf.mall.basic.service.module.cms.helpCategory.dto.HelpCategoryDTO;
import com.hhf.mall.basic.service.module.cms.helpCategory.dto.HelpCategoryDTOMapper;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategory;
import com.hhf.mall.basic.service.module.cms.helpCategory.service.HelpCateService;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 帮助分类
 * @date 2022-02-06 17:42
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/helpCategory")
@Validated
public class HelpCateWebController {

    @Autowired
    private HelpCateService helpCateService;
    @Autowired
    private HelpCategoryDTOMapper helpCategoryDTOMapper;

    /**
     * 查找所有分类
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_HELP_CATEGORY)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<CmsHelpCategory> list = helpCateService.list(listParam);
        List<HelpCategoryDTO> dtoList = helpCategoryDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }
}
