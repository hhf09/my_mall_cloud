package com.hhf.mall.product.service.module.pms.product.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:23
 */
@Component
public class ProductDTOMapper extends BaseDTOMapper<PmsProduct, ProductDTO> {
    @Override
    public ProductDTO mapToDTO(PmsProduct entity) {
        ProductDTO dto = new ProductDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<ProductDTO> mapToDTOList(List<PmsProduct> entityList) {
        List<ProductDTO> dtoList = new ArrayList<>();
        for (PmsProduct entity : entityList) {
            ProductDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
