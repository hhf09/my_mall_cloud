package com.hhf.mall.product.service.module.pms.productAttributeValue.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.productAttributeValue.entity.PmsProductAttributeValue;
import com.hhf.mall.product.service.module.pms.productAttributeValue.entity.PmsProductAttributeValueExample;
import com.hhf.mall.product.service.module.pms.productAttributeValue.mapper.PmsProductAttributeValueMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/3 14:05
 */
@Service
public class ProductAttributeValueService extends BaseService<PmsProductAttributeValue, Long, PmsProductAttributeValueExample> {
    @Autowired
    private PmsProductAttributeValueMapper productAttributeValueMapper;

    @Override
    protected void before(PmsProductAttributeValue entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(PmsProductAttributeValue entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsProductAttributeValue, Long, PmsProductAttributeValueExample> getMapper() {
        return productAttributeValueMapper;
    }

    public void selectOrderRule(PmsProductAttributeValueExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsProductAttributeValueExample example, ListParam listParam) {
        PmsProductAttributeValueExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "productId":
                    criteria.andProductIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
