package com.hhf.mall.product.service.module.pms.productCategory.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProductExample;
import com.hhf.mall.product.service.module.pms.product.service.ProductService;
import com.hhf.mall.product.service.module.pms.productCategory.dto.PmsProductCateWithChildrenItem;
import com.hhf.mall.product.service.module.pms.productCategory.dto.ProductCateSaleChartDTO;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategory;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategoryExample;
import com.hhf.mall.product.service.module.pms.productCategory.mapper.PmsProductCategoryMapper;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.entity.PmsProductCategoryAttributeRelation;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.entity.PmsProductCategoryAttributeRelationExample;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.service.ProductCategoryAttributeRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-09 0:07
 */
@Service
public class ProductCateService extends BaseService<PmsProductCategory, Long, PmsProductCategoryExample> {
    @Autowired
    private PmsProductCategoryMapper productCategoryMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductCategoryAttributeRelationService productCategoryAttributeRelationService;

//    @Autowired
//    private OmsOrderItemMapper orderItemMapper;


    @Override
    protected void before(PmsProductCategory entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(PmsProductCategory entity, ActionType actionType) {
        if (actionType == ActionType.UPDATE) {
            // 更新商品分类时要更新商品中的名称
            PmsProductExample example = new PmsProductExample();
            example.createCriteria().andProductCategoryIdEqualTo(entity.getId());
            PmsProduct product = new PmsProduct();
            product.setProductCategoryName(entity.getName());
            productService.updateByExampleSelective(product, example);

            //删除旧的分类-属性联系
            PmsProductCategoryAttributeRelationExample example2 = new PmsProductCategoryAttributeRelationExample();
            ListParam listParam = new ListParam().add("productCategoryId", String.valueOf(entity.getId()));
            productCategoryAttributeRelationService.selectCondition(example2, listParam);
            productCategoryAttributeRelationService.deleteByExample(example2);
        }
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            Long productCategoryId = entity.getId();
            List<Long> productAttributeIdList = entity.getProductAttributeIdList();
            if (CollUtil.isNotEmpty(productAttributeIdList)) {
                insertRelationList(productCategoryId, productAttributeIdList);
            }
        }
        if (actionType == ActionType.DELETE) {
            //删除商品
            PmsProductExample example = new PmsProductExample();
            example.createCriteria().andProductCategoryIdEqualTo(entity.getId());
            productService.deleteByExample(example);

            //删除分类-属性联系
            PmsProductCategoryAttributeRelationExample example2 = new PmsProductCategoryAttributeRelationExample();
            ListParam listParam = new ListParam().add("productCategoryId", String.valueOf(entity.getId()));
            productCategoryAttributeRelationService.selectCondition(example2, listParam);
            productCategoryAttributeRelationService.deleteByExample(example2);

            //删除子分类
            PmsProductCategoryExample example3 = new PmsProductCategoryExample();
            example3.createCriteria().andParentIdEqualTo(entity.getId());
            productCategoryMapper.deleteByExample(example3);
        }
    }

    @Override
    protected BaseMapper<PmsProductCategory, Long, PmsProductCategoryExample> getMapper() {
        return productCategoryMapper;
    }

    public List<PmsProductCategory> list(ListParam listParam) {
        PmsProductCategoryExample example = new PmsProductCategoryExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return productCategoryMapper.selectByExampleWithBLOBs(example);
    }

    private void insertRelationList(Long productCategoryId, List<Long> productAttributeIdList) {
        List<PmsProductCategoryAttributeRelation> relationList = new ArrayList<>();
        for (Long productAttrId : productAttributeIdList) {
            PmsProductCategoryAttributeRelation relation = new PmsProductCategoryAttributeRelation();
            relation.setProductAttributeId(productAttrId);
            relation.setProductCategoryId(productCategoryId);
            relationList.add(relation);
        }
        productCategoryAttributeRelationService.batchInsert(relationList);
    }

    public List<PmsProductCateWithChildrenItem> listWithChildren() {
        return productCategoryMapper.listWithChildren();
    }

    public List<ProductCateSaleChartDTO> getSaleChart() {
//        return orderItemMapper.getSaleChart();
        return null;
    }

    public void selectOrderRule(PmsProductCategoryExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsProductCategoryExample example, ListParam listParam) {
        PmsProductCategoryExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "parentId":
                    criteria.andParentIdEqualTo(Long.valueOf(value));
                    break;
                case "showStatus":
                    criteria.andShowStatusEqualTo(Integer.valueOf(value));
                    break;
            }
        }
    }
}
