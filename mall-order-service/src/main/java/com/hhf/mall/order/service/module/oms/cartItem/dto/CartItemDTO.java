package com.hhf.mall.order.service.module.oms.cartItem.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 13:37
 */
@Data
public class CartItemDTO {
    private Long id;
    private Long productId;
    private Long productSkuId;
    private Long memberId;
    private Integer quantity;
    private BigDecimal price;
    private String productPic;
    private String productName;
    private String productSubTitle;
    private String productSkuCode;
    private String memberNickname;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createDate;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date modifyDate;
    private Integer deleteStatus;
    private Long productCategoryId;
    private String productBrand;
    private String productSn;
    private String productAttr;
    private Integer checkStatus;
    private String checkStatusText;
    private Long brandId;
}
