package com.hhf.mall.marketing.service.module.sms.newProduct.dto;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.product.ProductDTO;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.NewProductRecommendStatus;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.SmsHomeNewProduct;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description:
 * @date 2022/12/28 14:32
 */
@Component
public class NewProductVOMapper extends BaseDTOMapper<SmsHomeNewProduct, NewProductVO> {
    @Autowired
    private ProductClient productClient;

    @Override
    public NewProductVO mapToDTO(SmsHomeNewProduct entity) {
        NewProductVO vo = new NewProductVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setRecommendStatusText(NewProductRecommendStatus.instanceOf(entity.getRecommendStatus()).getText());
        ResponseEntity<ApiResponse> responseEntity = productClient.getById(entity.getProductId());
        String jsonString = JSON.toJSONString(responseEntity.getBody().getData().get("object"));
        ProductDTO productDTO = JSONObject.parseObject(jsonString, ProductDTO.class);
        vo.setProductName(productDTO.getName());
        vo.setBrandName(productDTO.getBrandName());
        vo.setPrice(productDTO.getPrice());
        vo.setProductPic(productDTO.getPic());
        vo.setSubTitle(productDTO.getSubTitle());
        return vo;
    }

    @Override
    public List<NewProductVO> mapToDTOList(List<SmsHomeNewProduct> entityList) {
        List<NewProductVO> voList = new ArrayList<>();
        for (SmsHomeNewProduct entity : entityList) {
            NewProductVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
