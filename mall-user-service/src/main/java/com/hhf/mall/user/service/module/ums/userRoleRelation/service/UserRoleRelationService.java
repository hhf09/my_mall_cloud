package com.hhf.mall.user.service.module.ums.userRoleRelation.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.ums.userRoleRelation.entity.UmsUserRoleRelation;
import com.hhf.mall.user.service.module.ums.userRoleRelation.entity.UmsUserRoleRelationExample;
import com.hhf.mall.user.service.module.ums.userRoleRelation.mapper.UmsUserRoleRelationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/3 13:35
 */
@Service
public class UserRoleRelationService extends BaseService<UmsUserRoleRelation, Long, UmsUserRoleRelationExample> {
    @Autowired
    private UmsUserRoleRelationMapper userRoleRelationMapper;

    @Override
    protected void before(UmsUserRoleRelation entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(UmsUserRoleRelation entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<UmsUserRoleRelation, Long, UmsUserRoleRelationExample> getMapper() {
        return userRoleRelationMapper;
    }

    public void insertAll(List<UmsUserRoleRelation> list) {
        userRoleRelationMapper.insertAll(list);
    }

    public void selectOrderRule(UmsUserRoleRelationExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(UmsUserRoleRelationExample example, ListParam listParam) {
        UmsUserRoleRelationExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "userId":
                    criteria.andUserIdEqualTo(Long.valueOf(value));
                    break;
                case "roleId":
                    criteria.andRoleIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
