package com.hhf.mall.product.service.module.pms.comment.entity;

/**
 * @author hhf
 * @description: 评价程度枚举
 * @date 2022/12/28 10:50
 */
public enum CommentEvaluationStatus {
    BAD(0, "差评"),
    MED(1, "中评"),
    GOOD(2, "好评");

    private final int key;
    private final String text;

    CommentEvaluationStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }
}
