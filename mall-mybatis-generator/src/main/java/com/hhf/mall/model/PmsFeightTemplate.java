package com.hhf.mall.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class PmsFeightTemplate implements Serializable {
    private Long id;

    /**
     * 包装类型
     *
     * @mbggenerated
     */
    private String packingType;

    /**
     * 重量
     *
     * @mbggenerated
     */
    private String weight;

    /**
     * 是否易碎
     *
     * @mbggenerated
     */
    private String isFragile;

    /**
     * 总运费
     *
     * @mbggenerated
     */
    private BigDecimal totalFeight;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPackingType() {
        return packingType;
    }

    public void setPackingType(String packingType) {
        this.packingType = packingType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getIsFragile() {
        return isFragile;
    }

    public void setIsFragile(String isFragile) {
        this.isFragile = isFragile;
    }

    public BigDecimal getTotalFeight() {
        return totalFeight;
    }

    public void setTotalFeight(BigDecimal totalFeight) {
        this.totalFeight = totalFeight;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", packingType=").append(packingType);
        sb.append(", weight=").append(weight);
        sb.append(", isFragile=").append(isFragile);
        sb.append(", totalFeight=").append(totalFeight);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}