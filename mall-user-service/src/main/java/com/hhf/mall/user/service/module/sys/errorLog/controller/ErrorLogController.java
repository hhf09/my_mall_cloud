package com.hhf.mall.user.service.module.sys.errorLog.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.sys.errorLog.dto.ErrorLogVO;
import com.hhf.mall.user.service.module.sys.errorLog.dto.ErrorLogVOMapper;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLog;
import com.hhf.mall.user.service.module.sys.errorLog.service.ErrorLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2023-01-27 18:11
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/errorLog")
@Validated
public class ErrorLogController {
    @Autowired
    private ErrorLogService errorLogService;
    @Autowired
    private ErrorLogVOMapper errorLogVOMapper;

    /**
     * 分页查找
     * @param createTime
     * @param serviceSign
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ERROR_LOG)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"createTime", "serviceSign"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        String createTime = listParam.getParamValue("createTime");
        if (StrUtil.isNotEmpty(createTime)) {
            String start = createTime + " 00:00:00";
            String end = createTime + " 23:59:59";
            listParam.add("createTimeBt", start + "," + end);
        }
        listParam.remove("createTime");
        List<SysErrorLog> list = errorLogService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SysErrorLog> pageInfo = new PageInfo<>(list);
            List<ErrorLogVO> voList = errorLogVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }
}
