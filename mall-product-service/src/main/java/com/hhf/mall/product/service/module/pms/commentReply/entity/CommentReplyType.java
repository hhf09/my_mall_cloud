package com.hhf.mall.product.service.module.pms.commentReply.entity;

/**
 * @author hhf
 * @description: 品牌状态枚举
 * @date 2022/12/28 10:50
 */
public enum CommentReplyType {
    MEMBER(0, "用户"),
    ADMIN(1, "管理员");

    private final int key;
    private final String text;

    CommentReplyType(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static CommentReplyType instanceOf(int key) {
        for (CommentReplyType e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }

}
