package com.hhf.mall.basic.service.module.cms.helpCategory.dto;

import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategory;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.HelpCategoryStatus;
import com.hhf.mall.common.dto.BaseDTOMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 11:28
 */
@Component
public class HelpCategoryDTOMapper extends BaseDTOMapper<CmsHelpCategory, HelpCategoryDTO> {
    @Override
    public HelpCategoryDTO mapToDTO(CmsHelpCategory entity) {
        HelpCategoryDTO dto = new HelpCategoryDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setStatusText(HelpCategoryStatus.instanceOf(entity.getStatus()).getText());
        return dto;
    }

    @Override
    public List<HelpCategoryDTO> mapToDTOList(List<CmsHelpCategory> entityList) {
        List<HelpCategoryDTO> dtoList = new ArrayList<>();
        for (CmsHelpCategory entity : entityList) {
            HelpCategoryDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
