package com.hhf.mall.product.service.module.pms.commentReply.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.user.MemberDTO;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.product.service.module.pms.commentReply.entity.CommentReplyType;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReply;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 11:57
 */
@Component
public class CommentReplyDTOMapper extends BaseDTOMapper<PmsCommentReply, CommentReplyDTO> {
    @Autowired
    private MemberClient memberClient;

    @Override
    public CommentReplyDTO mapToDTO(PmsCommentReply entity) {
        CommentReplyDTO dto = new CommentReplyDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setTypeText(CommentReplyType.instanceOf(entity.getType()).getText());
        ResponseEntity<ApiResponse> responseEntity = memberClient.getById(entity.getMemberId());
        ApiResponse response = responseEntity.getBody();
        MemberDTO memberDTO = response.toObject(MemberDTO.class);
        dto.setMemberIcon(memberDTO.getIcon());
        dto.setMemberNickname(memberDTO.getNickname());
        return dto;
    }

    @Override
    public List<CommentReplyDTO> mapToDTOList(List<PmsCommentReply> entityList) {
        List<CommentReplyDTO> dtoList = new ArrayList<>();
        for (PmsCommentReply entity : entityList) {
            CommentReplyDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
