package com.hhf.mall.user.service.init.dto;

import lombok.Data;

/**
 * @author hhf
 * @description
 * @date 2023-02-18 10:52
 */
@Data
public class PermissionDTO {
    /**
     * 权限名称
     */
    private String title;
    /**
     * 权限标志
     */
    private String value;
}
