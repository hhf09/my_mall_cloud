package com.hhf.mall.user.service.module.sys.errorLog.mapper;

import java.util.List;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLog;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLogExample;
import org.apache.ibatis.annotations.Param;

public interface SysErrorLogMapper extends BaseMapper<SysErrorLog, Long, SysErrorLogExample> {
    int countByExample(SysErrorLogExample example);

    int deleteByExample(SysErrorLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SysErrorLog record);

    int insertSelective(SysErrorLog record);

    List<SysErrorLog> selectByExample(SysErrorLogExample example);

    SysErrorLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SysErrorLog record, @Param("example") SysErrorLogExample example);

    int updateByExample(@Param("record") SysErrorLog record, @Param("example") SysErrorLogExample example);

    int updateByPrimaryKeySelective(SysErrorLog record);

    int updateByPrimaryKey(SysErrorLog record);
}