package com.hhf.mall.common.constant;

/**
 * @author hhf
 * @description 用户类型枚举
 * @date 2023-01-26 21:31
 */
public enum UserType {
    MEMBER(0, "前台用户"),
    ADMIN(1, "后台用户");

    private final int key;
    private final String text;


    UserType(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }
}
