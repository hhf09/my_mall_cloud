package com.hhf.mall.order.service.module.oms.orderReturnApply.controller;

import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.user.MemberDTO;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItem;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItemExample;
import com.hhf.mall.order.service.module.oms.orderItem.service.OrderItemService;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApply;
import com.hhf.mall.order.service.module.oms.orderReturnApply.service.ReturnApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @author hhf
 * @description 退货申请
 * @date 2022-02-09 15:06
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/returnApply")
@Validated
public class ReturnApplyWebController {
    @Autowired
    private ReturnApplyService returnApplyService;
    @Autowired
    private OrderItemService orderItemService;
    @Autowired
    private MemberClient memberClient;

    @ApiLog(note = "添加退货申请")
    @RequirePermissions(PermissionConst.WEB_ORDER_REFUND)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody OmsOrderReturnApply apply) {
        ResponseEntity<ApiResponse> responseEntity = memberClient.getById(apply.getMemberId());
        ApiResponse response = responseEntity.getBody();
        MemberDTO memberDTO = response.toObject(MemberDTO.class);

        Long orderId = apply.getOrderId();
        Long productId = apply.getProductId();
        String productAttr = apply.getProductAttr();
        OmsOrderItemExample orderItemExample = new OmsOrderItemExample();
        ListParam listParam = new ListParam().add("orderId", String.valueOf(orderId)).add("productId", String.valueOf(productId))
                .add("productAttr", productAttr);
        orderItemService.selectCondition(orderItemExample, listParam);
        List<OmsOrderItem> orderItemList = orderItemService.selectByExample(orderItemExample);
        OmsOrderItem orderItem = orderItemList.get(0);

        apply.setMemberUsername(memberDTO.getUsername());
        apply.setProductBrand(orderItem.getProductBrand());
        apply.setProductCount(orderItem.getProductQuantity());
        apply.setProductPrice(orderItem.getProductPrice());
        apply.setProductRealPrice(orderItem.getProductPrice());
        apply.setReturnAmount(orderItem.getProductPrice());
        apply.setDefault();
        returnApplyService.add(apply);
        return ApiResponse.success();
    }
}
