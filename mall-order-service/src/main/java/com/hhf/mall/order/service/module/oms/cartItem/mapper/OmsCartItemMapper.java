package com.hhf.mall.order.service.module.oms.cartItem.mapper;


import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItem;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItemExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OmsCartItemMapper extends BaseMapper<OmsCartItem, Long, OmsCartItemExample> {
    int countByExample(OmsCartItemExample example);

    int deleteByExample(OmsCartItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OmsCartItem record);

    int insertSelective(OmsCartItem record);

    List<OmsCartItem> selectByExample(OmsCartItemExample example);

    OmsCartItem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OmsCartItem record, @Param("example") OmsCartItemExample example);

    int updateByExample(@Param("record") OmsCartItem record, @Param("example") OmsCartItemExample example);

    int updateByPrimaryKeySelective(OmsCartItem record);

    int updateByPrimaryKey(OmsCartItem record);
}