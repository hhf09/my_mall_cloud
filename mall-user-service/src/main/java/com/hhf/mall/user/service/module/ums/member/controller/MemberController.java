package com.hhf.mall.user.service.module.ums.member.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOForChart;
import com.hhf.mall.user.service.module.ums.member.dto.MemberIncreaseChartDTO;
import com.hhf.mall.user.service.module.ums.member.dto.MemberVO;
import com.hhf.mall.user.service.module.ums.member.dto.MemberVOMapper;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import com.hhf.mall.user.service.module.ums.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 消费者
 * @date 2022-01-24 17:35
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/member")
@Validated
public class MemberController {

    @Autowired
    private MemberService memberService;
    @Autowired
    private MemberVOMapper memberVOMapper;

    /**
     *
     * @param keyword
     * @param gender
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_MEMBER)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{ListParam.KEYWORD, "gender"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<UmsMember> list = memberService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<UmsMember> pageInfo = new PageInfo<>(list);
            List<MemberVO> voList = memberVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @ApiLog(note = "更新用户状态")
    @RequirePermissions(PermissionConst.ADMIN_MEMBER)
    @PostMapping("/updateStatus")
    public ResponseEntity<ApiResponse> updateStatus(@RequestParam Long id, @RequestParam Integer status) {
        UmsMember member = memberService.selectByPrimaryKey(id);
        if (member == null) {
            throw new CodeException("用户" + id + "数据不存在");
        }
        member.setStatus(status);
        memberService.updateByPrimaryKeySelective(member);
        return ApiResponse.success();
    }

    @GetMapping("/increaseChart")
    public ResponseEntity<ApiResponse> getIncreaseChart(@RequestParam String start, @RequestParam String end) {
        List<MemberIncreaseChartDTO> list = memberService.getIncreaseChart(start, end);
        MemberDTOForChart dto = memberService.getStatInfo();
        dto.setList(list);
        return ApiResponse.success(dto);
    }

}
