package com.hhf.mall.common.controller;

import lombok.Data;

/**
 * @author hhf
 * @description: 请求参数
 * @date 2023/1/4 10:11
 */
@Data
public class Param {
    /**
     * 参数key
     */
    private String key;
    /**
     * 参数value
     */
    private String value;

    public Param(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public Param() {
    }
}
