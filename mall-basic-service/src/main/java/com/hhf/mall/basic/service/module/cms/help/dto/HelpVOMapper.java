package com.hhf.mall.basic.service.module.cms.help.dto;

import com.hhf.mall.basic.service.module.cms.help.entity.CmsHelp;
import com.hhf.mall.basic.service.module.cms.help.entity.HelpStatus;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategory;
import com.hhf.mall.basic.service.module.cms.helpCategory.service.HelpCateService;
import com.hhf.mall.common.dto.BaseDTOMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 19:29
 */
@Component
public class HelpVOMapper extends BaseDTOMapper<CmsHelp, HelpVO> {
    @Autowired
    private HelpCateService helpCateService;

    @Override
    public HelpVO mapToDTO(CmsHelp entity) {
        HelpVO vo = new HelpVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(HelpStatus.instanceOf(entity.getStatus()).getText());
        CmsHelpCategory helpCategory = helpCateService.selectByPrimaryKey(vo.getCategoryId());
        vo.setCategoryName(Objects.nonNull(helpCategory) ? helpCategory.getName() : null);
        return vo;
    }

    @Override
    public List<HelpVO> mapToDTOList(List<CmsHelp> entityList) {
        List<HelpVO> voList = new ArrayList<>();
        for (CmsHelp entity : entityList) {
            HelpVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
