package com.hhf.mall.order.service.module.oms.orderReturnReason.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReason;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReasonExample;
import com.hhf.mall.order.service.module.oms.orderReturnReason.mapper.OmsOrderReturnReasonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-22 16:02
 */
@Service
public class ReturnReasonService extends BaseService<OmsOrderReturnReason, Long, OmsOrderReturnReasonExample> {
    @Autowired
    private OmsOrderReturnReasonMapper returnReasonMapper;

    @Override
    protected void before(OmsOrderReturnReason entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(OmsOrderReturnReason entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<OmsOrderReturnReason, Long, OmsOrderReturnReasonExample> getMapper() {
        return returnReasonMapper;
    }

    public List<OmsOrderReturnReason> list(ListParam listParam) {
        OmsOrderReturnReasonExample example = new OmsOrderReturnReasonExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return returnReasonMapper.selectByExample(example);
    }
    public void selectOrderRule(OmsOrderReturnReasonExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }


    public void selectCondition(OmsOrderReturnReasonExample example, ListParam listParam) {
        OmsOrderReturnReasonExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "":
                    break;
            }
        }
    }
}
