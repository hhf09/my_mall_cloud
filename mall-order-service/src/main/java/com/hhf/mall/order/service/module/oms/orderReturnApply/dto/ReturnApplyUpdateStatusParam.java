package com.hhf.mall.order.service.module.oms.orderReturnApply.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description 退货处理更新参数
 * @date 2022-01-22 19:20
 */
@Data
public class ReturnApplyUpdateStatusParam {
    private Long id;
    private BigDecimal returnAmount;
    private String handleNote;
    private String handleMan;
    private String receiveNote;
    private String receiveMan;
    private Integer status;
}
