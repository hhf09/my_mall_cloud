package com.hhf.mall.user.service.module.ums.member.service;

import cn.hutool.crypto.digest.BCrypt;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.common.util.StringUtil;
import com.hhf.mall.dto.auth.TokenDTO;
import com.hhf.mall.feign.auth.AuthClient;
import com.hhf.mall.user.service.config.AppConfig;
import com.hhf.mall.user.service.module.ums.member.dto.BindPhoneDTO;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOForChart;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOWithToken;
import com.hhf.mall.user.service.module.ums.member.dto.MemberIncreaseChartDTO;
import com.hhf.mall.user.service.module.ums.member.dto.MemberLoginDTO;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMemberExample;
import com.hhf.mall.user.service.module.ums.member.mapper.UmsMemberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hhf
 * @date 2022-01-24 17:35
 */
@Service
public class MemberService extends BaseService<UmsMember, Long, UmsMemberExample> {

    @Autowired
    private UmsMemberMapper memberMapper;
    @Autowired
    private AuthClient authClient;
    @Autowired
    private AppConfig appConfig;

    @Override
    protected void before(UmsMember entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(UmsMember entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<UmsMember, Long, UmsMemberExample> getMapper() {
        return memberMapper;
    }

    public List<UmsMember> list(ListParam listParam) {
        UmsMemberExample example = new UmsMemberExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return memberMapper.selectByExample(example);
    }

    public String getOpenId(String code) {
        //使用 code 换取 openid、unionid、session_key 等信息
        HashMap<String, Object> params = new HashMap<>();
        AppConfig.Mini miniConfig = appConfig.getMini();
        params.put("appid", miniConfig.getAppId());
        params.put("secret", miniConfig.getAppSecret());
        params.put("js_code", code);
        params.put("grant_type", miniConfig.getGrantType());
        String result;
        try {
            result = HttpUtil.get(miniConfig.getOpenIdUrl(), params);
        } catch (Exception e) {
            throw new CodeException("获取小程序openid失败：" + e.getMessage());
        }
        System.out.println(result);
        JSONObject jsonObject = JSON.parseObject(result);
        String openId = jsonObject.getString("openid");
        System.out.println("openId=" + openId);
        return openId;
    }

    public MemberDTOWithToken login(MemberLoginDTO loginDTO) throws HttpRequestMethodNotSupportedException {
        UmsMemberExample example = new UmsMemberExample();
        ListParam listParam = new ListParam().add("openid", loginDTO.getOpenid());
        selectCondition(example, listParam);
        List<UmsMember> list = memberMapper.selectByExample(example);
        MemberDTOWithToken dto = new MemberDTOWithToken();
        String username;
        String password;
        if (CollUtil.isEmpty(list)) {
            UmsMember entity = new UmsMember();
            entity.setDefault();
            entity.setUsername(StringUtil.createRandomUsername());
            //密码加密
            entity.setPassword(BCrypt.hashpw(entity.getUsername()));
            add(entity);
            dto.setType("add");
            dto.setUserId(entity.getId());
            username = entity.getUsername();
            password = entity.getUsername();
        } else {
            UmsMember member = list.get(0);
            member.setNickname(member.getNickname());
            member.setIcon(member.getIcon());
            updateByPrimaryKeySelective(member);
            dto.setType("update");
            dto.setUserId(member.getId());
            username = member.getUsername();
            password = member.getUsername();
        }
        //从认证服务获取token
        ResponseEntity<ApiResponse> responseEntity = authClient.token(AuthConst.PASSWORD_GRANT_TYPE, AuthConst.MEMBER_CLIENT_ID,
                AuthConst.MEMBER_CLIENT_SECRET, null, username, password);
        ApiResponse response = responseEntity.getBody();
        TokenDTO tokenDTO = response.toObject(TokenDTO.class);
        dto.setToken(tokenDTO);
        return dto;
    }

    public List<MemberIncreaseChartDTO> getIncreaseChart(String start, String end) {
        Map<String, Object> map = new HashMap<>();
        String startDate = start + "-01 00:00:00";

        String[] strings = end.split("-");
        String dateString = end + "-01 00:00:00";
        Date date = DateUtil.parse(dateString);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        if (strings[1].equals("12")) {
            calendar.add(Calendar.YEAR, 1);
            calendar.set(Calendar.MONTH, 1);
        } else {
            calendar.add(Calendar.MONTH, 1);
        }
        Date nextMonth = calendar.getTime();
        String endDate = DateUtil.format(nextMonth, Const.YYYY_MM_DD + " 00:00:00");

        map.put("start", startDate);
        map.put("end", endDate);
        return memberMapper.getMemberIncreaseChart(map);
    }

    public MemberDTOForChart getStatInfo() {
        UmsMemberExample example = new UmsMemberExample();
        int totalCount = memberMapper.countByExample(example);
        example.clear();
        
        Date date = new Date();
        String currentDateString = DateUtil.format(date, Const.YYYY_MM_DD);
        String start = currentDateString + " 00:00:00";
        String end = currentDateString + " 23:59:59";
        ListParam listParam = new ListParam().add("createTimeBt", start + "," + end);
        selectCondition(example, listParam);
        int todayCount = memberMapper.countByExample(example);
        example.clear();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, 0);
        //获取当月的第一天
        Date currentMonth = calendar.getTime();

        calendar.add(Calendar.MONTH, 1);
        //获取下个月的第一天
        Date nextMonth = calendar.getTime();
        String currentMonthString = DateUtil.format(currentMonth, Const.YYYY_MM_DD);
        String nextMonthString = DateUtil.format(nextMonth, Const.YYYY_MM_DD);
        String startMonth = currentMonthString + " 00:00:00";
        String endMonth = nextMonthString + " 00:00:00";
        ListParam listParam2 = new ListParam().add("createTimeBt", startMonth + "," + endMonth);
        selectCondition(example, listParam2);
        int currentMonthCount = memberMapper.countByExample(example);

        MemberDTOForChart dto = new MemberDTOForChart();
        dto.setTotalCount(totalCount);
        dto.setTodayCount(todayCount);
        dto.setCurrentMonthCount(currentMonthCount);
        return dto;
    }

    public void updatePhone(BindPhoneDTO bindPhoneDto) {
        // 模拟短信验证码：123456
        if (!bindPhoneDto.getValidateCode().equals("123456")) {
            throw new CodeException("短信验证码错误");
        }

        UmsMember member = new UmsMember();
        member.setId(bindPhoneDto.getId());
        member.setPhone(bindPhoneDto.getPhone());
        memberMapper.updateByPrimaryKeySelective(member);
    }

    public void selectOrderRule(UmsMemberExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(UmsMemberExample example, ListParam listParam) {
        UmsMemberExample.Criteria criteria = example.createCriteria();
        UmsMemberExample.Criteria criteria2 = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case ListParam.KEYWORD:
                    // username like ? or nickname like ?
                    criteria.andUsernameLike("%" + value + "%");
                    criteria2.andNicknameLike("%" + value + "%");
                    example.or(criteria2);
                    break;
                case "nicknameLike":
                    criteria.andNicknameLike("%" + value + "%");
                    break;
                case "gender":
                    criteria.andGenderEqualTo(Integer.valueOf(value));
                    break;
                case "openid":
                    criteria.andOpenidEqualTo(value);
                    break;
                case "createTimeBt":
                    String[] strings = value.split(",");
                    Date start = DateUtil.parse(strings[0]);
                    Date end = DateUtil.parse(strings[1]);
                    criteria.andCreateTimeBetween(start, end);
                    break;
                case "username":
                    criteria.andUsernameEqualTo(value);
                    break;
            }
        }
    }
}
