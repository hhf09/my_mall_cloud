package com.hhf.mall.product.service.module.pms.productCategory.dto;

import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategory;
import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-12 23:44
 */
@Data
public class PmsProductCateWithChildrenItem extends PmsProductCategory {
    //子分类列表
    private List<PmsProductCategory> children;
}
