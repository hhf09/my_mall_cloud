package com.hhf.mall.user.service.module.ums.role.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 9:58
 */
@Data
public class RoleVO {
    private Long id;
    private String name;
    private String description;
    private Integer adminCount;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private Integer status;
    private String statusText;
}
