package com.hhf.mall.order.service.module.oms.cartItem.service;

import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.dto.product.FeightTemplateDTO;
import com.hhf.mall.dto.user.MemberDTO;
import com.hhf.mall.dto.product.ProductDTO;
import com.hhf.mall.dto.user.ReceiveAddrDTO;
import com.hhf.mall.dto.product.SkuStockDTO;
import com.hhf.mall.feign.product.FeightTemplateClient;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.feign.user.MemberReceiveAddressClient;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.feign.product.SkuStockClient;
import com.hhf.mall.order.service.module.oms.cartItem.dto.CartItemDTOForOrderConfirm;
import com.hhf.mall.order.service.module.oms.cartItem.dto.OrderConfirmParam;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItem;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItemExample;
import com.hhf.mall.order.service.module.oms.cartItem.mapper.OmsCartItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-02-04 0:56
 */
@Service
public class CartItemService extends BaseService<OmsCartItem, Long, OmsCartItemExample> {
    @Autowired
    private OmsCartItemMapper cartItemMapper;
    @Autowired
    private MemberClient memberClient;
    @Autowired
    private MemberReceiveAddressClient receiveAddressClient;
    @Autowired
    private ProductClient productClient;
    @Autowired
    private SkuStockClient skuStockClient;
    @Autowired
    private FeightTemplateClient feightTemplateClient;

    @Override
    protected void before(OmsCartItem entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            ResponseEntity<ApiResponse> responseEntity1 = productClient.getById(entity.getProductId());
            ApiResponse response1 = responseEntity1.getBody();
            ProductDTO productDTO = response1.toObject(ProductDTO.class);
            entity.setProductBrand(productDTO.getBrandName());
            entity.setBrandId(productDTO.getBrandId());
            entity.setProductCategoryId(productDTO.getProductCategoryId());
            entity.setProductName(productDTO.getName());
            entity.setProductSn(productDTO.getProductSn());
            entity.setProductSubTitle(productDTO.getSubTitle());
            entity.setProductPic(productDTO.getPic());

            ResponseEntity<ApiResponse> responseEntity = memberClient.getById(entity.getMemberId());
            ApiResponse response = responseEntity.getBody();
            MemberDTO memberDTO = response.toObject(MemberDTO.class);
            entity.setMemberNickname(memberDTO.getNickname());

            ResponseEntity<ApiResponse> responseEntity2 = skuStockClient.getById(entity.getProductSkuId());
            ApiResponse response2 = responseEntity2.getBody();
            SkuStockDTO skuStockDTO = response2.toObject(SkuStockDTO.class);
            entity.setProductSkuCode(skuStockDTO.getSkuCode());
            entity.setPrice(skuStockDTO.getPrice());
            entity.setProductAttr(skuStockDTO.getSp());
        }
    }

    @Override
    protected void after(OmsCartItem entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<OmsCartItem, Long, OmsCartItemExample> getMapper() {
        return cartItemMapper;
    }

    public List<OmsCartItem> list(ListParam listParam) {
        OmsCartItemExample example = new OmsCartItemExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return cartItemMapper.selectByExample(example);
    }

    public int count(ListParam listParam) {
        OmsCartItemExample example = new OmsCartItemExample();
        selectCondition(example, listParam);
        List<OmsCartItem> cartItemList = cartItemMapper.selectByExample(example);
        int count = 0;
        for (OmsCartItem cartItem : cartItemList) {
            count += cartItem.getQuantity();
        }
        return count;
    }

    public void updateAllCheckStatus(ListParam listParam) {
        OmsCartItemExample example = new OmsCartItemExample();
        selectCondition(example, listParam);

        OmsCartItem cartItem = new OmsCartItem();
        cartItem.setCheckStatus(Integer.valueOf(listParam.getParamValue("checkStatus")));
        cartItemMapper.updateByExampleSelective(cartItem, example);
    }

    public void batchDelete(ListParam listParam) {
        OmsCartItemExample example = new OmsCartItemExample();
        selectCondition(example, listParam);
        cartItemMapper.deleteByExample(example);
    }

    public CartItemDTOForOrderConfirm getOrderConfirm(OrderConfirmParam param) {
        Long memberId = param.getMemberId();
        List<Long> cartItemIdList = param.getCartItemIdList();
        
        //购物车
        OmsCartItemExample example = new OmsCartItemExample();
        ListParam listParam = new ListParam().add("idIn", JSON.toJSONString(cartItemIdList));
        selectCondition(example, listParam);
        List<OmsCartItem> cartItemList = cartItemMapper.selectByExample(example);

        //计算费用
        double totalAmount = 0;
        int feightAmount = 0;
        int totalQuantity = 0;
        List<Long> productIdList = new ArrayList<>();

        for (OmsCartItem cartItem : cartItemList) {
            totalQuantity += cartItem.getQuantity();
            totalAmount += cartItem.getQuantity() * (cartItem.getPrice().doubleValue());
            productIdList.add(cartItem.getProductId());
        }

        int count = 0;
        for (Long productId : productIdList) {
            ResponseEntity<ApiResponse> responseEntity = productClient.getById(productId);
            ApiResponse response = responseEntity.getBody();
            ProductDTO productDTO = response.toObject(ProductDTO.class);
            String serviceIds = productDTO.getServiceIds();
            if (!serviceIds.contains("3")) {
                //不包邮
                ResponseEntity<ApiResponse> responseEntity2 = feightTemplateClient.getById(productDTO.getFeightTemplateId());
                ApiResponse response2 = responseEntity2.getBody();
                FeightTemplateDTO feightTemplateDTO = response2.toObject(FeightTemplateDTO.class);
                feightAmount += feightTemplateDTO.getTotalFeight().intValue();
                count++;
            }
        }

        if (count != 0) {
            feightAmount = feightAmount / count;
        }

        CartItemDTOForOrderConfirm dto = new CartItemDTOForOrderConfirm();
        dto.setList(cartItemList);
        dto.setAddress(getDefaultReceiveAddr(memberId));
        dto.setTotalAmount(totalAmount);
        dto.setFeightAmount(feightAmount);
        dto.setTotalQuantity(totalQuantity);
        return dto;
    }

    /**
     * 查找默认收货地址
     */
    public ReceiveAddrDTO getDefaultReceiveAddr(Long memberId) {
        ResponseEntity<ApiResponse> responseEntity = receiveAddressClient.getByMemberId(memberId);
        ApiResponse response = responseEntity.getBody();
        List<ReceiveAddrDTO> receiveAddrDTOList = response.toList(ReceiveAddrDTO.class);
        if (CollUtil.isNotEmpty(receiveAddrDTOList)) {
            for (ReceiveAddrDTO receiveAddrDTO : receiveAddrDTOList) {
                if (receiveAddrDTO.getDefaultStatus() == 1) {
                    return receiveAddrDTO;
                }
            }
            return null;
        }
        return null;
    }

    public void selectOrderRule(OmsCartItemExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(OmsCartItemExample example, ListParam listParam) {
        OmsCartItemExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "memberId":
                    criteria.andMemberIdEqualTo(Long.valueOf(value));
                    break;
                case "idIn":
                    criteria.andIdIn(JSON.parseArray(value, Long.class));
                    break;
                case "deleteStatus":
                    criteria.andDeleteStatusEqualTo(Integer.valueOf(value));
                    break;
            }
        }
    }
}
