package com.hhf.mall.product.service.module.pms.comment.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.product.service.module.pms.comment.dto.MiniCommentResult;
import com.hhf.mall.product.service.module.pms.comment.dto.MiniCommentStatInfo;
import com.hhf.mall.product.service.module.pms.comment.entity.CommentEvaluationStatus;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsComment;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsCommentExample;
import com.hhf.mall.product.service.module.pms.comment.mapper.PmsCommentMapper;
import com.hhf.mall.product.service.module.pms.commentReply.dto.CommentReplyDTO;
import com.hhf.mall.product.service.module.pms.commentReply.dto.CommentReplyDTOMapper;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReply;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReplyExample;
import com.hhf.mall.product.service.module.pms.commentReply.service.CommentReplyService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-24 22:55
 */
@Service
public class CommentService extends BaseService<PmsComment, Long, PmsCommentExample> {
    @Autowired
    private PmsCommentMapper commentMapper;
    @Autowired
    private CommentReplyService commentReplyService;
    @Autowired
    private MemberClient memberClient;
    @Autowired
    private CommentReplyDTOMapper commentReplyDTOMapper;

    @Override
    protected void before(PmsComment entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(PmsComment entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsComment, Long, PmsCommentExample> getMapper() {
        return commentMapper;
    }

    public List<PmsComment> list(String productName, String memberNickname, Integer star) {
        Map<String, Object> param = new HashMap<>();
        param.put("memberNickname", memberNickname);
        param.put("productName", productName);
        param.put("star", star);
        return commentMapper.listForAdmin(param);
    }

    public MiniCommentStatInfo getStatInfo(Long productId) {
        int countAll = commentMapper.countAll(productId);
        Map<String, Object> param = new HashMap<>();
        param.put("productId", productId);
        param.put("evaluationStatus", CommentEvaluationStatus.GOOD.getKey());
        int countGood = commentMapper.countByEvaluationStatus(param);
        param.put("evaluationStatus", CommentEvaluationStatus.MED.getKey());
        int countMedium = commentMapper.countByEvaluationStatus(param);
        param.put("evaluationStatus", CommentEvaluationStatus.BAD.getKey());
        int countBad = commentMapper.countByEvaluationStatus(param);
        int countPic = commentMapper.countPic(productId);
        double goodRate = 0;
        if (countAll != 0) {
            goodRate = (countGood / (countAll * 1.0)) * 100;
        }

        String s = String.valueOf(goodRate);
        String[] strings = s.split("\\.");

        MiniCommentStatInfo statInfo = new MiniCommentStatInfo();
        statInfo.setAllCount(countAll);
        statInfo.setGoodCount(countGood);
        statInfo.setMediumCount(countMedium);
        statInfo.setBadCount(countBad);
        statInfo.setPicCount(countPic);
        statInfo.setGoodRate(Double.parseDouble(strings[0]));
        return statInfo;
    }

    public List<PmsComment> listByPage(Long productId, Integer evaluate) {
        Map<String, Object> param = new HashMap<>();
        param.put("productId", productId);
        param.put("evaluate", evaluate);
        return commentMapper.listForWeb(param);
    }

    public MiniCommentResult getInfo(Long commentId, Integer orderType) {
        PmsComment comment = commentMapper.selectByPrimaryKey(commentId);
        MiniCommentResult commentResult = new MiniCommentResult();
        BeanUtils.copyProperties(comment, commentResult);

        PmsCommentReplyExample example = new PmsCommentReplyExample();
        ListParam listParam = new ListParam().add(ListParam.ORDERTYPE, String.valueOf(orderType))
                .add("commentId", String.valueOf(comment.getId()));
        commentReplyService.selectCondition(example, listParam);
        commentReplyService.selectOrderRule(example, listParam);
        List<PmsCommentReply> commentReplyList = commentReplyService.selectByExample(example);
        List<CommentReplyDTO> list = commentReplyDTOMapper.mapToDTOList(commentReplyList);
        commentResult.setReplyList(list);
        return commentResult;
    }

    public List<PmsComment> listByMemberId(ListParam listParam) {
        PmsCommentExample example = new PmsCommentExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return commentMapper.selectByExampleWithBLOBs(example);
    }

    public int countByMemberId(Long memberId) {
        PmsCommentExample example = new PmsCommentExample();
        ListParam listParam = new ListParam().add("memberId", String.valueOf(memberId));
        selectCondition(example, listParam);
        return commentMapper.countByExample(example);
    }

    public void selectOrderRule(PmsCommentExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "id desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsCommentExample example, ListParam listParam) {
        PmsCommentExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "memberId":
                    criteria.andMemberIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
