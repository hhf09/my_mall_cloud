package com.hhf.mall.user.service.module.ums.member.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.ums.member.dto.MemberIncreaseChartDTO;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMemberExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UmsMemberMapper extends BaseMapper<UmsMember, Long, UmsMemberExample> {
    int countByExample(UmsMemberExample example);

    int deleteByExample(UmsMemberExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsMember record);

    int insertSelective(UmsMember record);

    List<UmsMember> selectByExample(UmsMemberExample example);

    UmsMember selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsMember record, @Param("example") UmsMemberExample example);

    int updateByExample(@Param("record") UmsMember record, @Param("example") UmsMemberExample example);

    int updateByPrimaryKeySelective(UmsMember record);

    int updateByPrimaryKey(UmsMember record);

    //自定义start

    /**
     * 查询用户月份增长数
     * @param map
     * @return
     */
    List<MemberIncreaseChartDTO> getMemberIncreaseChart(Map<String, Object> map);
}