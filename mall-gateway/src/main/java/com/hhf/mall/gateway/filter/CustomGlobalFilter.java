package com.hhf.mall.gateway.filter;

import com.hhf.mall.common.constant.AuthConst;
import cn.hutool.core.util.StrUtil;
import com.nimbusds.jose.JWSObject;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.text.ParseException;

/**
 * @author hhf
 * @description: 自定义全局过滤器
 * @date 2023/2/8 13:57
 */
@Component
public class CustomGlobalFilter implements GlobalFilter, Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String token = exchange.getRequest().getHeaders().getFirst(AuthConst.JWT_REQUEST_HEADER);
        if (StrUtil.isEmpty(token)) {
            return chain.filter(exchange);
        }
        token = token.replace(AuthConst.TOKEN_HEADER + " ", "");
        try {
            JWSObject jwsObject = JWSObject.parse(token);
            String userString = jwsObject.getPayload().toString();
            System.out.println("userString=" + userString);
            ServerHttpRequest request = exchange.getRequest().mutate().header(AuthConst.USERINFO_REQUEST_HEADER, userString).build();
            exchange = exchange.mutate().request(request).build();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
