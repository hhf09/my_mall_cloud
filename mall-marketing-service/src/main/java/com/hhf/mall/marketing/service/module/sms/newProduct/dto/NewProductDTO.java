package com.hhf.mall.marketing.service.module.sms.newProduct.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2022-04-05 21:08
 */
@Data
public class NewProductDTO {
    private Long id;
    private Long productId;
    private Integer recommendStatus;
    private String recommendStatusText;
    private Integer sort;
}
