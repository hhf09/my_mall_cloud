package com.hhf.mall.common.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 字符串工具类
 *
 * @author hhf
 * @date 2022-01-01
 */
public class StringUtil {

    /**
     * 生成随机用户名
     * @return
     */
    public static String createRandomUsername() {
        String[] strings = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"
                , "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("mall_");
        for (int i = 0; i < 8; i++) {
            int k = (int) (Math.random() * strings.length);
            stringBuffer.append(strings[k]);
        }
        return stringBuffer.toString();
    }

    /**
     * md5加密
     * @param str
     * @return
     */
    public static String md5Encode(String str) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update((str).getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte b[] = md5.digest();

        int i;
        StringBuffer buf = new StringBuffer("");

        for (int offset = 0; offset < b.length; offset++) {
            i = b[offset];
            if (i < 0) {
                i += 256;
            }
            if (i < 16) {
                buf.append("0");
            }
            buf.append(Integer.toHexString(i));
        }
        return buf.toString();
    }
}
