package com.hhf.mall.order.service.module.oms.orderItem.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItem;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItemExample;
import com.hhf.mall.order.service.module.oms.orderItem.mapper.OmsOrderItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/3 14:26
 */
@Service
public class OrderItemService extends BaseService<OmsOrderItem, Long, OmsOrderItemExample> {
    @Autowired
    private OmsOrderItemMapper orderItemMapper;

    @Override
    protected void before(OmsOrderItem entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(OmsOrderItem entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<OmsOrderItem, Long, OmsOrderItemExample> getMapper() {
        return orderItemMapper;
    }

    public void selectOrderRule(OmsOrderItemExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(OmsOrderItemExample example, ListParam listParam) {
        OmsOrderItemExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "orderId":
                    criteria.andOrderIdEqualTo(Long.valueOf(value));
                    break;
                case "productId":
                    criteria.andProductIdEqualTo(Long.valueOf(value));
                    break;
                case "productAttr":
                    criteria.andProductAttrEqualTo(value);
                    break;
            }
        }
    }
}
