package com.hhf.mall.user.service.module.ums.admin.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.ums.admin.entity.AdminStatus;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdmin;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 9:35
 */
@Component
public class AdminVOMapper extends BaseDTOMapper<UmsAdmin, AdminVO> {
    @Override
    public AdminVO mapToDTO(UmsAdmin entity) {
        AdminVO vo = new AdminVO();
        BeanUtils.copyProperties(entity, vo, "password");
        vo.setStatusText(AdminStatus.instanceOf(entity.getStatus()).getText());
        return vo;
    }

    @Override
    public List<AdminVO> mapToDTOList(List<UmsAdmin> entityList) {
        List<AdminVO> voList = new ArrayList<>();
        for (UmsAdmin entity : entityList) {
            AdminVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
