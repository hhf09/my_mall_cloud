package com.hhf.mall.order.service.module.oms.orderReturnApply.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.order.service.module.oms.orderReturnApply.dto.OrderReturnApplyResult;
import com.hhf.mall.order.service.module.oms.orderReturnApply.dto.OrderReturnApplyVO;
import com.hhf.mall.order.service.module.oms.orderReturnApply.dto.OrderReturnApplyVOMapper;
import com.hhf.mall.order.service.module.oms.orderReturnApply.dto.ReturnApplyUpdateStatusParam;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApply;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.ReturnApplyStatus;
import com.hhf.mall.order.service.module.oms.orderReturnApply.service.ReturnApplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description 退货申请
 * @date 2022-01-22 16:39
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/returnApply")
@Validated
public class ReturnApplyController {
    @Autowired
    private ReturnApplyService returnApplyService;
    @Autowired
    private OrderReturnApplyVOMapper returnApplyVOMapper;

    /**
     * 分页查找
     * @param orderSn
     * @param status
     * @param createTime
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"orderSn", "status", "createTime"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        String createTime = listParam.getParamValue("createTime");
        if (StrUtil.isNotEmpty(createTime)) {
            String start = createTime + " 00:00:00";
            String end = createTime + " 23:59:59";
            listParam.add("createTimeBt", start + "," + end);
            listParam.remove("createTime");
        }
        List<OmsOrderReturnApply> list = returnApplyService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<OmsOrderReturnApply> pageInfo = new PageInfo<>(list);
            List<OrderReturnApplyVO> voList = returnApplyVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiLog(note = "删除退货申请")
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        returnApplyService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }

    /**
     * 查看退货详情
     *
     * @param id
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND)
    @GetMapping("/detail")
    public ResponseEntity<ApiResponse> detail(@RequestParam Long id) {
        OrderReturnApplyResult dto = returnApplyService.detail(id);
        return ApiResponse.success(dto);
    }

    /**
     * 修改
     *
     * @param id
     * @param param
     * @return
     */
    @ApiLog(note = "修改退货申请状态")
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND)
    @PostMapping("/updateStatus")
    public ResponseEntity<ApiResponse> updateStatus(@RequestParam Long id, @RequestBody ReturnApplyUpdateStatusParam param) {
        OmsOrderReturnApply returnApply = returnApplyService.selectByPrimaryKey(id);
        if (returnApply == null) {
            throw new CodeException("退货申请" + id + "数据不存在");
        }
        Integer status = param.getStatus();
        returnApply.setStatus(status);
        if (status == ReturnApplyStatus.RETURNING.getKey()) {
            //确认退货
            returnApply.setReturnAmount(param.getReturnAmount());
            returnApply.setHandleTime(new Date());
            returnApply.setHandleMan(param.getHandleMan());
            returnApply.setHandleNote(param.getHandleNote());
        } else if (status == ReturnApplyStatus.FINISH.getKey()) {
            //完成退货
            returnApply.setReceiveTime(new Date());
            returnApply.setReceiveMan(param.getReceiveMan());
            returnApply.setReceiveNote(param.getReceiveNote());
        } else if (status == ReturnApplyStatus.REFUSE.getKey()) {
            //拒绝退货
            returnApply.setHandleTime(new Date());
            returnApply.setHandleMan(param.getHandleMan());
            returnApply.setHandleNote(param.getHandleNote());
        }
        returnApplyService.updateByPrimaryKeySelective(returnApply);
        return ApiResponse.success();
    }
}
