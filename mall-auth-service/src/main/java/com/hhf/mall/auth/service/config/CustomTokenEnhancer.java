package com.hhf.mall.auth.service.config;

import com.hhf.mall.auth.service.dto.CustomUserDetails;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.constant.UserType;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hhf
 * @description: 自定义token内容增强器
 * @date 2023/2/7 16:17
 */
@Component
public class CustomTokenEnhancer implements TokenEnhancer {
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        CustomUserDetails userDetails = (CustomUserDetails) oAuth2Authentication.getPrincipal();
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userDetails.getId());
        map.put("username", userDetails.getUsername());
        map.put("clientId", userDetails.getClientId());
        map.put("userType", AuthConst.ADMIN_CLIENT_ID.equals(userDetails.getClientId()) ? UserType.ADMIN.getKey() : UserType.MEMBER.getKey());
        map.put("roles", userDetails.getRoles());
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(map);
        return oAuth2AccessToken;
    }
}
