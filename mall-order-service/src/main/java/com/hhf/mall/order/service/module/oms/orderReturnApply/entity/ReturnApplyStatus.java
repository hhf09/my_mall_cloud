package com.hhf.mall.order.service.module.oms.orderReturnApply.entity;

/**
 * @author hhf
 * @description: 退货申请状态枚举
 * @date 2022/12/28 10:50
 */
public enum ReturnApplyStatus {
    WAIT_FOR_HANDLE(0, "待处理"),
    RETURNING(1, "退货中"),
    FINISH(2, "已完成"),
    REFUSE(3, "已拒绝");

    private final int key;
    private final String text;

    ReturnApplyStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static ReturnApplyStatus instanceOf(int key) {
        for (ReturnApplyStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
