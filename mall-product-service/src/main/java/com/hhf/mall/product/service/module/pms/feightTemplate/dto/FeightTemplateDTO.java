package com.hhf.mall.product.service.module.pms.feightTemplate.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:45
 */
@Data
public class FeightTemplateDTO {
    private Long id;
    private String packingType;
    private String weight;
    private String isFragile;
    private BigDecimal totalFeight;
}
