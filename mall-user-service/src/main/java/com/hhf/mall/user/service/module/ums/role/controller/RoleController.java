package com.hhf.mall.user.service.module.ums.role.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.user.service.module.ums.permission.dto.PermissionVO;
import com.hhf.mall.user.service.module.ums.permission.dto.PermissionVOMapper;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import com.hhf.mall.user.service.module.ums.role.dto.RoleVO;
import com.hhf.mall.user.service.module.ums.role.dto.RoleVOMapper;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRoleExample;
import com.hhf.mall.user.service.module.ums.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @author hhf
 * @description 角色
 * @date 2022-01-01 23:23
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/role")
@Validated
public class RoleController {
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleVOMapper roleVOMapper;
    @Autowired
    private PermissionVOMapper permissionVOMapper;

    /**
     * 查找全部
     *
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ROLE)
    @GetMapping("/listAll")
    public ResponseEntity<ApiResponse> listAll() {
        List<UmsRole> list = roleService.selectByExample(new UmsRoleExample());
        List<RoleVO> voList = roleVOMapper.mapToDTOList(list);
        return ApiResponse.success(voList);
    }

    /**
     * 分页查找
     *
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ROLE)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{ListParam.KEYWORD};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<UmsRole> list = roleService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<UmsRole> pageInfo = new PageInfo<>(list);
            List<RoleVO> voList = roleVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 查找一个角色分配的菜单列表
     *
     * @param id
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ROLE)
    @GetMapping("/listPermissions")
    public ResponseEntity<ApiResponse> listPermissions(@RequestParam Long id) {
        List<UmsPermission> list = roleService.listMenu(id);
        List<PermissionVO> voList = permissionVOMapper.mapToDTOList(list);
        return ApiResponse.success(voList);
    }

    /**
     * 修改角色状态
     *
     * @param id
     * @param status
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ROLE)
    @PostMapping("/updateStatus")
    public ResponseEntity<ApiResponse> updateStatus(@RequestParam Long id, @RequestParam Integer status) {
        UmsRole role = roleService.selectByPrimaryKey(id);
        if (Objects.isNull(role)) {
            throw new CodeException("角色" + id + "数据不存在");
        }
        role.setStatus(status);
        roleService.updateByPrimaryKeySelective(role);
        return ApiResponse.success();
    }
}
