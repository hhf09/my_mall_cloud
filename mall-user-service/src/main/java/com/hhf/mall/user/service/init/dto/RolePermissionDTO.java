package com.hhf.mall.user.service.init.dto;

import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2023/2/17 18:01
 */
@Data
public class RolePermissionDTO {
    /**
     * 角色名称
     */
    private String name;
    /**
     * 角色备注
     */
    private String description;
    /**
     * 角色标志
     */
    private String value;
    /**
     * 权限列表
     */
    private List<PermissionDTO> permissions;
}
