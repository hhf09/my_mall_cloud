package com.hhf.mall.product.service.module.pms.productAttributeCategory.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.entity.PmsProductAttributeCategory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/12 9:24
 */
@Component
public class ProductAttributeCategoryVOMapper extends BaseDTOMapper<PmsProductAttributeCategory, ProductAttributeCategoryVO> {
    @Override
    public ProductAttributeCategoryVO mapToDTO(PmsProductAttributeCategory entity) {
        ProductAttributeCategoryVO vo = new ProductAttributeCategoryVO();
        BeanUtils.copyProperties(entity, vo);
        return vo;
    }

    @Override
    public List<ProductAttributeCategoryVO> mapToDTOList(List<PmsProductAttributeCategory> entityList) {
        List<ProductAttributeCategoryVO> voList = new ArrayList<>();
        for (PmsProductAttributeCategory entity : entityList) {
            ProductAttributeCategoryVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
