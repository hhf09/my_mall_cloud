package com.hhf.mall.order.service.module.oms.orderReturnReason.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 15:33
 */
@Data
public class OrderReturnReasonDTO {
    private Long id;
    private String name;
    private Integer sort;
    private Integer status;
    private String statusText;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
}
