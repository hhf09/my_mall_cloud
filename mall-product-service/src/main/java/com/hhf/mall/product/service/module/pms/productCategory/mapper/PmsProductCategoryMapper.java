package com.hhf.mall.product.service.module.pms.productCategory.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.product.service.module.pms.productCategory.dto.PmsProductCateWithChildrenItem;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategory;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductCategoryMapper extends BaseMapper<PmsProductCategory, Long, PmsProductCategoryExample> {
    int countByExample(PmsProductCategoryExample example);

    int deleteByExample(PmsProductCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductCategory record);

    int insertSelective(PmsProductCategory record);

    List<PmsProductCategory> selectByExampleWithBLOBs(PmsProductCategoryExample example);

    List<PmsProductCategory> selectByExample(PmsProductCategoryExample example);

    PmsProductCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductCategory record, @Param("example") PmsProductCategoryExample example);

    int updateByExampleWithBLOBs(@Param("record") PmsProductCategory record, @Param("example") PmsProductCategoryExample example);

    int updateByExample(@Param("record") PmsProductCategory record, @Param("example") PmsProductCategoryExample example);

    int updateByPrimaryKeySelective(PmsProductCategory record);

    int updateByPrimaryKeyWithBLOBs(PmsProductCategory record);

    int updateByPrimaryKey(PmsProductCategory record);

    //自定义start

    /**
     *
     * @return
     */
    List<PmsProductCateWithChildrenItem> listWithChildren();
}