package com.hhf.mall.user.service.module.ums.admin.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import com.hhf.mall.dto.auth.TokenDTO;
import com.hhf.mall.feign.auth.AuthClient;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminDTOWithToken;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdmin;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdminExample;
import com.hhf.mall.user.service.module.ums.admin.mapper.UmsAdminMapper;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import com.hhf.mall.user.service.module.ums.role.service.RoleService;
import com.hhf.mall.user.service.module.ums.userRoleRelation.entity.UmsUserRoleRelation;
import com.hhf.mall.user.service.module.ums.userRoleRelation.entity.UmsUserRoleRelationExample;
import com.hhf.mall.user.service.module.ums.userRoleRelation.service.UserRoleRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AdminService extends BaseService<UmsAdmin, Long, UmsAdminExample> {

    @Autowired
    private UmsAdminMapper adminMapper;
    @Autowired
    private RoleService roleService;
    @Autowired
    private AuthClient authClient;
    @Autowired
    private UserRoleRelationService userRoleRelationService;

    @Override
    protected BaseMapper<UmsAdmin, Long, UmsAdminExample> getMapper() {
        return adminMapper;
    }

    @Override
    protected void before(UmsAdmin entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            //查找是否存在相同用户名
            UmsAdminExample example = new UmsAdminExample();
            ListParam listParam = new ListParam().add("username", entity.getUsername());
            selectCondition(example, listParam);
            List<UmsAdmin> adminList = adminMapper.selectByExample(example);
            if (CollUtil.isNotEmpty(adminList)) {
                throw new CodeException("已存在相同用户名的用户");
            }
        }
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(UmsAdmin entity, ActionType actionType) {
        if (actionType == ActionType.DELETE) {
            UmsUserRoleRelationExample example = new UmsUserRoleRelationExample();
            ListParam listParam = new ListParam().add("userId", String.valueOf(entity.getId()));
            userRoleRelationService.selectCondition(example, listParam);
            userRoleRelationService.deleteByExample(example);
        }
    }

    public AdminDTOWithToken login(String username, String password) throws HttpRequestMethodNotSupportedException {
        UmsAdminExample example = new UmsAdminExample();
        ListParam listParam = new ListParam().add("username", username);
        selectCondition(example, listParam);
        List<UmsAdmin> list = adminMapper.selectByExample(example);
        if (CollUtil.isEmpty(list)) {
            throw new CodeException("用户不存在,username=" + username);
        }
        //更新登录时间
        UmsAdmin admin = list.get(0);
        admin.setLoginTime(new Date());
        admin.setUpdateTime(new Date());
        adminMapper.updateByPrimaryKeySelective(admin);

        //从认证服务获取token
        ResponseEntity<ApiResponse> responseEntity = authClient.token(AuthConst.PASSWORD_GRANT_TYPE, AuthConst.ADMIN_CLIENT_ID,
                AuthConst.ADMIN_CLIENT_SECRET, null, username, password);
        ApiResponse response = responseEntity.getBody();
        TokenDTO tokenDTO = response.toObject(TokenDTO.class);
        AdminDTOWithToken dto = new AdminDTOWithToken();
        dto.setUserId(admin.getId());
        dto.setToken(tokenDTO);
        return dto;
    }

    public List<UmsRole> getRoleList(Long adminId) {
        return roleService.getRoleListByUserId(adminId);
    }

    public List<UmsAdmin> list(ListParam listParam) {
        UmsAdminExample example = new UmsAdminExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return adminMapper.selectByExample(example);
    }

    public void updateRole(Long adminId, List<Long> roleIds) {
        //删除旧的管理员-角色关系
        UmsUserRoleRelationExample example = new UmsUserRoleRelationExample();
        ListParam listParam = new ListParam().add("userId", String.valueOf(adminId));
        userRoleRelationService.selectCondition(example, listParam);
        userRoleRelationService.deleteByExample(example);

        //添加新的管理员-角色关系
        List<UmsUserRoleRelation> list = new ArrayList<>();
        for (Long roleId : roleIds) {
            UmsUserRoleRelation userRoleRelation = new UmsUserRoleRelation();
            userRoleRelation.setUserId(adminId);
            userRoleRelation.setRoleId(roleId);
            list.add(userRoleRelation);
        }
        userRoleRelationService.insertAll(list);
    }

    public void selectOrderRule(UmsAdminExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(UmsAdminExample example, ListParam listParam) {
        UmsAdminExample.Criteria criteria = example.createCriteria();
        UmsAdminExample.Criteria criteria2 = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "username":
                    criteria.andUsernameEqualTo(value);
                    break;
                case "usernameLike":
                    criteria.andUsernameLike("%" + value + "%");
                    break;
                case "nicknameLike":
                    criteria.andNicknameLike("%" + value + "%");
                    break;
                case ListParam.KEYWORD:
                    // username like ? or nickname like ?
                    criteria.andUsernameLike("%" + value + "%");
                    criteria2.andNicknameLike("%" + value + "%");
                    example.or(criteria2);
                    break;
            }
        }
    }
}
