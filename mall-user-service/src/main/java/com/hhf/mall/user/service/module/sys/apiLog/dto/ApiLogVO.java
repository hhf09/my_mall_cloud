package com.hhf.mall.user.service.module.sys.apiLog.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2022-07-12 21:38
 */
@Data
public class ApiLogVO {
    private Long id;
    private String reqMethod;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private String ip;
    private String userAgent;
    private String note;
    private String reqParams;
    private String userId;
    private String userName;

}
