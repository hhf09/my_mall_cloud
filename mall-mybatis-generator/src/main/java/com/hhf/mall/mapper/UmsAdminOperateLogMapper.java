package com.hhf.mall.mapper;

import com.hhf.mall.model.UmsAdminOperateLog;
import com.hhf.mall.model.UmsAdminOperateLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsAdminOperateLogMapper {
    int countByExample(UmsAdminOperateLogExample example);

    int deleteByExample(UmsAdminOperateLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsAdminOperateLog record);

    int insertSelective(UmsAdminOperateLog record);

    List<UmsAdminOperateLog> selectByExampleWithBLOBs(UmsAdminOperateLogExample example);

    List<UmsAdminOperateLog> selectByExample(UmsAdminOperateLogExample example);

    UmsAdminOperateLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsAdminOperateLog record, @Param("example") UmsAdminOperateLogExample example);

    int updateByExampleWithBLOBs(@Param("record") UmsAdminOperateLog record, @Param("example") UmsAdminOperateLogExample example);

    int updateByExample(@Param("record") UmsAdminOperateLog record, @Param("example") UmsAdminOperateLogExample example);

    int updateByPrimaryKeySelective(UmsAdminOperateLog record);

    int updateByPrimaryKeyWithBLOBs(UmsAdminOperateLog record);

    int updateByPrimaryKey(UmsAdminOperateLog record);
}