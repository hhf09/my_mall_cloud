package com.hhf.mall.user.service.module.ums.member.controller;

import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.user.service.module.ums.member.dto.BindPhoneDTO;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTO;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOMapper;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOWithToken;
import com.hhf.mall.user.service.module.ums.member.dto.MemberLoginDTO;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import com.hhf.mall.user.service.module.ums.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * @author hhf
 * @description 用户
 * @date 2022-02-01 17:34
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/member")
@Validated
public class MemberWebController {
    @Autowired
    private MemberService memberService;
    @Autowired
    private MemberDTOMapper memberDTOMapper;

    /**
     * 根据code获取openId
     * @param code
     * @return
     */
    @GetMapping("/getOpenId")
    public ResponseEntity<ApiResponse> getOpenId(@RequestParam String code) {
        String openId = memberService.getOpenId(code);
        return ApiResponse.success(openId);
    }

    /**
     * 登录
     * @param loginDTO
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<ApiResponse> login(@Valid @RequestBody MemberLoginDTO loginDTO) throws HttpRequestMethodNotSupportedException {
        MemberDTOWithToken dto = memberService.login(loginDTO);
        return ApiResponse.success(dto);
    }

    /**
     * 查找个人信息
     *
     * @param id
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_MEMBER)
    @GetMapping("/get")
    public ResponseEntity<ApiResponse> get(@RequestParam Long id) {
        UmsMember member = memberService.selectByPrimaryKey(id);
        if (member == null) {
            throw new CodeException("用户" + id + "数据不存在");
        }
        MemberDTO dto = memberDTOMapper.mapToDTO(member);
        return ApiResponse.success(dto);
    }

    /**
     * 修改呢称、性别、手机号
     *
     * @param member
     * @return
     */
    @ApiLog(note = "修改用户信息")
    @RequirePermissions(PermissionConst.WEB_MEMBER)
    @PostMapping("/updateInfo")
    public ResponseEntity<ApiResponse> updateInfo(@RequestBody UmsMember member) {
        memberService.updateByPrimaryKeySelective(member);
        return ApiResponse.success();
    }

    /**
     * 修改手机号
     * @param bindPhoneDto
     * @return
     */
    @ApiLog(note = "修改手机号")
    @RequirePermissions(PermissionConst.WEB_MEMBER)
    @PostMapping("/updatePhone")
    public ResponseEntity<ApiResponse> updatePhone(@RequestBody BindPhoneDTO bindPhoneDto) {
        memberService.updatePhone(bindPhoneDto);
        return ApiResponse.success();
    }
}
