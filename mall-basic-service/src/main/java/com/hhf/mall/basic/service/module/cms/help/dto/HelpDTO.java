package com.hhf.mall.basic.service.module.cms.help.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 11:24
 */
@Data
public class HelpDTO {
    private Long id;
    private Long categoryId;
    private String title;
    private Integer status;
    private String statusText;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private String content;
}
