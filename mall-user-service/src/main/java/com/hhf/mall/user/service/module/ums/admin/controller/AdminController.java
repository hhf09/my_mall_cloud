package com.hhf.mall.user.service.module.ums.admin.controller;

import cn.hutool.crypto.digest.BCrypt;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequireLogin;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import cn.hutool.core.collection.CollUtil;
import com.hhf.mall.user.service.config.AppConfig;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminDTOWithDetail;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminDTOWithToken;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminLoginDTO;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminVO;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminVOMapper;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdmin;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdminExample;
import com.hhf.mall.user.service.module.ums.admin.service.AdminService;
import com.hhf.mall.user.service.module.ums.permission.service.PermissionService;
import com.hhf.mall.user.service.module.ums.role.dto.RoleVO;
import com.hhf.mall.user.service.module.ums.role.dto.RoleVOMapper;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hhf
 * @description 管理员
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/admin")
@Validated
public class AdminController {

    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminVOMapper adminVOMapper;
    @Autowired
    private RoleVOMapper roleVOMapper;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private AppConfig appConfig;

    /**
     * 登录
     *
     * @param adminLoginDto
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<ApiResponse> login(@Valid @RequestBody AdminLoginDTO adminLoginDto) throws HttpRequestMethodNotSupportedException {
        AdminDTOWithToken dto = adminService.login(adminLoginDto.getUsername(), adminLoginDto.getPassword());
        return ApiResponse.success(dto);
    }

    /**
     * 根据用户id获取用户名、头像、角色、权限
     * @param id
     * @return
     */
    @RequireLogin
    @GetMapping("/info")
    public ResponseEntity<ApiResponse> info(@RequestParam Long id) {
        UmsAdmin admin = adminService.selectByPrimaryKey(id);
        if (admin == null) {
            throw new CodeException("用户不存在：id=" + id);
        }
        List<UmsRole> roleList = adminService.getRoleList(id);
        List<String> permissionValueList = permissionService.getPermissionValueListByUserId(id);
        AdminDTOWithDetail dto = new AdminDTOWithDetail();
        dto.setUserId(admin.getId());
        dto.setUserIcon(admin.getIcon());
        dto.setUserName(admin.getUsername());
        dto.setRoles(roleList.stream().map(UmsRole::getValue).collect(Collectors.toList()));
        dto.setPermissions(permissionValueList);
        return ApiResponse.success(dto);
    }

    /**
     * 退出登录
     *
     * @return
     */
    @RequireLogin
    @PostMapping("/logout")
    public ResponseEntity<ApiResponse> logout(@RequestParam Long id) {
        UmsAdmin admin = adminService.selectByPrimaryKey(id);
        if (admin == null) {
            throw new CodeException("用户不存在：id=" + id);
        }
        admin.setLogoutTime(new Date());
        adminService.updateByPrimaryKeySelective(admin);
        return ApiResponse.success();
    }


    /**
     * 查找
     *
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_MANAGER)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{ListParam.KEYWORD};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<UmsAdmin> list = adminService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<UmsAdmin> pageInfo = new PageInfo<>(list);
            List<AdminVO> voList = adminVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 查找一个管理员的全部角色
     *
     * @param id
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_MANAGER)
    @GetMapping("/role")
    public ResponseEntity<ApiResponse> getRoleList(@RequestParam Long id) {
        List<UmsRole> list = adminService.getRoleList(id);
        List<RoleVO> voList = roleVOMapper.mapToDTOList(list);
        return ApiResponse.success(voList);
    }

    /**
     * 修改一个管理员的角色
     *
     * @param id
     * @param roleIds
     * @return
     */
    @ApiLog(note = "修改管理员角色")
    @RequirePermissions(PermissionConst.ADMIN_MANAGER)
    @PostMapping("/updateRole")
    public ResponseEntity<ApiResponse> updateRole(@RequestParam Long id, @NotNull @RequestParam List<Long> roleIds) {
        adminService.updateRole(id, roleIds);
        return ApiResponse.success();
    }

    /**
     * 添加
     *
     * @param admin
     * @return
     */
    @ApiLog(note = "新增管理员")
    @RequirePermissions(PermissionConst.ADMIN_MANAGER)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody UmsAdmin admin) {
        admin.setDefault();
        //密码加密
        admin.setPassword(BCrypt.hashpw(admin.getPassword()));
        adminService.add(admin);
        return ApiResponse.success();
    }

    /**
     * 修改
     *
     * @param admin
     * @return
     */
    @ApiLog(note = "更新管理员")
    @RequirePermissions(PermissionConst.ADMIN_MANAGER)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody UmsAdmin admin) {
        //查找是否存在相同用户名
        UmsAdminExample example = new UmsAdminExample();
        ListParam listParam = new ListParam().add("username", admin.getUsername());
        adminService.selectCondition(example, listParam);
        List<UmsAdmin> adminList = adminService.selectByExample(example);
        if (CollUtil.isNotEmpty(adminList)) {
            throw new CodeException("已存在相同用户名的用户");
        }
        adminService.updateByPrimaryKeySelective(admin);
        return ApiResponse.success();
    }

    /**
     * 重置密码
     *
     * @param id
     * @return
     */
    @ApiLog(note = "重置管理员密码")
    @RequirePermissions(PermissionConst.ADMIN_MANAGER)
    @PostMapping("/updatePwd")
    public ResponseEntity<ApiResponse> updatePwd(@RequestParam Long id) {
        UmsAdmin admin = adminService.selectByPrimaryKey(id);
        if (admin == null) {
            throw new CodeException("用户" + id + "数据不存在");
        }
        admin.setPassword(BCrypt.hashpw(appConfig.getDefaultPassword()));
        adminService.updateByPrimaryKeySelective(admin);
        return ApiResponse.success();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiLog(note = "删除管理员")
    @RequirePermissions(PermissionConst.ADMIN_MANAGER)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        adminService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }

}
