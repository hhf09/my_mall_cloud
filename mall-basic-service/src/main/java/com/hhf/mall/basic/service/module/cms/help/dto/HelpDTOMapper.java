package com.hhf.mall.basic.service.module.cms.help.dto;

import com.hhf.mall.basic.service.module.cms.help.entity.CmsHelp;
import com.hhf.mall.basic.service.module.cms.help.entity.HelpStatus;
import com.hhf.mall.common.dto.BaseDTOMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 19:29
 */
@Component
public class HelpDTOMapper extends BaseDTOMapper<CmsHelp, HelpDTO> {
    
    @Override
    public HelpDTO mapToDTO(CmsHelp entity) {
        HelpDTO dto = new HelpDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setStatusText(HelpStatus.instanceOf(entity.getStatus()).getText());
        return dto;
    }

    @Override
    public List<HelpDTO> mapToDTOList(List<CmsHelp> entityList) {
        List<HelpDTO> dtoList = new ArrayList<>();
        for (CmsHelp entity : entityList) {
            HelpDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
