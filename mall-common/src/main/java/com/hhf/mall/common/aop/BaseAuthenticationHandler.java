package com.hhf.mall.common.aop;

import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.annotation.RequireLogin;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.annotation.RequireRoles;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.common.exception.CodeException;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

/**
 * @author hhf
 * @description: 底层权限校验处理，每个服务需要各自继承该类
 * @date 2023/2/13 16:00
 */
public class BaseAuthenticationHandler {

    public void handle(JoinPoint joinPoint) throws ParseException {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        RequireRoles requireRoles = method.getAnnotation(RequireRoles.class);
        RequirePermissions requirePermissions = method.getAnnotation(RequirePermissions.class);
        RequireLogin requireLogin = method.getAnnotation(RequireLogin.class);
        //判断是否标记权限注解
        if (requireRoles == null && requirePermissions == null && requireLogin == null) {
            System.out.println("no need validate");
        } else {
            String userString = request.getHeader(AuthConst.USERINFO_REQUEST_HEADER);
            if (StrUtil.isEmpty(userString)) {
                throw new CodeException("请求头的用户信息不存在");
            }
            UserDTOForAuth dto = JSON.parseObject(userString, UserDTOForAuth.class);
            if (requireLogin != null) {
                //校验token
                if (StrUtil.isEmpty(request.getHeader(AuthConst.JWT_REQUEST_HEADER))) {
                    throw new CodeException("请求头token不存在");
                }
            }
            if (requireRoles != null) {
                //校验角色
                List<String> roles = Arrays.asList(requireRoles.value());
                List<String> userRoles = dto.getRoles();
                if (CollUtil.isEmpty(userRoles)) {
                    throw new CodeException("用户角色不存在");
                }
                if (!userRoles.containsAll(roles)) {
                    throw new CodeException("权限校验失败");
                }
            }
            if (requirePermissions != null) {
                //校验权限
                List<String> permissions = Arrays.asList(requirePermissions.value());
                List<String> userPermissions = dto.getAuthorities();
                if (CollUtil.isEmpty(userPermissions)) {
                    throw new CodeException("用户权限不存在");
                }
                if (!userPermissions.containsAll(permissions)) {
                    throw new CodeException("权限校验失败");
                }
            }
            System.out.println("validate pass");
        }
    }
}
