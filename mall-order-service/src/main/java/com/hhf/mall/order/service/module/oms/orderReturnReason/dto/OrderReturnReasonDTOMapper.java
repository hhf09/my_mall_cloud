package com.hhf.mall.order.service.module.oms.orderReturnReason.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReason;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.ReturnReasonStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 15:34
 */
@Component
public class OrderReturnReasonDTOMapper extends BaseDTOMapper<OmsOrderReturnReason, OrderReturnReasonDTO> {
    @Override
    public OrderReturnReasonDTO mapToDTO(OmsOrderReturnReason entity) {
        OrderReturnReasonDTO dto = new OrderReturnReasonDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setStatusText(ReturnReasonStatus.instanceOf(entity.getStatus()).getText());
        return dto;
    }

    @Override
    public List<OrderReturnReasonDTO> mapToDTOList(List<OmsOrderReturnReason> entityList) {
        List<OrderReturnReasonDTO> dtoList = new ArrayList<>();
        for (OmsOrderReturnReason entity : entityList) {
            OrderReturnReasonDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
