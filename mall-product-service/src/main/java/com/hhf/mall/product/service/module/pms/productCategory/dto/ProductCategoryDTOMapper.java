package com.hhf.mall.product.service.module.pms.productCategory.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategory;
import com.hhf.mall.product.service.module.pms.productCategory.entity.ProductCategoryLevelType;
import com.hhf.mall.product.service.module.pms.productCategory.entity.ProductCategoryStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 15:49
 */
@Component
public class ProductCategoryDTOMapper extends BaseDTOMapper<PmsProductCategory, ProductCategoryDTO> {
    @Override
    public ProductCategoryDTO mapToDTO(PmsProductCategory entity) {
        ProductCategoryDTO dto = new ProductCategoryDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setShowStatusText(ProductCategoryStatus.instanceOf(entity.getShowStatus()).getText());
        dto.setLevelText(ProductCategoryLevelType.instanceOf(entity.getLevel()).getText());
        return dto;
    }

    @Override
    public List<ProductCategoryDTO> mapToDTOList(List<PmsProductCategory> entityList) {
        List<ProductCategoryDTO> dtoList = new ArrayList<>();
        for (PmsProductCategory entity : entityList) {
            ProductCategoryDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
