package com.hhf.mall.product.service.module.pms.productCategory.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 商品分类销量图表
 * @date 2022-02-15 21:56
 */
@Data
public class ProductCateSaleChartDTO {
    /**
     * 商品分类名称
     */
    private String productCateName;

    /**
     * 总销量
     */
    private int totalSale;
}
