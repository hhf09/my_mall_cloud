package com.hhf.mall.marketing.service.module.sms.recommendProduct.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.SmsHomeRecommendProduct;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.SmsHomeRecommendProductExample;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.mapper.SmsHomeRecommendProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-23 21:55
 */
@Service
public class RecommendProductService extends BaseService<SmsHomeRecommendProduct, Long, SmsHomeRecommendProductExample> {
    @Autowired
    private SmsHomeRecommendProductMapper recommendProductMapper;
    @Autowired
    private ProductClient productClient;

    @Override
    protected void before(SmsHomeRecommendProduct entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(SmsHomeRecommendProduct entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<SmsHomeRecommendProduct, Long, SmsHomeRecommendProductExample> getMapper() {
        return recommendProductMapper;
    }

    public List<SmsHomeRecommendProduct> list(ListParam listParam) {
        SmsHomeRecommendProductExample example = new SmsHomeRecommendProductExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return recommendProductMapper.selectByExample(example);
    }

    public void selectOrderRule(SmsHomeRecommendProductExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "sort desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(SmsHomeRecommendProductExample example, ListParam listParam) {
        SmsHomeRecommendProductExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "recommendStatus":
                    criteria.andRecommendStatusEqualTo(Integer.valueOf(value));
                    break;
            }
        }
    }
}
