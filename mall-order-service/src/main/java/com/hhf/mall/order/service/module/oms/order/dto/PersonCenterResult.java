package com.hhf.mall.order.service.module.oms.order.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 个人中心页面数据
 * @date 2022-02-06 14:36
 */
@Data
public class PersonCenterResult {
    /**
     * 待付款订单统计
     */
    private int toPayOrderCount;

    /**
     * 待发货订单统计
     */
    private int toDeliveryOrderCount;

    /**
     * 待收货订单统计
     */
    private int toReceiveOrderCount;

    /**
     * 已完成订单统计
     */
    private int finishOrderCount;

    /**
     * 已取消订单统计
     */
    private int cancelOrderCount;

    /**
     * 评价统计
     */
    private int commentCount;

    /**
     * 回复统计
     */
    private int replyCount;

    /**
     * 用户呢称
     */
    private String memberNickname;

    /**
     * 用户头像
     */
    private String memberIcon;
}
