package com.hhf.mall.auth.service.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author hhf
 * @description: 白名单路径配置
 * @date 2023/2/8 10:39
 */
@Data
@EqualsAndHashCode
@ConfigurationProperties(prefix = "secure.ignore")
@Component
public class IgnoreUrlsConfig {
    private List<String> urls;
}
