package com.hhf.mall.order.service.module.oms.order.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 13:50
 */
@Data
public class OrderVO {
    private Long id;
    private Long memberId;
    private String orderSn;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private String memberUsername;
    private BigDecimal totalAmount;
    private BigDecimal payAmount;
    private BigDecimal freightAmount;
    private Integer payType;
    private String payTypeText;
    private Integer status;
    private String statusText;
    private String deliveryCompany;
    private String deliverySn;
    private Long receiveAddressId;
    private String note;
    private Integer confirmStatus;
    private String confirmStatusText;
    private Integer deleteStatus;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date paymentTime;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date deliveryTime;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date receiveTime;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date modifyTime;
    private Integer totalQuantity;
}
