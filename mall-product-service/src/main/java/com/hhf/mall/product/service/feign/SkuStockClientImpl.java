package com.hhf.mall.product.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.feign.product.SkuStockClient;
import com.hhf.mall.product.service.module.pms.skuStock.dto.SkuStockDTO;
import com.hhf.mall.product.service.module.pms.skuStock.dto.SkuStockDTOMapper;
import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStock;
import com.hhf.mall.product.service.module.pms.skuStock.service.SkuStockService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 10:30
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/skuStock")
public class SkuStockClientImpl implements SkuStockClient {
    @Autowired
    private SkuStockService skuStockService;
    @Autowired
    private SkuStockDTOMapper skuStockDTOMapper;

    @Override
    @GetMapping("/getById")
    public ResponseEntity<ApiResponse> getById(Long id) {
        PmsSkuStock skuStock = skuStockService.selectByPrimaryKey(id);
        if (skuStock == null) {
            throw new CodeException("sku库存" + id + "数据不存在");
        }
        SkuStockDTO dto = skuStockDTOMapper.mapToDTO(skuStock);
        return ApiResponse.success(dto);
    }

    @Override
    @PostMapping("/updateById")
    public ResponseEntity<ApiResponse> updateById(com.hhf.mall.dto.product.SkuStockDTO dto) {
        PmsSkuStock skuStock = new PmsSkuStock();
        BeanUtils.copyProperties(dto, skuStock);
        skuStockService.updateByPrimaryKeySelective(skuStock);
        return ApiResponse.success();
    }
}
