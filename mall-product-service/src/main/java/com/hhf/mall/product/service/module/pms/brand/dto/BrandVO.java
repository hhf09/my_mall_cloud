package com.hhf.mall.product.service.module.pms.brand.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/12 9:32
 */
@Data
public class BrandVO {
    private Long id;
    private String name;
    private String firstLetter;
    private Integer showStatus;
    private String showStatusText;
    private String logo;
    private String keywords;
}
