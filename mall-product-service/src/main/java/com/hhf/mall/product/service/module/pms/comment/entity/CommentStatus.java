package com.hhf.mall.product.service.module.pms.comment.entity;

/**
 * @author hhf
 * @description: 评论状态枚举
 * @date 2022/12/28 10:50
 */
public enum CommentStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    CommentStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }
}
