package com.hhf.mall.order.service.module.oms.order.dto;

import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/13 10:04
 */
@Data
public class OrderDTOForAdd {
    private List<Long> cartItemIdList;
    private Long orderId;
}
