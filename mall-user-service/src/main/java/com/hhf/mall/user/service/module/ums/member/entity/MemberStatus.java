package com.hhf.mall.user.service.module.ums.member.entity;

/**
 * @author hhf
 * @description: 用户状态枚举
 * @date 2022/12/28 10:50
 */
public enum MemberStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    MemberStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static MemberStatus instanceOf(int key) {
        for (MemberStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
