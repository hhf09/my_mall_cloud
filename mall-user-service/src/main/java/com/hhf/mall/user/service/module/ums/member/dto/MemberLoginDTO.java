package com.hhf.mall.user.service.module.ums.member.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author hhf
 * @description:
 * @date 2023/2/16 16:00
 */
@Data
public class MemberLoginDTO {
    @NotBlank(message = "openid不能为空")
    private String openid;
    @NotBlank(message = "nickname不能为空")
    private String nickname;
    @NotBlank(message = "icon不能为空")
    private String icon;
}
