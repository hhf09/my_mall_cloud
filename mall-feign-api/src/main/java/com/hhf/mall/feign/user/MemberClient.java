package com.hhf.mall.feign.user;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hhf
 * @description 消费者feign接口
 * @date 2022-07-13 21:59
 */
@FeignClient(value = Const.USER_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "MemberClient")
public interface MemberClient {
    String URL_PREFIX = Const.USER_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/member";

    /**
     * 通过id查找
     * @param id
     * @return
     */
    @GetMapping(URL_PREFIX + "/getById")
    ResponseEntity<ApiResponse> getById(@RequestParam Long id);

    /**
     * 通过用户名查找
     * @param username
     * @return
     */
    @GetMapping(URL_PREFIX + "/loadUserByUsername")
    ResponseEntity<ApiResponse> loadUserByUsername(@RequestParam String username);
}
