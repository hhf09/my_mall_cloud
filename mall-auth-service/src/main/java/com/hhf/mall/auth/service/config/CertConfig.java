package com.hhf.mall.auth.service.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author hhf
 * @description: 证书配置
 * @date 2023/2/10 10:12
 */
@Data
@EqualsAndHashCode
@ConfigurationProperties(prefix = "cert")
@Component
public class CertConfig {
    private String fileName;
    private String alias;
    private String password;
}
