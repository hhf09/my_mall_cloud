package com.hhf.mall.product.service.module.pms.brand.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.brand.entity.BrandStatus;
import com.hhf.mall.product.service.module.pms.brand.entity.PmsBrand;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/12 9:33
 */
@Component
public class BrandVOMapper extends BaseDTOMapper<PmsBrand, BrandVO> {
    @Override
    public BrandVO mapToDTO(PmsBrand entity) {
        BrandVO vo = new BrandVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setShowStatusText(BrandStatus.instanceOf(entity.getShowStatus()).getText());
        return vo;
    }

    @Override
    public List<BrandVO> mapToDTOList(List<PmsBrand> entityList) {
        List<BrandVO> voList = new ArrayList<>();
        for (PmsBrand entity : entityList) {
            BrandVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
