package com.hhf.mall.order.service.module.oms.orderOperateHistory.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.entity.OmsOrderOperateHistory;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.entity.OmsOrderOperateHistoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OmsOrderOperateHistoryMapper extends BaseMapper<OmsOrderOperateHistory, Long, OmsOrderOperateHistoryExample> {
    int countByExample(OmsOrderOperateHistoryExample example);

    int deleteByExample(OmsOrderOperateHistoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OmsOrderOperateHistory record);

    int insertSelective(OmsOrderOperateHistory record);

    List<OmsOrderOperateHistory> selectByExample(OmsOrderOperateHistoryExample example);

    OmsOrderOperateHistory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OmsOrderOperateHistory record, @Param("example") OmsOrderOperateHistoryExample example);

    int updateByExample(@Param("record") OmsOrderOperateHistory record, @Param("example") OmsOrderOperateHistoryExample example);

    int updateByPrimaryKeySelective(OmsOrderOperateHistory record);

    int updateByPrimaryKey(OmsOrderOperateHistory record);
}