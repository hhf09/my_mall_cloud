package com.hhf.mall.user.service.module.sys.errorLog.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 异常日志
 */
public class SysErrorLog implements Serializable {
    private Long id;

    /**
     * 用户id
     *
     * @mbggenerated
     */
    private Long userId;

    /**
     * 请求方法
     *
     * @mbggenerated
     */
    private String reqMethod;

    /**
     * 请求参数
     *
     * @mbggenerated
     */
    private String reqParams;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * ip
     *
     * @mbggenerated
     */
    private String ip;

    /**
     * 浏览器
     *
     * @mbggenerated
     */
    private String userAgent;

    /**
     * 备注
     *
     * @mbggenerated
     */
    private String note;

    /**
     * token
     *
     * @mbggenerated
     */
    private String token;

    /**
     * 用户类型（0->前台；1->后台）
     *
     * @mbggenerated
     */
    private Integer userType;

    /**
     * 服务类型标识
     *
     * @mbggenerated
     */
    private String serviceSign;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getReqMethod() {
        return reqMethod;
    }

    public void setReqMethod(String reqMethod) {
        this.reqMethod = reqMethod;
    }

    public String getReqParams() {
        return reqParams;
    }

    public void setReqParams(String reqParams) {
        this.reqParams = reqParams;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getServiceSign() {
        return serviceSign;
    }

    public void setServiceSign(String serviceSign) {
        this.serviceSign = serviceSign;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", reqMethod=").append(reqMethod);
        sb.append(", reqParams=").append(reqParams);
        sb.append(", createTime=").append(createTime);
        sb.append(", ip=").append(ip);
        sb.append(", userAgent=").append(userAgent);
        sb.append(", note=").append(note);
        sb.append(", token=").append(token);
        sb.append(", usertype=").append(userType);
        sb.append(", servicesign=").append(serviceSign);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public void setDefault() {
        this.setCreateTime(new Date());
    }
}