package com.hhf.mall.basic.service.module.cms.helpCategory.entity;

/**
 * @author hhf
 * @description: 帮助分类状态枚举
 * @date 2022/12/28 10:50
 */
public enum HelpCategoryStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    HelpCategoryStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static HelpCategoryStatus instanceOf(int key) {
        for (HelpCategoryStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
