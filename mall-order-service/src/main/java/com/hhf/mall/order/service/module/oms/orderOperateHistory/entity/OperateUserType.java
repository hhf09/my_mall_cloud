package com.hhf.mall.order.service.module.oms.orderOperateHistory.entity;

/**
 * @author hhf
 * @description: 订单操作类型枚举
 * @date 2022/12/28 10:50
 */
public enum OperateUserType {
    MEMBER(0, "用户"),
    SYSTEM(1, "系统"),
    ADMIN(2, "后台管理员");

    private final int key;
    private final String text;

    OperateUserType(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }
}
