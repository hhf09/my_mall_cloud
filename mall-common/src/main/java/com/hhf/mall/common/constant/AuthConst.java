package com.hhf.mall.common.constant;

/**
 * @author hhf
 * @description: 认证授权相关常量
 * @date 2023/2/10 14:29
 */
public interface AuthConst {
    /**
     * jwt请求头
     */
    String JWT_REQUEST_HEADER = "Authorization";
    /**
     * token头部
     */
    String TOKEN_HEADER = "Bearer";
    /**
     * 用户信息请求头
     */
    String USERINFO_REQUEST_HEADER = "userInfo";
    /**
     * 权限前缀
     */
    String AUTHORITY_PREFIX = "AUTH_";
    /**
     * 权限在redis的key
     */
    String AUTHORITY_REDIS_KEY = "auth:resourcePermissionValuesMap";
    /**
     * 后台用户client_id,client_secret
     */
    String ADMIN_CLIENT_ID = "admin-client";
    String ADMIN_CLIENT_SECRET = "123456";
    /**
     * 前台接口访问路径
     */
    String MEMBER_URL_PATTERN = "/mall/cloud/*/mini/**";
    /**
     * 前台用户client_id,client_secret
     */
    String MEMBER_CLIENT_ID = "member-client";
    String MEMBER_CLIENT_SECRET = "123456";
    String AUTHORITIES_CLAIM_NAME = "authorities";
    /**
     * 密码授权模式
     */
    String PASSWORD_GRANT_TYPE = "password";
    /**
     * 刷新令牌授权模式
     */
    String REFRESH_TOKEN_GRANT_TYPE = "refresh_token";
    /**
     * token过期时间，单位秒
     */
    int token_timeout = 60 * 30;
    /**
     * refresh_token过期时间，单位秒
     */
    int refresh_token_timeout = 60 * 60 * 2;
    String SCOPES = "all";
}
