package com.hhf.mall.user.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import cn.hutool.core.collection.CollUtil;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTO;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOMapper;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOMapperForAuth;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMemberExample;
import com.hhf.mall.user.service.module.ums.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-07-13 21:59
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/member")
public class MemberClientImpl implements MemberClient {
    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberDTOMapper memberDTOMapper;
    @Autowired
    private MemberDTOMapperForAuth memberDTOMapperForAuth;

    @Override
    @GetMapping("/getById")
    public ResponseEntity<ApiResponse> getById(Long id) {
        UmsMember member = memberService.selectByPrimaryKey(id);
        if (member == null) {
            throw new CodeException("用户" + id + "数据不存在");
        }
        MemberDTO dto = memberDTOMapper.mapToDTO(member);
        return ApiResponse.success(dto);
    }

    @Override
    @GetMapping("/loadUserByUsername")
    public ResponseEntity<ApiResponse> loadUserByUsername(String username) {
        UmsMemberExample example = new UmsMemberExample();
        ListParam listParam = new ListParam().add("username", username);
        memberService.selectCondition(example, listParam);
        List<UmsMember> list = memberService.selectByExample(example);
        if (CollUtil.isEmpty(list)) {
            throw new CodeException("当前用户不存在：username=" + username);
        }
        UmsMember member = list.get(0);
        UserDTOForAuth dto = memberDTOMapperForAuth.mapToDTO(member);
        return ApiResponse.success(dto);
    }
}
