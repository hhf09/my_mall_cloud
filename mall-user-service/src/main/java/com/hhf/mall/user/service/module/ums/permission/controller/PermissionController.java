package com.hhf.mall.user.service.module.ums.permission.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.user.service.module.ums.permission.dto.PermissionVO;
import com.hhf.mall.user.service.module.ums.permission.dto.PermissionVOMapper;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import com.hhf.mall.user.service.module.ums.permission.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @author hhf
 * @description 权限
 * @date 2022-01-02 23:39
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/permission")
@Validated
public class PermissionController {
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private PermissionVOMapper permissionVOMapper;

    /**
     * 分级别查找菜单
     *
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PERMISSION)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<UmsPermission> list = permissionService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<UmsPermission> pageInfo = new PageInfo<>(list);
            List<PermissionVO> voList = permissionVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 修改权限状态
     *
     * @param id
     * @param status
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PERMISSION)
    @PostMapping("/updateStatus")
    public ResponseEntity<ApiResponse> updateStatus(@RequestParam Long id, @RequestParam Integer status) {
        UmsPermission permission = permissionService.selectByPrimaryKey(id);
        if (Objects.isNull(permission)) {
            throw new CodeException("权限" + id + "数据不存在");
        }
        permission.setStatus(status);
        permissionService.updateByPrimaryKeySelective(permission);
        return ApiResponse.success();
    }
}
