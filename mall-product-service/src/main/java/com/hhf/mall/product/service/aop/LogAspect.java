package com.hhf.mall.product.service.aop;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.aop.BaseLogAspect;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.dto.user.ApiLogDTO;
import com.hhf.mall.dto.user.ErrorLogDTO;
import com.hhf.mall.feign.user.ApiLogClient;
import com.hhf.mall.feign.user.ErrorLogClient;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

/**
 * @author hhf
 * @description 日志处理切面
 * @date 2022-01-29 15:32
 */
@Aspect
@Component
@Order(1)
public class LogAspect extends BaseLogAspect {

    @Autowired
    private ApiLogClient apiLogClient;
    @Autowired
    private ErrorLogClient errorLogClient;

    /**
     * 新增切点表达式
     */
    @Pointcut("execution(public * " + Const.PRODUCT_SERVICE_MODULE_PACKAGE + "*.*.controller.*.add*(..))")
    public void addPointcut() {
    }

    /**
     * 更新切点表达式
     */
    @Pointcut("execution(public * " + Const.PRODUCT_SERVICE_MODULE_PACKAGE + "*.*.controller.*.update*(..))")
    public void updatePointcut() {
    }

    /**
     * 删除切点表达式
     */
    @Pointcut("execution(public * " + Const.PRODUCT_SERVICE_MODULE_PACKAGE + "*.*.controller.*.delete*(..))")
    public void deletePointcut() {
    }

    /**
     * 异常切点表达式
     */
    @Pointcut("execution(public * " + Const.PRODUCT_SERVICE_MODULE_PACKAGE + "*.*.*.*.*(..))")
    public void exceptionPointcut() {
    }

    /**
     * 异常后置通知
     *
     * @param joinPoint
     * @param throwing
     */
    @AfterThrowing(value = "exceptionPointcut()", throwing = "throwing")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable throwing) {
        Map<String, String> map = super.handleAfterThrowing(joinPoint, throwing);
        ErrorLogDTO dto = new ErrorLogDTO();
        dto.setIp(map.get("ip"));
        dto.setReqMethod(map.get("uri"));
        dto.setReqParams(map.get("params"));
        dto.setToken(map.get("token"));
        dto.setUserId(Long.valueOf(map.get("userId")));
        dto.setNote(map.get("note"));
        dto.setUserAgent(map.get("userAgent"));
        dto.setUserType(Integer.valueOf(map.get("userType")));
        dto.setServiceSign(Const.PRODUCT_SERVICE_NAME);
        ResponseEntity<ApiResponse> responseEntity = errorLogClient.add(dto);
        ApiResponse response = responseEntity.getBody();
        if (Objects.nonNull(response) && !response.isSuccess()) {
            throw new CodeException("异常日志记录失败");
        }
    }

    /**
     * 新增后置通知
     */
    @AfterReturning("addPointcut()")
    public void doAfterAdd(JoinPoint joinPoint) throws Throwable {
        doAfterReturning(joinPoint);
    }

    /**
     * 更新后置通知
     */
    @AfterReturning("updatePointcut()")
    public void doAfterUpdate(JoinPoint joinPoint) throws Throwable {
        doAfterReturning(joinPoint);
    }

    /**
     * 删除后置通知
     */
    @AfterReturning("deletePointcut()")
    public void doAfterDelete(JoinPoint joinPoint) throws Throwable {
        doAfterReturning(joinPoint);
    }

    public void doAfterReturning(JoinPoint joinPoint) throws Throwable {
        Map<String, String> map = super.handleAfterReturning(joinPoint);
        ApiLogDTO dto = new ApiLogDTO();
        dto.setIp(map.get("ip"));
        dto.setReqMethod(map.get("uri"));
        dto.setReqParams(map.get("params"));
        dto.setToken(map.get("token"));
        dto.setUserId(Long.valueOf(map.get("userId")));
        dto.setNote(map.get("note"));
        dto.setUserAgent(map.get("userAgent"));
        dto.setUserType(Integer.valueOf(map.get("userType")));
        dto.setServiceSign(Const.PRODUCT_SERVICE_NAME);
        ResponseEntity<ApiResponse> responseEntity = apiLogClient.add(dto);
        ApiResponse response = responseEntity.getBody();
        if (Objects.nonNull(response) && !response.isSuccess()) {
            throw new CodeException("异常日志记录失败");
        }
    }
}

