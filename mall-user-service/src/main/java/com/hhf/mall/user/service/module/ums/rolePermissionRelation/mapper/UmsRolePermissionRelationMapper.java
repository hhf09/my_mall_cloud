package com.hhf.mall.user.service.module.ums.rolePermissionRelation.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelation;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsRolePermissionRelationMapper extends BaseMapper<UmsRolePermissionRelation, Long, UmsRolePermissionRelationExample> {
    int countByExample(UmsRolePermissionRelationExample example);

    int deleteByExample(UmsRolePermissionRelationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsRolePermissionRelation record);

    int insertSelective(UmsRolePermissionRelation record);

    List<UmsRolePermissionRelation> selectByExample(UmsRolePermissionRelationExample example);

    UmsRolePermissionRelation selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsRolePermissionRelation record, @Param("example") UmsRolePermissionRelationExample example);

    int updateByExample(@Param("record") UmsRolePermissionRelation record, @Param("example") UmsRolePermissionRelationExample example);

    int updateByPrimaryKeySelective(UmsRolePermissionRelation record);

    int updateByPrimaryKey(UmsRolePermissionRelation record);

    //自定义start

    /**
     * 批量添加角色-权限关联
     * @param list
     */
    void insertAll(List<UmsRolePermissionRelation> list);
}