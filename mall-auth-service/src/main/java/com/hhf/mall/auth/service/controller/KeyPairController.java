package com.hhf.mall.auth.service.controller;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.Map;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/7 16:42
 */
@RestController
@RequestMapping("/rsa")
public class KeyPairController {
    @Autowired
    private KeyPair keyPair;

    /**
     * 获取rsa公钥
     * @return
     */
    @GetMapping("/publicKey")
    public Map<String, Object> publicKey() {
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAKey key = new RSAKey.Builder(publicKey).build();
        return new JWKSet(key).toJSONObject();
    }
}
