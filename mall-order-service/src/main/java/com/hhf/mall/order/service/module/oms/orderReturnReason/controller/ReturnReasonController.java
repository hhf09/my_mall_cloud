package com.hhf.mall.order.service.module.oms.orderReturnReason.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.order.service.module.oms.orderReturnReason.dto.OrderReturnReasonVO;
import com.hhf.mall.order.service.module.oms.orderReturnReason.dto.OrderReturnReasonVOMapper;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReason;
import com.hhf.mall.order.service.module.oms.orderReturnReason.service.ReturnReasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description 退货原因
 * @date 2022-01-22 16:02
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/returnReason")
@Validated
public class ReturnReasonController {
    @Autowired
    private ReturnReasonService returnReasonService;
    @Autowired
    private OrderReturnReasonVOMapper orderReturnReasonVOMapper;

    /**
     * 分页查找
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND_REASON)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<OmsOrderReturnReason> list = returnReasonService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<OmsOrderReturnReason> pageInfo = new PageInfo<>(list);
            List<OrderReturnReasonVO> voList = orderReturnReasonVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 添加
     *
     * @param returnReason
     * @return
     */
    @ApiLog(note = "添加退货原因")
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND_REASON)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody OmsOrderReturnReason returnReason) {
        returnReason.setDefault();
        returnReasonService.add(returnReason);
        return ApiResponse.success();
    }

    /**
     * 修改
     *
     * @param returnReason
     * @return
     */
    @ApiLog(note = "更新退货原因")
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND_REASON)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody OmsOrderReturnReason returnReason) {
        returnReason.setCreateTime(new Date());
        returnReasonService.updateByPrimaryKeySelective(returnReason);
        return ApiResponse.success();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiLog(note = "删除退货原因")
    @RequirePermissions(PermissionConst.ADMIN_ORDER_REFUND_REASON)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        returnReasonService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }
}
