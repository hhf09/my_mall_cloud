package com.hhf.mall.feign.product;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import com.hhf.mall.dto.product.SkuStockDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hhf
 * @description: sku库存feign接口
 * @date 2022/7/14 10:26
 */
@FeignClient(value = Const.PRODUCT_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "SkuStockClient")
public interface SkuStockClient {
    String URL_PREFIX = Const.PRODUCT_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/skuStock";

    /**
     * 通过id查找
     *
     * @param id
     * @return
     */
    @GetMapping(URL_PREFIX + "/getById")
    ResponseEntity<ApiResponse> getById(@RequestParam Long id);

    /**
     * 通过id修改
     *
     * @param skuStockDTO
     * @return
     */
    @PostMapping(URL_PREFIX + "/updateById")
    ResponseEntity<ApiResponse> updateById(@RequestBody SkuStockDTO skuStockDTO);
}
