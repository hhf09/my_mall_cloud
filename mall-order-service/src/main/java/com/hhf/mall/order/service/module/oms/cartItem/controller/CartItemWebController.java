package com.hhf.mall.order.service.module.oms.cartItem.controller;

import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.order.service.module.oms.cartItem.dto.CartItemDTO;
import com.hhf.mall.order.service.module.oms.cartItem.dto.CartItemDTOForOrderConfirm;
import com.hhf.mall.order.service.module.oms.cartItem.dto.CartItemDTOMapper;
import com.hhf.mall.order.service.module.oms.cartItem.dto.CartItemParam;
import com.hhf.mall.order.service.module.oms.cartItem.dto.OrderConfirmParam;
import com.hhf.mall.order.service.module.oms.cartItem.entity.CartItemCheckStatus;
import com.hhf.mall.order.service.module.oms.cartItem.entity.CartItemDeleteStatus;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItem;
import com.hhf.mall.order.service.module.oms.cartItem.service.CartItemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 购物车
 * @date 2022-02-04 0:55
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/cartItem")
@Validated
public class CartItemWebController {
    @Autowired
    private CartItemService cartItemService;
    @Autowired
    private CartItemDTOMapper cartItemDTOMapper;

    /**
     * 添加商品进入购物车
     *
     * @param param
     * @return
     */
    @ApiLog(note = "添加商品进入购物车")
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody CartItemParam param) {
        OmsCartItem cartItem = new OmsCartItem();
        BeanUtils.copyProperties(param, cartItem);
        cartItem.setDefault();
        cartItem.setCheckStatus(CartItemCheckStatus.instanceOf(param.getType()).getKey());
        cartItemService.add(cartItem);
        Long cartItemId = cartItem.getId();
        if (cartItemId == null) {
            throw new CodeException("添加购物车失败");
        }
        return ApiResponse.success(cartItemId);
    }

    /**
     * 查找用户的购物车商品
     *
     * @param memberId
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("deleteStatus", String.valueOf(CartItemDeleteStatus.NO.getKey()));
        List<OmsCartItem> list = cartItemService.list(listParam);
        List<CartItemDTO> dtoList = cartItemDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }

    /**
     * 修改商品数量
     *
     * @param id
     * @param quantity
     * @return
     */
    @ApiLog(note = "修改商品数量")
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @PostMapping("/updateQuantity")
    public ResponseEntity<ApiResponse> updateQuantity(@RequestParam Long id, @RequestParam Integer quantity) {
        OmsCartItem cartItem = cartItemService.selectByPrimaryKey(id);
        if (cartItem == null) {
            throw new CodeException("购物车" + id + "数据不存在");
        }
        cartItem.setQuantity(cartItem.getQuantity() + quantity);
        cartItemService.updateByPrimaryKeySelective(cartItem);
        return ApiResponse.success();
    }

    /**
     * 计算购物车商品总数
     *
     * @param memberId
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @GetMapping("/count")
    public ResponseEntity<ApiResponse> count(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        int count = cartItemService.count(listParam);
        return ApiResponse.success(count);
    }

    /**
     * 修改选中状态
     *
     * @param id
     * @param checkStatus
     * @return
     */
    @ApiLog(note = "修改选中状态")
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @PostMapping("/updateCheckStatus")
    public ResponseEntity<ApiResponse> updateCheckStatus(@RequestParam Long id, @RequestParam Integer checkStatus) {
        OmsCartItem cartItem = cartItemService.selectByPrimaryKey(id);
        if (cartItem == null) {
            throw new CodeException("购物车" + id + "数据不存在");
        }
        cartItem.setCheckStatus(checkStatus);
        cartItemService.updateByPrimaryKeySelective(cartItem);
        return ApiResponse.success();
    }

    /**
     * 修改全选状态
     *
     * @param memberId
     * @param checkStatus
     * @return
     */
    @ApiLog(note = "修改全选状态")
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @PostMapping("/updateAllCheckStatus")
    public ResponseEntity<ApiResponse> updateAllCheckStatus(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId", "checkStatus"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        cartItemService.updateAllCheckStatus(listParam);
        return ApiResponse.success();
    }

    /**
     * 批量删除
     *
     * @param cartItemIdList
     * @return
     */
    @ApiLog(note = "批量删除")
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @PostMapping("/batchDelete")
    public ResponseEntity<ApiResponse> batchDelete(@RequestParam(value = "ids") List<Long> cartItemIdList) {
        ListParam listParam = new ListParam().add("idIn", JSON.toJSONString(cartItemIdList));
        cartItemService.batchDelete(listParam);
        return ApiResponse.success();
    }

    /**
     * 查找默认收货地址、购买商品列表、总金额、运费、商品总数
     *
     * @param param
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_CART_ITEM)
    @PostMapping("/getOrderConfirm")
    public ResponseEntity<ApiResponse> getOrderConfirm(@RequestBody OrderConfirmParam param) {
        CartItemDTOForOrderConfirm dto = cartItemService.getOrderConfirm(param);
        return ApiResponse.success(dto);
    }
}
