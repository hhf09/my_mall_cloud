/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : my_mall

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2023-02-25 21:39:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cms_help
-- ----------------------------
DROP TABLE IF EXISTS `cms_help`;
CREATE TABLE `cms_help` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL COMMENT '标题',
  `status` int(1) DEFAULT '1' COMMENT '启用状态：0->禁用；1->启用',
  `create_time` datetime DEFAULT NULL COMMENT '修改时间',
  `content` text COMMENT '内容',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='帮助表';

-- ----------------------------
-- Records of cms_help
-- ----------------------------
INSERT INTO `cms_help` VALUES ('3', '4', '取消订单失败怎么办', '1', '2022-01-29 16:53:09', '订单取消失败，可能是该订单不支持取消或订单的商品已发货，如果订单已发货我们会尽量拦截该订单为您申请退款，若未能拦截成功，您可根据需求选择签收或拒收商品。', null);
INSERT INTO `cms_help` VALUES ('4', '4', '订单如何取消', '1', '2022-01-29 20:35:37', '登录微信客户端，点击【我的】-【全部订单】，查找需要取消的订单，点击【取消订单】按钮即可。', null);
INSERT INTO `cms_help` VALUES ('5', '2', '申请退货后，能不能换成换货', '1', '2022-02-19 21:40:46', '若退货服务单未审核，您可以通过前台自行取消服务单；若服务单已审核，建议您联系在线客服为您处理。', null);
INSERT INTO `cms_help` VALUES ('6', '2', '商品购买后可以申请无理由退货吗', '1', '2022-02-19 21:43:18', '如果商品详情页面有说明支持7天无理由退货，在该周期内支持无理由退货；如果有明确标注不支持无理由退货，则不可申请无理由退货。', null);
INSERT INTO `cms_help` VALUES ('7', '2', '7天无理由退货标准', '1', '2022-02-19 21:45:35', '签收商品之日起7天内，商品保持原有品质、功能，商品本身、配件、商标标识齐全，即可申请7天无理由退货。', null);
INSERT INTO `cms_help` VALUES ('8', '4', '下单之后如何修改商品的数量和颜色', '1', '2022-02-19 21:38:08', '订单提交成功后将无法修改商品的数量和颜色。如需修改，请重新下单', null);
INSERT INTO `cms_help` VALUES ('9', '5', '包裹现在到哪里了', '1', '2022-02-19 23:55:57', '点击【我的】-【订单列表】-【待收货】，点击对应订单即可查看物流信息。', null);
INSERT INTO `cms_help` VALUES ('10', '5', '如何自提', '1', '2022-02-19 23:58:29', '下单时，配送方式选择“上门自提”，自提订单提交成功后，订单到达指定自提点或自提柜，系统会向收货人手机发送自提码，到自提点或自提柜通过自提码提货即可。', null);
INSERT INTO `cms_help` VALUES ('11', '6', '怎么开发票', '1', '2022-02-20 00:01:42', '下单时根据需求在提交订单页面选择发票类型并填写对应的发票信息，我们将按照您填写的发票信息为您开具。', null);
INSERT INTO `cms_help` VALUES ('12', '6', '可以开具纸质发票吗', '1', '2022-02-20 00:03:46', '为了响应国家无纸化号召，不再为顾客开具普通纸质发票，全面启用电子普通发票，电子发票与普通发票具备同等法律效力。', null);
INSERT INTO `cms_help` VALUES ('13', '6', '如何查看发票信息', '1', '2022-02-20 00:05:47', '点击【我的】-【订单列表】-【查看发票】-【发票详情】即可查看或下载电子普通发票。', null);

-- ----------------------------
-- Table structure for cms_help_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_help_category`;
CREATE TABLE `cms_help_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `help_count` int(11) DEFAULT NULL COMMENT '帮助数量',
  `status` int(1) DEFAULT '1' COMMENT '启用状态：0->禁用；1->启用',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='帮助分类表';

-- ----------------------------
-- Records of cms_help_category
-- ----------------------------
INSERT INTO `cms_help_category` VALUES ('2', '售后', '3', '1', '8', null);
INSERT INTO `cms_help_category` VALUES ('4', '订单', '3', '1', '12', null);
INSERT INTO `cms_help_category` VALUES ('5', '配送', '2', '1', '10', null);
INSERT INTO `cms_help_category` VALUES ('6', '财务', '3', '1', '5', null);

-- ----------------------------
-- Table structure for oms_cart_item
-- ----------------------------
DROP TABLE IF EXISTS `oms_cart_item`;
CREATE TABLE `oms_cart_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `product_sku_id` bigint(20) DEFAULT NULL,
  `member_id` bigint(20) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL COMMENT '购买数量',
  `price` decimal(10,2) DEFAULT NULL COMMENT '添加到购物车的价格',
  `product_pic` varchar(1000) DEFAULT NULL COMMENT '商品主图',
  `product_name` varchar(500) DEFAULT NULL COMMENT '商品名称',
  `product_sub_title` varchar(500) DEFAULT NULL COMMENT '商品副标题（卖点）',
  `product_sku_code` varchar(200) DEFAULT NULL COMMENT '商品sku条码',
  `member_nickname` varchar(500) DEFAULT NULL COMMENT '会员昵称',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_date` datetime DEFAULT NULL COMMENT '修改时间',
  `delete_status` int(1) DEFAULT '0' COMMENT '是否删除(1->否)',
  `product_category_id` bigint(20) DEFAULT NULL COMMENT '商品分类',
  `product_brand` varchar(200) DEFAULT NULL,
  `product_sn` varchar(200) DEFAULT NULL,
  `product_attr` varchar(500) DEFAULT NULL COMMENT '商品销售属性:[{"key":"颜色","value":"颜色"},{"key":"容量","value":"4G"}]',
  `check_status` int(1) DEFAULT '0' COMMENT '是否选中(1->是)',
  `brand_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='购物车表';

-- ----------------------------
-- Records of oms_cart_item
-- ----------------------------
INSERT INTO `oms_cart_item` VALUES ('17', '23', '78', '1', '1', '99.00', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '男士T恤', '男士T恤副标题', '201806070023001', 'windir', '2022-02-04 01:20:36', null, '1', '11', '李宁', 'NO.1098', '颜色:米白色, 尺寸:M, null', '1', '59');
INSERT INTO `oms_cart_item` VALUES ('18', '23', '78', '1', '1', '99.00', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '男士T恤', '男士T恤副标题', '201806070023001', 'windir', '2022-02-05 22:25:42', null, '1', '11', '李宁', 'NO.1098', '颜色:米白色, 尺寸:M, null', '1', '59');
INSERT INTO `oms_cart_item` VALUES ('19', '23', '78', '1', '1', '99.00', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '男士T恤', '男士T恤副标题', '201806070023001', 'test', '2022-02-09 00:02:01', null, '1', '11', '李宁', 'NO.1098', '颜色:米白色, 尺寸:M, null', '1', '59');
INSERT INTO `oms_cart_item` VALUES ('20', '23', '80', '1', '1', '98.00', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '男士T恤', '男士T恤副标题', '201806070023003', 'test', '2022-02-09 16:47:16', null, '1', '11', '李宁', 'NO.1098', '颜色:浅黄色, 尺寸:M, null', '0', '59');

-- ----------------------------
-- Table structure for oms_order
-- ----------------------------
DROP TABLE IF EXISTS `oms_order`;
CREATE TABLE `oms_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `member_id` bigint(20) DEFAULT NULL,
  `order_sn` varchar(64) DEFAULT NULL COMMENT '订单编号',
  `create_time` datetime DEFAULT NULL COMMENT '提交时间',
  `member_username` varchar(64) DEFAULT NULL COMMENT '用户帐号',
  `total_amount` decimal(10,2) DEFAULT NULL COMMENT '订单总金额（商品合计）',
  `pay_amount` decimal(10,2) DEFAULT NULL COMMENT '应付金额（实际支付金额）',
  `freight_amount` decimal(10,2) DEFAULT NULL COMMENT '运费金额',
  `pay_type` int(1) DEFAULT NULL COMMENT '支付方式：0->未支付；1->支付宝；2->微信',
  `status` int(1) DEFAULT NULL COMMENT '订单状态：0->待付款；1->待发货；2->待收货；3->已完成；4->已关闭；5->无效订单',
  `delivery_company` varchar(64) DEFAULT NULL COMMENT '物流公司(配送方式)',
  `delivery_sn` varchar(64) DEFAULT NULL COMMENT '物流单号',
  `receive_address_id` bigint(20) DEFAULT NULL COMMENT '收货信息id',
  `note` varchar(500) DEFAULT NULL COMMENT '订单备注',
  `confirm_status` int(1) DEFAULT NULL COMMENT '确认收货状态：0->未确认；1->已确认',
  `delete_status` int(1) DEFAULT '0' COMMENT '删除状态：0->未删除；1->已删除',
  `payment_time` datetime DEFAULT NULL COMMENT '支付时间',
  `delivery_time` datetime DEFAULT NULL COMMENT '发货时间',
  `receive_time` datetime DEFAULT NULL COMMENT '确认收货时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `total_quantity` int(3) DEFAULT NULL COMMENT '商品总数',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='订单表';

-- ----------------------------
-- Records of oms_order
-- ----------------------------
INSERT INTO `oms_order` VALUES ('12', '1', '201809150101000001', '2018-09-15 12:24:27', 'test', '18732.00', '18752.00', '20.00', '0', '4', '', '', '1', 'xxx', '0', '0', null, null, null, '2018-10-30 14:43:49', '0', null);
INSERT INTO `oms_order` VALUES ('13', '1', '201809150102000002', '2018-09-15 14:24:29', 'test', '18732.00', '16377.75', '0.00', '1', '2', '顺丰快递', 'deli_20220118235917', '1', null, '0', '0', '2018-10-11 14:04:19', '2022-01-18 23:59:25', null, null, '0', null);
INSERT INTO `oms_order` VALUES ('14', '1', '201809130101000001', '2018-09-13 16:57:40', 'test', '18732.00', '16377.75', '0.00', '2', '2', '顺丰快递', '201707196398345', '1', null, '0', '0', '2018-10-13 13:44:04', '2018-10-16 13:43:41', null, null, '0', null);
INSERT INTO `oms_order` VALUES ('15', '1', '201809130102000002', '2018-09-13 17:03:00', 'test', '18732.00', '16377.75', '0.00', '1', '3', '顺丰快递', '201707196398346', '1', null, '1', '0', '2018-10-13 13:44:54', '2018-10-16 13:45:01', '2018-10-18 14:05:31', null, '0', null);
INSERT INTO `oms_order` VALUES ('16', '1', '201809140101000001', '2018-09-14 16:16:16', 'test', '18732.00', '16377.75', '0.00', '2', '4', null, null, '1', null, '0', '1', null, null, null, null, '0', null);
INSERT INTO `oms_order` VALUES ('17', '1', '201809150101000003', '2018-09-15 12:24:27', 'test', '18732.00', '16377.75', '0.00', '0', '4', '顺丰快递', '201707196398345', '1', null, '0', '0', null, '2018-10-12 14:01:28', null, null, '0', null);
INSERT INTO `oms_order` VALUES ('18', '1', '201809150102000004', '2018-09-15 14:24:29', 'test', '18732.00', '16377.75', '0.00', '1', '1', '圆通快递', 'xx', '1', null, '0', '0', null, '2018-10-16 14:42:17', null, null, '0', null);
INSERT INTO `oms_order` VALUES ('19', '1', '201809130101000003', '2018-09-13 16:57:40', 'test', '18732.00', '16377.75', '0.00', '2', '2', null, null, '1', null, '0', '0', null, null, null, null, '0', null);
INSERT INTO `oms_order` VALUES ('20', '1', '201809130102000004', '2018-09-13 17:03:00', 'test', '18732.00', '16377.75', '0.00', '1', '3', null, null, '1', null, '0', '0', null, null, null, null, '0', null);
INSERT INTO `oms_order` VALUES ('21', '1', '201809140101000002', '2018-09-14 16:16:16', 'test', '18732.00', '16377.75', '0.00', '2', '4', null, null, '1', null, '0', '0', null, null, null, null, '0', null);
INSERT INTO `oms_order` VALUES ('22', '1', '201809150101000005', '2018-09-15 12:24:27', 'test', '18732.00', '16377.75', '0.00', '0', '4', '顺丰快递', '201707196398345', '1', null, '0', '0', null, '2018-10-12 14:01:28', null, null, '0', null);
INSERT INTO `oms_order` VALUES ('23', '1', '201809150102000006', '2018-09-15 14:24:29', 'test', '18732.00', '16377.75', '0.00', '1', '1', '顺丰快递', 'xxx', '1', null, '0', '0', null, '2018-10-16 14:41:28', null, null, '0', null);
INSERT INTO `oms_order` VALUES ('24', '1', '201809130101000005', '2018-09-13 16:57:40', 'test', '18732.00', '16377.75', '0.00', '2', '2', null, null, '1', null, '0', '0', null, null, null, null, '0', null);
INSERT INTO `oms_order` VALUES ('25', '1', '201809130102000006', '2018-09-13 17:03:00', 'test', '18732.00', '16377.75', '10.00', '1', '4', null, null, '1', 'xxx', '0', '0', null, null, null, '2022-02-06 00:54:24', '0', null);
INSERT INTO `oms_order` VALUES ('26', '1', '201809140101000003', '2018-09-14 16:16:16', 'test', '18732.00', '18732.00', '0.00', '0', '0', null, null, '1', null, '1', '0', null, null, '2022-02-05 21:28:43', null, '0', null);
INSERT INTO `oms_order` VALUES ('27', '1', 'ord_20220205154149', '2022-02-05 15:41:50', 'test', '99.00', '99.00', '0.00', '2', '1', null, null, '1', null, '0', '0', '2022-02-05 16:15:28', null, null, null, '1', null);
INSERT INTO `oms_order` VALUES ('28', '10', 'ord_20220326173133', '2022-03-26 17:31:33', 'mall_s46l5plz', '101.00', '101.00', '0.00', '2', '3', '顺丰快递', 'deli_20220327140700', '1', '备注信息', '1', '0', '2022-03-26 17:33:08', '2022-03-27 14:07:39', '2022-03-28 22:53:04', '2022-03-28 22:53:04', '1', null);
INSERT INTO `oms_order` VALUES ('29', '10', 'ord_20220327235740', '2022-03-27 23:57:40', 'mall_s46l5plz', '906.00', '913.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-03-27 23:58:04', null, null, '2022-03-27 23:58:04', '1', null);
INSERT INTO `oms_order` VALUES ('30', '10', 'ord_20220328224323', '2022-03-28 22:43:24', 'mall_s46l5plz', '406.00', '413.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-03-28 22:43:25', null, null, '2022-03-28 22:43:25', '1', null);
INSERT INTO `oms_order` VALUES ('31', '10', 'ord_20220406231044', '2022-04-06 23:10:45', 'mall_s46l5plz', '406.00', '413.00', '7.00', '2', '1', null, null, '6', null, '0', '0', '2022-04-06 23:10:48', null, null, '2022-04-06 23:10:48', '1', null);
INSERT INTO `oms_order` VALUES ('32', '10', 'ord_20220416231256', '2022-04-16 23:12:56', 'mall_s46l5plz', '1805.00', '1812.00', '7.00', '0', '0', null, null, '1', null, '0', '0', null, null, null, '2022-04-16 23:12:56', '2', null);
INSERT INTO `oms_order` VALUES ('33', '10', 'ord_20220416231331', '2022-04-16 23:13:31', 'mall_s46l5plz', '906.00', '913.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-04-16 23:13:55', null, null, '2022-04-16 23:13:55', '1', null);
INSERT INTO `oms_order` VALUES ('34', '10', 'ord_20220417232223', '2022-04-17 23:22:23', 'mall_s46l5plz', '906.00', '913.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-04-17 23:22:24', null, null, '2022-04-17 23:22:24', '1', null);
INSERT INTO `oms_order` VALUES ('35', '10', 'ord_20220417233531', '2022-04-17 23:35:31', 'mall_s46l5plz', '889.00', '896.00', '7.00', '0', '0', null, null, '1', null, '0', '0', null, null, null, '2022-04-17 23:35:31', '1', null);
INSERT INTO `oms_order` VALUES ('36', '10', 'ord_20220501165440', '2022-05-01 16:54:41', 'mall_s46l5plz', '399.00', '406.00', '7.00', '2', '1', null, null, '3', null, '0', '0', '2022-05-01 16:54:42', null, null, '2022-05-01 16:54:42', '1', null);
INSERT INTO `oms_order` VALUES ('37', '10', 'ord_20220501165725', '2022-05-01 16:57:25', 'mall_s46l5plz', '238.00', '238.00', '0.00', '2', '1', null, null, '1', null, '0', '0', '2022-05-01 16:57:26', null, null, '2022-05-01 16:57:26', '1', null);
INSERT INTO `oms_order` VALUES ('38', '10', 'ord_20220502170605', '2022-05-02 17:06:05', 'mall_s46l5plz', '339.00', '346.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-05-02 17:06:07', null, null, '2022-05-02 17:06:07', '2', null);
INSERT INTO `oms_order` VALUES ('39', '10', 'ord_20220502171124', '2022-05-02 17:11:25', 'mall_s46l5plz', '278.00', '278.00', '0.00', '2', '1', null, null, '1', null, '0', '0', '2022-05-02 17:11:26', null, null, '2022-05-02 17:11:26', '2', null);
INSERT INTO `oms_order` VALUES ('40', '10', 'ord_20220503160451', '2022-05-03 16:04:51', 'mall_s46l5plz', '1798.00', '1805.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-05-03 16:04:54', null, null, '2022-05-03 16:04:54', '2', null);
INSERT INTO `oms_order` VALUES ('41', '10', 'ord_20220503161138', '2022-05-03 16:11:39', 'mall_s46l5plz', '1798.00', '1805.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-05-03 16:11:40', null, null, '2022-05-03 16:11:40', '2', null);
INSERT INTO `oms_order` VALUES ('42', '10', 'ord_20220503161831', '2022-05-03 16:18:31', 'mall_s46l5plz', '1798.00', '1805.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-05-03 16:18:33', null, null, '2022-05-03 16:18:33', '2', null);
INSERT INTO `oms_order` VALUES ('43', '10', 'ord_20220503163221', '2022-05-03 16:32:21', 'mall_s46l5plz', '1798.00', '1805.00', '7.00', '2', '1', null, null, '1', null, '0', '0', '2022-05-03 16:32:23', null, null, '2022-05-03 16:32:23', '2', null);

-- ----------------------------
-- Table structure for oms_order_item
-- ----------------------------
DROP TABLE IF EXISTS `oms_order_item`;
CREATE TABLE `oms_order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
  `order_sn` varchar(64) DEFAULT NULL COMMENT '订单编号',
  `product_id` bigint(20) DEFAULT NULL,
  `product_pic` varchar(500) DEFAULT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `product_brand` varchar(200) DEFAULT NULL,
  `product_sn` varchar(64) DEFAULT NULL,
  `product_price` decimal(10,2) DEFAULT NULL COMMENT '销售价格',
  `product_quantity` int(11) DEFAULT NULL COMMENT '购买数量',
  `product_sku_id` bigint(20) DEFAULT NULL COMMENT '商品sku编号',
  `product_sku_code` varchar(50) DEFAULT NULL COMMENT '商品sku条码',
  `product_category_id` bigint(20) DEFAULT NULL COMMENT '商品分类id',
  `product_attr` varchar(500) DEFAULT NULL COMMENT '商品销售属性:[{"key":"颜色","value":"颜色"},{"key":"容量","value":"4G"}]',
  `brand_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='订单中所包含的商品';

-- ----------------------------
-- Records of oms_order_item
-- ----------------------------
INSERT INTO `oms_order_item` VALUES ('46', '27', 'ord_20220205154149', '23', 'https://img13.360buyimg.com/n1/jfs/t1/147595/39/18882/152132/6067416bE249067bb/9be63383a030446a.jpg', '男士T恤', '李宁', 'prod_20220301133710', '99.00', '1', '78', '201806070023001', '11', '颜色:米白色, 尺寸:M, null', '59');
INSERT INTO `oms_order_item` VALUES ('47', '28', 'ord_20220326173133', '23', 'https://img13.360buyimg.com/n1/jfs/t1/147595/39/18882/152132/6067416bE249067bb/9be63383a030446a.jpg', '男士T恤', '李宁', 'prod_20220301133710', '101.00', '1', '81', '201806070023004', '11', '颜色:浅黄色, 尺寸:L', '59');
INSERT INTO `oms_order_item` VALUES ('48', '29', 'ord_20220327235740', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '1', '119', 'sku_20220327193301h1', '31', '颜色:棉花糖色, 尺码:40', '59');
INSERT INTO `oms_order_item` VALUES ('49', '30', 'ord_20220328224323', '7', 'https://img14.360buyimg.com/n1/jfs/t1/122107/10/23938/414767/623c1477E0f92b9a4/0f23c80e354c8804.jpg', '水花2代篮球鞋', '安踏', null, '399.00', '1', '122', 'sku_2022032719410w2', '31', '颜色:白色, 尺码:43', '1');
INSERT INTO `oms_order_item` VALUES ('50', '31', 'ord_20220406231044', '7', 'https://img14.360buyimg.com/n1/jfs/t1/122107/10/23938/414767/623c1477E0f92b9a4/0f23c80e354c8804.jpg', '水花2代篮球鞋', '安踏', null, '399.00', '1', '122', 'sku_2022032719410w2', '31', '颜色:白色, 尺码:43, null', '1');
INSERT INTO `oms_order_item` VALUES ('51', '32', 'ord_20220416231256', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '2', '119', 'sku_20220327193301h1', '31', '颜色:棉花糖色, 尺码:40, null', '59');
INSERT INTO `oms_order_item` VALUES ('52', '33', 'ord_20220416231331', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '1', '119', 'sku_20220327193301h1', '31', '颜色:棉花糖色, 尺码:40, null', '59');
INSERT INTO `oms_order_item` VALUES ('53', '34', 'ord_20220417232223', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '1', '119', 'sku_20220327193301h1', '31', '颜色:棉花糖色,尺码:40', '59');
INSERT INTO `oms_order_item` VALUES ('54', '35', 'ord_20220417233531', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '889.00', '1', '121', 'sku_20220327193302w4', '31', '颜色:白色,尺码:40', '59');
INSERT INTO `oms_order_item` VALUES ('55', '36', 'ord_20220501165440', '7', 'https://img14.360buyimg.com/n1/jfs/t1/122107/10/23938/414767/623c1477E0f92b9a4/0f23c80e354c8804.jpg', '水花2代篮球鞋', '安踏', null, '399.00', '1', '122', 'sku_2022032719410w2', '31', '颜色:白色,尺码:43', '1');
INSERT INTO `oms_order_item` VALUES ('56', '37', 'ord_20220501165725', '1', 'https://img14.360buyimg.com/n0/jfs/t1/123618/14/21289/108217/62020d09Eec77b385/5f1981433c1b7eff.jpg', '男子长裤', '耐克', null, '238.00', '1', '110', 'sku_20220313135617a1', '29', '颜色:黑色,尺码:M/170', '58');
INSERT INTO `oms_order_item` VALUES ('57', '38', 'ord_20220502170605', '2', 'https://img14.360buyimg.com/n0/jfs/t1/128372/36/22619/65265/621ed23dEadc6a3a7/201433f8df8c6bdc.jpg', '男子短裤', '耐克', null, '180.00', '1', '130', 'sku_20220502163809u3', '59', '颜色:黑色,尺码:M/170', '58');
INSERT INTO `oms_order_item` VALUES ('58', '38', 'ord_20220502170605', '4', 'https://img14.360buyimg.com/n0/jfs/t1/138317/1/25616/160124/61d08456E3c791953/3ca116a62a61604c.jpg', '网面透气跑步鞋', '鸿星尔克', null, '159.00', '1', '131', 'sku_20220502164308i8', '30', '颜色:蓝白色,尺码:43', '3');
INSERT INTO `oms_order_item` VALUES ('59', '39', 'ord_20220502171124', '5', 'https://img14.360buyimg.com/n0/jfs/t1/209309/38/14162/113753/61cad944Ea327e3a3/de77c7d5e4f48d1e.jpg', '运动双肩背包', '阿迪达斯', null, '139.00', '2', '129', 'sku_20220502163801u2', '32', '颜色:深蓝色', '4');
INSERT INTO `oms_order_item` VALUES ('60', '40', 'ord_20220503160451', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '2', '120', 'sku_20220327193302o1', '31', '颜色:棉花糖色,尺码:41', '59');
INSERT INTO `oms_order_item` VALUES ('61', '41', 'ord_20220503161138', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '2', '120', 'sku_20220327193302o1', '31', '颜色:棉花糖色,尺码:41', '59');
INSERT INTO `oms_order_item` VALUES ('62', '42', 'ord_20220503161831', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '2', '120', 'sku_20220327193302o1', '31', '颜色:棉花糖色,尺码:41', '59');
INSERT INTO `oms_order_item` VALUES ('63', '43', 'ord_20220503163221', '8', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '韦德全城10篮球鞋', '李宁', null, '899.00', '2', '120', 'sku_20220327193302o1', '31', '颜色:棉花糖色,尺码:41', '59');

-- ----------------------------
-- Table structure for oms_order_operate_history
-- ----------------------------
DROP TABLE IF EXISTS `oms_order_operate_history`;
CREATE TABLE `oms_order_operate_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
  `operate_man` varchar(100) DEFAULT NULL COMMENT '操作人：用户；系统；后台管理员',
  `create_time` datetime DEFAULT NULL COMMENT '操作时间',
  `order_status` int(1) DEFAULT NULL COMMENT '订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单',
  `note` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='订单操作历史记录';

-- ----------------------------
-- Records of oms_order_operate_history
-- ----------------------------
INSERT INTO `oms_order_operate_history` VALUES ('5', '12', '后台管理员', '2018-10-12 14:01:29', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('6', '13', '后台管理员', '2018-10-12 14:01:29', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('7', '12', '后台管理员', '2018-10-12 14:13:10', '4', '订单关闭:买家退货');
INSERT INTO `oms_order_operate_history` VALUES ('8', '13', '后台管理员', '2018-10-12 14:13:10', '4', '订单关闭:买家退货');
INSERT INTO `oms_order_operate_history` VALUES ('9', '22', '后台管理员', '2018-10-15 16:31:48', '4', '订单关闭:xxx');
INSERT INTO `oms_order_operate_history` VALUES ('10', '22', '后台管理员', '2018-10-15 16:35:08', '4', '订单关闭:xxx');
INSERT INTO `oms_order_operate_history` VALUES ('11', '22', '后台管理员', '2018-10-15 16:35:59', '4', '订单关闭:xxx');
INSERT INTO `oms_order_operate_history` VALUES ('12', '17', '后台管理员', '2018-10-15 16:43:40', '4', '订单关闭:xxx');
INSERT INTO `oms_order_operate_history` VALUES ('13', '25', '后台管理员', '2018-10-15 16:52:14', '4', '订单关闭:xxx');
INSERT INTO `oms_order_operate_history` VALUES ('14', '26', '后台管理员', '2018-10-15 16:52:14', '4', '订单关闭:xxx');
INSERT INTO `oms_order_operate_history` VALUES ('15', '23', '后台管理员', '2018-10-16 14:41:28', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('16', '13', '后台管理员', '2018-10-16 14:42:17', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('17', '18', '后台管理员', '2018-10-16 14:42:17', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('18', '26', '后台管理员', '2018-10-30 14:37:44', '4', '订单关闭:关闭订单');
INSERT INTO `oms_order_operate_history` VALUES ('19', '25', '后台管理员', '2018-10-30 15:07:01', '0', '修改收货人信息');
INSERT INTO `oms_order_operate_history` VALUES ('20', '25', '后台管理员', '2018-10-30 15:08:13', '0', '修改费用信息');
INSERT INTO `oms_order_operate_history` VALUES ('21', '25', '后台管理员', '2018-10-30 15:08:31', '0', '修改备注信息：xxx');
INSERT INTO `oms_order_operate_history` VALUES ('22', '25', '后台管理员', '2018-10-30 15:08:39', '4', '订单关闭:2222');
INSERT INTO `oms_order_operate_history` VALUES ('23', '22', '后台管理员', '2022-01-17 23:48:41', '4', '管理员');
INSERT INTO `oms_order_operate_history` VALUES ('24', '22', '后台管理员', '2022-01-17 23:52:09', '4', '管理员关闭');
INSERT INTO `oms_order_operate_history` VALUES ('25', '13', '后台管理员', '2022-01-18 23:56:16', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('26', '13', '后台管理员', '2022-01-18 23:59:25', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('27', '27', '用户', '2022-02-05 15:41:50', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('28', '27', '用户', '2022-02-05 16:15:28', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('29', '26', '用户', '2022-02-05 21:28:43', '3', '已收货');
INSERT INTO `oms_order_operate_history` VALUES ('30', '26', '用户', '2022-02-06 00:33:44', '4', '删除订单');
INSERT INTO `oms_order_operate_history` VALUES ('31', '25', '用户', '2022-02-06 00:54:24', '4', '取消订单');
INSERT INTO `oms_order_operate_history` VALUES ('32', '28', '用户', '2022-03-26 17:31:33', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('33', '28', '用户', '2022-03-26 17:33:08', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('34', '28', '后台管理员', '2022-03-27 14:07:39', '2', '完成发货');
INSERT INTO `oms_order_operate_history` VALUES ('35', '29', '用户', '2022-03-27 23:57:40', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('36', '29', '用户', '2022-03-27 23:58:04', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('37', '30', '用户', '2022-03-28 22:43:24', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('38', '30', '用户', '2022-03-28 22:43:25', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('39', '28', '用户', '2022-03-28 22:53:04', '3', '已收货');
INSERT INTO `oms_order_operate_history` VALUES ('40', '31', '用户', '2022-04-06 23:10:45', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('41', '31', '用户', '2022-04-06 23:10:48', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('42', '32', '用户', '2022-04-16 23:12:56', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('43', '33', '用户', '2022-04-16 23:13:31', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('44', '33', '用户', '2022-04-16 23:13:55', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('45', '34', '用户', '2022-04-17 23:22:23', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('46', '34', '用户', '2022-04-17 23:22:24', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('47', '35', '用户', '2022-04-17 23:35:31', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('48', '36', '用户', '2022-05-01 16:54:41', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('49', '36', '用户', '2022-05-01 16:54:42', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('50', '37', '用户', '2022-05-01 16:57:25', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('51', '37', '用户', '2022-05-01 16:57:26', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('52', '38', '用户', '2022-05-02 17:06:05', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('53', '38', '用户', '2022-05-02 17:06:07', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('54', '39', '用户', '2022-05-02 17:11:25', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('55', '39', '用户', '2022-05-02 17:11:26', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('56', '40', '用户', '2022-05-03 16:04:52', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('57', '40', '用户', '2022-05-03 16:04:54', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('58', '41', '用户', '2022-05-03 16:11:39', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('59', '41', '用户', '2022-05-03 16:11:40', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('60', '42', '用户', '2022-05-03 16:18:32', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('61', '42', '用户', '2022-05-03 16:18:33', '1', '订单已付款');
INSERT INTO `oms_order_operate_history` VALUES ('62', '43', '用户', '2022-05-03 16:32:21', '0', '提交订单');
INSERT INTO `oms_order_operate_history` VALUES ('63', '43', '用户', '2022-05-03 16:32:23', '1', '订单已付款');

-- ----------------------------
-- Table structure for oms_order_return_apply
-- ----------------------------
DROP TABLE IF EXISTS `oms_order_return_apply`;
CREATE TABLE `oms_order_return_apply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL COMMENT '订单id',
  `product_id` bigint(20) DEFAULT NULL COMMENT '退货商品id',
  `order_sn` varchar(64) DEFAULT NULL COMMENT '订单编号',
  `create_time` datetime DEFAULT NULL COMMENT '申请时间',
  `member_username` varchar(64) DEFAULT NULL COMMENT '会员用户名',
  `return_amount` decimal(10,2) DEFAULT NULL COMMENT '退款金额',
  `return_name` varchar(100) DEFAULT NULL COMMENT '退货人姓名',
  `return_phone` varchar(100) DEFAULT NULL COMMENT '退货人电话',
  `status` int(1) DEFAULT NULL COMMENT '申请状态：0->待处理；1->退货中；2->已完成；3->已拒绝',
  `handle_time` datetime DEFAULT NULL COMMENT '处理时间',
  `product_name` varchar(200) DEFAULT NULL COMMENT '商品名称',
  `product_brand` varchar(200) DEFAULT NULL COMMENT '商品品牌',
  `product_attr` varchar(500) DEFAULT NULL COMMENT '商品销售属性：颜色：红色；尺码：xl;',
  `product_count` int(11) DEFAULT NULL COMMENT '退货数量',
  `product_price` decimal(10,2) DEFAULT NULL COMMENT '商品单价',
  `product_real_price` decimal(10,2) DEFAULT NULL COMMENT '商品实际支付单价',
  `reason_id` bigint(20) DEFAULT NULL COMMENT '原因',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `proof_pics` varchar(1000) DEFAULT NULL COMMENT '凭证图片，以逗号隔开',
  `handle_note` varchar(500) DEFAULT NULL COMMENT '处理备注',
  `handle_man` varchar(100) DEFAULT NULL COMMENT '处理人员',
  `receive_man` varchar(100) DEFAULT NULL COMMENT '收货人',
  `receive_time` datetime DEFAULT NULL COMMENT '收货时间',
  `receive_note` varchar(500) DEFAULT NULL COMMENT '收货备注',
  `member_id` bigint(20) DEFAULT NULL,
  `brand_id` bigint(20) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='订单退货申请';

-- ----------------------------
-- Records of oms_order_return_apply
-- ----------------------------
INSERT INTO `oms_order_return_apply` VALUES ('3', '12', '26', '201809150101000001', '2018-10-14 14:34:57', 'test', '30.00', '大梨', '18000000000', '2', '2022-01-22 19:31:41', '华为 HUAWEI P20', '李宁', '颜色：金色;内存：16G', '1', '3788.00', '3585.98', '1', '老是卡', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/5ac1bf58Ndefaac16.jpg,http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/xiaomi.jpg', '批准退货', 'admin', 'admin', '2022-01-22 19:32:48', '确认收货', null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('4', '12', '27', '201809150101000001', '2018-10-17 14:40:21', 'test', '3585.98', '大梨', '18000000000', '1', '2018-10-18 13:54:10', '小米8', '李宁', '颜色：黑色;内存：32G', '1', '2699.00', '2022.81', '1', '不够高端', '', '已经处理了', 'admin', null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('8', '13', '28', '201809150102000002', '2018-10-17 14:44:18', 'test', null, '大梨', '18000000000', '3', '2018-10-18 13:57:12', '红米5A', '李宁', '颜色：金色;内存：16G', '1', '649.00', '591.05', '1', '颜色太土', '', '理由不够充分', 'admin', null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('9', '14', '26', '201809130101000001', '2018-10-17 14:34:57', 'test', '3500.00', '大梨', '18000000000', '2', '2018-10-24 15:44:56', '华为 HUAWEI P20', '李宁', '颜色：金色;内存：16G', '1', '3788.00', '3585.98', '1', '老是卡', '', '呵呵', 'admin', 'admin', '2018-10-24 15:46:35', '收货了', null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('10', '14', '27', '201809130101000001', '2018-10-17 14:40:21', 'test', null, '大梨', '18000000000', '3', '2018-10-24 15:46:57', '小米8', '李宁', '颜色：黑色;内存：32G', '1', '2699.00', '2022.81', '1', '不够高端', '', '就是不退', 'admin', null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('11', '14', '28', '201809130101000001', '2018-10-17 14:44:18', 'test', '591.05', '大梨', '18000000000', '1', '2018-10-24 17:09:04', '红米5A', '李宁', '颜色：金色;内存：16G', '1', '649.00', '591.05', '1', '颜色太土', '', '可以退款', 'admin', null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('12', '15', '26', '201809130102000002', '2018-10-17 14:34:57', 'test', '3500.00', '大梨', '18000000000', '2', '2018-10-24 17:22:54', '华为 HUAWEI P20', '李宁', '颜色：金色;内存：16G', '1', '3788.00', '3585.98', '1', '老是卡', '', '退货了', 'admin', 'admin', '2018-10-24 17:23:06', '收货了', null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('13', '15', '27', '201809130102000002', '2018-10-17 14:40:21', 'test', null, '大梨', '18000000000', '3', '2018-10-24 17:23:30', '小米8', '李宁', '颜色：黑色;内存：32G', '1', '2699.00', '2022.81', '1', '不够高端', '', '无法退货', 'admin', null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('15', '16', '26', '201809140101000001', '2018-10-17 14:34:57', 'test', null, '大梨', '18000000000', '3', '2022-01-22 19:34:15', '华为 HUAWEI P20', '李宁', '颜色：金色;内存：16G', '1', '3788.00', '3585.98', '1', '老是卡', '', '拒绝退货', 'admin', null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('16', '16', '27', '201809140101000001', '2018-10-17 14:40:21', 'test', null, '大梨', '18000000000', '0', null, '小米8', '李宁', '颜色：黑色;内存：32G', '1', '2699.00', '2022.81', '1', '不够高端', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('17', '16', '28', '201809140101000001', '2018-10-17 14:44:18', 'test', null, '大梨', '18000000000', '0', null, '红米5A', '李宁', '颜色：金色;内存：16G', '1', '649.00', '591.05', '1', '颜色太土', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('18', '17', '26', '201809150101000003', '2018-10-17 14:34:57', 'test', null, '大梨', '18000000000', '0', null, '华为 HUAWEI P20', '李宁', '颜色：金色;内存：16G', '1', '3788.00', '3585.98', '1', '老是卡', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('19', '17', '27', '201809150101000003', '2018-10-17 14:40:21', 'test', null, '大梨', '18000000000', '0', null, '小米8', '李宁', '颜色：黑色;内存：32G', '1', '2699.00', '2022.81', '1', '不够高端', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('20', '17', '28', '201809150101000003', '2018-10-17 14:44:18', 'test', null, '大梨', '18000000000', '0', null, '红米5A', '李宁', '颜色：金色;内存：16G', '1', '649.00', '591.05', '1', '颜色太土', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('21', '18', '26', '201809150102000004', '2018-10-17 14:34:57', 'test', null, '大梨', '18000000000', '0', null, '华为 HUAWEI P20', '李宁', '颜色：金色;内存：16G', '1', '3788.00', '3585.98', '1', '老是卡', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('22', '18', '27', '201809150102000004', '2018-10-17 14:40:21', 'test', null, '大梨', '18000000000', '0', null, '小米8', '李宁', '颜色：黑色;内存：32G', '1', '2699.00', '2022.81', '1', '不够高端', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('23', '18', '28', '201809150102000004', '2018-10-17 14:44:18', 'test', null, '大梨', '18000000000', '0', null, '红米5A', '李宁', '颜色：金色;内存：16G', '1', '649.00', '591.05', '1', '颜色太土', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('24', '19', '26', '201809130101000003', '2018-10-17 14:34:57', 'test', null, '大梨', '18000000000', '0', null, '华为 HUAWEI P20', '李宁', '颜色：金色;内存：16G', '1', '3788.00', '3585.98', '1', '老是卡', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('25', '19', '27', '201809130101000003', '2018-10-17 14:40:21', 'test', null, '大梨', '18000000000', '0', null, '小米8', '李宁', '颜色：黑色;内存：32G', '1', '2699.00', '2022.81', '1', '不够高端', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('26', '19', '28', '201809130101000003', '2018-10-17 14:44:18', 'test', null, '大梨', '18000000000', '0', null, '红米5A', '李宁', '颜色：金色;内存：16G', '1', '649.00', '591.05', '1', '颜色太土', '', null, null, null, null, null, null, '59', null);
INSERT INTO `oms_order_return_apply` VALUES ('27', '20', '28', '201809130102000004', '2022-02-09 15:51:33', 'test', '699.00', '大梨', '18000000000', '0', null, '红米5A', '李宁', '[{\"key\":\"颜色\",\"value\":\"金色\"},{\"key\":\"容量\",\"value\":\"32G\"}]', '1', '699.00', '699.00', '1', '黑屏了', null, null, null, null, null, null, '1', '59', null);

-- ----------------------------
-- Table structure for oms_order_return_reason
-- ----------------------------
DROP TABLE IF EXISTS `oms_order_return_reason`;
CREATE TABLE `oms_order_return_reason` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '退货类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `status` int(1) DEFAULT NULL COMMENT '状态：0->不启用；1->启用',
  `create_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='退货原因表';

-- ----------------------------
-- Records of oms_order_return_reason
-- ----------------------------
INSERT INTO `oms_order_return_reason` VALUES ('1', '质量问题', '1', '0', '2018-10-17 10:00:45');
INSERT INTO `oms_order_return_reason` VALUES ('2', '尺寸码数不合适', '1', '1', '2022-02-20 00:54:07');
INSERT INTO `oms_order_return_reason` VALUES ('4', '发货出错', '1', '1', '2022-02-20 00:54:31');
INSERT INTO `oms_order_return_reason` VALUES ('5', '7天无理由退货', '1', '1', '2022-02-20 00:55:07');
INSERT INTO `oms_order_return_reason` VALUES ('12', '发票问题', '0', '1', '2018-10-19 16:28:36');
INSERT INTO `oms_order_return_reason` VALUES ('13', '其他问题', '0', '1', '2018-10-19 16:28:51');
INSERT INTO `oms_order_return_reason` VALUES ('14', '物流问题', '0', '1', '2018-10-19 16:29:01');
INSERT INTO `oms_order_return_reason` VALUES ('15', '下单出错', '0', '1', '2022-02-20 00:56:29');
INSERT INTO `oms_order_return_reason` VALUES ('16', '不喜欢', '0', '1', '2022-02-20 00:56:01');

-- ----------------------------
-- Table structure for pms_brand
-- ----------------------------
DROP TABLE IF EXISTS `pms_brand`;
CREATE TABLE `pms_brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `first_letter` varchar(8) DEFAULT NULL COMMENT '首字母',
  `show_status` int(1) DEFAULT NULL COMMENT '启用状态：0->禁用；1->启用',
  `logo` varchar(255) DEFAULT NULL COMMENT '品牌logo',
  `keywords` text COMMENT '销售的商品分类，逗号分隔',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='品牌表';

-- ----------------------------
-- Records of pms_brand
-- ----------------------------
INSERT INTO `pms_brand` VALUES ('1', '安踏', 'A', '1', 'https://img2.baidu.com/it/u=3902389457,1182511395&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=325', '跑步鞋,篮球鞋,T恤,卫衣,运动长裤,运动短裤', null);
INSERT INTO `pms_brand` VALUES ('2', '匹克', 'P', '1', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fnimg.ws.126.net%2F%3Furl%3Dhttp%253A%252F%252Fdingyue.ws.126.net%252F2021%252F0926%252F6009228bj00r00sna0011c000no00bvm.jpg%26thumbnail%3D650x2147483647%26quality%3D80%26type%3Djpg', '跑步鞋,篮球鞋,休闲鞋,板鞋,T恤,卫衣,运动长裤,运动短裤,夹克,篮球服', null);
INSERT INTO `pms_brand` VALUES ('3', '鸿星尔克', 'H', '1', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp7.itc.cn%2Fimages01%2F20210727%2F84b442428ee84d45aabf38898b79d170.jpeg', '跑步鞋,休闲鞋,板鞋,T恤,卫衣,夹克,运动长裤,运动短裤,袜子', null);
INSERT INTO `pms_brand` VALUES ('4', '阿迪达斯', 'A', '1', 'https://ss0.baidu.com/-Po3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/cf1b9d16fdfaaf51f888ce16875494eef01f7a2b.jpg', '跑步鞋,篮球鞋,足球鞋,训练鞋,休闲鞋,T恤,卫衣,运动长裤,运动短裤,夹克,运动背包,运动袜', null);
INSERT INTO `pms_brand` VALUES ('6', '彪马', 'P', '1', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20170829%2F9822a6bba9794f62912d6c08b6684d74.jpeg', '跑步鞋,篮球鞋,足球鞋,训练鞋,休闲鞋,T恤,卫衣,运动长裤,运动短裤,夹克,泳装,运动背包,手提运动包,运动袜', null);
INSERT INTO `pms_brand` VALUES ('21', '安德玛', 'U', '1', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.mshishang.com%2Fpics%2Fdefault%2Fbrand%2F47cb929659fede96f01223c3f3fabad3.gif', '跑步鞋,篮球鞋,训练鞋,休闲鞋,T恤,卫衣,运动裤,运动背心,夹克,运动背包,运动袜', null);
INSERT INTO `pms_brand` VALUES ('49', '七匹狼', 'S', '1', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/18d8bc3eb13533fab466d702a0d3fd1f40345bcd.jpg', 'T恤,跑步鞋,篮球鞋', null);
INSERT INTO `pms_brand` VALUES ('50', '海澜之家', 'H', '1', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/99d3279f1029d32b929343b09d3c72de_222_222.jpg', 'T恤,跑步鞋,篮球鞋', null);
INSERT INTO `pms_brand` VALUES ('58', '耐克', 'N', '1', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.ef43.com.cn%2FnewsImages%2F2021%2F9%2F28084329685.jpg', '跑步鞋,篮球鞋,足球鞋训练鞋,休闲鞋,T恤,卫衣,运动裤,运动背心,夹克,运动背包,运动袜', null);
INSERT INTO `pms_brand` VALUES ('59', '李宁', 'L', '1', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fp0.itc.cn%2Fimages01%2F20211125%2Fdb8b5bf3c35546878642f698a8701732.jpeg', '跑步鞋,篮球鞋,休闲鞋,板鞋,训练鞋,T恤,卫衣,卫裤,运动长裤,运动短裤,夹克,运动服,袜子,护具,篮球,足球', null);

-- ----------------------------
-- Table structure for pms_comment
-- ----------------------------
DROP TABLE IF EXISTS `pms_comment`;
CREATE TABLE `pms_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sku_stock_id` bigint(20) DEFAULT NULL,
  `member_id` bigint(20) DEFAULT NULL,
  `star` int(3) DEFAULT NULL COMMENT '评价星数：1->5',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `show_status` int(1) DEFAULT NULL COMMENT '是否显示：0->否；1->是',
  `content` text COMMENT '内容',
  `pics` varchar(1000) DEFAULT NULL COMMENT '上传图片地址，以逗号隔开',
  `reply_count` int(11) DEFAULT NULL COMMENT '回复数',
  `evaluation_status` int(1) DEFAULT NULL COMMENT '评价程度(0->差评, 1->中评, 2->好评)',
  `picture_status` int(1) DEFAULT NULL COMMENT '是否有图(0->无, 1->有)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='商品评价表';

-- ----------------------------
-- Records of pms_comment
-- ----------------------------
INSERT INTO `pms_comment` VALUES ('1', '78', '1', '5', '2022-01-24 21:46:46', '1', '物流很快，商家立马发货，好评', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/5ab46a3cN616bdc41.jpg,http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '5', '2', '1');
INSERT INTO `pms_comment` VALUES ('2', '78', '10', '2', '2022-02-03 18:12:26', '1', '价格实惠，下次还会再买', null, '1', '1', '1');
INSERT INTO `pms_comment` VALUES ('3', '78', '6', '1', '2022-02-03 18:13:24', '1', '发错货了，我要的是黑色，发了红色给我，差评', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/5ac1bf58Ndefaac16.jpg', '0', '0', '1');
INSERT INTO `pms_comment` VALUES ('4', '78', '1', '5', '2022-02-07 22:43:03', '1', '很喜欢，穿起来很舒服，真的价廉物美，白色比黑色要好看', null, '0', '2', '0');
INSERT INTO `pms_comment` VALUES ('5', '78', '1', '3', '2022-02-07 22:44:00', '1', '材质是全棉的，摸在手上非常细腻柔软。之前买的型号大了一码，跟客户换了小码的，客服非常客气，态度很好', null, '0', '1', '0');
INSERT INTO `pms_comment` VALUES ('6', '78', '1', '5', '2022-02-07 22:46:27', '1', '质量挺好的，性价比很高', null, '0', '2', '0');
INSERT INTO `pms_comment` VALUES ('7', '78', '1', '5', '2022-02-07 22:53:19', '1', '店家态度很好，问问题都会仔细回答，讲不清楚的还会拍照发过来', null, '0', '2', '0');
INSERT INTO `pms_comment` VALUES ('8', '78', '1', '5', '2022-02-09 00:07:51', '1', '非常满意，吸汗透气，很喜欢', null, '0', '2', '0');
INSERT INTO `pms_comment` VALUES ('9', '8', '1', '5', '2021-12-01 19:41:54', '1', '物流很快，商家立马发货，好评', 'https://img10.360buyimg.com/n1/jfs/t1/219914/9/11633/131074/61f27b7bEb3ab6f00/06e753c525c69563.jpg,https://img10.360buyimg.com/n1/jfs/t1/219234/40/11424/131673/61f27b7bEee0f2821/569194179f999151.jpg', '3', '2', '1');
INSERT INTO `pms_comment` VALUES ('10', '8', '3', '2', '2022-01-25 13:55:29', '1', '价格实惠，下次还会再买', null, '0', '1', '0');
INSERT INTO `pms_comment` VALUES ('11', '8', '10', '4', '2022-03-01 20:04:07', '1', '店家态度很好，问问题都会仔细回答，讲不清楚的还会拍照发过来', null, '0', '2', '0');
INSERT INTO `pms_comment` VALUES ('12', '78', '10', '5', '2022-04-06 23:15:09', '1', '1', null, '0', '2', '0');

-- ----------------------------
-- Table structure for pms_comment_reply
-- ----------------------------
DROP TABLE IF EXISTS `pms_comment_reply`;
CREATE TABLE `pms_comment_reply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) DEFAULT NULL,
  `member_id` bigint(20) DEFAULT NULL,
  `content` varchar(1000) DEFAULT NULL COMMENT '内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `type` int(1) DEFAULT NULL COMMENT '评论人员类型；0->会员；1->管理员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='产品评价回复表';

-- ----------------------------
-- Records of pms_comment_reply
-- ----------------------------
INSERT INTO `pms_comment_reply` VALUES ('1', '1', '1', 'reply123456789opessa', '2022-01-28 18:43:51', '0');
INSERT INTO `pms_comment_reply` VALUES ('2', '1', '3', 'reply123456789opessa', '2022-01-28 18:45:23', '1');
INSERT INTO `pms_comment_reply` VALUES ('3', '1', '1', '123', '2022-02-04 00:19:19', '0');
INSERT INTO `pms_comment_reply` VALUES ('4', '1', '10', '质量挺好的，性价比很高。很喜欢，穿起来很舒服，真的价廉物美，白色比黑色要好看', '2022-02-04 00:22:45', '0');
INSERT INTO `pms_comment_reply` VALUES ('5', '9', '10', '优惠券满200减30，下次还会购买', '2022-03-04 00:35:15', '0');
INSERT INTO `pms_comment_reply` VALUES ('6', '9', '1', '对评价的回复', '2022-01-01 20:06:31', '0');
INSERT INTO `pms_comment_reply` VALUES ('7', '9', '3', '谢谢亲的好评，我们会继续努力的', '2022-03-01 20:07:29', '1');
INSERT INTO `pms_comment_reply` VALUES ('8', '9', '10', '', '2022-04-18 21:40:00', '0');

-- ----------------------------
-- Table structure for pms_feight_template
-- ----------------------------
DROP TABLE IF EXISTS `pms_feight_template`;
CREATE TABLE `pms_feight_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `packing_type` varchar(50) DEFAULT NULL COMMENT '包装类型',
  `weight` varchar(50) DEFAULT NULL COMMENT '重量',
  `is_fragile` varchar(50) DEFAULT NULL COMMENT '是否易碎',
  `total_feight` decimal(10,2) DEFAULT NULL COMMENT '总运费',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='运费模版';

-- ----------------------------
-- Records of pms_feight_template
-- ----------------------------
INSERT INTO `pms_feight_template` VALUES ('1', '小包装', '小于1kg', '否', '5.00');
INSERT INTO `pms_feight_template` VALUES ('2', '小包装', '1-5kg', '是', '10.00');
INSERT INTO `pms_feight_template` VALUES ('3', '小包装', '大于5kg', '否', '9.00');
INSERT INTO `pms_feight_template` VALUES ('4', '小包装', '小于1kg', '是', '8.00');
INSERT INTO `pms_feight_template` VALUES ('5', '小包装', '1-5kg', '否', '7.00');
INSERT INTO `pms_feight_template` VALUES ('6', '小包装', '大于5kg', '是', '12.00');
INSERT INTO `pms_feight_template` VALUES ('7', '中包装', '小于1kg', '否', '7.00');
INSERT INTO `pms_feight_template` VALUES ('8', '中包装', '1-5kg', '是', '12.00');
INSERT INTO `pms_feight_template` VALUES ('9', '中包装', '大于5kg', '否', '11.00');
INSERT INTO `pms_feight_template` VALUES ('10', '中包装', '小于1kg', '是', '10.00');
INSERT INTO `pms_feight_template` VALUES ('11', '中包装', '1-5kg', '否', '9.00');
INSERT INTO `pms_feight_template` VALUES ('12', '中包装', '大于5kg', '是', '14.00');
INSERT INTO `pms_feight_template` VALUES ('13', '大包装', '小于1kg', '否', '9.00');
INSERT INTO `pms_feight_template` VALUES ('14', '大包装', '1-5kg', '是', '14.00');
INSERT INTO `pms_feight_template` VALUES ('15', '大包装', '大于5kg', '否', '13.00');
INSERT INTO `pms_feight_template` VALUES ('16', '大包装', '小于1kg', '是', '12.00');
INSERT INTO `pms_feight_template` VALUES ('17', '大包装', '1-5kg', '否', '11.00');
INSERT INTO `pms_feight_template` VALUES ('18', '大包装', '大于5kg', '是', '16.00');

-- ----------------------------
-- Table structure for pms_product
-- ----------------------------
DROP TABLE IF EXISTS `pms_product`;
CREATE TABLE `pms_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) DEFAULT NULL,
  `product_category_id` bigint(20) DEFAULT NULL,
  `feight_template_id` bigint(20) DEFAULT NULL,
  `product_attribute_category_id` bigint(20) DEFAULT NULL,
  `name` varchar(64) NOT NULL COMMENT '名称',
  `pic` varchar(255) DEFAULT NULL COMMENT '图片',
  `product_sn` varchar(64) NOT NULL COMMENT '货号',
  `delete_status` int(1) DEFAULT NULL COMMENT '删除状态：0->未删除；1->已删除',
  `publish_status` int(1) DEFAULT NULL COMMENT '上架状态：0->下架；1->上架',
  `new_status` int(1) DEFAULT NULL COMMENT '新品状态:0->不是新品；1->新品',
  `recommand_status` int(1) DEFAULT NULL COMMENT '推荐状态；0->不推荐；1->推荐',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `sale` int(11) DEFAULT NULL COMMENT '销量',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `sub_title` varchar(255) DEFAULT NULL COMMENT '副标题',
  `description` text COMMENT '商品描述',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  `low_stock` int(11) DEFAULT NULL COMMENT '库存预警值',
  `unit` varchar(16) DEFAULT NULL COMMENT '单位',
  `weight` decimal(10,2) DEFAULT NULL COMMENT '商品重量，默认为克',
  `service_ids` varchar(64) DEFAULT NULL COMMENT '以逗号分割的产品服务：1->7天无理由退货；2->快速发货；3->免费包邮',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键词',
  `note` varchar(255) DEFAULT NULL COMMENT '备注',
  `pics` varchar(500) DEFAULT NULL COMMENT '画册图片，连产品图片限制为5张，以逗号分割',
  `detail_title` varchar(255) DEFAULT NULL COMMENT '详情标题',
  `detail_desc` text COMMENT '详情描述',
  `detail_mobile_html` text COMMENT '移动端网页详情',
  `brand_name` varchar(255) DEFAULT NULL COMMENT '品牌名称',
  `product_category_name` varchar(255) DEFAULT NULL COMMENT '商品分类名称',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='商品信息';

-- ----------------------------
-- Records of pms_product
-- ----------------------------
INSERT INTO `pms_product` VALUES ('1', '58', '29', '7', '2', '男子长裤', 'https://img14.360buyimg.com/n0/jfs/t1/123618/14/21289/108217/62020d09Eec77b385/5f1981433c1b7eff.jpg', 'prod_20220313125555', '1', '1', '1', '1', '100', '1', '238.00', '运动裤男士春夏季新薄款针织户外跑步长裤卫裤健身篮球服饰秋透气裤子训练男裤休闲小脚束脚裤百搭宽松', null, '99', '20', '件', '495.00', '1,2,3', '运动长裤', null, null, null, null, null, '耐克', '运动长裤', null);
INSERT INTO `pms_product` VALUES ('2', '58', '59', '7', '2', '男子短裤', 'https://img14.360buyimg.com/n0/jfs/t1/128372/36/22619/65265/621ed23dEadc6a3a7/201433f8df8c6bdc.jpg', 'prod_20220313141121', '1', '1', '1', '1', '1', '1', '180.00', '短裤男运动裤夏季短裤男冰丝速干凉爽透气健身五分裤男裤', null, '99', '20', '件', '300.00', '1,2', '运动短裤', null, null, null, null, null, '耐克', '运动短裤', null);
INSERT INTO `pms_product` VALUES ('3', '59', '60', '7', '10', '男士运动卫衣', 'https://img14.360buyimg.com/n0/jfs/t1/159752/21/8378/174098/6035e8faE85970e18/67de92c0644ef9e2.jpg', 'prod_20220313145419', '1', '1', '1', '1', '1', '0', '97.00', null, null, '100', '20', '件', '780.00', '1,2,3', '卫衣', null, null, null, null, null, '李宁', '卫衣', null);
INSERT INTO `pms_product` VALUES ('4', '3', '30', '7', '3', '网面透气跑步鞋', 'https://img14.360buyimg.com/n0/jfs/t1/138317/1/25616/160124/61d08456E3c791953/3ca116a62a61604c.jpg', 'prod_20220313151527', '1', '1', '1', '1', '1', '1', '159.00', '鸿星尔克男鞋网面透气跑步鞋轻便休闲运动鞋舒适学生鞋', null, '99', '20', '件', '715.00', '1,2', '跑步鞋', null, null, null, null, null, '鸿星尔克', '跑步鞋', null);
INSERT INTO `pms_product` VALUES ('5', '4', '32', '7', '0', '运动双肩背包', 'https://img14.360buyimg.com/n0/jfs/t1/209309/38/14162/113753/61cad944Ea327e3a3/de77c7d5e4f48d1e.jpg', 'prod_20220313152030', '1', '1', '1', '1', '1', '1', '139.00', null, null, '99', '20', '件', '447.00', '3', '背包', null, null, null, null, null, '阿迪达斯', '运动背包', null);
INSERT INTO `pms_product` VALUES ('6', '1', '30', '7', '4', '水花3代篮球鞋', 'https://img13.360buyimg.com/n1/jfs/t1/212185/18/16043/244125/623ea13cEaf9ef648/6f11f91009c27353.jpg', 'prod_20220327181512', '1', '1', '0', '0', '1', '0', '519.00', '2022夏季KT汤普森球鞋低帮专业实战运动鞋', null, '100', '20', '双', '700.00', '1,2', '', null, null, null, null, null, '安踏', '跑步鞋', null);
INSERT INTO `pms_product` VALUES ('7', '1', '31', '7', '4', '水花2代篮球鞋', 'https://img14.360buyimg.com/n1/jfs/t1/122107/10/23938/414767/623c1477E0f92b9a4/0f23c80e354c8804.jpg', 'prod_20220327182209', '1', '1', '0', '0', '0', '3', '399.00', '汤普森KT低帮战靴男鞋运动鞋', null, '97', '0', '双', '700.00', 'NaN', '', null, 'string', null, null, null, '安踏', '篮球鞋', null);
INSERT INTO `pms_product` VALUES ('8', '59', '31', '7', '4', '韦德全城10篮球鞋', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', 'prod_20220327182243', '1', '1', '1', '0', '0', '9', '899.00', '棉花糖轻量高回弹专业比赛鞋官方旗舰', null, '93', '0', '双', '700.00', '1,2', '篮球鞋,李宁', '20220327测试', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg,https://img10.360buyimg.com/n1/jfs/t1/145423/4/27386/131640/61f27b7bE00a1ed62/47caf7d3694e45c1.jpg,https://img10.360buyimg.com/n1/jfs/t1/219914/9/11633/131074/61f27b7bEb3ab6f00/06e753c525c69563.jpg,https://img10.360buyimg.com/n1/jfs/t1/219234/40/11424/131673/61f27b7bEee0f2821/569194179f999151.jpg', null, null, null, '李宁', '篮球鞋', null);
INSERT INTO `pms_product` VALUES ('9', '59', '31', '7', '1', '闪击VI premium篮球鞋', 'https://img11.360buyimg.com/n1/jfs/t1/197699/7/21009/202811/623d9f74Eb957da28/6630c618cf8ca2af.jpg', 'prod_20220327182208', '1', '1', '0', '0', '0', '0', '558.00', '男子一体织减震支撑中帮篮球专业比赛鞋', null, '100', '0', '双', '700.00', 'NaN,1', '', null, 'string', null, null, null, '李宁', '篮球鞋', null);
INSERT INTO `pms_product` VALUES ('10', '58', '31', '7', '1', 'KD14 EP 篮球鞋', '	https://img13.360buyimg.com/n1/jfs/t1/93542/13/24954/206362/623d541bE7125c56c/624742fbf0a9f38e.jpg', 'prod_20220327180512', '1', '1', '0', '0', '0', '0', '799.00', null, null, '100', '0', '双', '800.00', 'NaN,1,2,3', '', null, 'string', null, null, null, '耐克', '篮球鞋', null);
INSERT INTO `pms_product` VALUES ('11', '4', '31', '7', '1', '米切尔3代篮球鞋', '	https://img11.360buyimg.com/n1/jfs/t1/102600/24/25573/170106/623c04cbE155520b5/8dddef626eded1e7.jpg', 'prod_20220327171209', '1', '1', '0', '0', '0', '0', '469.00', 'GCA男子篮球鞋', null, '100', '0', '双', '700.00', 'NaN,1,2,3', '', null, 'string', null, null, null, '阿迪达斯', '篮球鞋', null);
INSERT INTO `pms_product` VALUES ('12', '1', '11', '7', '1', '女式超柔软拉毛运动开衫2', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png', 'No86577', '1', '1', '0', '1', '0', '0', '249.00', null, null, '100', '0', '件', '0.00', 'string', '女式超柔软拉毛运动开衫', null, 'string', null, null, null, '安踏', 'T恤', null);
INSERT INTO `pms_product` VALUES ('13', '1', '11', '7', '1', '女式超柔软拉毛运动开衫3', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png', 'No86577', '1', '1', '0', '1', '0', '100', '249.00', null, null, '100', '0', '件', '0.00', 'string', '女式超柔软拉毛运动开衫', null, 'string', null, null, null, '安踏', 'T恤', null);
INSERT INTO `pms_product` VALUES ('14', '1', '11', '7', '1', '女式超柔软拉毛运动开衫3', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png', 'No86577', '1', '0', '0', '1', '0', '10', '249.00', null, null, '100', '0', '件', '0.00', 'string', '女式超柔软拉毛运动开衫', null, 'string', null, null, null, '安踏', 'T恤', null);
INSERT INTO `pms_product` VALUES ('18', '1', '11', '7', '1', '女式超柔软拉毛运动开衫3', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png', 'No86577', '1', '0', '0', '1', '0', '0', '249.00', null, null, '100', '0', '件', '0.00', 'string', '女式超柔软拉毛运动开衫', null, 'string', null, null, null, '安踏', 'T恤', null);
INSERT INTO `pms_product` VALUES ('22', '6', '11', '7', '1', 'test', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '', '1', '1', '0', '0', '0', '0', '0.00', null, null, '100', '0', '', '0.00', '1,2', '', null, '', null, null, null, '彪马', 'T恤', null);
INSERT INTO `pms_product` VALUES ('23', '59', '11', '4', '1', '男士T恤', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', 'prod_20220301133710', '1', '1', '1', '1', '0', '200', '99.00', '男士T恤副标题', '男士T恤男士T恤男士T恤男士T恤男士T恤男士T恤男士T恤男士T恤', '98', '20', '件', '1000.00', '1,2,3', 'T恤', null, 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/5ab46a3cN616bdc41.jpg,http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', null, null, null, '李宁', 'T恤', null);
INSERT INTO `pms_product` VALUES ('24', '6', '7', '7', null, 'xxx', '', '', '1', '0', '0', '0', '0', '0', '0.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '彪马', '外套', null);
INSERT INTO `pms_product` VALUES ('30', '50', '8', '7', '1', 'HLA海澜之家简约动物印花短袖T恤', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5ad83a4fN6ff67ecd.jpg!cc_350x449.jpg', 'HNTBJ2E042A', '0', '1', '1', '1', '0', '0', '98.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '海澜之家', 'T恤', null);
INSERT INTO `pms_product` VALUES ('31', '50', '8', '7', '1', 'HLA海澜之家蓝灰花纹圆领针织布短袖T恤', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5ac98b64N70acd82f.jpg!cc_350x449.jpg', 'HNTBJ2E080A', '0', '1', '0', '0', '0', '0', '98.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '海澜之家', 'T恤', null);
INSERT INTO `pms_product` VALUES ('32', '50', '8', '7', null, 'HLA海澜之家短袖T恤男基础款', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5a51eb88Na4797877.jpg', 'HNTBJ2E153A', '0', '1', '0', '0', '0', '0', '68.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '海澜之家', 'T恤', null);
INSERT INTO `pms_product` VALUES ('33', '6', '35', '7', null, '小米（MI）小米电视4A ', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5b02804dN66004d73.jpg', '4609652', '0', '1', '0', '0', '0', '0', '2499.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '彪马', '手机数码', null);
INSERT INTO `pms_product` VALUES ('34', '6', '35', '7', null, '小米（MI）小米电视4A 65英寸', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5b028530N51eee7d4.jpg', '4609660', '0', '1', '0', '0', '0', '0', '3999.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '彪马', '手机数码', null);
INSERT INTO `pms_product` VALUES ('35', '58', '29', '7', null, '耐克NIKE 男子 休闲鞋 ROSHE RUN 运动鞋 511881-010黑色41码', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5b235bb9Nf606460b.jpg', '6799342', '0', '1', '0', '0', '0', '0', '369.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '耐克', '运动长裤', null);
INSERT INTO `pms_product` VALUES ('36', '58', '29', '7', null, '耐克NIKE 男子 气垫 休闲鞋 AIR MAX 90 ESSENTIAL 运动鞋 AJ1285-101白色41码', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/5b19403eN9f0b3cb8.jpg', '6799345', '0', '1', '1', '1', '0', '0', '499.00', null, null, '100', '0', '', '0.00', '', '', null, '', null, null, null, '耐克', '运动长裤', null);
INSERT INTO `pms_product` VALUES ('37', '49', '7', '1', '1', '测试', '', '123', '0', '1', '1', '1', null, '0', '120.00', null, null, '10', '0', '123', '120.00', '2,1', '12', null, '', null, null, null, '七匹狼', '外套', null);
INSERT INTO `pms_product` VALUES ('38', '21', '31', '1', '4', '测试', '', 'prod_20220413232407', '0', '1', '1', '1', null, '0', '1000.00', '测试', '测试', '10', '0', '双', '700.00', '1,2,3', '测试', '测试', '', '测试', '测试', '', '安德玛', '篮球鞋', null);
INSERT INTO `pms_product` VALUES ('39', '1', '31', '1', '3', '12', '', 'prod_20220414003115', '0', '0', '0', '0', null, '0', '0.00', '', '', '0', '0', '1', '0.00', '', '', '', '', '', '', '', '安踏', '篮球鞋', null);
INSERT INTO `pms_product` VALUES ('40', '3', '11', '1', '3', '1', '', 'prod_20220414003514', '0', '0', '0', '0', null, '0', '0.00', '', '', '0', '0', '1', '0.00', '', '', '', '', '', '', '', '鸿星尔克', 'T恤', null);

-- ----------------------------
-- Table structure for pms_product_attribute
-- ----------------------------
DROP TABLE IF EXISTS `pms_product_attribute`;
CREATE TABLE `pms_product_attribute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_attribute_category_id` bigint(20) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `input_type` int(1) DEFAULT NULL COMMENT '属性录入方式：0->手工录入；1->从列表中选取',
  `input_list` varchar(255) DEFAULT NULL COMMENT '可选值列表，以逗号隔开',
  `sort` int(11) DEFAULT NULL COMMENT '排序字段：最高的可以单独上传图片',
  `hand_add_status` int(1) DEFAULT NULL COMMENT '是否支持手动新增；0->不支持；1->支持',
  `type` int(1) DEFAULT NULL COMMENT '属性的类型；0->规格；1->参数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='商品属性参数表';

-- ----------------------------
-- Records of pms_product_attribute
-- ----------------------------
INSERT INTO `pms_product_attribute` VALUES ('1', '1', '尺寸', '1', 'S/165,M/170,L/175,XL/180,2XL/185,3XL/190,4XL/195', '0', '0', '0');
INSERT INTO `pms_product_attribute` VALUES ('7', '1', '颜色', '0', '黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色,褐色,橙色,紫色', '100', '1', '0');
INSERT INTO `pms_product_attribute` VALUES ('13', '0', '上市年份', '1', '2013年,2014年,2015年,2016年,2017年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('14', '0', '上市年份1', '1', '2013年,2014年,2015年,2016年,2017年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('15', '0', '上市年份2', '1', '2013年,2014年,2015年,2016年,2017年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('16', '0', '上市年份3', '1', '2013年,2014年,2015年,2016年,2017年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('17', '0', '上市年份4', '1', '2013年,2014年,2015年,2016年,2017年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('18', '0', '上市年份5', '1', '2013年,2014年,2015年,2016年,2017年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('19', '0', '适用对象', '1', '青年女性,中年女性', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('20', '0', '适用对象1', '1', '青年女性,中年女性', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('21', '0', '适用对象3', '1', '青年女性,中年女性', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('24', '1', '商品编号', '0', '', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('25', '1', '适用季节', '1', '春季,夏季,秋季,冬季', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('32', '2', '适用人群', '1', '老年,青年,中年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('33', '2', '风格', '1', '嘻哈风格,基础大众,商务正装', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('35', '2', '颜色', '0', '黑色,红色,白色,蓝色,灰色', '100', '1', '0');
INSERT INTO `pms_product_attribute` VALUES ('37', '1', '适用人群', '1', '儿童,青年,中年,老年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('38', '1', '上市时间', '1', '2017年秋,2017年冬,2018年春,2018年夏', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('39', '1', '袖长', '1', '短袖,长袖,中袖', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('40', '2', '尺码', '1', 'S/165,M/170,L/175,XL/180,2XL/185,3XL/190,4XL/195', '0', '0', '0');
INSERT INTO `pms_product_attribute` VALUES ('41', '2', '适用场景', '1', '居家,运动,正装', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('42', '2', '上市时间', '1', '2018年春,2018年夏', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('43', '3', '颜色', '0', '黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色,橙色,紫色', '100', '1', '0');
INSERT INTO `pms_product_attribute` VALUES ('44', '3', '尺码', '1', '39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,45,46', '0', '0', '0');
INSERT INTO `pms_product_attribute` VALUES ('45', '3', '适用人群', '1', '青年,中年,老年', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('46', '3', '适用季节', '1', '春季,夏季,秋季,冬季', '0', '0', '1');
INSERT INTO `pms_product_attribute` VALUES ('52', '4', '颜色', '0', '黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色,橙色,紫色', '0', '1', '0');
INSERT INTO `pms_product_attribute` VALUES ('53', '4', '尺码', '1', '39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,45,46', '0', '0', '0');
INSERT INTO `pms_product_attribute` VALUES ('54', '5', '颜色', '0', '黑色,红色,白色,蓝色,灰色,绿色,橙色', '0', '1', '0');
INSERT INTO `pms_product_attribute` VALUES ('55', '6', '颜色', '0', '黑色,红色,白色,蓝色,黄色,灰色,绿色,橙色,紫色', '0', '1', '0');
INSERT INTO `pms_product_attribute` VALUES ('56', '6', '尺码', '1', '37,38,39,40,41,42,43,44,45,46', '0', '0', '0');
INSERT INTO `pms_product_attribute` VALUES ('57', '10', '颜色', '0', '黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色', '0', '1', '0');
INSERT INTO `pms_product_attribute` VALUES ('58', '10', '尺码', '1', 'S/165,M/170,L/175,XL/180,2XL/185,3XL/190,4XL/195', '0', '0', '0');

-- ----------------------------
-- Table structure for pms_product_attribute_category
-- ----------------------------
DROP TABLE IF EXISTS `pms_product_attribute_category`;
CREATE TABLE `pms_product_attribute_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `attribute_count` int(11) DEFAULT '0' COMMENT '属性数量',
  `param_count` int(11) DEFAULT '0' COMMENT '参数数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='产品属性分类表';

-- ----------------------------
-- Records of pms_product_attribute_category
-- ----------------------------
INSERT INTO `pms_product_attribute_category` VALUES ('1', '服装-T恤', '2', '5');
INSERT INTO `pms_product_attribute_category` VALUES ('2', '服装-裤装', '2', '4');
INSERT INTO `pms_product_attribute_category` VALUES ('3', '鞋包-跑步鞋', '2', '4');
INSERT INTO `pms_product_attribute_category` VALUES ('4', '鞋包-篮球鞋', '0', '0');
INSERT INTO `pms_product_attribute_category` VALUES ('5', '鞋包-背包', '0', '0');
INSERT INTO `pms_product_attribute_category` VALUES ('6', '鞋包-足球鞋', '0', '0');
INSERT INTO `pms_product_attribute_category` VALUES ('10', '服装-卫衣', '0', '0');

-- ----------------------------
-- Table structure for pms_product_attribute_value
-- ----------------------------
DROP TABLE IF EXISTS `pms_product_attribute_value`;
CREATE TABLE `pms_product_attribute_value` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `product_attribute_id` bigint(20) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL COMMENT '手动添加规格或参数的值，参数单值，规格有多个时以逗号隔开',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8 COMMENT='存储产品参数信息的表';

-- ----------------------------
-- Records of pms_product_attribute_value
-- ----------------------------
INSERT INTO `pms_product_attribute_value` VALUES ('4', '12', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('5', '13', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('6', '14', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('7', '18', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('11', '22', '7', 'x,xx');
INSERT INTO `pms_product_attribute_value` VALUES ('12', '22', '24', 'no110');
INSERT INTO `pms_product_attribute_value` VALUES ('13', '22', '25', '春季');
INSERT INTO `pms_product_attribute_value` VALUES ('14', '22', '37', '青年');
INSERT INTO `pms_product_attribute_value` VALUES ('15', '22', '38', '2018年春');
INSERT INTO `pms_product_attribute_value` VALUES ('16', '22', '39', '长袖');
INSERT INTO `pms_product_attribute_value` VALUES ('124', '23', '7', '米白色,浅黄色');
INSERT INTO `pms_product_attribute_value` VALUES ('125', '23', '24', 'no1098');
INSERT INTO `pms_product_attribute_value` VALUES ('126', '23', '25', '春季');
INSERT INTO `pms_product_attribute_value` VALUES ('127', '23', '37', '青年');
INSERT INTO `pms_product_attribute_value` VALUES ('128', '23', '38', '2018年春');
INSERT INTO `pms_product_attribute_value` VALUES ('129', '23', '39', '长袖');
INSERT INTO `pms_product_attribute_value` VALUES ('139', '2', '13', null);
INSERT INTO `pms_product_attribute_value` VALUES ('140', '2', '14', null);
INSERT INTO `pms_product_attribute_value` VALUES ('141', '2', '15', null);
INSERT INTO `pms_product_attribute_value` VALUES ('142', '2', '16', null);
INSERT INTO `pms_product_attribute_value` VALUES ('143', '2', '17', null);
INSERT INTO `pms_product_attribute_value` VALUES ('144', '2', '18', null);
INSERT INTO `pms_product_attribute_value` VALUES ('145', '2', '19', null);
INSERT INTO `pms_product_attribute_value` VALUES ('146', '2', '20', null);
INSERT INTO `pms_product_attribute_value` VALUES ('147', '2', '21', null);
INSERT INTO `pms_product_attribute_value` VALUES ('183', '31', '24', null);
INSERT INTO `pms_product_attribute_value` VALUES ('184', '31', '25', '夏季');
INSERT INTO `pms_product_attribute_value` VALUES ('185', '31', '37', '青年');
INSERT INTO `pms_product_attribute_value` VALUES ('186', '31', '38', '2018年夏');
INSERT INTO `pms_product_attribute_value` VALUES ('187', '31', '39', '短袖');
INSERT INTO `pms_product_attribute_value` VALUES ('198', '30', '24', null);
INSERT INTO `pms_product_attribute_value` VALUES ('199', '30', '25', '夏季');
INSERT INTO `pms_product_attribute_value` VALUES ('200', '30', '37', '青年');
INSERT INTO `pms_product_attribute_value` VALUES ('201', '30', '38', '2018年夏');
INSERT INTO `pms_product_attribute_value` VALUES ('202', '30', '39', '短袖');
INSERT INTO `pms_product_attribute_value` VALUES ('246', '23', '1', 'M,L');
INSERT INTO `pms_product_attribute_value` VALUES ('247', '1', '13', null);
INSERT INTO `pms_product_attribute_value` VALUES ('248', '1', '14', null);
INSERT INTO `pms_product_attribute_value` VALUES ('249', '1', '15', null);
INSERT INTO `pms_product_attribute_value` VALUES ('250', '1', '16', null);
INSERT INTO `pms_product_attribute_value` VALUES ('251', '1', '17', null);
INSERT INTO `pms_product_attribute_value` VALUES ('252', '1', '18', null);
INSERT INTO `pms_product_attribute_value` VALUES ('253', '1', '19', null);
INSERT INTO `pms_product_attribute_value` VALUES ('254', '1', '20', null);
INSERT INTO `pms_product_attribute_value` VALUES ('255', '1', '21', null);
INSERT INTO `pms_product_attribute_value` VALUES ('256', '7', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('257', '7', '1', 'XL');
INSERT INTO `pms_product_attribute_value` VALUES ('258', '7', '1', 'XXL');
INSERT INTO `pms_product_attribute_value` VALUES ('259', '9', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('260', '10', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('261', '11', '1', 'X');
INSERT INTO `pms_product_attribute_value` VALUES ('262', '8', '52', '棉花糖色');
INSERT INTO `pms_product_attribute_value` VALUES ('263', '8', '52', '白色');
INSERT INTO `pms_product_attribute_value` VALUES ('264', '8', '53', '40');
INSERT INTO `pms_product_attribute_value` VALUES ('265', '8', '53', '41');
INSERT INTO `pms_product_attribute_value` VALUES ('266', '8', '53', '42');
INSERT INTO `pms_product_attribute_value` VALUES ('267', '8', '53', '43');

-- ----------------------------
-- Table structure for pms_product_category
-- ----------------------------
DROP TABLE IF EXISTS `pms_product_category`;
CREATE TABLE `pms_product_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级分类的编号：0表示一级分类',
  `name` varchar(64) DEFAULT NULL COMMENT '名称',
  `level` int(1) DEFAULT NULL COMMENT '分类级别：0->1级；1->2级',
  `show_status` int(1) DEFAULT NULL COMMENT '显示状态：0->不显示；1->显示',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `icon` varchar(500) DEFAULT NULL COMMENT '图标',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键词',
  `description` text COMMENT '描述',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='产品分类';

-- ----------------------------
-- Records of pms_product_category
-- ----------------------------
INSERT INTO `pms_product_category` VALUES ('1', '0', '运动服装', '0', '1', '1', null, '服装', '运动服装', null);
INSERT INTO `pms_product_category` VALUES ('2', '0', '运动鞋包', '0', '1', '1', null, '鞋包', '运动鞋包', null);
INSERT INTO `pms_product_category` VALUES ('3', '0', '户外鞋服', '0', '1', '1', '', '户外鞋服', '户外鞋服', null);
INSERT INTO `pms_product_category` VALUES ('4', '0', '健身器械', '0', '1', '1', null, '健身器械', '健身器械', null);
INSERT INTO `pms_product_category` VALUES ('5', '0', '户外装备', '0', '1', '1', null, '户外装备', '户外装备', null);
INSERT INTO `pms_product_category` VALUES ('7', '0', '垂钓用品', '0', '0', '0', '', '垂钓用品', '垂钓用品', null);
INSERT INTO `pms_product_category` VALUES ('8', '0', '游泳用品', '0', '1', '0', '', '游泳用品', '游泳用品', null);
INSERT INTO `pms_product_category` VALUES ('9', '0', '体育用品', '0', '1', '0', '', '体育用品', '体育用品', null);
INSERT INTO `pms_product_category` VALUES ('10', '0', '骑行运动', '0', '1', '0', '', '骑行运动', '骑行运动', null);
INSERT INTO `pms_product_category` VALUES ('11', '1', 'T恤', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FmGcxh_01PwM0OJGCtJFr9A.png_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649696637&t=5db6f0775b61e927addb34a4425369ee', 'T恤', 'T恤', null);
INSERT INTO `pms_product_category` VALUES ('13', '8', '男士泳衣', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2Fa13034e7d443b73731cd92fb64a759943bdc45616c2e6-i44tVY_fw658&refer=http%3A%2F%2Fhbimg.b0.upaiyun.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737298&t=550cfc0e162039f37a329a1ff99b2919', 'string', 'string', null);
INSERT INTO `pms_product_category` VALUES ('14', '8', '女士泳衣', '1', '1', '0', 'http://t15.baidu.com/it/u=1813785804,1756667009&fm=224&app=112&f=JPEG?w=500&h=500', 'string', 'string', null);
INSERT INTO `pms_product_category` VALUES ('29', '1', '运动长裤', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FAoR4Tfmtlqtc8sQ7dXYwyQ.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649697031&t=661b42ed6c87517170cdd2dd058e1ed1', '', '', null);
INSERT INTO `pms_product_category` VALUES ('30', '2', '跑步鞋', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2Fd2ggkzhjG6wUUigssTYAiQ.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649697191&t=1d59f78b4fe6988a9fd17de901f44f22', '', '', null);
INSERT INTO `pms_product_category` VALUES ('31', '2', '篮球鞋', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2Ff_q-ZhoPXn_6cF8VJ9D2HA.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649697293&t=dd71c7fe380bcdf4be9852db785b514c', '', '', null);
INSERT INTO `pms_product_category` VALUES ('32', '2', '运动背包', '1', '1', '0', 'http://t14.baidu.com/it/u=3926790543,3686523262&fm=224&app=112&f=JPEG?w=500&h=500', '', '', null);
INSERT INTO `pms_product_category` VALUES ('33', '2', '足球鞋', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FtBVprtY-e1byEAhyr1XVfA.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649697321&t=4b6de2d9d3fa5eb37765cd53c1f99661', '', '', null);
INSERT INTO `pms_product_category` VALUES ('34', '8', '泳镜', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FAedwcOKrnfEEjCKKkaggAA.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737464&t=d589e5b4205fcc21dd160c3e137e6f83', '', '', null);
INSERT INTO `pms_product_category` VALUES ('35', '3', '登山鞋', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FUqxPOlBXS13KB1nHsnmq-g.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737034&t=9cdde10271992928ee117d375c0ff770', '', '', null);
INSERT INTO `pms_product_category` VALUES ('36', '3', '徒步鞋', '1', '1', '0', 'https://img.pddpic.com/open-gw/2022-02-22/452ce5e0-01f1-431f-b1f5-fe5ef053c1b6.jpeg', '', '', null);
INSERT INTO `pms_product_category` VALUES ('37', '3', '速干衣', '1', '1', '0', 'http://t13.baidu.com/it/u=3580269808,979862406&fm=224&app=112&f=JPEG?w=500&h=500', '', '', null);
INSERT INTO `pms_product_category` VALUES ('38', '3', '冲锋衣', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FS9s2YsgfMlE4zB_0ARv2xQ.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737203&t=da483d36d900f1dfacf03f015c43fb5f', '', '', null);
INSERT INTO `pms_product_category` VALUES ('39', '8', '泳帽', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.alicdn.com%2Fbao%2Fuploaded%2FTB1DGM7TY2pK1RjSZFsXXaNlXXa.png&refer=http%3A%2F%2Fimg.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737514&t=94ec29a2e2a115b2cb3816835bbce603', '', '', null);
INSERT INTO `pms_product_category` VALUES ('40', '9', '篮球', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2F4aXu0CEDE4iHO0PE1vkPGw.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737537&t=04a93becc327115fa3f4dcacf55d1dd8', '', '', null);
INSERT INTO `pms_product_category` VALUES ('41', '9', '羽毛球', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimage1.suning.cn%2Fuimg%2Fb2c%2Fnewcatentries%2F0070072184-000000000634998224_2_800x800.jpg&refer=http%3A%2F%2Fimage1.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737586&t=93aa9d53fbffe461fcfd5017eea9cb25', '', '', null);
INSERT INTO `pms_product_category` VALUES ('42', '9', '乒乓球', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fec4.images-amazon.com%2Fimages%2FI%2F61BKjHYfRNL._SL1000_.jpg&refer=http%3A%2F%2Fec4.images-amazon.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737697&t=9c8157083f65b38d7b82c8f8b07a5fde', '', '', null);
INSERT INTO `pms_product_category` VALUES ('43', '4', '跑步机', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2F3jyI_7jYQIkp9V-EGhI2PA.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737727&t=97c5cfc555a95f0393fa336554bbe630', '', '', null);
INSERT INTO `pms_product_category` VALUES ('44', '4', '哑铃', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.alicdn.com%2Fbao%2Fuploaded%2Fi2%2F124675140%2FO1CN01paEhqD1nqAL3lRt1y_%21%21124675140.jpg&refer=http%3A%2F%2Fimg.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737764&t=0fc341cbb3376d375f5b12dcfccfd610', '', '', null);
INSERT INTO `pms_product_category` VALUES ('45', '4', '健身车', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FAVYyPC6etwtcXsv7XcG-ow.png_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737796&t=611ddb925301d5495de99e70727d5f85', '', '', null);
INSERT INTO `pms_product_category` VALUES ('46', '4', '仰卧板', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fm.360buyimg.com%2Fn12%2Fg9%2FM01%2F0E%2F15%2FrBEHaVCp-Q0IAAAAAALZrKuuUgYAAC3UgNMwZ0AAtnE009.jpg%21q70.jpg&refer=http%3A%2F%2Fm.360buyimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737841&t=74f50f9131b6ffc566e8aedabaf0481b', '', '', null);
INSERT INTO `pms_product_category` VALUES ('47', '4', '踏步机', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimage1.suning.cn%2Fuimg%2Fb2c%2Fnewcatentries%2F0070169056-000000000663666737_2_800x800.jpg&refer=http%3A%2F%2Fimage1.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737862&t=3c103414a27f0c8807eb8b3ea00e280d', '', '', null);
INSERT INTO `pms_product_category` VALUES ('48', '5', '帐篷', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FnQZP6-0hgM-kx4BIq5N3MA.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737901&t=93939a74fd74763d473b5995b1d65f5c', '', '', null);
INSERT INTO `pms_product_category` VALUES ('49', '5', '登山攀岩', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.alicdn.com%2Fbao%2Fuploaded%2Fi1%2F572367541%2FO1CN01bFvkAM25ZpQFDd1FV_%21%21572367541.jpg&refer=http%3A%2F%2Fimg.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737941&t=abb68b2e70ce925957fb0025de7c50d5', '', '', null);
INSERT INTO `pms_product_category` VALUES ('50', '5', '潜水冲浪', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180804%2F98011c69c1df4f18bb6280331dca26e6.jpeg&refer=http%3A%2F%2F5b0988e595225.cdn.sohucs.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649738065&t=107e303b3af68c5c42861d726cb2f14f', '', '', null);
INSERT INTO `pms_product_category` VALUES ('51', '5', '滑雪装备', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.alicdn.com%2Fbao%2Fuploaded%2Fi1%2F2208109742634%2FO1CN01BCpYR81VKPqo5czC8_%21%210-item_pic.jpg&refer=http%3A%2F%2Fimg.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649737994&t=c22862fba0a9b7e16ec1302c994acef6', '', '', null);
INSERT INTO `pms_product_category` VALUES ('58', '9', '足球', '1', '1', '0', 'https://img2.baidu.com/it/u=1214675793,1661781797&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500', '123', '123', null);
INSERT INTO `pms_product_category` VALUES ('59', '1', '运动短裤', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FhxdFbFbgNqfIBEL6sspqhg.png_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649697097&t=58039e885f357ed50033841df858482e', null, null, null);
INSERT INTO `pms_product_category` VALUES ('60', '1', '卫衣', '1', '1', '0', 'http://t13.baidu.com/it/u=2038835223,3976080724&fm=224&app=112&f=JPEG?w=500&h=500', null, null, null);
INSERT INTO `pms_product_category` VALUES ('61', '10', '山地车', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgservice.suning.cn%2Fuimg1%2Fb2c%2Fimage%2FVnAdqhW52X-Pg-qcp_1H-A.jpg_800w_800h_4e&refer=http%3A%2F%2Fimgservice.suning.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649738177&t=fbddb9e5aa13f412e3e7185fb283adfe', null, null, null);
INSERT INTO `pms_product_category` VALUES ('62', '10', '公路车', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.alicdn.com%2Fbao%2Fuploaded%2Fi1%2F2145088292%2FO1CN01uDI9zW2B7mmYjrbkA_%21%212145088292-2-lubanu-s.png&refer=http%3A%2F%2Fimg.alicdn.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649738202&t=3db524bdeaf512916f47d8f85831115e', null, null, null);
INSERT INTO `pms_product_category` VALUES ('63', '10', '骑行装备', '1', '1', '0', 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fm.360buyimg.com%2Fn12%2Fjfs%2Ft193%2F230%2F2463980232%2F174196%2F652966fc%2F53d09316N77663c9f.jpg%21q70.jpg&refer=http%3A%2F%2Fm.360buyimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1649738221&t=dd61c3af7abc1b9dc6c8b1a0ba04b883', null, null, null);

-- ----------------------------
-- Table structure for pms_product_category_attribute_relation
-- ----------------------------
DROP TABLE IF EXISTS `pms_product_category_attribute_relation`;
CREATE TABLE `pms_product_category_attribute_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_category_id` bigint(20) DEFAULT NULL,
  `product_attribute_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='产品的分类和属性的关系表，用于设置分类筛选条件（只支持一级分类）';

-- ----------------------------
-- Records of pms_product_category_attribute_relation
-- ----------------------------
INSERT INTO `pms_product_category_attribute_relation` VALUES ('1', '24', '24');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('5', '26', '24');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('7', '28', '24');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('9', '25', '24');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('10', '25', '25');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('12', '58', '24');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('13', '58', '25');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('14', '11', '1');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('15', '11', '7');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('16', '29', '35');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('17', '29', '40');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('18', '30', '43');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('19', '30', '44');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('20', '31', '52');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('21', '31', '53');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('22', '32', '54');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('23', '59', '35');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('24', '59', '40');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('25', '33', '55');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('26', '33', '56');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('27', '60', '57');
INSERT INTO `pms_product_category_attribute_relation` VALUES ('28', '60', '58');

-- ----------------------------
-- Table structure for pms_sku_stock
-- ----------------------------
DROP TABLE IF EXISTS `pms_sku_stock`;
CREATE TABLE `pms_sku_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `sku_code` varchar(64) DEFAULT NULL COMMENT 'sku编码',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `stock` int(11) DEFAULT '0' COMMENT '库存',
  `low_stock` int(11) DEFAULT '5' COMMENT '预警库存',
  `sp` varchar(64) DEFAULT NULL COMMENT '销售属性1',
  `pic` varchar(255) DEFAULT NULL COMMENT '展示图片',
  `sale` int(11) DEFAULT '0' COMMENT '销量',
  `lock_stock` int(11) DEFAULT '0' COMMENT '锁定库存',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COMMENT='sku的库存';

-- ----------------------------
-- Records of pms_sku_stock
-- ----------------------------
INSERT INTO `pms_sku_stock` VALUES ('6', '12', 'string', '100.00', '0', '5', 'string,string', 'string', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('7', '13', 'string', '100.00', '0', '5', 'string,string', 'string', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('8', '14', 'string', '100.00', '0', '5', 'string,string', 'string', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('9', '18', 'string', '100.00', '0', '5', 'string,string', 'string', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('12', '22', '111', '99.00', '0', null, 'x,M', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('13', '22', '112', '99.00', '0', null, 'xx,M', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/2018032614134591_20180326141345650 (4).png', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('78', '23', '201806070023001', '99.00', '99', null, '颜色:米白色,尺寸:M', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('79', '23', '201806070023002', '100.00', '0', null, '颜色:米白色,尺寸:L', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/1522738681.jpg', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('80', '23', '201806070023003', '98.00', '0', null, '颜色:浅黄色,尺寸:M', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/2017091716493787_20170917164937650 (1).png', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('81', '23', '201806070023004', '101.00', '-1', null, '颜色:浅黄色,尺寸:L', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180604/2017091716493787_20170917164937650 (1).png', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('110', '1', 'sku_20220313135617a1', '238.00', '19', '5', '颜色:黑色,尺码:M/170', 'https://img14.360buyimg.com/n0/jfs/t1/94347/38/20908/107799/62020d07E81e89d20/d6a11cfbd467f9b0.jpg', '1', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('111', '1', 'sku_20220313140358b8', '238.00', '20', '5', '颜色:黑色,尺码:L/175', 'https://img14.360buyimg.com/n0/jfs/t1/94347/38/20908/107799/62020d07E81e89d20/d6a11cfbd467f9b0.jpg', '0', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('112', '7', 'string', '100.00', '0', '5', 'string,string', 'string', '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('113', '7', 'string', '0.00', '0', '0', 'string,string', 'string', '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('114', '7', 'string', '0.00', '0', '0', 'string,string', 'string', '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('116', '9', 'string', '100.00', '0', '5', 'string,string', 'string', '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('117', '10', 'string', '100.00', '0', '5', 'string,string', 'string', '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('118', '11', 'string', '100.00', '0', '5', 'string,string', 'string', '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('119', '8', 'sku_20220327193301h1', '899.00', '97', '5', '颜色:棉花糖色,尺码:40', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '10', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('120', '8', 'sku_20220327193302o1', '899.00', '96', '5', '颜色:棉花糖色,尺码:41', 'https://img10.360buyimg.com/n1/jfs/t1/132139/10/25516/222816/623d9f77E799397d1/55b46b87bde58d1c.jpg', '14', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('121', '8', 'sku_20220327193302w4', '889.00', '100', '5', '颜色:白色,尺码:40', null, '10', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('122', '7', 'sku_2022032719410w2', '399.00', '97', '5', '颜色:白色,尺码:43', 'https://img14.360buyimg.com/n1/jfs/t1/122107/10/23938/414767/623c1477E0f92b9a4/0f23c80e354c8804.jpg', '1', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('123', '38', '123', '40.00', '10', '5', null, null, '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('124', '38', '1234', '40.00', '20', '5', null, null, '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('125', '39', null, null, null, null, '颜色:黑色尺码:40,', null, '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('126', '39', null, null, null, null, '颜色:黑色尺码:41,', null, '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('127', '40', '1', '1.00', '1', '1', '颜色:白色,尺码:39', null, '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('128', '40', '1', '1.00', '1', '1', '颜色:白色,尺码:42', null, '0', null, null);
INSERT INTO `pms_sku_stock` VALUES ('129', '5', 'sku_20220502163801u2', '139.00', '99', '5', '颜色:深蓝色', 'https://img14.360buyimg.com/n0/jfs/t1/209309/38/14162/113753/61cad944Ea327e3a3/de77c7d5e4f48d1e.jpg', '1', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('130', '2', 'sku_20220502163809u3', '180.00', '89', '5', '颜色:黑色,尺码:M/170', 'https://img14.360buyimg.com/n0/jfs/t1/128372/36/22619/65265/621ed23dEadc6a3a7/201433f8df8c6bdc.jpg', '1', '0', null);
INSERT INTO `pms_sku_stock` VALUES ('131', '4', 'sku_20220502164308i8', '159.00', '-1', '5', '颜色:蓝白色,尺码:43', 'https://img14.360buyimg.com/n0/jfs/t1/138317/1/25616/160124/61d08456E3c791953/3ca116a62a61604c.jpg', '1', '0', null);

-- ----------------------------
-- Table structure for sms_home_advertise
-- ----------------------------
DROP TABLE IF EXISTS `sms_home_advertise`;
CREATE TABLE `sms_home_advertise` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `pic` varchar(500) DEFAULT NULL COMMENT '图片',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int(1) DEFAULT NULL COMMENT '上下线状态：0->下线；1->上线',
  `note` varchar(500) DEFAULT NULL COMMENT '备注',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `product_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='首页轮播广告表';

-- ----------------------------
-- Records of sms_home_advertise
-- ----------------------------
INSERT INTO `sms_home_advertise` VALUES ('2', '夏季大热促销', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/xiaomi.jpg', '2018-11-01 14:01:37', '2018-11-15 14:01:37', '0', '夏季大热促销', '0', '23', '2022-04-05 16:44:50', null);
INSERT INTO `sms_home_advertise` VALUES ('3', '夏季大热促销1', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/5ac1bf58Ndefaac16.jpg', '2018-11-13 14:01:37', '2018-11-13 14:01:37', '0', '夏季大热促销1', '0', '23', '2022-04-05 16:44:50', null);
INSERT INTO `sms_home_advertise` VALUES ('4', '夏季大热促销2', 'https://img12.360buyimg.com/n1/jfs/t1/100394/1/21554/111040/61d8fdc6Ef790eb14/3aba6201993dcc7c.jpg', '2018-11-13 14:01:37', '2018-11-13 14:01:37', '1', '夏季大热促销2', '0', '23', '2022-04-05 16:44:50', null);
INSERT INTO `sms_home_advertise` VALUES ('10', '鸿星尔克跑步鞋', 'https://img11.360buyimg.com/n1/jfs/t1/144224/20/25597/268425/623d0ddbEa8701010/3f83856590376c43.jpg', '2022-01-01 00:00:00', '2022-07-01 00:00:00', '1', null, '99', '4', '2022-04-05 16:44:50', null);
INSERT INTO `sms_home_advertise` VALUES ('11', '耐克运动短裤', '	https://img11.360buyimg.com/n1/jfs/t1/133423/11/27240/171480/623d42abEcbcd33c8/fe9a22b6361a4ed3.jpg', '2022-01-01 00:00:00', '2022-06-12 00:00:00', '1', null, '98', '2', '2022-04-05 16:44:50', null);
INSERT INTO `sms_home_advertise` VALUES ('12', '阿迪达斯运动背包', 'https://img10.360buyimg.com/n1/jfs/t1/92193/18/25048/419991/624114c1E55ea4256/3aa9475b0bb99b48.jpg', '2022-01-24 16:48:10', '2022-11-26 00:00:00', '1', '', '97', '5', '2022-04-05 16:44:50', null);

-- ----------------------------
-- Table structure for sms_home_new_product
-- ----------------------------
DROP TABLE IF EXISTS `sms_home_new_product`;
CREATE TABLE `sms_home_new_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `recommend_status` int(1) DEFAULT NULL COMMENT '推荐状态：0->否；1->是',
  `sort` int(1) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='新鲜好物表';

-- ----------------------------
-- Records of sms_home_new_product
-- ----------------------------
INSERT INTO `sms_home_new_product` VALUES ('12', '30', '0', '0');
INSERT INTO `sms_home_new_product` VALUES ('13', '1', '1', '100');
INSERT INTO `sms_home_new_product` VALUES ('14', '4', '1', '90');

-- ----------------------------
-- Table structure for sms_home_recommend_product
-- ----------------------------
DROP TABLE IF EXISTS `sms_home_recommend_product`;
CREATE TABLE `sms_home_recommend_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `recommend_status` int(1) DEFAULT NULL COMMENT '推荐状态：0->否；1->是',
  `sort` int(1) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='人气推荐商品表';

-- ----------------------------
-- Records of sms_home_recommend_product
-- ----------------------------
INSERT INTO `sms_home_recommend_product` VALUES ('7', '30', '0', '100');
INSERT INTO `sms_home_recommend_product` VALUES ('8', '23', '0', '0');
INSERT INTO `sms_home_recommend_product` VALUES ('9', '2', '1', '101');

-- ----------------------------
-- Table structure for sys_api_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_api_log`;
CREATE TABLE `sys_api_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `req_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '请求接口',
  `req_params` text COLLATE utf8_unicode_ci COMMENT '请求参数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `ip` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '客户端IP',
  `user_agent` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '浏览器类型',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `token` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'token',
  `userType` int(1) DEFAULT NULL COMMENT '用户类型（0->前台；1->后台）',
  `serviceSign` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '服务类型标识',
  PRIMARY KEY (`id`),
  KEY `admin_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='后台用户操作日志表';

-- ----------------------------
-- Records of sys_api_log
-- ----------------------------
INSERT INTO `sys_api_log` VALUES ('8', '3', '/mall/opLog/deleteAll', 'null', '2022-01-29 19:05:53', '0:0:0:0:0:0:0:1', 'chrome9', '清空操作日志', null, null, null);
INSERT INTO `sys_api_log` VALUES ('9', '3', '/mall/help/add', '{\"categoryId\":2,\"content\":\"12\",\"icon\":\"12\",\"status\":1,\"title\":\"12\"}', '2022-01-29 20:35:37', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('10', '3', '/mall/errLog/deleteAll', 'null', '2022-01-29 21:02:40', '0:0:0:0:0:0:0:1', 'chrome9', '清空异常日志', null, null, null);
INSERT INTO `sys_api_log` VALUES ('11', '3', '/mall/admin/role/update', '[{\"adminId\":5},{\"roleIds\":[2]}]', '2022-02-12 23:49:10', '0:0:0:0:0:0:0:1', 'chrome9', '修改管理员角色', null, null, null);
INSERT INTO `sys_api_log` VALUES ('12', '3', '/mall/admin/role/update', '[{\"adminId\":5},{\"roleIds\":[2,3]}]', '2022-02-12 23:51:55', '0:0:0:0:0:0:0:1', 'chrome9', '修改管理员角色', null, null, null);
INSERT INTO `sys_api_log` VALUES ('13', '3', '/mall/admin/role/update', '[{\"adminId\":5},{\"roleIds\":[2]}]', '2022-02-12 23:55:17', '0:0:0:0:0:0:0:1', 'chrome9', '修改管理员角色', null, null, null);
INSERT INTO `sys_api_log` VALUES ('14', '3', '/mall/home/recommendProduct/add', '[{\"brandName\":\"海澜之家\",\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"price\":100,\"productId\":1,\"productName\":\"银色星芒刺绣网纱底裤\",\"recommendStatus\":1,\"sort\":0}]', '2022-02-14 00:07:26', '0:0:0:0:0:0:0:1', 'chrome9', '添加人气推荐', null, null, null);
INSERT INTO `sys_api_log` VALUES ('15', '3', '/mall/admin/update/5', '{\"createTime\":1641094694000,\"email\":\"wangjin@126.com\",\"id\":5,\"nickname\":\"王进\",\"note\":\"备注\",\"password\":\"e10adc3949ba59abbe56e057f20f883e\",\"phone\":\"13002061234\",\"status\":0,\"username\":\"王进\"}', '2022-02-19 18:39:55', '0:0:0:0:0:0:0:1', 'chrome9', '修改管理员', null, null, null);
INSERT INTO `sys_api_log` VALUES ('16', '3', '/mall/admin/role/update', '[{\"adminId\":1},{\"roleIds\":[2]}]', '2022-02-19 18:46:05', '0:0:0:0:0:0:0:1', 'chrome9', '修改管理员角色', null, null, null);
INSERT INTO `sys_api_log` VALUES ('17', '3', '/mall/helpCate/update/4', '{\"helpCount\":2,\"id\":4,\"name\":\"订单\",\"sort\":12,\"status\":1}', '2022-02-19 21:25:08', '0:0:0:0:0:0:0:1', 'chrome9', '修改帮助分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('18', '3', '/mall/help/update/3', '{\"categoryId\":4,\"content\":\"订单取消失败，可能是该订单不支持取消或订单的商品已发货，如果订单已发货我们会尽量拦截该订单为您申请退款，若未能拦截成功，您可根据需求选择签收或拒收商品。\",\"createTime\":1643446389000,\"id\":3,\"status\":1,\"title\":\"取消订单失败怎么办\"}', '2022-02-19 21:29:06', '0:0:0:0:0:0:0:1', 'chrome9', '修改帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('19', '3', '/mall/help/update/4', '{\"categoryId\":4,\"content\":\"登录微信客户端，点击【我的】-【全部订单】，查找需要取消的订单，点击【取消订单】按钮即可。\",\"createTime\":1643459737000,\"id\":4,\"status\":1,\"title\":\"订单如何取消\"}', '2022-02-19 21:33:07', '0:0:0:0:0:0:0:1', 'chrome9', '修改帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('20', '3', '/mall/help/add', '{\"categoryId\":4,\"content\":\"订单提交成功后将无法修改商品的数量和颜色。如需修改，请重新下单\",\"createTime\":1645277888274,\"status\":1,\"title\":\"下单之后如何修改商品的数量和颜色\"}', '2022-02-19 21:38:08', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('21', '3', '/mall/helpCate/update/2', '{\"helpCount\":3,\"id\":2,\"name\":\"售后\",\"sort\":0,\"status\":1}', '2022-02-19 21:39:16', '0:0:0:0:0:0:0:1', 'chrome9', '修改帮助分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('22', '3', '/mall/help/update/5', '{\"categoryId\":2,\"content\":\"若退货服务单未审核，您可以通过前台自行取消服务单；若服务单已审核，建议您联系在线客服为您处理。\",\"createTime\":1645278045604,\"id\":5,\"status\":1,\"title\":\"申请退货后，能不能换成换货\"}', '2022-02-19 21:40:46', '0:0:0:0:0:0:0:1', 'chrome9', '修改帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('23', '3', '/mall/help/update/6', '{\"categoryId\":2,\"content\":\"如果商品详情页面有说明支持7天无理由退货，在该周期内支持无理由退货；如果有明确标注不支持无理由退货，则不可申请无理由退货。\",\"createTime\":1645278197954,\"id\":6,\"status\":1,\"title\":\"商品购买后可以申请无理由退货吗\"}', '2022-02-19 21:43:18', '0:0:0:0:0:0:0:1', 'chrome9', '修改帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('24', '3', '/mall/help/update/7', '{\"categoryId\":2,\"content\":\"签收商品之日起7天内，商品保持原有品质、功能，商品本身、配件、商标标识齐全，即可申请7天无理由退货。\",\"createTime\":1645278335081,\"id\":7,\"status\":1,\"title\":\"7天无理由退货标准\"}', '2022-02-19 21:45:35', '0:0:0:0:0:0:0:1', 'chrome9', '修改帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('25', '3', '/mall/helpCate/add', '{\"helpCount\":0,\"name\":\"配送\",\"sort\":10,\"status\":1}', '2022-02-19 23:54:04', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('26', '3', '/mall/help/add', '{\"categoryId\":5,\"content\":\"点击【我的】-【订单列表】-【待收货】，点击对应订单即可查看物流信息。\",\"createTime\":1645286157336,\"status\":1,\"title\":\"包裹现在到哪里了\"}', '2022-02-19 23:55:57', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('27', '3', '/mall/help/add', '{\"categoryId\":5,\"content\":\"下单时，配送方式选择“上门自提”，自提订单提交成功后，订单到达指定自提点或自提柜，系统会向收货人手机发送自提码，到自提点或自提柜通过自提码提货即可。\",\"createTime\":1645286308999,\"status\":1,\"title\":\"如何自提\"}', '2022-02-19 23:58:29', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('28', '3', '/mall/helpCate/add', '{\"helpCount\":0,\"name\":\"财务\",\"sort\":5,\"status\":1}', '2022-02-20 00:00:01', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('29', '3', '/mall/help/add', '{\"categoryId\":6,\"content\":\"下单时根据需求在提交订单页面选择发票类型并填写对应的发票信息，我们将按照您填写的发票信息为您开具。\",\"createTime\":1645286501548,\"status\":1,\"title\":\"怎么开发票\"}', '2022-02-20 00:01:42', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('30', '3', '/mall/help/add', '{\"categoryId\":6,\"content\":\"为了响应国家无纸化号召，不再为顾客开具普通纸质发票，全面启用电子普通发票，电子发票与普通发票具备同等法律效力。\",\"createTime\":1645286625770,\"status\":1,\"title\":\"可以开具纸质发票吗\"}', '2022-02-20 00:03:46', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('31', '3', '/mall/help/add', '{\"categoryId\":6,\"content\":\"点击【我的】-【订单列表】-【查看发票】-【发票详情】即可查看或下载电子普通发票。\",\"createTime\":1645286747236,\"status\":1,\"title\":\"如何查看发票信息\"}', '2022-02-20 00:05:47', '0:0:0:0:0:0:0:1', 'chrome9', '添加帮助', null, null, null);
INSERT INTO `sys_api_log` VALUES ('32', '3', '/mall/returnReason/update/2', '{\"createTime\":1645289646877,\"id\":2,\"name\":\"尺寸码数不合适\",\"sort\":1,\"status\":1}', '2022-02-20 00:54:07', '0:0:0:0:0:0:0:1', 'chrome9', '修改退货原因', null, null, null);
INSERT INTO `sys_api_log` VALUES ('33', '3', '/mall/returnReason/update/4', '{\"createTime\":1645289670894,\"id\":4,\"name\":\"发货出错\",\"sort\":1,\"status\":1}', '2022-02-20 00:54:31', '0:0:0:0:0:0:0:1', 'chrome9', '修改退货原因', null, null, null);
INSERT INTO `sys_api_log` VALUES ('34', '3', '/mall/returnReason/update/5', '{\"createTime\":1645289707152,\"id\":5,\"name\":\"7天无理由退货\",\"sort\":1,\"status\":1}', '2022-02-20 00:55:07', '0:0:0:0:0:0:0:1', 'chrome9', '修改退货原因', null, null, null);
INSERT INTO `sys_api_log` VALUES ('35', '3', '/mall/returnReason/update/16', '{\"createTime\":1645289761169,\"id\":16,\"name\":\"不喜欢\",\"sort\":0,\"status\":1}', '2022-02-20 00:56:01', '0:0:0:0:0:0:0:1', 'chrome9', '修改退货原因', null, null, null);
INSERT INTO `sys_api_log` VALUES ('36', '3', '/mall/returnReason/update/15', '{\"createTime\":1645289789160,\"id\":15,\"name\":\"下单出错\",\"sort\":0,\"status\":1}', '2022-02-20 00:56:29', '0:0:0:0:0:0:0:1', 'chrome9', '修改退货原因', null, null, null);
INSERT INTO `sys_api_log` VALUES ('37', '3', '/mall/brand/update/1', '{\"firstLetter\":\"W\",\"id\":1,\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg(5).jpg\",\"name\":\"万\",\"showStatus\":1}', '2022-02-20 01:16:28', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('38', '3', '/mall/brand/update/1', '{\"firstLetter\":\"A\",\"id\":1,\"keywords\":\"跑步鞋\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg(5).jpg\",\"name\":\"安踏\",\"showStatus\":1}', '2022-02-20 01:57:41', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('39', '3', '/mall/brand/update/1', '{\"firstLetter\":\"A\",\"id\":1,\"keywords\":\"跑步鞋,篮球鞋,T恤,卫衣,运动长裤,运动短裤\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg(5).jpg\",\"name\":\"安踏\",\"showStatus\":1}', '2022-02-20 12:00:37', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('40', '3', '/mall/brand/update/2', '{\"firstLetter\":\"P\",\"id\":2,\"keywords\":\"跑步鞋,篮球鞋,休闲鞋,板鞋,T恤,卫衣,运动长裤,运动短裤,夹克,篮球服\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg (1).jpg\",\"name\":\"匹克\",\"showStatus\":1}', '2022-02-20 12:04:40', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('41', '3', '/mall/brand/update/59', '{\"firstLetter\":\"L\",\"id\":59,\"keywords\":\"跑步鞋,篮球鞋,休闲鞋,板鞋,训练鞋,T恤,卫衣,卫裤,运动长裤,运动短裤,夹克,运动服,袜子,护具,篮球,足球\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/timg (51).jpg\",\"name\":\"李宁\",\"showStatus\":1}', '2022-02-20 12:08:43', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('42', '3', '/mall/brand/update/3', '{\"firstLetter\":\"H\",\"id\":3,\"keywords\":\"跑步鞋,休闲鞋,板鞋,T恤,卫衣,夹克,运动长裤,运动短裤,袜子\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/17f2dd9756d9d333bee8e60ce8c03e4c_222_222.jpg\",\"name\":\"鸿星尔克\",\"showStatus\":1}', '2022-02-20 12:11:18', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('43', '3', '/mall/brand/update/4', '{\"firstLetter\":\"A\",\"id\":4,\"keywords\":\"跑步鞋,篮球鞋,足球鞋,训练鞋,休闲鞋,T恤,卫衣,运动长裤,运动短裤,夹克,运动背包,运动袜\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/dc794e7e74121272bbe3ce9bc41ec8c3_222_222.jpg\",\"name\":\"阿迪达斯\",\"showStatus\":1}', '2022-02-20 12:14:30', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('44', '3', '/mall/brand/update/6', '{\"firstLetter\":\"P\",\"id\":6,\"keywords\":\"跑步鞋,篮球鞋,足球鞋,训练鞋,休闲鞋,T恤,卫衣,运动长裤,运动短裤,夹克,泳装,运动背包,手提运动包,运动袜\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/1e34aef2a409119018a4c6258e39ecfb_222_222.png\",\"name\":\"彪马\",\"showStatus\":1}', '2022-02-20 12:18:19', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('45', '3', '/mall/brand/update/21', '{\"firstLetter\":\"U\",\"id\":21,\"keywords\":\"跑步鞋,篮球鞋,训练鞋,休闲鞋,T恤,卫衣,运动裤,运动背心,夹克,运动背包,运动袜\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg(6).jpg\",\"name\":\"安德玛\",\"showStatus\":1}', '2022-02-20 12:20:22', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('46', '3', '/mall/brand/update/58', '{\"firstLetter\":\"N\",\"id\":58,\"keywords\":\"跑步鞋,篮球鞋,足球鞋训练鞋,休闲鞋,T恤,卫衣,运动裤,运动背心,夹克,运动背包,运动袜\",\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180615/timg (51).jpg\",\"name\":\"耐克\",\"showStatus\":1}', '2022-02-20 12:22:22', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌', null, null, null);
INSERT INTO `sys_api_log` VALUES ('47', '3', '/mall/productAttribute/update/7', '{\"filterType\":0,\"handAddStatus\":1,\"id\":7,\"inputList\":\"黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色,褐色,橙色,紫色\",\"inputType\":1,\"name\":\"颜色\",\"productAttributeCategoryId\":1,\"relatedStatus\":0,\"searchType\":0,\"selectType\":2,\"sort\":100,\"type\":0}', '2022-02-20 15:22:25', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('48', '3', '/mall/productAttribute/update/35', '{\"filterType\":0,\"handAddStatus\":1,\"id\":35,\"inputList\":\"黑色,红色,白色,蓝色,灰色\",\"inputType\":1,\"name\":\"颜色\",\"productAttributeCategoryId\":2,\"relatedStatus\":0,\"searchType\":0,\"selectType\":1,\"sort\":100,\"type\":0}', '2022-02-20 15:28:21', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('49', '3', '/mall/productAttribute/update/40', '{\"filterType\":0,\"handAddStatus\":0,\"id\":40,\"inputList\":\"S/165,M/170,L/175,XL/180,2XL/185,3XL/190,4XL/195\",\"inputType\":1,\"name\":\"尺码\",\"productAttributeCategoryId\":2,\"relatedStatus\":0,\"searchType\":0,\"selectType\":0,\"sort\":0,\"type\":0}', '2022-02-20 15:29:10', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('50', '3', '/mall/productAttribute/category/update/3', '{\"name\":\"鞋包-跑步鞋\"}', '2022-02-20 15:30:42', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('51', '3', '/mall/productAttribute/update/43', '{\"filterType\":0,\"handAddStatus\":1,\"id\":43,\"inputList\":\"黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色,橙色,紫色\",\"inputType\":1,\"name\":\"颜色\",\"productAttributeCategoryId\":3,\"relatedStatus\":0,\"searchType\":0,\"selectType\":0,\"sort\":100,\"type\":0}', '2022-02-20 15:33:20', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('52', '3', '/mall/productAttribute/update/44', '{\"filterType\":0,\"handAddStatus\":0,\"id\":44,\"inputList\":\"39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,45,46\",\"inputType\":1,\"name\":\"尺码\",\"productAttributeCategoryId\":3,\"relatedStatus\":0,\"searchType\":0,\"selectType\":1,\"sort\":0,\"type\":0}', '2022-02-20 15:36:17', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('53', '3', '/mall/productAttribute/category/update/4', '{\"name\":\"鞋包-篮球鞋\"}', '2022-02-20 15:36:48', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('54', '3', '/mall/productAttribute/add', '{\"filterType\":0,\"handAddStatus\":0,\"inputList\":\"黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色,橙色,紫色\",\"inputType\":1,\"name\":\"颜色\",\"productAttributeCategoryId\":4,\"relatedStatus\":0,\"searchType\":0,\"selectType\":1,\"sort\":0,\"type\":0}', '2022-02-20 15:38:36', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('55', '3', '/mall/productAttribute/add', '{\"filterType\":0,\"handAddStatus\":0,\"inputList\":\"39,39.5,40,40.5,41,41.5,42,42.5,43,43.5,44,45,46\",\"inputType\":1,\"name\":\"尺码\",\"productAttributeCategoryId\":4,\"relatedStatus\":0,\"searchType\":0,\"selectType\":1,\"sort\":0,\"type\":0}', '2022-02-20 15:39:05', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('56', '3', '/mall/productAttribute/category/update/5', '{\"name\":\"鞋包-背包\"}', '2022-02-20 15:42:12', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('57', '3', '/mall/productAttribute/add', '{\"filterType\":0,\"handAddStatus\":0,\"inputList\":\"黑色,红色,白色,蓝色,灰色,绿色,橙色\",\"inputType\":1,\"name\":\"颜色\",\"productAttributeCategoryId\":5,\"relatedStatus\":0,\"searchType\":0,\"selectType\":1,\"sort\":0,\"type\":0}', '2022-02-20 15:43:12', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('58', '3', '/mall/productCategory/update/11', '{\"description\":\"T恤\",\"icon\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/product_cate_chenshan.png\",\"id\":11,\"keywords\":\"T恤\",\"level\":1,\"name\":\"T恤\",\"parentId\":1,\"productAttributeIdList\":[1,7],\"productUnit\":\"件\",\"showStatus\":1,\"sort\":0}', '2022-02-20 15:49:06', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('59', '3', '/mall/productCategory/update/29', '{\"description\":\"\",\"icon\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/product_cate_xie.png\",\"id\":29,\"keywords\":\"\",\"level\":1,\"name\":\"运动长裤\",\"parentId\":1,\"productAttributeIdList\":[35,40],\"productUnit\":\"条\",\"showStatus\":1,\"sort\":0}', '2022-02-20 15:50:39', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('60', '3', '/mall/productCategory/update/30', '{\"description\":\"\",\"icon\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/product_cate_peijian.png\",\"id\":30,\"keywords\":\"\",\"level\":1,\"name\":\"跑步鞋\",\"parentId\":2,\"productAttributeIdList\":[43,44],\"productUnit\":\"双\",\"showStatus\":1,\"sort\":0}', '2022-02-20 15:52:12', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('61', '3', '/mall/productCategory/update/31', '{\"description\":\"\",\"icon\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/product_cate_sheying.png\",\"id\":31,\"keywords\":\"\",\"level\":1,\"name\":\"篮球鞋\",\"parentId\":2,\"productAttributeIdList\":[52,53],\"productUnit\":\"双\",\"showStatus\":1,\"sort\":0}', '2022-02-20 15:52:34', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('62', '3', '/mall/productCategory/update/32', '{\"description\":\"\",\"icon\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/product_cate_yule.png\",\"id\":32,\"keywords\":\"\",\"level\":1,\"name\":\"运动背包\",\"parentId\":2,\"productAttributeIdList\":[54],\"productUnit\":\"件\",\"showStatus\":1,\"sort\":0}', '2022-02-20 15:53:34', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('63', '3', '/mall/productCategory/add', '{\"id\":59,\"level\":1,\"name\":\"运动短裤\",\"parentId\":1,\"productAttributeIdList\":[35,40],\"productUnit\":\"条\",\"showStatus\":1,\"sort\":0}', '2022-02-20 15:58:32', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('64', '3', '/mall/productAttribute/category/update/6', '{\"name\":\"鞋包-足球鞋\"}', '2022-02-20 16:17:16', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('65', '3', '/mall/productAttribute/add', '{\"filterType\":0,\"handAddStatus\":0,\"inputList\":\"黑色,红色,白色,蓝色,黄色,灰色,绿色,橙色,紫色\",\"inputType\":1,\"name\":\"颜色\",\"productAttributeCategoryId\":6,\"relatedStatus\":0,\"searchType\":0,\"selectType\":0,\"sort\":0,\"type\":0}', '2022-02-20 16:19:19', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('66', '3', '/mall/productAttribute/add', '{\"filterType\":0,\"handAddStatus\":0,\"inputList\":\"37,38,39,40,41,42,43,44,45,46\",\"inputType\":1,\"name\":\"尺码\",\"productAttributeCategoryId\":6,\"relatedStatus\":0,\"searchType\":0,\"selectType\":0,\"sort\":0,\"type\":0}', '2022-02-20 16:20:11', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('67', '3', '/mall/productCategory/update/33', '{\"description\":\"\",\"icon\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20190129/product_cate_yule.png\",\"id\":33,\"keywords\":\"\",\"level\":1,\"name\":\"足球鞋\",\"parentId\":2,\"productAttributeIdList\":[55,56],\"productUnit\":\"双\",\"showStatus\":1,\"sort\":0}', '2022-02-20 16:20:43', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('68', '3', '/mall/productAttribute/category/update/10', '{\"name\":\"服装-卫衣\"}', '2022-02-20 16:22:52', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品属性分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('69', '3', '/mall/productAttribute/add', '{\"filterType\":0,\"handAddStatus\":0,\"inputList\":\"黑色,红色,白色,粉色,蓝色,黄色,灰色,绿色\",\"inputType\":1,\"name\":\"颜色\",\"productAttributeCategoryId\":10,\"relatedStatus\":0,\"searchType\":0,\"selectType\":0,\"sort\":0,\"type\":0}', '2022-02-20 16:23:49', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('70', '3', '/mall/productAttribute/add', '{\"filterType\":0,\"handAddStatus\":0,\"inputList\":\"S/165,M/170,L/175,XL/180,2XL/185,3XL/190,4XL/195\",\"inputType\":1,\"name\":\"尺码\",\"productAttributeCategoryId\":10,\"relatedStatus\":0,\"searchType\":0,\"selectType\":0,\"sort\":0,\"type\":0}', '2022-02-20 16:24:34', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品属性', null, null, null);
INSERT INTO `sys_api_log` VALUES ('71', '3', '/mall/productCategory/add', '{\"id\":60,\"level\":1,\"name\":\"卫衣\",\"parentId\":1,\"productAttributeIdList\":[57,58],\"productUnit\":\"件\",\"showStatus\":1,\"sort\":0}', '2022-02-20 16:28:40', '0:0:0:0:0:0:0:1', 'chrome9', '添加商品分类', null, null, null);
INSERT INTO `sys_api_log` VALUES ('72', '3', '/mall/product/update/4', '{\"brandId\":3,\"brandName\":\"鸿星尔克\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":4,\"isFragile\":\"否\",\"keywords\":\"\",\"lowStock\":20,\"name\":\"网面透气跑步鞋\",\"newStatus\":1,\"packingType\":\"中包装\",\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"price\":159,\"productAttributeCategoryId\":0,\"productAttributeValueList\":[],\"productCategoryId\":30,\"productCategoryName\":\"跑步鞋\",\"productSn\":\"No86580\",\"publishStatus\":1,\"recommandStatus\":1,\"sale\":0,\"serviceIds\":\"1,2\",\"skuStockList\":[],\"sort\":1,\"stock\":100,\"subTitle\":\"鸿星尔克男鞋网面透气跑步鞋轻便休闲运动鞋舒适学生鞋\",\"unit\":\"件\",\"weight\":715,\"weightType\":\"小于1kg\"}', '2022-03-13 15:14:32', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('73', '3', '/mall/product/update/5', '{\"brandId\":4,\"brandName\":\"阿迪达斯\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":5,\"keywords\":\"\",\"lowStock\":20,\"name\":\"运动双肩背包\",\"newStatus\":1,\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"price\":139,\"productAttributeCategoryId\":0,\"productAttributeValueList\":[],\"productCategoryId\":32,\"productCategoryName\":\"运动背包\",\"productSn\":\"No86581\",\"publishStatus\":1,\"recommandStatus\":1,\"sale\":0,\"serviceIds\":\"3\",\"skuStockList\":[],\"sort\":1,\"stock\":100,\"unit\":\"件\",\"weight\":447}', '2022-03-13 15:19:40', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('74', '3', '/mall/order/update/delivery/28', '[{\"deliveryCompany\":\"顺丰快递\"},{\"deliverySn\":\"deli_20220327140700\"}]', '2022-03-27 14:07:39', '0:0:0:0:0:0:0:1', 'chrome9', '订单发货', null, null, null);
INSERT INTO `sys_api_log` VALUES ('75', '3', '/mall/product/update/6', '{\"brandId\":1,\"brandName\":\"安踏\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":6,\"isFragile\":\"否\",\"keywords\":\"\",\"lowStock\":20,\"name\":\"水花3代篮球鞋\",\"newStatus\":0,\"packingType\":\"中包装\",\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"price\":519,\"productAttributeCategoryId\":0,\"productAttributeValueList\":[],\"productCategoryId\":30,\"productCategoryName\":\"跑步鞋\",\"productSn\":\"No86582\",\"publishStatus\":1,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"1,2\",\"skuStockList\":[],\"sort\":1,\"stock\":100,\"subTitle\":\"2022夏季KT汤普森球鞋低帮专业实战运动鞋\",\"unit\":\"双\",\"weight\":700,\"weightType\":\"小于1kg\"}', '2022-03-27 18:14:06', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('76', '3', '/mall/product/update/7', '{\"brandId\":1,\"brandName\":\"安踏\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":7,\"isFragile\":\"否\",\"keywords\":\"\",\"lowStock\":0,\"name\":\"水花2代篮球鞋\",\"newStatus\":0,\"packingType\":\"中包装\",\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"pics\":\"string\",\"price\":399,\"productAttributeCategoryId\":4,\"productAttributeValueList\":[{\"id\":8,\"productAttributeId\":1,\"productId\":7,\"value\":\"X\"},{\"id\":9,\"productAttributeId\":1,\"productId\":7,\"value\":\"XL\"},{\"id\":10,\"productAttributeId\":1,\"productId\":7,\"value\":\"XXL\"}],\"productCategoryId\":31,\"productCategoryName\":\"篮球鞋\",\"productSn\":\"No86577\",\"publishStatus\":1,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"NaN\",\"skuStockList\":[{\"id\":1,\"lowStock\":5,\"pic\":\"string\",\"price\":100,\"productId\":7,\"sale\":0,\"skuCode\":\"string\",\"sp1\":\"string\",\"sp2\":\"string\",\"sp3\":\"string\",\"stock\":0},{\"id\":10,\"lowStock\":0,\"pic\":\"string\",\"price\":0,\"productId\":7,\"sale\":0,\"skuCode\":\"string\",\"sp1\":\"string\",\"sp2\":\"string\",\"sp3\":\"sp3\",\"stock\":0},{\"id\":11,\"lowStock\":0,\"pic\":\"string\",\"price\":0,\"productId\":7,\"sale\":0,\"skuCode\":\"string\",\"sp1\":\"string\",\"sp2\":\"string\",\"sp3\":\"sp33\",\"stock\":0}],\"sort\":0,\"stock\":100,\"subTitle\":\"汤普森KT低帮战靴男鞋运动鞋\",\"unit\":\"双\",\"weight\":700,\"weightType\":\"小于1kg\"}', '2022-03-27 18:21:53', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('77', '3', '/mall/product/update/8', '{\"brandId\":59,\"brandName\":\"李宁\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":8,\"keywords\":\"\",\"lowStock\":0,\"name\":\"韦德全城10篮球鞋\",\"newStatus\":1,\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"pics\":\"string\",\"price\":899,\"productAttributeCategoryId\":4,\"productAttributeValueList\":[],\"productCategoryId\":31,\"productCategoryName\":\"篮球鞋\",\"productSn\":\"No86577\",\"publishStatus\":1,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"NaN,1,2,3\",\"skuStockList\":[{\"id\":2,\"lowStock\":5,\"pic\":\"string\",\"price\":100,\"productId\":8,\"sale\":0,\"skuCode\":\"string\",\"sp1\":\"string\",\"sp2\":\"string\",\"sp3\":\"string\",\"stock\":0}],\"sort\":0,\"stock\":100,\"subTitle\":\"棉花糖轻量高回弹专业比赛鞋官方旗舰\",\"unit\":\"双\",\"weight\":700}', '2022-03-27 18:27:34', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('78', '3', '/mall/product/update/9', '{\"brandId\":59,\"brandName\":\"李宁\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":9,\"isFragile\":\"否\",\"keywords\":\"\",\"lowStock\":0,\"name\":\"闪击VI premium篮球鞋\",\"newStatus\":0,\"packingType\":\"中包装\",\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"pics\":\"string\",\"price\":558,\"productAttributeCategoryId\":1,\"productAttributeValueList\":[{\"id\":1,\"productAttributeId\":1,\"productId\":9,\"value\":\"X\"}],\"productCategoryId\":31,\"productCategoryName\":\"篮球鞋\",\"productSn\":\"No86577\",\"publishStatus\":1,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"NaN,1\",\"skuStockList\":[{\"id\":3,\"lowStock\":5,\"pic\":\"string\",\"price\":100,\"productId\":9,\"sale\":0,\"skuCode\":\"string\",\"sp1\":\"string\",\"sp2\":\"string\",\"sp3\":\"string\",\"stock\":0}],\"sort\":0,\"stock\":100,\"subTitle\":\"男子一体织减震支撑中帮篮球专业比赛鞋\",\"unit\":\"双\",\"weight\":700,\"weightType\":\"小于1kg\"}', '2022-03-27 18:52:38', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('79', '3', '/mall/product/update/10', '{\"brandId\":58,\"brandName\":\"耐克\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":10,\"keywords\":\"\",\"lowStock\":0,\"name\":\"KD14 EP 篮球鞋\",\"newStatus\":0,\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"pics\":\"string\",\"price\":799,\"productAttributeCategoryId\":1,\"productAttributeValueList\":[{\"id\":2,\"productAttributeId\":1,\"productId\":10,\"value\":\"X\"}],\"productCategoryId\":31,\"productCategoryName\":\"篮球鞋\",\"productSn\":\"No86577\",\"publishStatus\":1,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"NaN,1,2,3\",\"skuStockList\":[{\"id\":4,\"lowStock\":5,\"pic\":\"string\",\"price\":100,\"productId\":10,\"sale\":0,\"skuCode\":\"string\",\"sp1\":\"string\",\"sp2\":\"string\",\"sp3\":\"string\",\"stock\":0}],\"sort\":0,\"stock\":100,\"unit\":\"双\",\"weight\":800}', '2022-03-27 19:04:22', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('80', '3', '/mall/product/update/11', '{\"brandId\":4,\"brandName\":\"阿迪达斯\",\"deleteStatus\":1,\"feightTemplateId\":0,\"id\":11,\"keywords\":\"\",\"lowStock\":0,\"name\":\"米切尔3代篮球鞋\",\"newStatus\":0,\"pic\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180522/web.png\",\"pics\":\"string\",\"price\":469,\"productAttributeCategoryId\":1,\"productAttributeValueList\":[{\"id\":3,\"productAttributeId\":1,\"productId\":11,\"value\":\"X\"}],\"productCategoryId\":31,\"productCategoryName\":\"篮球鞋\",\"productSn\":\"No86577\",\"publishStatus\":1,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"NaN,1,2,3\",\"skuStockList\":[{\"id\":5,\"lowStock\":5,\"pic\":\"string\",\"price\":100,\"productId\":11,\"sale\":0,\"skuCode\":\"string\",\"sp1\":\"string\",\"sp2\":\"string\",\"sp3\":\"string\",\"stock\":0}],\"sort\":0,\"stock\":100,\"subTitle\":\"GCA男子篮球鞋\",\"unit\":\"双\",\"weight\":700}', '2022-03-27 19:07:57', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('81', '3', '/mall/product/add', '{\"brandId\":21,\"brandName\":\"安德玛\",\"deleteStatus\":0,\"description\":\"测试\",\"detailDesc\":\"测试\",\"detailMobileHtml\":\"\",\"detailTitle\":\"测试\",\"feightTemplateId\":0,\"isFragile\":\"否\",\"keywords\":\"测试\",\"lowStock\":0,\"name\":\"测试\",\"newStatus\":1,\"note\":\"测试\",\"packingType\":\"小包装\",\"pic\":\"\",\"pics\":\"\",\"price\":1000,\"productAttributeCategoryId\":4,\"productAttributeValueList\":[],\"productCategoryId\":31,\"productCategoryName\":\"篮球鞋\",\"productSn\":\"\",\"publishStatus\":1,\"recommandStatus\":1,\"sale\":0,\"serviceIds\":\"1,2,3\",\"skuStockList\":[{\"lowStock\":5,\"price\":40,\"productId\":38,\"skuCode\":\"123\",\"stock\":10},{\"lowStock\":5,\"price\":40,\"productId\":38,\"skuCode\":\"1234\",\"stock\":20}],\"stock\":10,\"subTitle\":\"测试\",\"unit\":\"双\",\"weight\":700,\"weightType\":\"小于1kg\"}', '2022-04-13 23:24:07', '0:0:0:0:0:0:0:1', 'chrome10', '添加商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('82', '3', '/mall/product/add', '{\"brandId\":1,\"brandName\":\"安踏\",\"deleteStatus\":0,\"description\":\"\",\"detailDesc\":\"\",\"detailMobileHtml\":\"\",\"detailTitle\":\"\",\"feightTemplateId\":0,\"isFragile\":\"否\",\"keywords\":\"\",\"lowStock\":0,\"name\":\"12\",\"newStatus\":0,\"note\":\"\",\"packingType\":\"小包装\",\"pic\":\"\",\"pics\":\"\",\"price\":0,\"productAttributeCategoryId\":3,\"productAttributeValueList\":[],\"productCategoryId\":31,\"productCategoryName\":\"篮球鞋\",\"productSn\":\"\",\"publishStatus\":0,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"\",\"skuStockList\":[{\"spData\":\"[{\\\"key\\\":\\\"颜色\\\",\\\"value\\\":\\\"黑色\\\"},{\\\"key\\\":\\\"尺码\\\",\\\"value\\\":\\\"40\\\"}]\"},{\"spData\":\"[{\\\"key\\\":\\\"颜色\\\",\\\"value\\\":\\\"黑色\\\"},{\\\"key\\\":\\\"尺码\\\",\\\"value\\\":\\\"41\\\"}]\"}],\"stock\":0,\"subTitle\":\"\",\"unit\":\"1\",\"weight\":0,\"weightType\":\"小于1kg\"}', '2022-04-14 00:31:16', '0:0:0:0:0:0:0:1', 'chrome10', '添加商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('83', '3', '/mall/product/add', '{\"brandId\":3,\"brandName\":\"鸿星尔克\",\"deleteStatus\":0,\"description\":\"\",\"detailDesc\":\"\",\"detailMobileHtml\":\"\",\"detailTitle\":\"\",\"feightTemplateId\":0,\"isFragile\":\"否\",\"keywords\":\"\",\"lowStock\":0,\"name\":\"1\",\"newStatus\":0,\"note\":\"\",\"packingType\":\"小包装\",\"pic\":\"\",\"pics\":\"\",\"price\":0,\"productAttributeCategoryId\":3,\"productAttributeValueList\":[],\"productCategoryId\":11,\"productCategoryName\":\"T恤\",\"productSn\":\"\",\"publishStatus\":0,\"recommandStatus\":0,\"sale\":0,\"serviceIds\":\"\",\"skuStockList\":[{\"lowStock\":1,\"price\":1,\"skuCode\":\"1\",\"spData\":\"[{\\\"key\\\":\\\"颜色\\\",\\\"value\\\":\\\"白色\\\"},{\\\"key\\\":\\\"尺码\\\",\\\"value\\\":\\\"39\\\"}]\",\"stock\":1},{\"lowStock\":1,\"price\":1,\"skuCode\":\"1\",\"spData\":\"[{\\\"key\\\":\\\"颜色\\\",\\\"value\\\":\\\"白色\\\"},{\\\"key\\\":\\\"尺码\\\",\\\"value\\\":\\\"42\\\"}]\",\"stock\":1}],\"stock\":0,\"subTitle\":\"\",\"unit\":\"1\",\"weight\":0,\"weightType\":\"小于1kg\"}', '2022-04-14 00:35:15', '0:0:0:0:0:0:0:1', 'chrome10', '添加商品', null, null, null);
INSERT INTO `sys_api_log` VALUES ('84', '3', '/mall/home/newProduct/add', '[{\"productId\":4,\"recommendStatus\":1,\"sort\":0}]', '2022-04-27 23:10:16', '0:0:0:0:0:0:0:1', 'chrome10', '添加新品推荐', null, null, null);
INSERT INTO `sys_api_log` VALUES ('85', '3', '/mall/home/newProduct/update/sort/14', '{\"sort\":90}', '2022-04-27 23:10:27', '0:0:0:0:0:0:0:1', 'chrome10', '修改新品推荐排序', null, null, null);
INSERT INTO `sys_api_log` VALUES ('86', '123', '/mall/cloud/user/admin/login', 'null', '2023-01-26 22:50:37', '0:0:0:0:0:0:0:1', 'unknown', '管理员登录', 'testtoken', '1', 'user');
INSERT INTO `sys_api_log` VALUES ('87', null, '/mall/cloud/user/apiLog/add', '{\"name\":\"123\"}', '2023-01-27 21:27:34', '0:0:0:0:0:0:0:1', 'unknown', '', null, null, 'user');
INSERT INTO `sys_api_log` VALUES ('88', '3', '/mall/cloud/user/manage/admin/login', '{\"password\":\"123456\",\"username\":\"admin\"}', '2023-02-19 23:07:16', '192.168.163.1', 'unknown', '管理员登录', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6ImFkbWluLWNsaWVudCIsInVzZXJfbmFtZSI6ImFkbWluIiwic2NvcGUiOlsiYWxsIl0sInJvbGVzIjpbIlJfQURNSU4iXSwidXNlclR5cGUiOjEsImV4cCI6MTY3NjgyMTAzNSwidXNlcklkIjozLCJhdXRob3JpdGllcyI6WyJQX0FETUlOX0FEVkVSVElTRSIsIlBfQURNSU5fUFJPRFVDVCIsIlBfQURNSU5fQVBJX0xPRyIsIlBfQURNSU5fTUFOQUdFUiIsIlBfQURNSU5fT1JERVJfUkVGVU5EIiwiUF9BRE1JTl9ST0xFIiwiUF9BRE1JTl9IRUxQX0NBVEVHT1JZIiwiUF9BRE1JTl9NRU1CRVJfQUREUkVTUyIsIlBfQURNSU5fQ09NTUVOVF9SRVBMWSIsIlBfQURNSU5fRVJST1JfTE9HIiwiUF9BRE1JTl9DT01NRU5UIiwiUF9BRE1JTl9QT1BVTEFSX1JFQ09NTUVORCIsIlBfQURNSU5fSEVMUCIsIlBfQURNSU5fQlJBTkQiLCJQX0FETUlOX09SREVSIiwiUF9BRE1JTl9PUkRFUl9SRUZVTkRfUkVBU09OIiwiUF9BRE1JTl9ORVdfUkVDT01NRU5EIiwiUF9BRE1JTl9QUk9EVUNUX0FUVFJJQlVURSIsIlBfQURNSU5fUEVSTUlTU0lPTiIsIlBfQURNSU5fTUVNQkVSIiwiUF9BRE1JTl9QUk9EVUNUX0NBVEVHT1JZIiwiUF9BRE1JTl9QUk9EVUNUX0FUVFJJQlVURV9DQVRFR09SWSJdLCJqdGkiOiI3ZTk0ODQzNS1jOTQ0LTRjMWMtYmYzMS0yM2Q1ODA2YmRiMzUiLCJjbGllbnRfaWQiOiJhZG1pbi1jbGllbnQiLCJ1c2VybmFtZSI6ImFkbWluIn0.lRWSZjH2Mj1LmedgR_QAdJuWf8KDlW8rVRlmC928WU6m7W_fK8sje5pFRAX2vxGrBw3bhXRHswyinQInwqHAOkhWaeHJ_TZyNbbgJUjKjX0RyxK4-Vh0K1bIlfHdbCzb4vFSWAaIUfkziLzdhWN_Qi8tBamxQpI1VuSA4Sqe6LMZ6xRVQGtbbRg95D5cw-M9vBg4zWeQqVUzXRuaPdLIgZyd4tU-DXXSJ2tqgfrn-9u8b7bpJVsJuSQnBJ474n1xbHo-hHNfP0BjFJlmZcZD6sUT5lPRNXldiywKG5FfmXzG8vrOOvM9u1vhAvzb_DAIwlM9UqVNG75HegqI2H4wpA', '1', 'mall-user-service');
INSERT INTO `sys_api_log` VALUES ('89', '3', '/mall/cloud/user/manage/admin/login', '{\"password\":\"123456\",\"username\":\"admin\"}', '2023-02-19 23:17:35', '192.168.163.1', 'unknown', '管理员登录', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6ImFkbWluLWNsaWVudCIsInVzZXJfbmFtZSI6ImFkbWluIiwic2NvcGUiOlsiYWxsIl0sInJvbGVzIjpbIlJfQURNSU4iXSwidXNlclR5cGUiOjEsImV4cCI6MTY3NjgyMTY1NCwidXNlcklkIjozLCJhdXRob3JpdGllcyI6WyJQX0FETUlOX0FEVkVSVElTRSIsIlBfQURNSU5fUFJPRFVDVCIsIlBfQURNSU5fQVBJX0xPRyIsIlBfQURNSU5fTUFOQUdFUiIsIlBfQURNSU5fT1JERVJfUkVGVU5EIiwiUF9BRE1JTl9ST0xFIiwiUF9BRE1JTl9IRUxQX0NBVEVHT1JZIiwiUF9BRE1JTl9NRU1CRVJfQUREUkVTUyIsIlBfQURNSU5fQ09NTUVOVF9SRVBMWSIsIlBfQURNSU5fRVJST1JfTE9HIiwiUF9BRE1JTl9DT01NRU5UIiwiUF9BRE1JTl9QT1BVTEFSX1JFQ09NTUVORCIsIlBfQURNSU5fSEVMUCIsIlBfQURNSU5fQlJBTkQiLCJQX0FETUlOX09SREVSIiwiUF9BRE1JTl9PUkRFUl9SRUZVTkRfUkVBU09OIiwiUF9BRE1JTl9ORVdfUkVDT01NRU5EIiwiUF9BRE1JTl9QUk9EVUNUX0FUVFJJQlVURSIsIlBfQURNSU5fUEVSTUlTU0lPTiIsIlBfQURNSU5fTUVNQkVSIiwiUF9BRE1JTl9QUk9EVUNUX0NBVEVHT1JZIiwiUF9BRE1JTl9QUk9EVUNUX0FUVFJJQlVURV9DQVRFR09SWSJdLCJqdGkiOiIzYTg4NWM0Ni1hMWZlLTQ0YTMtYWI1OC02YzFhOGYxMTZhZjIiLCJjbGllbnRfaWQiOiJhZG1pbi1jbGllbnQiLCJ1c2VybmFtZSI6ImFkbWluIn0.KTWfMI1xlcKqFgGvDaWe-4GPN8ZdW0vQN_06hRzalVG3pxJjOUpFX0sOEmyMBbcsuSpxDaCuu6P4Z341KkGEohUhb6ooRCtXGRD_fxt03iL1qFT4Zqf-ZIZN9GC-jRi1kvcn9jBq6K45ckE6TGO4xSa106EgoD_r3xtugS-BCLXhedIoMUtNpMMb0XB1kPUq8akMXzy7ug57GPwHsJfcePEvmzLGrZUW98n-7mj8VjQ55PX2cF4e4JnqTvvCgSVT7QhVbsJ0Enb6jz81OZsbpAOd8tTbKYsPPujbH8Z7G2ueiPQuZUBQCi7KwpLf_1ZYzUnMFpmoZr1y8u2VzoXguA', '1', 'mall-user-service');

-- ----------------------------
-- Table structure for sys_error_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_error_log`;
CREATE TABLE `sys_error_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `req_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '请求方法',
  `req_params` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '请求参数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `ip` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'ip',
  `user_agent` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '浏览器',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `token` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'token',
  `userType` int(1) DEFAULT NULL COMMENT '用户类型（0->前台；1->后台）',
  `serviceSign` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '服务类型标识',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_error_log
-- ----------------------------
INSERT INTO `sys_error_log` VALUES ('2', '3', '/mall/brand/update/1', '{\"firstLetter\":\"W\",\"id\":1,\"logo\":\"http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/timg(5).jpg\",\"name\":\"万\",\"showStatus\":1}', '2022-02-20 01:11:44', '0:0:0:0:0:0:0:1', 'chrome9', '修改品牌;org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.reflection.ReflectionException: There is no getter for property named \'record\' in \'class com.hhf.my_mall.model.PmsBrand\'', null, null, null);
INSERT INTO `sys_error_log` VALUES ('3', '3', '/mall/product/updateInfo/2', 'null', '2022-02-20 16:02:19', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品;java.lang.NullPointerException', null, null, null);
INSERT INTO `sys_error_log` VALUES ('4', '3', '/mall/product/updateInfo/22', 'null', '2022-02-20 16:12:24', '0:0:0:0:0:0:0:1', 'chrome9', '修改商品;java.lang.NullPointerException', null, null, null);
INSERT INTO `sys_error_log` VALUES ('5', null, '/mall/cloud/user/admin/login', 'null', '2023-01-27 18:51:41', '0:0:0:0:0:0:0:1', 'unknown', '管理员登录:com.hhf.mall.common.exception.CodeException: test error', null, null, 'user');
INSERT INTO `sys_error_log` VALUES ('6', null, '/mall/cloud/user/admin/login', 'null', '2023-01-27 18:54:42', '0:0:0:0:0:0:0:1', 'unknown', '管理员登录:com.hhf.mall.common.exception.CodeException: test error', null, null, 'user');
INSERT INTO `sys_error_log` VALUES ('7', null, '/mall/cloud/user/admin/login', 'null', '2023-01-27 19:40:56', '0:0:0:0:0:0:0:1', 'unknown', '管理员登录:com.hhf.mall.common.exception.CodeException: test error', null, null, 'user');
INSERT INTO `sys_error_log` VALUES ('8', null, '/mall/cloud/user/admin/login', 'null', '2023-01-27 19:42:52', '0:0:0:0:0:0:0:1', 'unknown', '管理员登录:com.hhf.mall.common.exception.CodeException: test error', null, null, 'user');
INSERT INTO `sys_error_log` VALUES ('9', null, '/mall/cloud/user/admin/login', 'null', '2023-01-27 19:47:58', '0:0:0:0:0:0:0:1', 'unknown', '管理员登录:com.hhf.mall.common.exception.CodeException: test error', null, null, 'user');
INSERT INTO `sys_error_log` VALUES ('10', null, '/mall/cloud/user/admin/login', 'null', '2023-01-27 20:24:12', '0:0:0:0:0:0:0:1', 'unknown', '管理员登录:com.hhf.mall.common.exception.CodeException: 用户名或密码错误', null, null, 'user');

-- ----------------------------
-- Table structure for ums_admin
-- ----------------------------
DROP TABLE IF EXISTS `ums_admin`;
CREATE TABLE `ums_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `icon` varchar(500) DEFAULT NULL COMMENT '头像',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `nickname` varchar(200) DEFAULT NULL COMMENT '昵称',
  `note` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `status` int(1) DEFAULT '1' COMMENT '是否启用：0->禁用；1->启用',
  `updateTime` datetime DEFAULT NULL COMMENT '更新',
  `logoutTime` datetime DEFAULT NULL COMMENT '退出登录时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='后台用户表';

-- ----------------------------
-- Records of ums_admin
-- ----------------------------
INSERT INTO `ums_admin` VALUES ('1', '陈静', 'e10adc3949ba59abbe56e057f20f883e', 'xxx', '13002061900', 'chenjing@qq.com', '陈静', '商品管理员', '2018-09-29 13:55:30', '2018-09-29 13:55:39', '1', null, null);
INSERT INTO `ums_admin` VALUES ('3', 'admin', '$2a$10$wXoZYnAu/U/75V3fxqMkvet.Ro8SU3xK6hqdLMSN.dBgMhpvV1wku', 'http://my-mall-image.oss-cn-guangzhou.aliyuncs.com/images/admin/default_icon.png', '13002061901', 'admin@163.com', '超级管理员', '超级管理员', '2021-12-26 21:32:47', '2023-02-19 23:17:34', '1', '2023-02-19 23:17:34', null);
INSERT INTO `ums_admin` VALUES ('4', '李洋', 'e10adc3949ba59abbe56e057f20f883e', 'xxx', '13002061902', 'liyang@126.com', '李洋', '订单管理员', '2021-12-02 16:51:07', null, '1', null, null);
INSERT INTO `ums_admin` VALUES ('5', '王进', 'e10adc3949ba59abbe56e057f20f883e', null, '13002061234', 'wangjin@126.com', '王进', '备注', '2022-01-02 11:38:14', null, '0', null, null);

-- ----------------------------
-- Table structure for ums_member
-- ----------------------------
DROP TABLE IF EXISTS `ums_member`;
CREATE TABLE `ums_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(64) DEFAULT NULL COMMENT '昵称',
  `phone` varchar(64) DEFAULT NULL COMMENT '手机号码',
  `status` int(1) DEFAULT NULL COMMENT '启用状态:0->禁用；1->启用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `icon` varchar(500) DEFAULT NULL COMMENT '头像',
  `gender` int(1) DEFAULT NULL COMMENT '性别：0->未知；1->男；2->女',
  `openid` varchar(100) DEFAULT NULL COMMENT '用户在当前小程序的唯一标识',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_username` (`username`),
  UNIQUE KEY `idx_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='前台用户表';

-- ----------------------------
-- Records of ums_member
-- ----------------------------
INSERT INTO `ums_member` VALUES ('1', 'mall_202cb962', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '于东', '18061581849', '1', '2021-11-02 10:35:44', 'http://macro-oss.oss-cn-shenzhen.aliyuncs.com/mall/images/20180607/5ac1bf58Ndefaac16.jpg', '1', null, null);
INSERT INTO `ums_member` VALUES ('3', 'mall_2d234b70', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '林咏华', '18061581848', '0', '2021-11-03 16:46:38', null, '2', null, null);
INSERT INTO `ums_member` VALUES ('4', 'mall_a8c902yi', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '郑杰', '18061581847', '1', '2021-12-12 14:12:04', null, '1', null, null);
INSERT INTO `ums_member` VALUES ('5', 'mall_hj3u90po', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '陈天明', '18061581841', '1', '2021-12-12 14:12:38', null, '1', null, null);
INSERT INTO `ums_member` VALUES ('6', 'mall_wa2e4tre', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '王田', '18061581842', '1', '2021-12-12 14:13:09', null, '2', null, null);
INSERT INTO `ums_member` VALUES ('7', 'mall_1qvb87kl', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '赖宗辉', '18061581845', '1', '2022-01-12 14:21:39', null, '1', null, null);
INSERT INTO `ums_member` VALUES ('8', 'mall_27zwq0f4', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '罗战勇', '18061581844', '1', '2022-02-12 14:22:00', null, '1', null, null);
INSERT INTO `ums_member` VALUES ('9', 'mall_apm5lk3f', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '星空', '18061581843', '1', '2022-02-12 14:22:55', null, '2', null, null);
INSERT INTO `ums_member` VALUES ('10', 'mall_s46l5plz', '695668b992b4a789f1bab5a259578235', '大黄蜂', '13002061432', '1', '2022-02-01 22:45:36', 'https://thirdwx.qlogo.cn/mmopen/vi_32/1hcHmIncYVFb8YgEPyh4HEJHj9YyOD5Ieg3dLpUTFJPZX6WdSy7Y4nVWjtxfvck3rgcZbg84ic25fa7mMb29PRA/132', '1', 'oqGhq5G9Tl3ZszfFiOLliQCFmzzY', null);
INSERT INTO `ums_member` VALUES ('11', 'mall_u98fhl28', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '樱花', '13019087678', '1', '2022-03-23 10:00:00', null, '2', null, null);
INSERT INTO `ums_member` VALUES ('12', 'mall_op34kj78', '$2a$10$NZ5o7r2E.ayT2ZoxgjlI.eJ6OEYqjH7INR/F.mXDbjZJi9HF0YCVG', '三文鱼', '13556787121', '1', '2022-03-24 09:00:00', null, '1', null, null);

-- ----------------------------
-- Table structure for ums_member_receive_address
-- ----------------------------
DROP TABLE IF EXISTS `ums_member_receive_address`;
CREATE TABLE `ums_member_receive_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '收货人名称',
  `phone` varchar(64) DEFAULT NULL COMMENT '收货手机',
  `default_status` int(1) DEFAULT NULL COMMENT '是否为默认(1->默认)',
  `province` varchar(100) DEFAULT NULL COMMENT '省份/直辖市',
  `city` varchar(100) DEFAULT NULL COMMENT '城市',
  `region` varchar(100) DEFAULT NULL COMMENT '区',
  `detail_address` varchar(128) DEFAULT NULL COMMENT '详细地址(街道)',
  `member_username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `updateTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='会员收货地址表';

-- ----------------------------
-- Records of ums_member_receive_address
-- ----------------------------
INSERT INTO `ums_member_receive_address` VALUES ('1', '10', '何鸿峰', '18033441849', '1', '广东省', '广州市', '天河区', '黄埔大道中10号', 'mall_s46l5plz', null);
INSERT INTO `ums_member_receive_address` VALUES ('3', '10', '李明', '18033441848', '0', '广东省', '深圳市', '福田区', '解放北路12号御景半岛1栋402', 'mall_s46l5plz', null);
INSERT INTO `ums_member_receive_address` VALUES ('4', '10', '张佳慧', '18033441845', '0', '广东省', '佛山市', '南海区', '中山一路和平街道7号', 'mall_s46l5plz', null);
INSERT INTO `ums_member_receive_address` VALUES ('5', '1', '于东', '13012031708', '0', '天津市', '天津市', '和平区', '人民中路7号', 'mall_202cb962', null);
INSERT INTO `ums_member_receive_address` VALUES ('6', '10', '1', '19927520730', '0', '北京市', '北京市', '东城区', '1', null, null);

-- ----------------------------
-- Table structure for ums_permission
-- ----------------------------
DROP TABLE IF EXISTS `ums_permission`;
CREATE TABLE `ums_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL COMMENT '名称',
  `value` varchar(200) DEFAULT NULL COMMENT '权限值',
  `status` int(1) DEFAULT '1' COMMENT '启用状态；0->禁用；1->启用',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COMMENT='后台用户权限表';

-- ----------------------------
-- Records of ums_permission
-- ----------------------------
INSERT INTO `ums_permission` VALUES ('38', '品牌管理', 'P_ADMIN_BRAND', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('39', '商品分类', 'P_ADMIN_PRODUCT_CATEGORY', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('40', '商品管理', 'P_ADMIN_PRODUCT', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('41', '商品属性', 'P_ADMIN_PRODUCT_ATTRIBUTE', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('42', '商品属性分类', 'P_ADMIN_PRODUCT_ATTRIBUTE_CATEGORY', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('43', '评论管理', 'P_ADMIN_COMMENT', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('44', '评论回复管理', 'P_ADMIN_COMMENT_REPLY', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('45', '订单管理', 'P_ADMIN_ORDER', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('46', '退货管理', 'P_ADMIN_ORDER_REFUND', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('47', '退货原因', 'P_ADMIN_ORDER_REFUND_REASON', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('48', '广告管理', 'P_ADMIN_ADVERTISE', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('49', '新品推荐', 'P_ADMIN_NEW_RECOMMEND', '1', '2023-02-19 18:27:56');
INSERT INTO `ums_permission` VALUES ('50', '人气推荐', 'P_ADMIN_POPULAR_RECOMMEND', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('51', '帮助分类', 'P_ADMIN_HELP_CATEGORY', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('52', '帮助管理', 'P_ADMIN_HELP', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('53', '后台用户管理', 'P_ADMIN_MANAGER', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('54', '角色管理', 'P_ADMIN_ROLE', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('55', '权限管理', 'P_ADMIN_PERMISSION', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('56', '前台用户管理', 'P_ADMIN_MEMBER', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('57', '收货地址', 'P_ADMIN_MEMBER_ADDRESS', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('58', '操作日志', 'P_ADMIN_API_LOG', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('59', '异常日志', 'P_ADMIN_ERROR_LOG', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('60', '用户', 'P_WEB_MEMBER', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('61', '收货地址', 'P_WEB_MEMBER_ADDRESS', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('62', '帮助', 'P_WEB_HELP', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('63', '帮助分类', 'P_WEB_HELP_CATEGORY', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('64', '广告', 'P_WEB_ADVERTISE', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('65', '新品推荐', 'P_WEB_NEW_RECOMMEND', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('66', '人气推荐', 'P_WEB_POPULAR_RECOMMEND', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('67', '购物车', 'P_WEB_CART_ITEM', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('68', '订单', 'P_WEB_ORDER', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('69', '退货', 'P_WEB_ORDER_REFUND', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('70', '退货原因', 'P_WEB_ORDER_REFUND_REASON', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('71', '商品分类', 'P_WEB_PRODUCT_CATEGORY', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('72', '商品', 'P_WEB_PRODUCT', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('73', '评论', 'P_WEB_COMMENT', '1', '2023-02-19 18:27:57');
INSERT INTO `ums_permission` VALUES ('74', '评论回复', 'P_WEB_COMMENT_REPLY', '1', '2023-02-19 18:27:57');

-- ----------------------------
-- Table structure for ums_role
-- ----------------------------
DROP TABLE IF EXISTS `ums_role`;
CREATE TABLE `ums_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(1) DEFAULT '1' COMMENT '启用状态：0->禁用；1->启用',
  `value` varchar(200) DEFAULT NULL COMMENT '角色标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='后台用户角色表';

-- ----------------------------
-- Records of ums_role
-- ----------------------------
INSERT INTO `ums_role` VALUES ('5', '后台超级管理员', '', '2023-02-19 18:27:56', '1', 'R_ADMIN');
INSERT INTO `ums_role` VALUES ('6', '后台普通管理员', '', '2023-02-19 18:27:57', '1', 'R_MANAGER');
INSERT INTO `ums_role` VALUES ('7', '前台用户', '', '2023-02-19 18:27:57', '1', 'R_USER');
INSERT INTO `ums_role` VALUES ('8', '前台游客', '', '2023-02-19 18:27:57', '1', 'R_GUEST');

-- ----------------------------
-- Table structure for ums_role_permission_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_role_permission_relation`;
CREATE TABLE `ums_role_permission_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `permission_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `permission_id` (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8 COMMENT='后台用户角色和权限关系表';

-- ----------------------------
-- Records of ums_role_permission_relation
-- ----------------------------
INSERT INTO `ums_role_permission_relation` VALUES ('66', '5', '38');
INSERT INTO `ums_role_permission_relation` VALUES ('67', '5', '39');
INSERT INTO `ums_role_permission_relation` VALUES ('68', '5', '40');
INSERT INTO `ums_role_permission_relation` VALUES ('69', '5', '41');
INSERT INTO `ums_role_permission_relation` VALUES ('70', '5', '42');
INSERT INTO `ums_role_permission_relation` VALUES ('71', '5', '43');
INSERT INTO `ums_role_permission_relation` VALUES ('72', '5', '44');
INSERT INTO `ums_role_permission_relation` VALUES ('73', '5', '45');
INSERT INTO `ums_role_permission_relation` VALUES ('74', '5', '46');
INSERT INTO `ums_role_permission_relation` VALUES ('75', '5', '47');
INSERT INTO `ums_role_permission_relation` VALUES ('76', '5', '48');
INSERT INTO `ums_role_permission_relation` VALUES ('77', '5', '49');
INSERT INTO `ums_role_permission_relation` VALUES ('78', '5', '50');
INSERT INTO `ums_role_permission_relation` VALUES ('79', '5', '51');
INSERT INTO `ums_role_permission_relation` VALUES ('80', '5', '52');
INSERT INTO `ums_role_permission_relation` VALUES ('81', '5', '53');
INSERT INTO `ums_role_permission_relation` VALUES ('82', '5', '54');
INSERT INTO `ums_role_permission_relation` VALUES ('83', '5', '55');
INSERT INTO `ums_role_permission_relation` VALUES ('84', '5', '56');
INSERT INTO `ums_role_permission_relation` VALUES ('85', '5', '57');
INSERT INTO `ums_role_permission_relation` VALUES ('86', '5', '58');
INSERT INTO `ums_role_permission_relation` VALUES ('87', '5', '59');
INSERT INTO `ums_role_permission_relation` VALUES ('88', '6', '38');
INSERT INTO `ums_role_permission_relation` VALUES ('89', '6', '39');
INSERT INTO `ums_role_permission_relation` VALUES ('90', '6', '40');
INSERT INTO `ums_role_permission_relation` VALUES ('91', '6', '41');
INSERT INTO `ums_role_permission_relation` VALUES ('92', '6', '42');
INSERT INTO `ums_role_permission_relation` VALUES ('93', '6', '43');
INSERT INTO `ums_role_permission_relation` VALUES ('94', '6', '44');
INSERT INTO `ums_role_permission_relation` VALUES ('95', '6', '45');
INSERT INTO `ums_role_permission_relation` VALUES ('96', '6', '46');
INSERT INTO `ums_role_permission_relation` VALUES ('97', '6', '47');
INSERT INTO `ums_role_permission_relation` VALUES ('98', '6', '48');
INSERT INTO `ums_role_permission_relation` VALUES ('99', '6', '49');
INSERT INTO `ums_role_permission_relation` VALUES ('100', '6', '50');
INSERT INTO `ums_role_permission_relation` VALUES ('101', '6', '51');
INSERT INTO `ums_role_permission_relation` VALUES ('102', '6', '52');
INSERT INTO `ums_role_permission_relation` VALUES ('103', '6', '56');
INSERT INTO `ums_role_permission_relation` VALUES ('104', '6', '57');
INSERT INTO `ums_role_permission_relation` VALUES ('105', '6', '58');
INSERT INTO `ums_role_permission_relation` VALUES ('106', '6', '59');
INSERT INTO `ums_role_permission_relation` VALUES ('107', '7', '60');
INSERT INTO `ums_role_permission_relation` VALUES ('108', '7', '61');
INSERT INTO `ums_role_permission_relation` VALUES ('109', '7', '62');
INSERT INTO `ums_role_permission_relation` VALUES ('110', '7', '63');
INSERT INTO `ums_role_permission_relation` VALUES ('111', '7', '64');
INSERT INTO `ums_role_permission_relation` VALUES ('112', '7', '65');
INSERT INTO `ums_role_permission_relation` VALUES ('113', '7', '66');
INSERT INTO `ums_role_permission_relation` VALUES ('114', '7', '67');
INSERT INTO `ums_role_permission_relation` VALUES ('115', '7', '68');
INSERT INTO `ums_role_permission_relation` VALUES ('116', '7', '69');
INSERT INTO `ums_role_permission_relation` VALUES ('117', '7', '70');
INSERT INTO `ums_role_permission_relation` VALUES ('118', '7', '71');
INSERT INTO `ums_role_permission_relation` VALUES ('119', '7', '72');
INSERT INTO `ums_role_permission_relation` VALUES ('120', '7', '73');
INSERT INTO `ums_role_permission_relation` VALUES ('121', '7', '74');
INSERT INTO `ums_role_permission_relation` VALUES ('122', '8', '62');
INSERT INTO `ums_role_permission_relation` VALUES ('123', '8', '63');
INSERT INTO `ums_role_permission_relation` VALUES ('124', '8', '64');
INSERT INTO `ums_role_permission_relation` VALUES ('125', '8', '65');
INSERT INTO `ums_role_permission_relation` VALUES ('126', '8', '66');
INSERT INTO `ums_role_permission_relation` VALUES ('127', '8', '71');
INSERT INTO `ums_role_permission_relation` VALUES ('128', '8', '72');
INSERT INTO `ums_role_permission_relation` VALUES ('129', '8', '73');
INSERT INTO `ums_role_permission_relation` VALUES ('130', '8', '74');

-- ----------------------------
-- Table structure for ums_user_role_relation
-- ----------------------------
DROP TABLE IF EXISTS `ums_user_role_relation`;
CREATE TABLE `ums_user_role_relation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL COMMENT '用户id',
  `roleId` bigint(20) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色关联';

-- ----------------------------
-- Records of ums_user_role_relation
-- ----------------------------
INSERT INTO `ums_user_role_relation` VALUES ('1', '3', '5');
