package com.hhf.mall.order.service.module.oms.order.service;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.Const;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.dto.user.MemberDTO;
import com.hhf.mall.dto.product.ProductDTO;
import com.hhf.mall.dto.user.ReceiveAddrDTO;
import com.hhf.mall.dto.product.SkuStockDTO;
import com.hhf.mall.feign.product.CommentClient;
import com.hhf.mall.feign.product.CommentReplyClient;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.feign.user.MemberReceiveAddressClient;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.feign.product.SkuStockClient;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItem;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItemExample;
import com.hhf.mall.order.service.module.oms.cartItem.service.CartItemService;
import com.hhf.mall.order.service.module.oms.order.dto.MiniOrderParam;
import com.hhf.mall.order.service.module.oms.order.dto.OrderChartDTO;
import com.hhf.mall.order.service.module.oms.order.dto.OrderDTOForAdd;
import com.hhf.mall.order.service.module.oms.order.dto.OrderDetailDTO;
import com.hhf.mall.order.service.module.oms.order.dto.PersonCenterResult;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrder;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrderExample;
import com.hhf.mall.order.service.module.oms.order.entity.OrderConfirmStatus;
import com.hhf.mall.order.service.module.oms.order.entity.OrderDeleteStatus;
import com.hhf.mall.order.service.module.oms.order.entity.OrderPayType;
import com.hhf.mall.order.service.module.oms.order.entity.OrderStatus;
import com.hhf.mall.order.service.module.oms.order.mapper.OmsOrderMapper;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItem;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItemExample;
import com.hhf.mall.order.service.module.oms.orderItem.service.OrderItemService;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.entity.OmsOrderOperateHistory;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.entity.OperateUserType;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.service.OrderOperateHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-16 21:06
 */
@Service
public class OrderService extends BaseService<OmsOrder, Long, OmsOrderExample> {

    @Autowired
    private OmsOrderMapper orderMapper;

    @Autowired
    private OrderOperateHistoryService orderOperateHistoryService;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private MemberClient memberClient;
    @Autowired
    private MemberReceiveAddressClient receiveAddressClient;
    @Autowired
    private CommentClient commentClient;
    @Autowired
    private CommentReplyClient commentReplyClient;
    @Autowired
    private SkuStockClient skuStockClient;
    @Autowired
    private ProductClient productClient;

    @Override
    protected void before(OmsOrder entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            ResponseEntity<ApiResponse> responseEntity = memberClient.getById(entity.getMemberId());
            ApiResponse response = responseEntity.getBody();
            MemberDTO memberDTO = response.toObject(MemberDTO.class);
            entity.setMemberUsername(memberDTO.getUsername());
        }
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(OmsOrder entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            addOrderOperateHistory(entity.getId(), OrderStatus.WAIT_FOR_PAY.getKey(), OperateUserType.MEMBER.getText(), "提交订单");
        }
    }

    @Override
    protected BaseMapper<OmsOrder, Long, OmsOrderExample> getMapper() {
        return orderMapper;
    }

    public List<OmsOrder> list(ListParam listParam) {
        OmsOrderExample example = new OmsOrderExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return orderMapper.selectByExample(example);
    }

    public void delete(Long orderId, String man) {
        OmsOrder order = new OmsOrder();
        order.setId(orderId);
        order.setDeleteStatus(OrderDeleteStatus.YES.getKey());
        order.setModifyTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);

//        OmsOrderItemExample example = new OmsOrderItemExample();
//        example.createCriteria().andOrderIdEqualTo(orderId);
//        orderItemService.deleteByExample(example);

        addOrderOperateHistory(orderId, OrderStatus.CLOSED.getKey(), man, "删除订单");
    }

    public void close(Long orderId, String note) {
        OmsOrder order = new OmsOrder();
        order.setId(orderId);
        order.setStatus(OrderStatus.CLOSED.getKey());
        order.setModifyTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);
        addOrderOperateHistory(orderId, OrderStatus.CLOSED.getKey(), OperateUserType.ADMIN.getText(), note);
    }

    public void delivery(Long orderId, String deliveryCompany, String deliverySn) {
        OmsOrder order = new OmsOrder();
        order.setId(orderId);
        order.setStatus(OrderStatus.WAIT_FOR_DELIVER.getKey());
        order.setDeliverySn(deliverySn);
        order.setDeliveryCompany(deliveryCompany);
        order.setDeliveryTime(new Date());
        order.setModifyTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);
        addOrderOperateHistory(orderId, OrderStatus.WAIT_FOR_RECEIVE.getKey(), OperateUserType.ADMIN.getText(), "完成发货");
    }

    public OrderDetailDTO detail(Long orderId) {
        OrderDetailDTO dto = orderMapper.detail(orderId);
        ResponseEntity<ApiResponse> responseEntity = receiveAddressClient.getById(dto.getReceiveAddressId());
        ApiResponse response = responseEntity.getBody();
        ReceiveAddrDTO receiveAddrDTO = response.toObject(ReceiveAddrDTO.class);
        dto.setAddress(receiveAddrDTO);
        return dto;
    }

    public void note(Long orderId, String note, Integer status) {
        OmsOrder order = new OmsOrder();
        order.setId(orderId);
        order.setNote(note);
        order.setModifyTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);
        addOrderOperateHistory(orderId, status, OperateUserType.ADMIN.getText(), note);
    }

    public OrderDTOForAdd add(MiniOrderParam param) {
        List<OmsCartItem> cartItemList = param.getCartItemList();
        double freightAmount = param.getFreightAmount();
        double totalAmount = param.getTotalAmount();
        double payAmount = freightAmount + totalAmount;
        BigDecimal freight = new BigDecimal(String.valueOf(freightAmount));
        BigDecimal total = new BigDecimal(String.valueOf(totalAmount));
        BigDecimal pay = new BigDecimal(String.valueOf(payAmount));

        //添加订单
        OmsOrder order = new OmsOrder();
        String orderSn = "ord_" + DateUtil.format(new Date(), Const.YYYYMMDDHHMMSS);
        order.setOrderSn(orderSn);
        order.setMemberId(param.getMemberId());
        order.setFreightAmount(freight);
        order.setTotalAmount(total);
        order.setPayAmount(pay);
        order.setDefault();
        order.setReceiveAddressId(param.getReceiveAddressId());

        int totalQuantity = 0;
        for (OmsCartItem cartItem : cartItemList) {
            totalQuantity += cartItem.getQuantity();
        }
        order.setTotalQuantity(totalQuantity);
        add(order);
        Long orderId = order.getId();

        List<Long> cartItemIdList = addOrderItem(orderId, orderSn, cartItemList);
        OrderDTOForAdd dto = new OrderDTOForAdd();
        dto.setOrderId(orderId);
        dto.setCartItemIdList(cartItemIdList);
        return dto;
    }

    /**
     * 添加购买商品列表
     *
     * @param orderId
     * @param orderSn
     * @param cartItemList
     */
    private List<Long> addOrderItem(Long orderId, String orderSn, List<OmsCartItem> cartItemList) {
        List<Long> cartItemIdList = new ArrayList<>();
        for (OmsCartItem cartItem : cartItemList) {
            cartItemIdList.add(cartItem.getId());

            OmsOrderItem orderItem = new OmsOrderItem();
            orderItem.setOrderId(orderId);
            orderItem.setOrderSn(orderSn);
            orderItem.setProductId(cartItem.getProductId());
            orderItem.setProductName(cartItem.getProductName());
            orderItem.setProductBrand(cartItem.getProductBrand());
            orderItem.setBrandId(cartItem.getBrandId());
            orderItem.setProductPic(cartItem.getProductPic());
            orderItem.setProductAttr(cartItem.getProductAttr());
            orderItem.setProductCategoryId(cartItem.getProductCategoryId());
            orderItem.setProductPrice(cartItem.getPrice());
            orderItem.setProductQuantity(cartItem.getQuantity());
            orderItem.setProductSkuId(cartItem.getProductSkuId());
            orderItem.setProductSkuCode(cartItem.getProductSkuCode());

            orderItemService.add(orderItem);
        }

        return cartItemIdList;
    }

    public void updatePayInfo(Long id, List<Long> cartItemIds) {
        //清空购物车
        OmsCartItemExample example = new OmsCartItemExample();
        ListParam listParam = new ListParam().add("idIn", JSON.toJSONString(cartItemIds));
        cartItemService.selectCondition(example, listParam);
        cartItemService.deleteByExample(example);

        //更新订单
        OmsOrder order = new OmsOrder();
        order.setId(id);
        order.setStatus(OrderStatus.WAIT_FOR_DELIVER.getKey());
        order.setPayType(OrderPayType.WECHAT_PAY.getKey());
        order.setPaymentTime(new Date());
        order.setModifyTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);

        //更新商品sku库存
        OmsOrderItemExample example2 = new OmsOrderItemExample();
        ListParam listParam2 = new ListParam().add("orderId", String.valueOf(id));
        orderItemService.selectCondition(example2, listParam2);
        List<OmsOrderItem> orderItemList = orderItemService.selectByExample(example2);

        for (OmsOrderItem orderItem : orderItemList) {
            Long productId = orderItem.getProductId();
            Long skuId = orderItem.getProductSkuId();

            ResponseEntity<ApiResponse> responseEntity1 = productClient.getById(productId);
            ApiResponse response1 = responseEntity1.getBody();
            ProductDTO productDTO = response1.toObject(ProductDTO.class);
            productDTO.setStock(productDTO.getStock() - 1);
            productDTO.setSale(productDTO.getSale() + 1);
            productClient.updateById(productDTO);

            ResponseEntity<ApiResponse> responseEntity2 = skuStockClient.getById(skuId);
            ApiResponse response2 = responseEntity2.getBody();
            SkuStockDTO skuStockDTO = response2.toObject(SkuStockDTO.class);
            skuStockDTO.setStock(skuStockDTO.getStock() - 1);
            skuStockDTO.setSale(skuStockDTO.getSale() + 1);
            skuStockClient.updateById(skuStockDTO);
        }

        addOrderOperateHistory(id, OrderStatus.WAIT_FOR_DELIVER.getKey(), OperateUserType.MEMBER.getText(), "订单已付款");
    }

    /**
     * 添加操作历史
     *
     * @param orderId
     * @param orderStatus
     * @param man
     * @param note
     */
    private void addOrderOperateHistory(Long orderId, Integer orderStatus, String man, String note) {
        OmsOrderOperateHistory history = new OmsOrderOperateHistory();
        history.setOrderId(orderId);
        history.setOrderStatus(orderStatus);
        history.setCreateTime(new Date());
        history.setOperateMan(man);
        history.setNote(note);
        orderOperateHistoryService.add(history);
    }

    public List<OmsOrder> get(ListParam listParam) {
        OmsOrderExample example = new OmsOrderExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return orderMapper.selectByExample(example);
//        Collections.reverse(omsOrderList);
    }

    public void updateReceive(Long id) {
        OmsOrder order = new OmsOrder();
        order.setId(id);
        order.setStatus(OrderStatus.FINISH.getKey());
        order.setConfirmStatus(OrderConfirmStatus.YES.getKey());
        order.setReceiveTime(new Date());
        order.setModifyTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);
        addOrderOperateHistory(id, OrderStatus.FINISH.getKey(), OperateUserType.MEMBER.getText(), "已收货");
    }

    public void updateCancel(Long id) {
        OmsOrder order = new OmsOrder();
        order.setId(id);
        order.setStatus(OrderStatus.CLOSED.getKey());
        order.setModifyTime(new Date());
        orderMapper.updateByPrimaryKeySelective(order);
        addOrderOperateHistory(id, OrderStatus.CLOSED.getKey(), OperateUserType.MEMBER.getText(), "取消订单");
    }

    public PersonCenterResult getStatInfo(Long memberId) {
        //统计订单
        OmsOrderExample example = new OmsOrderExample();
        ListParam listParam = new ListParam().add("memberId", String.valueOf(memberId))
                .add("status", String.valueOf(OrderStatus.WAIT_FOR_PAY.getKey()))
                .add("deleteStatus", String.valueOf(OrderDeleteStatus.NO.getKey()));
        selectCondition(example, listParam);
        int toPayOrderCount = orderMapper.countByExample(example);

        OmsOrderExample example2 = new OmsOrderExample();
        ListParam listParam2 = new ListParam().add("memberId", String.valueOf(memberId))
                .add("status", String.valueOf(OrderStatus.WAIT_FOR_DELIVER.getKey()))
                .add("deleteStatus", String.valueOf(OrderDeleteStatus.NO.getKey()));
        selectCondition(example2, listParam2);
        int toDeliveryOrderCount = orderMapper.countByExample(example2);

        OmsOrderExample example3 = new OmsOrderExample();
        ListParam listParam3 = new ListParam().add("memberId", String.valueOf(memberId))
                .add("status", String.valueOf(OrderStatus.WAIT_FOR_RECEIVE.getKey()))
                .add("deleteStatus", String.valueOf(OrderDeleteStatus.NO.getKey()));
        selectCondition(example3, listParam3);
        int toReceiveOrderCount = orderMapper.countByExample(example3);

        OmsOrderExample example4 = new OmsOrderExample();
        ListParam listParam4 = new ListParam().add("memberId", String.valueOf(memberId))
                .add("status", String.valueOf(OrderStatus.FINISH.getKey()))
                .add("deleteStatus", String.valueOf(OrderDeleteStatus.NO.getKey()));
        selectCondition(example4, listParam4);
        int finishOrderCount = orderMapper.countByExample(example4);

        OmsOrderExample example5 = new OmsOrderExample();
        ListParam listParam5 = new ListParam().add("memberId", String.valueOf(memberId))
                .add("status", String.valueOf(OrderStatus.CLOSED.getKey()))
                .add("deleteStatus", String.valueOf(OrderDeleteStatus.NO.getKey()));
        selectCondition(example5, listParam5);
        int cancelOrderCount = orderMapper.countByExample(example5);

        //统计评论、回复
        ResponseEntity<ApiResponse> responseEntity1 = commentClient.countByMemberId(memberId);
        ApiResponse response1 = responseEntity1.getBody();
        Integer commentCount = response1.toObject(Integer.class);

        ResponseEntity<ApiResponse> responseEntity2 = commentReplyClient.countByMemberId(memberId);
        ApiResponse response2 = responseEntity2.getBody();
        Integer replyCount = response2.toObject(Integer.class);

        //查找用户信息
        ResponseEntity<ApiResponse> responseEntity3 = memberClient.getById(memberId);
        ApiResponse response3 = responseEntity3.getBody();
        MemberDTO memberDTO = response3.toObject(MemberDTO.class);

        PersonCenterResult result = new PersonCenterResult();
        result.setToPayOrderCount(toPayOrderCount);
        result.setToDeliveryOrderCount(toDeliveryOrderCount);
        result.setToReceiveOrderCount(toReceiveOrderCount);
        result.setFinishOrderCount(finishOrderCount);
        result.setCancelOrderCount(cancelOrderCount);
        result.setCommentCount(commentCount);
        result.setReplyCount(replyCount);
        result.setMemberIcon(memberDTO.getIcon());
        result.setMemberNickname(memberDTO.getNickname());
        return result;
    }

    public List<OrderChartDTO> getOrderChart(String start, String end) {
        Map<String, Object> map = new HashMap<>();
        String startDate = start + " 00:00:00";
        String endDate = end + " 23:59:59";

        map.put("start", startDate);
        map.put("end", endDate);
        return orderMapper.getOrderChart(map);
    }

    public void selectOrderRule(OmsOrderExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "id desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(OmsOrderExample example, ListParam listParam) {
        OmsOrderExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "orderSn":
                    criteria.andOrderSnEqualTo(value);
                    break;
                case "status":
                    criteria.andStatusEqualTo(Integer.valueOf(value));
                    break;
                case "deleteStatus":
                    criteria.andDeleteStatusEqualTo(Integer.valueOf(value));
                    break;
                case "memberId":
                    criteria.andMemberIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
