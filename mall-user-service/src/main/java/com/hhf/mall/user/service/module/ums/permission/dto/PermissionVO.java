package com.hhf.mall.user.service.module.ums.permission.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 10:06
 */
@Data
public class PermissionVO {
    private Long id;
    private String title;
    private String value;
    private Integer status;
    private String statusText;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
}
