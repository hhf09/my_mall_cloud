package com.hhf.mall.product.service.module.pms.commentReply.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 11:24
 */
@Data
public class CommentReplyDTO {
    private Long id;
    private Long commentId;
    private Long memberId;
    private String content;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private Integer type;
    private String typeText;
    private String memberNickname;
    private String memberIcon;
}
