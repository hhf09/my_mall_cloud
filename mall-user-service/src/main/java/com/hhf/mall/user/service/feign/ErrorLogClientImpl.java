package com.hhf.mall.user.service.feign;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.user.ErrorLogDTO;
import com.hhf.mall.feign.user.ErrorLogClient;
import com.hhf.mall.user.service.module.sys.errorLog.dto.ErrorLogVO;
import com.hhf.mall.user.service.module.sys.errorLog.dto.ErrorLogVOMapper;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLog;
import com.hhf.mall.user.service.module.sys.errorLog.service.ErrorLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023-01-27 21:06
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/errorLog")
public class ErrorLogClientImpl implements ErrorLogClient {
    @Autowired
    private ErrorLogService errorLogService;
    @Autowired
    private ErrorLogVOMapper errorLogVOMapper;

    @Override
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(ErrorLogDTO dto) {
        SysErrorLog entity = new SysErrorLog();
        entity.setDefault();
        BeanUtils.copyProperties(dto, entity);
        errorLogService.add(entity);
        return ApiResponse.success();
    }

    @Override
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(ListParam listParam) {
        List<SysErrorLog> list = errorLogService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SysErrorLog> pageInfo = new PageInfo<>(list);
            List<ErrorLogVO> voList = errorLogVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

}
