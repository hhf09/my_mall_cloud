package com.hhf.mall.user.service.module.ums.permission.entity;

/**
 * @author hhf
 * @description: 权限状态枚举
 * @date 2022/12/28 10:50
 */
public enum PermissionStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    PermissionStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static PermissionStatus instanceOf(int key) {
        for (PermissionStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
