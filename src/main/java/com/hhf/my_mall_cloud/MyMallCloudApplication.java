package com.hhf.my_mall_cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyMallCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyMallCloudApplication.class, args);
	}

}
