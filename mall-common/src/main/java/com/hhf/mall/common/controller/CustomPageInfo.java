package com.hhf.mall.common.controller;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hhf
 * @description: 自定义分页信息
 * @date 2023/2/10 17:24
 */
@Data
public class CustomPageInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 数据总数
     */
    private long total;
    /**
     * 页码
     */
    private int pageNum;
    /**
     * 页大小
     */
    private int pageSize;
    /**
     * 页数
     */
    private int pageCount;

    public CustomPageInfo() {
    }

    /**
     * 构建分页信息
     * @param total
     * @param pageNum
     * @param pageSize
     * @param pageCount
     * @return
     */
    public static CustomPageInfo build(long total, int pageNum, int pageSize, int pageCount) {
        CustomPageInfo customPageInfo = new CustomPageInfo();
        customPageInfo.setTotal(total);
        customPageInfo.setPageNum(pageNum);
        customPageInfo.setPageSize(pageSize);
        customPageInfo.setPageCount(pageCount);
        return customPageInfo;
    }
}
