package com.hhf.mall.dto.product;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:45
 */
@Data
public class FeightTemplateDTO {
    private Long id;

    /**
     * 包装类型
     *
     * @mbggenerated
     */
    private String packingType;

    /**
     * 重量
     *
     * @mbggenerated
     */
    private String weight;

    /**
     * 是否易碎
     *
     * @mbggenerated
     */
    private String isFragile;

    /**
     * 总运费
     *
     * @mbggenerated
     */
    private BigDecimal totalFeight;
}
