package com.hhf.mall.common.dto;

import java.util.List;

/**
 * @author hhf
 * @description: 底层DTOMapper
 * @date 2023/1/9 9:44
 */
public abstract class BaseDTOMapper<E, D> {
    /**
     * entity -> dto
     * @param entity
     * @return
     */
    public abstract D mapToDTO(E entity);

    /**
     * List<entity> -> List<dto>
     * @param entityList
     * @return
     */
    public abstract List<D> mapToDTOList(List<E> entityList);

}
