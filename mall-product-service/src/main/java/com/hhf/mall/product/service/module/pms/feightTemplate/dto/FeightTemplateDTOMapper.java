package com.hhf.mall.product.service.module.pms.feightTemplate.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:50
 */
@Component
public class FeightTemplateDTOMapper extends BaseDTOMapper<PmsFeightTemplate, FeightTemplateDTO> {
    @Override
    public FeightTemplateDTO mapToDTO(PmsFeightTemplate entity) {
        FeightTemplateDTO dto = new FeightTemplateDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<FeightTemplateDTO> mapToDTOList(List<PmsFeightTemplate> entityList) {
        List<FeightTemplateDTO> dtoList = new ArrayList<>();
        for (PmsFeightTemplate entity : entityList) {
            FeightTemplateDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
