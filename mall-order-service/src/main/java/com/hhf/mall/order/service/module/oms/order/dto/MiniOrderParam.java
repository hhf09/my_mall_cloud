package com.hhf.mall.order.service.module.oms.order.dto;

import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItem;
import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description 提交订单的参数
 * @date 2022-02-05 11:58
 */
@Data
public class MiniOrderParam {
    //购买商品列表
    private List<OmsCartItem> cartItemList;

    //用户id
    private Long memberId;

    //商品合计金额
    private double totalAmount;

    //运费
    private double freightAmount;

    //收货地址
    private Long receiveAddressId;
}
