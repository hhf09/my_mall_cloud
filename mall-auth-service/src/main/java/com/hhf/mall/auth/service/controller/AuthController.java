package com.hhf.mall.auth.service.controller;

import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.auth.TokenDTO;
import com.hhf.mall.feign.auth.AuthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/7 16:57
 */
@RestController
@RequestMapping("/oauth")
public class AuthController implements AuthClient {
    @Autowired
    private TokenEndpoint tokenEndpoint;
    @Autowired
    private HttpServletRequest request;

    /**
     * 获取token
     * 刷新token
     * @param grant_type
     * @param client_id
     * @param client_secret
     * @param refresh_token
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/token")
    public ResponseEntity<ApiResponse> token(@RequestParam String grant_type,
                                             @RequestParam String client_id,
                                             @RequestParam String client_secret,
                                             @RequestParam(required = false) String refresh_token,
                                             @RequestParam(required = false) String username,
                                             @RequestParam(required = false) String password) throws HttpRequestMethodNotSupportedException {
        Map<String, String> map = new HashMap<>();
        map.put("grant_type", grant_type);
        map.put("client_id", client_id);
        map.put("client_secret", client_secret);
        map.putIfAbsent("refresh_token", refresh_token);
        map.putIfAbsent("username", username);
        map.putIfAbsent("password", password);
        OAuth2AccessToken oAuth2AccessToken = tokenEndpoint.postAccessToken(request.getUserPrincipal(), map).getBody();
        TokenDTO dto = TokenDTO.builder()
                .token(oAuth2AccessToken.getValue())
                .refreshToken(oAuth2AccessToken.getRefreshToken().getValue())
                //注意不加空格
                .tokenHead(AuthConst.TOKEN_HEADER)
                .expiresIn(oAuth2AccessToken.getExpiresIn()).build();
        return ApiResponse.success(dto);
    }
}
