package com.hhf.mall.feign.user;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author hhf
 * @description: 权限feign接口
 * @date 2023/2/15 10:34
 */
@FeignClient(value = Const.USER_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "PermissionClient")
public interface PermissionClient {
    String URL_PREFIX = Const.USER_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/permission";

    /**
     * 查找全部权限标志
     * @return
     */
    @GetMapping(URL_PREFIX + "/listValues")
    ResponseEntity<ApiResponse> listValues();
}
