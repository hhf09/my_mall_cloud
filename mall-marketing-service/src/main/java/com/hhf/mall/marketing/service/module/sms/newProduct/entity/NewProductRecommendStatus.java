package com.hhf.mall.marketing.service.module.sms.newProduct.entity;

/**
 * @author hhf
 * @description: 新品推荐状态枚举
 * @date 2022/12/28 10:50
 */
public enum NewProductRecommendStatus {
    NO(0, "否"),
    YES(1, "是");

    private final int key;
    private final String text;

    NewProductRecommendStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static NewProductRecommendStatus instanceOf(int key) {
        for (NewProductRecommendStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
