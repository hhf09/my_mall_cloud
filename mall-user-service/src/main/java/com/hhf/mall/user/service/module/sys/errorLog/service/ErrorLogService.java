package com.hhf.mall.user.service.module.sys.errorLog.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLog;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLogExample;
import com.hhf.mall.user.service.module.sys.errorLog.mapper.SysErrorLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2023-01-26 23:46
 */
@Service
public class ErrorLogService extends BaseService<SysErrorLog, Long, SysErrorLogExample> {
    @Autowired
    private SysErrorLogMapper errorLogMapper;

    @Override
    protected void before(SysErrorLog entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            entity.setCreateTime(new Date());
        }
    }

    @Override
    protected void after(SysErrorLog entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<SysErrorLog, Long, SysErrorLogExample> getMapper() {
        return errorLogMapper;
    }

    public List<SysErrorLog> list(ListParam listParam) {
        SysErrorLogExample example = new SysErrorLogExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return errorLogMapper.selectByExample(example);
    }

    public void selectOrderRule(SysErrorLogExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "id desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(SysErrorLogExample example, ListParam listParam) {
        SysErrorLogExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "createTimeBt":
                    String[] strings = value.split(",");
                    Date start = DateUtil.parse(strings[0]);
                    Date end = DateUtil.parse(strings[1]);
                    criteria.andCreateTimeBetween(start, end);
                    break;
                case "serviceSign":
                    criteria.andServiceSignEqualTo(value);
                    break;
            }
        }
    }
}
