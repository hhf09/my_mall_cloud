package com.hhf.mall.marketing.service.module.sms.advertise.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertise;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertiseExample;
import com.hhf.mall.marketing.service.module.sms.advertise.mapper.SmsHomeAdvertiseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-23 23:09
 */
@Service
public class AdvertiseService extends BaseService<SmsHomeAdvertise, Long, SmsHomeAdvertiseExample> {
    @Autowired
    private SmsHomeAdvertiseMapper advertiseMapper;
    @Autowired
    private ProductClient productClient;

    @Override
    protected void before(SmsHomeAdvertise entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(SmsHomeAdvertise entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<SmsHomeAdvertise, Long, SmsHomeAdvertiseExample> getMapper() {
        return advertiseMapper;
    }

    public List<SmsHomeAdvertise> list(ListParam listParam) {
        SmsHomeAdvertiseExample example = new SmsHomeAdvertiseExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return advertiseMapper.selectByExample(example);
    }

    public void selectOrderRule(SmsHomeAdvertiseExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "sort desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(SmsHomeAdvertiseExample example, ListParam listParam) {
        SmsHomeAdvertiseExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "status":
                    criteria.andStatusEqualTo(Integer.valueOf(value));
                    break;
                case "nameLike":
                case ListParam.KEYWORD:
                    criteria.andNameLike("%" + value + "%");
                    break;
            }
        }
    }
}
