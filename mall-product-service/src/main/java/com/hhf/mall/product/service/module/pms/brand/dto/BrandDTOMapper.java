package com.hhf.mall.product.service.module.pms.brand.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.brand.entity.BrandStatus;
import com.hhf.mall.product.service.module.pms.brand.entity.PmsBrand;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/12 9:33
 */
@Component
public class BrandDTOMapper extends BaseDTOMapper<PmsBrand, BrandDTO> {
    @Override
    public BrandDTO mapToDTO(PmsBrand entity) {
        BrandDTO dto = new BrandDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setShowStatusText(BrandStatus.instanceOf(entity.getShowStatus()).getText());
        return dto;
    }

    @Override
    public List<BrandDTO> mapToDTOList(List<PmsBrand> entityList) {
        List<BrandDTO> dtoList = new ArrayList<>();
        for (PmsBrand entity : entityList) {
            BrandDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
