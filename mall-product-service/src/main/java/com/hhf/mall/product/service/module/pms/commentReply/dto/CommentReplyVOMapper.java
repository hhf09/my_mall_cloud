package com.hhf.mall.product.service.module.pms.commentReply.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.user.MemberDTO;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.product.service.module.pms.commentReply.entity.CommentReplyType;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReply;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 11:57
 */
@Component
public class CommentReplyVOMapper extends BaseDTOMapper<PmsCommentReply, CommentReplyVO> {
    @Autowired
    private MemberClient memberClient;

    @Override
    public CommentReplyVO mapToDTO(PmsCommentReply entity) {
        CommentReplyVO vo = new CommentReplyVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setTypeText(CommentReplyType.instanceOf(entity.getType()).getText());
        ResponseEntity<ApiResponse> responseEntity = memberClient.getById(entity.getMemberId());
        ApiResponse response = responseEntity.getBody();
        MemberDTO memberDTO = response.toObject(MemberDTO.class);
        vo.setMemberIcon(memberDTO.getIcon());
        vo.setMemberNickname(memberDTO.getNickname());
        return vo;
    }

    @Override
    public List<CommentReplyVO> mapToDTOList(List<PmsCommentReply> entityList) {
        List<CommentReplyVO> voList = new ArrayList<>();
        for (PmsCommentReply entity : entityList) {
            CommentReplyVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
