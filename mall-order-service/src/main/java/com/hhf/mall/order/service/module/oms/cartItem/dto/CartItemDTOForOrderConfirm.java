package com.hhf.mall.order.service.module.oms.cartItem.dto;

import com.hhf.mall.dto.user.ReceiveAddrDTO;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItem;
import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/13 9:56
 */
@Data
public class CartItemDTOForOrderConfirm {
    private List<OmsCartItem> list;
    private ReceiveAddrDTO address;
    private double totalAmount;
    private int feightAmount;
    private int totalQuantity;
}
