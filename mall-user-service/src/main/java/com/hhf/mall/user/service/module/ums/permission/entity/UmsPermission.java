package com.hhf.mall.user.service.module.ums.permission.entity;

import java.io.Serializable;
import java.util.Date;

public class UmsPermission implements Serializable {


    private Long id;

    /**
     * 名称
     *
     * @mbggenerated
     */
    private String title;

    /**
     * 权限标志
     *
     * @mbggenerated
     */
    private String value;

    /**
     * 启用状态；0->禁用；1->启用
     *
     * @mbggenerated
     */
    private Integer status;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setDefault() {
        this.setCreateTime(new Date());
        this.setStatus(PermissionStatus.STATUS_ENABLE.getKey());
    }
}