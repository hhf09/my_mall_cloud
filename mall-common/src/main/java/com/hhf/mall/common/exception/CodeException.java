package com.hhf.mall.common.exception;

import org.springframework.http.HttpStatus;

/**
 * @author hhf
 * @description: 业务逻辑异常
 * @date 2022/12/28 10:20
 */
public class CodeException extends RuntimeException {
    private int code;
    private String message;
    private HttpStatus httpStatus;

    public CodeException() {
    }

    public CodeException(String message) {
        this.message = message;
    }

    public CodeException(int code, String message) {
        this.code = code;
        this.message = message;
        this.httpStatus = HttpStatus.OK;
    }

    public CodeException(int code, String message, HttpStatus httpStatus) {
        this.code = code;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public CodeException(ApiCode apiCode) {
        this.code = apiCode.getCode();
        this.message = apiCode.getMessage();
        this.httpStatus = apiCode.getHttpStatus();
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
