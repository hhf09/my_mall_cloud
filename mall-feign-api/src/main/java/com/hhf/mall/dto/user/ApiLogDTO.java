package com.hhf.mall.dto.user;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023-01-27 21:03
 */
@Data
public class ApiLogDTO {
    private Long id;
    private String reqMethod;
    private String ip;
    private String userAgent;
    private String note;
    private String reqParams;
    private Long userId;
    private String token;
    private Integer userType;
    private String serviceSign;
}
