package com.hhf.mall.user.service.module.ums.role.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import com.hhf.mall.user.service.module.ums.permission.service.PermissionService;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRoleExample;
import com.hhf.mall.user.service.module.ums.role.mapper.UmsRoleMapper;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelation;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelationExample;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.service.RolePermissionRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-01 23:27
 */
@Service
public class RoleService extends BaseService<UmsRole, Long, UmsRoleExample> {
    @Autowired
    private UmsRoleMapper roleMapper;

    @Autowired
    private RolePermissionRelationService rolePermissionRelationService;

    @Autowired
    private PermissionService permissionService;

    @Override
    protected void before(UmsRole entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(UmsRole entity, ActionType actionType) {
        if (actionType == ActionType.DELETE) {
            UmsRolePermissionRelationExample example = new UmsRolePermissionRelationExample();
            ListParam listParam = new ListParam().add("roleId", String.valueOf(entity.getId()));
            rolePermissionRelationService.selectCondition(example, listParam);
            rolePermissionRelationService.deleteByExample(example);
        }
    }

    @Override
    protected BaseMapper<UmsRole, Long, UmsRoleExample> getMapper() {
        return roleMapper;
    }

    public List<UmsRole> list(ListParam listParam) {
        UmsRoleExample example = new UmsRoleExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return roleMapper.selectByExample(example);
    }

    public List<UmsPermission> listMenu(Long roleId) {
        return permissionService.getMenuListByRoleId(roleId);
    }

    public void allocMenu(Long roleId, List<Long> permissionIds) {
        //删除旧的角色-权限关系
        UmsRolePermissionRelationExample example = new UmsRolePermissionRelationExample();
        ListParam listParam = new ListParam().add("roleId", String.valueOf(roleId));
        rolePermissionRelationService.selectCondition(example, listParam);
        rolePermissionRelationService.deleteByExample(example);

        //添加新的角色-权限关系
        List<UmsRolePermissionRelation> rolePermissionRelationList = new ArrayList<>();
        for (Long permissionId : permissionIds) {
            UmsRolePermissionRelation rolePermissionRelation = new UmsRolePermissionRelation();
            rolePermissionRelation.setRoleId(roleId);
            rolePermissionRelation.setPermissionId(permissionId);
            rolePermissionRelationList.add(rolePermissionRelation);
        }

        rolePermissionRelationService.insertAll(rolePermissionRelationList);
    }

    public List<UmsRole> getRoleListByUserId(Long userId) {
        return roleMapper.getRoleListByUserId(userId);
    }

    public void selectOrderRule(UmsRoleExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(UmsRoleExample example, ListParam listParam) {
        UmsRoleExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case ListParam.KEYWORD:
                    criteria.andNameLike("%" + value + "%");
                    break;
                case "name":
                    criteria.andNameEqualTo(value);
                    break;
                case "value":
                    criteria.andValueEqualTo(value);
                    break;
            }
        }
    }
}
