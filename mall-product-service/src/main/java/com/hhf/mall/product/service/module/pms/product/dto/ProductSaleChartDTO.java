package com.hhf.mall.product.service.module.pms.product.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 商品销量图表
 * @date 2022-02-15 19:15
 */
@Data
public class ProductSaleChartDTO {
    //总销量
    private int totalSale;

    //商品名称
    private String productName;
}
