package com.hhf.mall.product.service.module.pms.productCategory.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PmsProductCategory implements Serializable {

    private Long id;

    /**
     * 上级分类的编号：0表示一级分类
     *
     * @mbggenerated
     */
    private Long parentId;

    /**
     * 名称
     *
     * @mbggenerated
     */
    private String name;

    /**
     * 分类级别：0->1级；1->2级
     *
     * @mbggenerated
     */
    private Integer level;

    /**
     * 显示状态：0->不显示；1->显示
     *
     * @mbggenerated
     */
    private Integer showStatus;

    /**
     * 排序
     *
     * @mbggenerated
     */
    private Integer sort;

    /**
     * 图标
     *
     * @mbggenerated
     */
    private String icon;

    /**
     * 关键词
     *
     * @mbggenerated
     */
    private String keywords;

    /**
     * 描述
     *
     * @mbggenerated
     */
    private String description;

    /**
     * 属性id列表
     */
    private List<Long> productAttributeIdList;

    /**
     * 更新时间
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(Integer showStatus) {
        this.showStatus = showStatus;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getProductAttributeIdList() {
        return productAttributeIdList;
    }

    public void setProductAttributeIdList(List<Long> productAttributeIdList) {
        this.productAttributeIdList = productAttributeIdList;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parentId=").append(parentId);
        sb.append(", name=").append(name);
        sb.append(", level=").append(level);
        sb.append(", showStatus=").append(showStatus);
        sb.append(", sort=").append(sort);
        sb.append(", icon=").append(icon);
        sb.append(", keywords=").append(keywords);
        sb.append(", description=").append(description);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public void setDefault() {
        this.setShowStatus(ProductCategoryStatus.STATUS_ENABLE.getKey());
        this.setSort(0);
        this.setUpdateTime(new Date());
    }
}