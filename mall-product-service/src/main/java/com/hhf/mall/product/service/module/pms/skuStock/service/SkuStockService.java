package com.hhf.mall.product.service.module.pms.skuStock.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStock;
import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStockExample;
import com.hhf.mall.product.service.module.pms.skuStock.mapper.PmsSkuStockMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 10:32
 */
@Service
public class SkuStockService extends BaseService<PmsSkuStock, Long, PmsSkuStockExample> {
    @Autowired
    private PmsSkuStockMapper skuStockMapper;

    @Override
    protected void before(PmsSkuStock entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(PmsSkuStock entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsSkuStock, Long, PmsSkuStockExample> getMapper() {
        return skuStockMapper;
    }

    public void selectOrderRule(PmsSkuStockExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsSkuStockExample example, ListParam listParam) {
        PmsSkuStockExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "productId":
                    criteria.andProductIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
