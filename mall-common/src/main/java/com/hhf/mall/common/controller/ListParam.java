package com.hhf.mall.common.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author hhf
 * @description: 请求参数列表
 * @date 2023/1/4 10:16
 */
@Data
public class ListParam {
    /**
     * 参数列表
     */
    private List<Param> paramList;

    /**
     * 页码
     */
    public static final String PAGENUM = "pageNum";
    /**
     * 页大小
     */
    public static final String PAGESIZE = "pageSize";
    /**
     * 排序规则
     */
    public static final String ORDERTYPE = "orderType";

    /**
     * 后台搜索关键字
     */
    public static final String KEYWORD = "keyword";

    public ListParam() {
        paramList = new ArrayList<>();
    }

    /**
     * 从request获取参数
     * @param request
     * @param paramKeys
     * @return
     */
    public ListParam create(HttpServletRequest request, String[] paramKeys) {
        if (CollUtil.isEmpty(Arrays.asList(paramKeys))) {
            return this;
        }
        for (String paramKey : paramKeys) {
            String paramValue = request.getParameter(paramKey);
            if (StrUtil.isNotEmpty(paramValue)) {
                paramList.add(new Param(paramKey, paramValue));
            }
        }
        //分页参数
        String pageNum = request.getParameter(PAGENUM);
        String pageSize = request.getParameter(PAGESIZE);
        paramList.add(new Param(PAGENUM, StrUtil.isNotEmpty(pageNum) ? pageNum : "1"));
        paramList.add(new Param(PAGESIZE, StrUtil.isNotEmpty(pageSize) ? pageSize : "5"));
        //排序规则
        String orderType = request.getParameter(ORDERTYPE);
        paramList.add(new Param(ORDERTYPE, StrUtil.isNotEmpty(orderType) ? orderType : "1"));
        return this;
    }

    /**
     * 单独添加key-value参数
     * @param paramKey
     * @param paramValue
     * @return
     */
    public ListParam add(String paramKey, String paramValue) {
        if (!StrUtil.isNotEmpty(paramKey) || !StrUtil.isNotEmpty(paramValue)) {
            return this;
        }
        paramList.add(new Param(paramKey, paramValue));
        paramList.add(new Param(ORDERTYPE, "1"));
        return this;
    }

    /**
     * 根据key移除参数
     * @param paramKey
     * @return
     */
    public ListParam remove(String paramKey) {
        if (!StrUtil.isNotEmpty(paramKey)) {
            return this;
        }
        paramList.removeIf(param -> param.getKey().equals(paramKey));
        return this;
    }

    /**
     * 根据key获取参数value
     * @param paramKey
     * @return
     */
    public String getParamValue(String paramKey) {
        if (!StrUtil.isNotEmpty(paramKey)) {
            return null;
        }
        for (Param param : paramList) {
            if (param.getKey().equals(paramKey)) {
                return param.getValue();
            }
        }
        return null;
    }

    /**
     * 清空参数
     * @return
     */
    public ListParam clear() {
        paramList.clear();
        return this;
    }
}
