package com.hhf.mall.user.service.module.ums.userRoleRelation.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.ums.userRoleRelation.entity.UmsUserRoleRelation;
import com.hhf.mall.user.service.module.ums.userRoleRelation.entity.UmsUserRoleRelationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsUserRoleRelationMapper extends BaseMapper<UmsUserRoleRelation, Long, UmsUserRoleRelationExample> {
    int countByExample(UmsUserRoleRelationExample example);

    int deleteByExample(UmsUserRoleRelationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsUserRoleRelation record);

    int insertSelective(UmsUserRoleRelation record);

    List<UmsUserRoleRelation> selectByExample(UmsUserRoleRelationExample example);

    UmsUserRoleRelation selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsUserRoleRelation record, @Param("example") UmsUserRoleRelationExample example);

    int updateByExample(@Param("record") UmsUserRoleRelation record, @Param("example") UmsUserRoleRelationExample example);

    int updateByPrimaryKeySelective(UmsUserRoleRelation record);

    int updateByPrimaryKey(UmsUserRoleRelation record);

    //自定义start

    /**
     * 批量添加
     * @param list
     */
    void insertAll(List<UmsUserRoleRelation> list);
}