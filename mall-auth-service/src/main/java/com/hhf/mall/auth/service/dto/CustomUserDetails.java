package com.hhf.mall.auth.service.dto;

import com.hhf.mall.common.dto.UserDTOForAuth;
import cn.hutool.core.collection.CollUtil;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author hhf
 * @description: 自定义用户详细信息
 * @date 2023/2/7 12:14
 */
@Data
public class CustomUserDetails implements UserDetails {
    /**
     * 用户id
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 是否启用
     */
    private Boolean enabled;
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 权限列表
     */
    private Collection<SimpleGrantedAuthority> authorities;
    /**
     * 角色列表
     */
    private List<String> roles;

    public CustomUserDetails() {
    }

    public CustomUserDetails(UserDTOForAuth dto) {
        setId(dto.getId());
        setUsername(dto.getUsername());
        setPassword(dto.getPassword());
        setClientId(dto.getClientId());
        setEnabled(dto.getStatus() == 1);
        if (CollUtil.isNotEmpty(dto.getAuthorities())) {
            authorities = new ArrayList<>();
            dto.getAuthorities().forEach(item -> authorities.add(new SimpleGrantedAuthority(item)));
        }
        if (CollUtil.isNotEmpty(dto.getRoles())) {
            setRoles(dto.getRoles());
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
