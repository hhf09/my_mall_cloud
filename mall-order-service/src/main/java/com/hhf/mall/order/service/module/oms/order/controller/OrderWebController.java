package com.hhf.mall.order.service.module.oms.order.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.order.service.module.oms.order.dto.MiniOrderParam;
import com.hhf.mall.order.service.module.oms.order.dto.OrderDTOForAdd;
import com.hhf.mall.order.service.module.oms.order.dto.OrderDetailDTO;
import com.hhf.mall.order.service.module.oms.order.dto.OrderDetailDTOMapper;
import com.hhf.mall.order.service.module.oms.order.dto.PersonCenterResult;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrder;
import com.hhf.mall.order.service.module.oms.order.entity.OrderDeleteStatus;
import com.hhf.mall.order.service.module.oms.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 订单
 * @date 2022-02-05 11:56
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/order")
@Validated
public class OrderWebController {
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailDTOMapper orderDetailDTOMapper;

    /**
     * 提交订单，未付款
     *
     * @param param
     * @return
     */
    @ApiLog(note = "提交订单")
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody MiniOrderParam param) {
        OrderDTOForAdd dto = orderService.add(param);
        return ApiResponse.success(dto);
    }

    /**
     * 更新支付信息，库存，清空购物车
     * @param id
     * @return
     */
    @ApiLog(note = "更新支付信息")
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @PostMapping("/updatePayInfo")
    public ResponseEntity<ApiResponse> updatePayInfo(@RequestParam Long id, @RequestParam List<Long> cartItemIds) {
        orderService.updatePayInfo(id, cartItemIds);
        return ApiResponse.success();
    }

    /**
     * 查找订单列表
     * @param memberId
     * @param status
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @GetMapping("/get")
    public ResponseEntity<ApiResponse> get(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId", "status"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("deleteStatus", String.valueOf(OrderDeleteStatus.NO.getKey()));
        List<OmsOrder> list = orderService.get(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<OmsOrder> pageInfo = new PageInfo<>(list);
            List<OrderDetailDTO> dtoList = orderDetailDTOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(dtoList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 查看订单详情
     * @param orderId
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @GetMapping("/detail")
    public ResponseEntity<ApiResponse> detail(@RequestParam Long orderId) {
        OrderDetailDTO dto = orderService.detail(orderId);
        return ApiResponse.success(dto);
    }

    /**
     * 收货
     * @param id
     * @return
     */
    @ApiLog(note = "收货")
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @PostMapping("/updateReceive")
    public ResponseEntity<ApiResponse> updateReceive(@RequestParam Long id) {
        orderService.updateReceive(id);
        return ApiResponse.success();
    }

    /**
     * 修改删除状态
     * @param id
     * @return
     */
    @ApiLog(note = "修改删除状态")
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        orderService.delete(id, "用户");
        return ApiResponse.success();
    }

    /**
     * 取消订单
     * @param id
     * @return
     */
    @ApiLog(note = "取消订单")
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @PostMapping("/updateCancel")
    public ResponseEntity<ApiResponse> updateCancel(@RequestParam Long id) {
        orderService.updateCancel(id);
        return ApiResponse.success();
    }

    /**
     * 查找订单统计数据
     * @param memberId
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_ORDER)
    @GetMapping("/getStatInfo")
    public ResponseEntity<ApiResponse> getStatInfo(@RequestParam Long memberId) {
        PersonCenterResult dto = orderService.getStatInfo(memberId);
        return ApiResponse.success(dto);
    }
}
