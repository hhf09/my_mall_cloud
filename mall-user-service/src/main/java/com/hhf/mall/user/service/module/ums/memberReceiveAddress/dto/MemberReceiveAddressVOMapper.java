package com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.MemberReceiveAddressDefaultStatus;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddress;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 10:47
 */
@Component
public class MemberReceiveAddressVOMapper extends BaseDTOMapper<UmsMemberReceiveAddress, MemberReceiveAddressVO> {
    @Override
    public MemberReceiveAddressVO mapToDTO(UmsMemberReceiveAddress entity) {
        MemberReceiveAddressVO vo = new MemberReceiveAddressVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setDefaultStatusText(MemberReceiveAddressDefaultStatus.instanceOf(entity.getDefaultStatus()).getText());
        return vo;
    }

    @Override
    public List<MemberReceiveAddressVO> mapToDTOList(List<UmsMemberReceiveAddress> entityList) {
        List<MemberReceiveAddressVO> voList = new ArrayList<>();
        for (UmsMemberReceiveAddress entity : entityList) {
            MemberReceiveAddressVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
