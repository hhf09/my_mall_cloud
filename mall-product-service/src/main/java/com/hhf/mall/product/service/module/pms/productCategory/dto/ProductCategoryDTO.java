package com.hhf.mall.product.service.module.pms.productCategory.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 15:46
 */
@Data
public class ProductCategoryDTO {
    private Long id;
    private Long parentId;
    private String name;
    private Integer level;
    private String levelText;
    private Integer showStatus;
    private String showStatusText;
    private Integer sort;
    private String icon;
    private String keywords;
    private String description;
}
