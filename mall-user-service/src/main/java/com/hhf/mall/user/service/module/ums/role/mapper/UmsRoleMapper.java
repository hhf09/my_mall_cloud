package com.hhf.mall.user.service.module.ums.role.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsRoleMapper extends BaseMapper<UmsRole, Long, UmsRoleExample> {
    int countByExample(UmsRoleExample example);

    int deleteByExample(UmsRoleExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsRole record);

    int insertSelective(UmsRole record);

    List<UmsRole> selectByExample(UmsRoleExample example);

    UmsRole selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsRole record, @Param("example") UmsRoleExample example);

    int updateByExample(@Param("record") UmsRole record, @Param("example") UmsRoleExample example);

    int updateByPrimaryKeySelective(UmsRole record);

    int updateByPrimaryKey(UmsRole record);

    //自定义start

    /**
     * 根据用户id查询角色列表
     * @param userId
     * @return
     */
    List<UmsRole> getRoleListByUserId(Long userId);
}