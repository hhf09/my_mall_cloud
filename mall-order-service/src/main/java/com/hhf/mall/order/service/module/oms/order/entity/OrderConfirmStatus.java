package com.hhf.mall.order.service.module.oms.order.entity;

/**
 * @author hhf
 * @description: 订单确认收货状态枚举
 * @date 2022/12/28 10:50
 */
public enum OrderConfirmStatus {
    NO(0, "否"),
    YES(1, "是");

    private final int key;
    private final String text;

    OrderConfirmStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static OrderConfirmStatus instanceOf(int key) {
        for (OrderConfirmStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
