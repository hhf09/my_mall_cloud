package com.hhf.mall.auth.service.service;

import com.hhf.mall.auth.service.dto.CustomUserDetails;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.feign.user.AdminClient;
import com.hhf.mall.feign.user.MemberClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author hhf
 * @description: 自定义UserDetailsService
 * @date 2023/2/7 10:32
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AdminClient adminClient;
    @Autowired
    private MemberClient memberClient;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String clientId = request.getParameter("client_id");
        ResponseEntity<ApiResponse> responseEntity;
        //判断后台还是前台用户
        if (AuthConst.ADMIN_CLIENT_ID.equals(clientId)) {
            responseEntity = adminClient.loadUserByUsername(username);
        } else if (AuthConst.MEMBER_CLIENT_ID.equals(clientId)) {
            responseEntity = memberClient.loadUserByUsername(username);
        } else {
            throw new CodeException("client_id出错");
        }
        ApiResponse response = responseEntity.getBody();
        UserDTOForAuth dto = response.toObject(UserDTOForAuth.class);
        CustomUserDetails userDetails = new CustomUserDetails(dto);
        if (!userDetails.getEnabled()) {
            throw new DisabledException("该账号已被禁用");
        } else if (!userDetails.isAccountNonLocked()) {
            throw new LockedException("该账号已被锁定");
        } else if (!userDetails.isAccountNonExpired()) {
            throw new AccountExpiredException("该账号已过期");
        } else if (!userDetails.isCredentialsNonExpired()) {
            throw new CredentialsExpiredException("该账号的登录凭证已过期");
        }
        return userDetails;
    }
}
