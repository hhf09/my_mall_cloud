package com.hhf.mall.product.service.module.pms.comment.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.comment.dto.CommentDTO;
import com.hhf.mall.product.service.module.pms.comment.dto.CommentDTOMapper;
import com.hhf.mall.product.service.module.pms.comment.dto.MiniCommentResult;
import com.hhf.mall.product.service.module.pms.comment.dto.MiniCommentStatInfo;
import com.hhf.mall.product.service.module.pms.comment.entity.CommentEvaluationStatus;
import com.hhf.mall.product.service.module.pms.comment.entity.CommentPictureStatus;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsComment;
import com.hhf.mall.product.service.module.pms.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 评论
 * @date 2022-02-03 17:36
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/comment")
@Validated
public class CommentWebController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentDTOMapper commentDTOMapper;

    /**
     * 获取评价统计信息
     *
     * @param productId
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_COMMENT)
    @GetMapping("/getStatInfo")
    public ResponseEntity<ApiResponse> getStatInfo(@RequestParam Long productId) {
        MiniCommentStatInfo dto = commentService.getStatInfo(productId);
        return ApiResponse.success(dto);
    }

    /**
     * 分页获取评论
     *
     * @param productId
     * @param evaluate
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_COMMENT)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(@RequestParam Long productId,
                                              @RequestParam Integer evaluate,
                                              HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsComment> list = commentService.listByPage(productId, evaluate);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<PmsComment> pageInfo = new PageInfo<>(list);
            List<CommentDTO> dtoList = commentDTOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(dtoList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 查找评论信息及回复列表
     *
     * @param commentId
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_COMMENT)
    @GetMapping("/getInfo")
    public ResponseEntity<ApiResponse> getInfo(@RequestParam Long commentId,
                                                 @RequestParam(required = false) Integer orderType) {
        MiniCommentResult dto = commentService.getInfo(commentId, orderType);
        return ApiResponse.success(dto);
    }

    /**
     * 查找某个用户的所有评论
     * @param memberId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_COMMENT)
    @GetMapping("/listByMemberId")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsComment> list = commentService.listByMemberId(listParam);
        List<CommentDTO> dtoList = commentDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }

    /**
     * 添加评价
     *
     * @param comment
     * @return
     */
    @ApiLog(note = "添加评价")
    @RequirePermissions(PermissionConst.WEB_COMMENT)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody PmsComment comment) {
        comment.setDefault();
        Integer star = comment.getStar();
        if (star == 1) {
            comment.setEvaluationStatus(CommentEvaluationStatus.BAD.getKey());
        } else if (star == 2 || star == 3) {
            comment.setEvaluationStatus(CommentEvaluationStatus.MED.getKey());
        } else if (star == 4 || star == 5) {
            comment.setEvaluationStatus(CommentEvaluationStatus.GOOD.getKey());
        }
        String pics = comment.getPics();
        if (StrUtil.isNotEmpty(pics)) {
            comment.setPictureStatus(CommentPictureStatus.YES.getKey());
        } else {
            comment.setPictureStatus(CommentPictureStatus.NO.getKey());
        }
        commentService.add(comment);
        return ApiResponse.success();
    }
}
