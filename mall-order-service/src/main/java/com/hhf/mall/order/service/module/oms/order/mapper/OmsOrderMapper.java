package com.hhf.mall.order.service.module.oms.order.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.order.service.module.oms.order.dto.OrderChartDTO;
import com.hhf.mall.order.service.module.oms.order.dto.OrderDetailDTO;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrder;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrderExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OmsOrderMapper extends BaseMapper<OmsOrder, Long, OmsOrderExample> {
    int countByExample(OmsOrderExample example);

    int deleteByExample(OmsOrderExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OmsOrder record);

    int insertSelective(OmsOrder record);

    List<OmsOrder> selectByExample(OmsOrderExample example);

    OmsOrder selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OmsOrder record, @Param("example") OmsOrderExample example);

    int updateByExample(@Param("record") OmsOrder record, @Param("example") OmsOrderExample example);

    int updateByPrimaryKeySelective(OmsOrder record);

    int updateByPrimaryKey(OmsOrder record);

    //自定义start

    /**
     * 查询订单详情，包括关联商品、订单历史、收货信息
     * @param id
     * @return
     */
    OrderDetailDTO detail(Long id);

    /**
     * 查询订单图表
     * @param map
     * @return
     */
    List<OrderChartDTO> getOrderChart(Map<String, Object> map);
}