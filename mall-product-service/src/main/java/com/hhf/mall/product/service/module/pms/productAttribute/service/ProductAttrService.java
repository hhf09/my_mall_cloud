package com.hhf.mall.product.service.module.pms.productAttribute.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.productAttribute.dto.ProductAttrInfo;
import com.hhf.mall.product.service.module.pms.productAttribute.entity.PmsProductAttribute;
import com.hhf.mall.product.service.module.pms.productAttribute.entity.PmsProductAttributeExample;
import com.hhf.mall.product.service.module.pms.productAttribute.mapper.PmsProductAttributeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-09 20:44
 */
@Service
public class ProductAttrService extends BaseService<PmsProductAttribute, Long, PmsProductAttributeExample> {
    @Autowired
    private PmsProductAttributeMapper productAttributeMapper;

    @Override
    protected void before(PmsProductAttribute entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(PmsProductAttribute entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsProductAttribute, Long, PmsProductAttributeExample> getMapper() {
        return productAttributeMapper;
    }

    public List<ProductAttrInfo> getProductAttrInfo(Long productCategoryId) {
        return productAttributeMapper.getProductAttrInfo(productCategoryId);
    }

    public List<PmsProductAttribute> list(ListParam listParam) {
        PmsProductAttributeExample example = new PmsProductAttributeExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return productAttributeMapper.selectByExample(example);
    }

    public void selectOrderRule(PmsProductAttributeExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsProductAttributeExample example, ListParam listParam) {
        PmsProductAttributeExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "productAttributeCategoryId":
                    criteria.andProductAttributeCategoryIdEqualTo(Long.valueOf(value));
                    break;
                case "type":
                    criteria.andTypeEqualTo(Integer.valueOf(value));
                    break;
            }
        }
    }

}
