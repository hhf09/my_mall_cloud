package com.hhf.mall.marketing.service.module.sms.recommendProduct.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.dto.RecommendProductVO;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.dto.RecommendProductVOMapper;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.SmsHomeRecommendProduct;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.service.RecommendProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 人气推荐
 * @date 2022-01-23 21:55
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/recommendProduct")
@Validated
public class RecommendProductController {
    @Autowired
    private RecommendProductService recommendProductService;
    @Autowired
    private RecommendProductVOMapper recommendProductVOMapper;

    /**
     * 分页查找
     * @param recommendStatus
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_POPULAR_RECOMMEND)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"recommendStatus"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<SmsHomeRecommendProduct> list = recommendProductService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SmsHomeRecommendProduct> pageInfo = new PageInfo<>(list);
            List<RecommendProductVO> voList = recommendProductVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 修改推荐状态
     * @param id
     * @param recommendStatus
     * @return
     */
    @ApiLog(note = "更新推荐状态")
    @RequirePermissions(PermissionConst.ADMIN_POPULAR_RECOMMEND)
    @PostMapping("/updateRecommendStatus")
    public ResponseEntity<ApiResponse> updateRecommendStatus(@RequestParam Long id, @RequestParam Integer recommendStatus) {
        SmsHomeRecommendProduct recommendProduct = recommendProductService.selectByPrimaryKey(id);
        if (recommendProduct == null) {
            throw new CodeException("人气推荐" + id + "数据不存在");
        }
        recommendProduct.setRecommendStatus(recommendStatus);
        recommendProductService.updateByPrimaryKeySelective(recommendProduct);
        return ApiResponse.success();
    }

    /**
     * 修改排序
     * @param id
     * @param sort
     * @return
     */
    @ApiLog(note = "更新排序")
    @RequirePermissions(PermissionConst.ADMIN_POPULAR_RECOMMEND)
    @PostMapping("/updateSort")
    public ResponseEntity<ApiResponse> updateSort(@RequestParam Long id, @RequestParam Integer sort) {
        SmsHomeRecommendProduct recommendProduct = recommendProductService.selectByPrimaryKey(id);
        if (recommendProduct == null) {
            throw new CodeException("人气推荐" + id + "数据不存在");
        }
        recommendProduct.setSort(sort);
        recommendProductService.updateByPrimaryKeySelective(recommendProduct);
        return ApiResponse.success();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiLog(note = "删除人气推荐")
    @RequirePermissions(PermissionConst.ADMIN_POPULAR_RECOMMEND)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        recommendProductService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }

    /**
     * 批量添加
     * @param recommendProductList
     * @return
     */
    @ApiLog(note = "添加人气推荐")
    @RequirePermissions(PermissionConst.ADMIN_POPULAR_RECOMMEND)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody List<SmsHomeRecommendProduct> recommendProductList) {
        for (SmsHomeRecommendProduct recommendProduct : recommendProductList) {
            recommendProduct.setDefault();
            recommendProductService.add(recommendProduct);
        }
        return ApiResponse.success();
    }
}
