package com.hhf.mall.user.service.module.ums.member.dto;

import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023-02-12 23:36
 */
@Data
public class MemberDTOForChart {
    int totalCount;
    int todayCount;
    int currentMonthCount;
    List<MemberIncreaseChartDTO> list;
}
