package com.hhf.mall.product.service.module.pms.productAttributeCategory.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.dto.ProductAttributeCateItem;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.entity.PmsProductAttributeCategory;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.entity.PmsProductAttributeCategoryExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductAttributeCategoryMapper extends BaseMapper<PmsProductAttributeCategory, Long, PmsProductAttributeCategoryExample> {
    int countByExample(PmsProductAttributeCategoryExample example);

    int deleteByExample(PmsProductAttributeCategoryExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductAttributeCategory record);

    int insertSelective(PmsProductAttributeCategory record);

    List<PmsProductAttributeCategory> selectByExample(PmsProductAttributeCategoryExample example);

    PmsProductAttributeCategory selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductAttributeCategory record, @Param("example") PmsProductAttributeCategoryExample example);

    int updateByExample(@Param("record") PmsProductAttributeCategory record, @Param("example") PmsProductAttributeCategoryExample example);

    int updateByPrimaryKeySelective(PmsProductAttributeCategory record);

    int updateByPrimaryKey(PmsProductAttributeCategory record);

    //自定义start

    /**
     *
     * @return
     */
    List<ProductAttributeCateItem> getListWithAttr();
}