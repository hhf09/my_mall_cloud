package com.hhf.mall.user.service.module.ums.member.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 9:44
 */
@Data
public class MemberDTO {
    private Long id;
    private String username;
    private String password;
    private String nickname;
    private String phone;
    private Integer status;
    private String statusText;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private String icon;
    private Integer gender;
    private String genderText;
    private String openid;
}
