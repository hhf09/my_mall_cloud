package com.hhf.mall.feign.user;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hhf
 * @description: 管理员feign接口
 * @date 2023/2/7 11:41
 */
@FeignClient(value = Const.USER_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "AdminClient")
public interface AdminClient {
    String URL_PREFIX = Const.USER_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/admin";

    /**
     * 通过用户名查找
     * @param username
     * @return
     */
    @GetMapping(URL_PREFIX + "/loadUserByUsername")
    ResponseEntity<ApiResponse> loadUserByUsername(@RequestParam String username);
}
