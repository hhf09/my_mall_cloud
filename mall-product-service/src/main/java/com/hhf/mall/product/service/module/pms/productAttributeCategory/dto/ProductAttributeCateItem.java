package com.hhf.mall.product.service.module.pms.productAttributeCategory.dto;

import com.hhf.mall.product.service.module.pms.productAttribute.entity.PmsProductAttribute;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.entity.PmsProductAttributeCategory;
import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-09 19:53
 */
@Data
public class ProductAttributeCateItem extends PmsProductAttributeCategory {
    //商品属性列表
    private List<PmsProductAttribute> productAttributeList;
}
