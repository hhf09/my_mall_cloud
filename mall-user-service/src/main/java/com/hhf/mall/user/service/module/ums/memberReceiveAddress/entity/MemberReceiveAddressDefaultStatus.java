package com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity;

/**
 * @author hhf
 * @description: 收货地址默认状态枚举
 * @date 2022/12/28 10:50
 */
public enum MemberReceiveAddressDefaultStatus {
    NO(0, "否"),
    YES(1, "是");

    private final int key;
    private final String text;

    MemberReceiveAddressDefaultStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static MemberReceiveAddressDefaultStatus instanceOf(int key) {
        for (MemberReceiveAddressDefaultStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
