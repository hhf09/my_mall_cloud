package com.hhf.mall.product.service.module.pms.productCategory.controller;

import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.productCategory.dto.ProductCategoryDTO;
import com.hhf.mall.product.service.module.pms.productCategory.dto.ProductCategoryDTOMapper;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategory;
import com.hhf.mall.product.service.module.pms.productCategory.entity.ProductCategoryStatus;
import com.hhf.mall.product.service.module.pms.productCategory.service.ProductCateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 商品分类
 * @date 2022-02-02 15:19
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/productCate")
@Validated
public class ProductCateWebController {
    @Autowired
    private ProductCateService productCateService;
    @Autowired
    private ProductCategoryDTOMapper productCategoryDTOMapper;

    /**
     * @param parentId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_PRODUCT_CATEGORY)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"parentId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("showStatus", String.valueOf(ProductCategoryStatus.STATUS_ENABLE.getKey()));
        List<PmsProductCategory> list = productCateService.list(listParam);
        List<ProductCategoryDTO> dtoList = productCategoryDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }
}
