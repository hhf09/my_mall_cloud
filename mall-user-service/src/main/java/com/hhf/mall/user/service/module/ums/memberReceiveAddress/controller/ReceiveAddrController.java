package com.hhf.mall.user.service.module.ums.memberReceiveAddress.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto.MemberReceiveAddressVO;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto.MemberReceiveAddressVOMapper;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddress;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.service.ReceiveAddrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 收货地址
 * @date 2022-01-24 21:07
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/receiveAddress")
@Validated
public class ReceiveAddrController {

    @Autowired
    private ReceiveAddrService receiveAddrService;
    @Autowired
    private MemberReceiveAddressVOMapper memberReceiveAddressVOMapper;

    /**
     *
     * @param keyword
     * @param phone
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_MEMBER_ADDRESS)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{ListParam.KEYWORD, "phone"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<UmsMemberReceiveAddress> list = receiveAddrService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<UmsMemberReceiveAddress> pageInfo = new PageInfo<>(list);
            List<MemberReceiveAddressVO> voList = memberReceiveAddressVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @RequirePermissions(PermissionConst.ADMIN_MEMBER_ADDRESS)
    @GetMapping("/item")
    public ResponseEntity<ApiResponse> item(@RequestParam Long id) {
        UmsMemberReceiveAddress address = receiveAddrService.selectByPrimaryKey(id);
        if (address == null) {
            throw new CodeException("收货地址" + id + "数据不存在");
        }
        MemberReceiveAddressVO vo = memberReceiveAddressVOMapper.mapToDTO(address);
        return ApiResponse.success(vo);
    }

}
