package com.hhf.mall.product.service.module.pms.productCategory.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategory;
import com.hhf.mall.product.service.module.pms.productCategory.entity.ProductCategoryLevelType;
import com.hhf.mall.product.service.module.pms.productCategory.entity.ProductCategoryStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 15:49
 */
@Component
public class ProductCategoryVOMapper extends BaseDTOMapper<PmsProductCategory, ProductCategoryVO> {
    @Override
    public ProductCategoryVO mapToDTO(PmsProductCategory entity) {
        ProductCategoryVO vo = new ProductCategoryVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setShowStatusText(ProductCategoryStatus.instanceOf(entity.getShowStatus()).getText());
        vo.setLevelText(ProductCategoryLevelType.instanceOf(entity.getLevel()).getText());
        return vo;
    }

    @Override
    public List<ProductCategoryVO> mapToDTOList(List<PmsProductCategory> entityList) {
        List<ProductCategoryVO> voList = new ArrayList<>();
        for (PmsProductCategory entity : entityList) {
            ProductCategoryVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
