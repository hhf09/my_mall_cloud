package com.hhf.mall.dto.product;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 10:36
 */
@Data
public class SkuStockDTO {
    private Long id;

    private Long productId;

    /**
     * sku编码
     *
     * @mbggenerated
     */
    private String skuCode;

    /**
     * 价格
     *
     * @mbggenerated
     */
    private BigDecimal price;

    /**
     * 库存
     *
     * @mbggenerated
     */
    private Integer stock;

    /**
     * 预警库存
     *
     * @mbggenerated
     */
    private Integer lowStock;

    /**
     * 销售属性1
     *
     * @mbggenerated
     */
    private String sp;

    /**
     * 展示图片
     *
     * @mbggenerated
     */
    private String pic;

    /**
     * 销量
     *
     * @mbggenerated
     */
    private Integer sale;

    /**
     * 锁定库存
     *
     * @mbggenerated
     */
    private Integer lockStock;
}
