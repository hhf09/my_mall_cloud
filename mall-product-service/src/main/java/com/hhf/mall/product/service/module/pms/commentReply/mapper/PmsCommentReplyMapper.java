package com.hhf.mall.product.service.module.pms.commentReply.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReply;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReplyExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsCommentReplyMapper extends BaseMapper<PmsCommentReply, Long, PmsCommentReplyExample> {
    int countByExample(PmsCommentReplyExample example);

    int deleteByExample(PmsCommentReplyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsCommentReply record);

    int insertSelective(PmsCommentReply record);

    List<PmsCommentReply> selectByExample(PmsCommentReplyExample example);

    PmsCommentReply selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsCommentReply record, @Param("example") PmsCommentReplyExample example);

    int updateByExample(@Param("record") PmsCommentReply record, @Param("example") PmsCommentReplyExample example);

    int updateByPrimaryKeySelective(PmsCommentReply record);

    int updateByPrimaryKey(PmsCommentReply record);
}