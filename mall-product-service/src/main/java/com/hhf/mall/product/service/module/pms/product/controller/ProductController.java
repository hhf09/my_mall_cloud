package com.hhf.mall.product.service.module.pms.product.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplate;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplateExample;
import com.hhf.mall.product.service.module.pms.feightTemplate.mapper.PmsFeightTemplateMapper;
import com.hhf.mall.product.service.module.pms.product.dto.PmsProductParam;
import com.hhf.mall.product.service.module.pms.product.dto.PmsProductResult;
import com.hhf.mall.product.service.module.pms.product.dto.ProductSaleChartDTO;
import com.hhf.mall.product.service.module.pms.product.dto.ProductVO;
import com.hhf.mall.product.service.module.pms.product.dto.ProductVOMapper;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.product.entity.ProductDeleteStatus;
import com.hhf.mall.product.service.module.pms.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author hhf
 * @description 商品
 * @date 2022-01-10 23:23
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/product")
@Validated
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private PmsFeightTemplateMapper feightTemplateMapper;
    @Autowired
    private ProductVOMapper productVOMapper;

    /**
     *
     * @param keyword
     * @param productSn
     * @param brandId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{ListParam.KEYWORD, "productSn", "brandId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsProduct> list = productService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<PmsProduct> pageInfo = new PageInfo<>(list);
            List<ProductVO> voList = productVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @RequirePermissions(PermissionConst.ADMIN_PRODUCT)
    @PostMapping("/updateInfo")
    public ResponseEntity<ApiResponse> get(@RequestParam Long id) {
        PmsProductResult dto = productService.updateInfo(id);
        return ApiResponse.success(dto);
    }

    @ApiLog(note = "添加商品")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody PmsProductParam productParam) {
        productService.add(productParam);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新商品")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@RequestParam Long id, @Valid @RequestBody PmsProductParam productParam) {
        productService.update(id, productParam);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新商品删除状态")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT)
    @PostMapping("/updateDeleteStatus")
    public ResponseEntity<ApiResponse> updateDeleteStatus(@RequestParam Long id) {
        PmsProduct product = productService.selectByPrimaryKey(id);
        if (product == null) {
            throw new CodeException("商品" + id + "数据不存在");
        }
        product.setDeleteStatus(ProductDeleteStatus.YES.getKey());
        productService.updateByPrimaryKeySelective(product);
        return ApiResponse.success();
    }

    @Deprecated
    @GetMapping("/test")
    public void test() {
        List<PmsFeightTemplate> pmsFeightTemplates = feightTemplateMapper.selectByExample(new PmsFeightTemplateExample());
        for (int i = 0; i < pmsFeightTemplates.size(); i++) {

            PmsFeightTemplate template = pmsFeightTemplates.get(i);

            BigDecimal decimal = new BigDecimal(calTotalFeight(template));
            template.setTotalFeight(decimal);

            feightTemplateMapper.updateByPrimaryKeySelective(template);
        }

    }

    @Deprecated
    private int calTotalFeight(PmsFeightTemplate template) {
        int base = 5;
        if (template.getPackingType().equals("中包装")) {
            base += 2;
        } else if (template.getPackingType().equals("大包装")) {
            base += 4;
        }

        if (template.getWeight().equals("1-5kg")) {
            base += 2;
        } else if (template.getWeight().equals("大于5kg")) {
            base += 4;
        }

        if (template.getIsFragile().equals("是")) {
            base += 3;
        }
        return base;
    }

    @GetMapping("/saleChart")
    public ResponseEntity<ApiResponse> getSaleChart() {
        List<ProductSaleChartDTO> dtoList = productService.getSaleChart();
        return ApiResponse.success(dtoList);
    }

}
