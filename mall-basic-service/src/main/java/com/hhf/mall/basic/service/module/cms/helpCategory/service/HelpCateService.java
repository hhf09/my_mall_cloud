package com.hhf.mall.basic.service.module.cms.helpCategory.service;

import com.hhf.mall.basic.service.module.cms.help.entity.CmsHelpExample;
import com.hhf.mall.basic.service.module.cms.help.service.HelpService;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategory;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategoryExample;
import com.hhf.mall.basic.service.module.cms.helpCategory.mapper.CmsHelpCategoryMapper;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-29 0:02
 */
@Service
public class HelpCateService extends BaseService<CmsHelpCategory, Long, CmsHelpCategoryExample> {
    @Autowired
    private CmsHelpCategoryMapper helpCategoryMapper;

    @Autowired
    private HelpService helpService;

    @Override
    protected void before(CmsHelpCategory entity, ActionType actionType) {
        if (actionType == ActionType.DELETE) {
            CmsHelpExample example = new CmsHelpExample();
            ListParam listParam = new ListParam().add("categoryId", String.valueOf(entity.getId()));
            helpService.selectCondition(example, listParam);
            helpService.deleteByExample(example);
        }
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(CmsHelpCategory entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<CmsHelpCategory, Long, CmsHelpCategoryExample> getMapper() {
        return helpCategoryMapper;
    }

    public List<CmsHelpCategory> list(ListParam listParam) {
        CmsHelpCategoryExample example = new CmsHelpCategoryExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return helpCategoryMapper.selectByExample(example);
    }

    public void selectOrderRule(CmsHelpCategoryExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "sort desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(CmsHelpCategoryExample example, ListParam listParam) {
        CmsHelpCategoryExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "":
                    break;
            }
        }
    }
}
