package com.hhf.mall.mapper;

import com.hhf.mall.model.UmsAdminErrorLog;
import com.hhf.mall.model.UmsAdminErrorLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UmsAdminErrorLogMapper {
    int countByExample(UmsAdminErrorLogExample example);

    int deleteByExample(UmsAdminErrorLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsAdminErrorLog record);

    int insertSelective(UmsAdminErrorLog record);

    List<UmsAdminErrorLog> selectByExample(UmsAdminErrorLogExample example);

    UmsAdminErrorLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsAdminErrorLog record, @Param("example") UmsAdminErrorLogExample example);

    int updateByExample(@Param("record") UmsAdminErrorLog record, @Param("example") UmsAdminErrorLogExample example);

    int updateByPrimaryKeySelective(UmsAdminErrorLog record);

    int updateByPrimaryKey(UmsAdminErrorLog record);
}