package com.hhf.mall.order.service.module.oms.order.entity;

/**
 * @author hhf
 * @description: 订单状态枚举
 * @date 2022/12/28 10:50
 */
public enum OrderStatus {
    WAIT_FOR_PAY(0, "待付款"),
    WAIT_FOR_DELIVER(1, "待发货"),
    WAIT_FOR_RECEIVE(2, "待收货"),
    FINISH(3, "已完成"),
    CLOSED(4, "已关闭"),
    INVALID(5, "无效");

    private final int key;
    private final String text;

    OrderStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static OrderStatus instanceOf(int key) {
        for (OrderStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
