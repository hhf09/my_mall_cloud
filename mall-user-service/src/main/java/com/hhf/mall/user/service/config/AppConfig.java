package com.hhf.mall.user.service.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author hhf
 * @description: 应用配置
 * @date 2023/2/16 16:19
 */
@Data
@EqualsAndHashCode
@ConfigurationProperties(prefix = "app")
@Component
public class AppConfig {
    private String defaultPassword;
    private Mini mini;

    @Data
    public static class Mini {
        private String appId;
        private String appSecret;
        private String openIdUrl;
        private String grantType;
    }

}
