package com.hhf.mall.product.service.module.pms.comment.entity;

import java.io.Serializable;
import java.util.Date;

public class PmsComment implements Serializable {

    private Long id;

    private Long skuStockId;

    private Long memberId;

    /**
     * 评价星数：1->5
     *
     * @mbggenerated
     */
    private Integer star;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 是否显示：0->否；1->是
     *
     * @mbggenerated
     */
    private Integer showStatus;

    /**
     * 上传图片地址，以逗号隔开
     *
     * @mbggenerated
     */
    private String pics;

    /**
     * 回复数
     *
     * @mbggenerated
     */
    private Integer replyCount;

    /**
     * 评价程度(0->差评, 1->中评, 2->好评)
     *
     * @mbggenerated
     */
    private Integer evaluationStatus;

    /**
     * 是否有图(0->无, 1->有)
     *
     * @mbggenerated
     */
    private Integer pictureStatus;

    /**
     * 内容
     *
     * @mbggenerated
     */
    private String content;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSkuStockId() {
        return skuStockId;
    }

    public void setSkuStockId(Long skuStockId) {
        this.skuStockId = skuStockId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(Integer showStatus) {
        this.showStatus = showStatus;
    }

    public String getPics() {
        return pics;
    }

    public void setPics(String pics) {
        this.pics = pics;
    }

    public Integer getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(Integer replyCount) {
        this.replyCount = replyCount;
    }

    public Integer getEvaluationStatus() {
        return evaluationStatus;
    }

    public void setEvaluationStatus(Integer evaluationStatus) {
        this.evaluationStatus = evaluationStatus;
    }

    public Integer getPictureStatus() {
        return pictureStatus;
    }

    public void setPictureStatus(Integer pictureStatus) {
        this.pictureStatus = pictureStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", skuStockId=").append(skuStockId);
        sb.append(", memberId=").append(memberId);
        sb.append(", star=").append(star);
        sb.append(", createTime=").append(createTime);
        sb.append(", showStatus=").append(showStatus);
        sb.append(", pics=").append(pics);
        sb.append(", replyCount=").append(replyCount);
        sb.append(", evaluationStatus=").append(evaluationStatus);
        sb.append(", pictureStatus=").append(pictureStatus);
        sb.append(", content=").append(content);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public void setDefault() {
        this.setCreateTime(new Date());
        this.setReplyCount(0);
        this.setShowStatus(CommentStatus.STATUS_ENABLE.getKey());
    }
}