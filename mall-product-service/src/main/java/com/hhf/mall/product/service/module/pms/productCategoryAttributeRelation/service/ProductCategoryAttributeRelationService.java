package com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.entity.PmsProductCategoryAttributeRelation;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.entity.PmsProductCategoryAttributeRelationExample;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.mapper.PmsProductCategoryAttributeRelationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/3 14:16
 */
@Service
public class ProductCategoryAttributeRelationService extends BaseService<PmsProductCategoryAttributeRelation, Long, PmsProductCategoryAttributeRelationExample> {
    @Autowired
    private PmsProductCategoryAttributeRelationMapper productCategoryAttributeRelationMapper;

    @Override
    protected void before(PmsProductCategoryAttributeRelation entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(PmsProductCategoryAttributeRelation entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsProductCategoryAttributeRelation, Long, PmsProductCategoryAttributeRelationExample> getMapper() {
        return productCategoryAttributeRelationMapper;
    }

    public void batchInsert(List<PmsProductCategoryAttributeRelation> relationList) {
        productCategoryAttributeRelationMapper.batchInsert(relationList);
    }

    public void selectOrderRule(PmsProductCategoryAttributeRelationExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsProductCategoryAttributeRelationExample example, ListParam listParam) {
        PmsProductCategoryAttributeRelationExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "productCategoryId":
                    criteria.andProductCategoryIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
