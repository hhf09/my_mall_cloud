package com.hhf.mall.order.service.module.oms.order.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrder;
import com.hhf.mall.order.service.module.oms.order.entity.OrderConfirmStatus;
import com.hhf.mall.order.service.module.oms.order.entity.OrderPayType;
import com.hhf.mall.order.service.module.oms.order.entity.OrderStatus;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItem;
import com.hhf.mall.order.service.module.oms.orderItem.entity.OmsOrderItemExample;
import com.hhf.mall.order.service.module.oms.orderItem.mapper.OmsOrderItemMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/12/28 15:57
 */
@Component
public class OrderDetailDTOMapper extends BaseDTOMapper<OmsOrder, OrderDetailDTO> {
    @Autowired
    private OmsOrderItemMapper orderItemMapper;

    @Override
    public OrderDetailDTO mapToDTO(OmsOrder entity) {
        OrderDetailDTO dto = new OrderDetailDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setStatusText(OrderStatus.instanceOf(entity.getStatus()).getText());
        dto.setConfirmStatusText(OrderConfirmStatus.instanceOf(entity.getConfirmStatus()).getText());
        dto.setPayTypeText(OrderPayType.instanceOf(entity.getPayType()).getText());
        OmsOrderItemExample orderItemExample = new OmsOrderItemExample();
        OmsOrderItemExample.Criteria criteria1 = orderItemExample.createCriteria();
        criteria1.andOrderIdEqualTo(entity.getId());
        List<OmsOrderItem> orderItemList = orderItemMapper.selectByExample(orderItemExample);
        dto.setOrderItemList(orderItemList);
        return dto;
    }

    @Override
    public List<OrderDetailDTO> mapToDTOList(List<OmsOrder> entityList) {
        List<OrderDetailDTO> dtoList = new ArrayList<>();
        for (OmsOrder entity : entityList) {
            OrderDetailDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
