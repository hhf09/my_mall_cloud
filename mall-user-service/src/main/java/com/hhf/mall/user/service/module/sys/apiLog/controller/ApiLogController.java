package com.hhf.mall.user.service.module.sys.apiLog.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.sys.apiLog.dto.ApiLogVO;
import com.hhf.mall.user.service.module.sys.apiLog.dto.ApiLogVOMapper;
import com.hhf.mall.user.service.module.sys.apiLog.entity.SysApiLog;
import com.hhf.mall.user.service.module.sys.apiLog.service.ApiLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2023-01-27 17:51
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/apiLog")
@Validated
public class ApiLogController {
    @Autowired
    private ApiLogService apiLogService;
    @Autowired
    private ApiLogVOMapper apiLogVOMapper;

    /**
     * 分页查找
     * @param createTime
     * @param serviceSign
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_API_LOG)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"createTime", "serviceSign"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        String createTime = listParam.getParamValue("createTime");
        if (StrUtil.isNotEmpty(createTime)) {
            String start = createTime + " 00:00:00";
            String end = createTime + " 23:59:59";
            listParam.add("createTimeBt", start + "," + end);
        }
        listParam.remove("createTime");
        List<SysApiLog> list = apiLogService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SysApiLog> pageInfo = new PageInfo<>(list);
            List<ApiLogVO> voList = apiLogVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }
}
