package com.hhf.mall.common;

/**
 * @author hhf
 * @description 常量
 * @date 2022-07-20 23:02
 */
public class Const {
    /* 服务名称 */
    /*----------------------------------------------------------------------------------------------------------*/
    public static final String SERVICE_NAME_PREFIX = "mall-";
    public static final String SERVICE_NAME_SUFFIX = "-service";
    //用户服务名称
    public static final String USER_SERVICE_NAME = SERVICE_NAME_PREFIX + "user" + SERVICE_NAME_SUFFIX;

    //商品服务名称
    public static final String PRODUCT_SERVICE_NAME = SERVICE_NAME_PREFIX + "product" + SERVICE_NAME_SUFFIX;

    //订单服务名称
    public static final String ORDER_SERVICE_NAME = SERVICE_NAME_PREFIX + "order" + SERVICE_NAME_SUFFIX;

    //营销服务名称
    public static final String MARKETING_SERVICE_NAME = SERVICE_NAME_PREFIX + "marketing" + SERVICE_NAME_SUFFIX;

    //基础服务名称
    public static final String BASIC_SERVICE_NAME = SERVICE_NAME_PREFIX + "basic" + SERVICE_NAME_SUFFIX;

    //认证服务名称
    public static final String AUTH_SERVICE_NAME = SERVICE_NAME_PREFIX + "auth" + SERVICE_NAME_SUFFIX;
    /*----------------------------------------------------------------------------------------------------------*/

    /* 包路径 */
    /*----------------------------------------------------------------------------------------------------------*/
    //Mybatis Mapper扫描包
    public static final String MAPPER_SCAN_PACKAGE = "*.*.mapper";

    //用户服务模块包
    public static final String USER_SERVICE_MODULE_PACKAGE = "com.hhf.mall.user.service.module.";

    //商品服务模块包
    public static final String PRODUCT_SERVICE_MODULE_PACKAGE = "com.hhf.mall.product.service.module.";

    //订单服务模块包
    public static final String ORDER_SERVICE_MODULE_PACKAGE = "com.hhf.mall.order.service.module.";

    //营销服务模块包
    public static final String MARKETING_SERVICE_MODULE_PACKAGE = "com.hhf.mall.marketing.service.module.";

    //基础服务模块包
    public static final String BASIC_SERVICE_MODULE_PACKAGE = "com.hhf.mall.basic.service.module.";

    //feign接口包
    public static final String FEIGN_PACKAGE = "com.hhf.mall.feign.*";
    /*----------------------------------------------------------------------------------------------------------*/

    /* controller,feign返回data里面的key */
    /*----------------------------------------------------------------------------------------------------------*/
    //list: 数组类型
    public static final String LIST_KEY_TYPE = "list";

    //total: 数值类型（分页查询的总记录数）
    public static final String TOTAL_KEY_TYPE = "total";
    /*----------------------------------------------------------------------------------------------------------*/

    /* 服务上下文路径 */
    /*----------------------------------------------------------------------------------------------------------*/
    public static final String CONTEXT_PATH_PREFIX = "/mall/cloud";
    public static final String BASIC_SERVICE_CONTEXT_PATH = CONTEXT_PATH_PREFIX + "/basic";
    public static final String USER_SERVICE_CONTEXT_PATH = CONTEXT_PATH_PREFIX + "/user";
    public static final String PRODUCT_SERVICE_CONTEXT_PATH = CONTEXT_PATH_PREFIX + "/product";
    public static final String ORDER_SERVICE_CONTEXT_PATH = CONTEXT_PATH_PREFIX + "/order";
    public static final String MARKETING_SERVICE_CONTEXT_PATH = CONTEXT_PATH_PREFIX + "/marketing";
    public static final String AUTH_SERVICE_CONTEXT_PATH = CONTEXT_PATH_PREFIX + "/auth";
    /*----------------------------------------------------------------------------------------------------------*/

    /**
     * user-agent请求头
     */
    public static final String USER_AGENT_HEADER = "User-Agent";

    /**
     * 年月日时分秒格式
     */
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    /**
     * 年月日格式
     */
    public static final String YYYY_MM_DD = "yyyy-MM-dd";

}
