package com.hhf.mall.order.service.module.oms.orderReturnReason.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReason;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReasonExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OmsOrderReturnReasonMapper extends BaseMapper<OmsOrderReturnReason, Long, OmsOrderReturnReasonExample> {
    int countByExample(OmsOrderReturnReasonExample example);

    int deleteByExample(OmsOrderReturnReasonExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OmsOrderReturnReason record);

    int insertSelective(OmsOrderReturnReason record);

    List<OmsOrderReturnReason> selectByExample(OmsOrderReturnReasonExample example);

    OmsOrderReturnReason selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OmsOrderReturnReason record, @Param("example") OmsOrderReturnReasonExample example);

    int updateByExample(@Param("record") OmsOrderReturnReason record, @Param("example") OmsOrderReturnReasonExample example);

    int updateByPrimaryKeySelective(OmsOrderReturnReason record);

    int updateByPrimaryKey(OmsOrderReturnReason record);
}