package com.hhf.mall.order.service.module.oms.orderReturnApply.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 14:10
 */
@Data
public class OrderReturnApplyVO {
    private Long id;
    private Long orderId;
    private Long productId;
    private String orderSn;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private String memberUsername;
    private BigDecimal returnAmount;
    private String returnName;
    private String returnPhone;
    private Integer status;
    private String statusText;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date handleTime;
    private String productName;
    private String productBrand;
    private String productAttr;
    private Integer productCount;
    private BigDecimal productPrice;
    private BigDecimal productRealPrice;
    private Long reasonId;
    private String description;
    private String proofPics;
    private String handleNote;
    private String handleMan;
    private String receiveMan;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date receiveTime;
    private String receiveNote;
    private Long memberId;
    private Long brandId;
}
