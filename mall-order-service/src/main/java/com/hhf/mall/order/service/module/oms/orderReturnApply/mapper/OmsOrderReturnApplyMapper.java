package com.hhf.mall.order.service.module.oms.orderReturnApply.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApply;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApplyExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OmsOrderReturnApplyMapper extends BaseMapper<OmsOrderReturnApply, Long, OmsOrderReturnApplyExample> {
    int countByExample(OmsOrderReturnApplyExample example);

    int deleteByExample(OmsOrderReturnApplyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(OmsOrderReturnApply record);

    int insertSelective(OmsOrderReturnApply record);

    List<OmsOrderReturnApply> selectByExample(OmsOrderReturnApplyExample example);

    OmsOrderReturnApply selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") OmsOrderReturnApply record, @Param("example") OmsOrderReturnApplyExample example);

    int updateByExample(@Param("record") OmsOrderReturnApply record, @Param("example") OmsOrderReturnApplyExample example);

    int updateByPrimaryKeySelective(OmsOrderReturnApply record);

    int updateByPrimaryKey(OmsOrderReturnApply record);
}