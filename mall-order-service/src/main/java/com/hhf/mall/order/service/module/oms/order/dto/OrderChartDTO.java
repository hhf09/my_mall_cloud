package com.hhf.mall.order.service.module.oms.order.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 订单图表
 * @date 2022-02-16 22:20
 */
@Data
public class OrderChartDTO {
    /**
     * 日期
     */
    private String date;

    /**
     * 订单总数
     */
    private int totalCount;

    /**
     * 成交总额
     */
    private double totalAmount;
}
