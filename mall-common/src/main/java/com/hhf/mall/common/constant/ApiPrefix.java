package com.hhf.mall.common.constant;

/**
 * @author hhf
 * @description: 接口前缀
 * @date 2023/2/10 14:00
 */
public interface ApiPrefix {
    /**
     * 小程序接口前缀
     */
    String WECHAT_WEB = "/mini";
    /**
     * feign接口前缀
     */
    String FEIGN = "/feign";
    /**
     * 管理后台接口前缀
     */
    String MANAGE = "/manage";
}
