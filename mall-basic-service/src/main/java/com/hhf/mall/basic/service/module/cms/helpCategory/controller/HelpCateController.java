package com.hhf.mall.basic.service.module.cms.helpCategory.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.basic.service.module.cms.helpCategory.dto.HelpCategoryVO;
import com.hhf.mall.basic.service.module.cms.helpCategory.dto.HelpCategoryVOMapper;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategory;
import com.hhf.mall.basic.service.module.cms.helpCategory.service.HelpCateService;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author hhf
 * @description 帮助分类
 * @date 2022-01-29 0:01
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/helpCategory")
@Validated
public class HelpCateController {
    @Autowired
    private HelpCateService helpCateService;
    @Autowired
    private HelpCategoryVOMapper helpCategoryVOMapper;

    /**
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_HELP_CATEGORY)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<CmsHelpCategory> list = helpCateService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<CmsHelpCategory> pageInfo = new PageInfo<>(list);
            List<HelpCategoryVO> voList = helpCategoryVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @ApiLog(note = "添加帮助分类")
    @RequirePermissions(PermissionConst.ADMIN_HELP_CATEGORY)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody CmsHelpCategory helpCategory) {
        helpCategory.setDefault();
        helpCateService.add(helpCategory);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新帮助分类")
    @RequirePermissions(PermissionConst.ADMIN_HELP_CATEGORY)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody CmsHelpCategory helpCategory) {
        helpCateService.updateByPrimaryKeySelective(helpCategory);
        return ApiResponse.success();
    }

    @ApiLog(note = "删除帮助分类")
    @RequirePermissions(PermissionConst.ADMIN_HELP_CATEGORY)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        helpCateService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }
}
