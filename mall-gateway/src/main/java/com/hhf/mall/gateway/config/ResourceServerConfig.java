package com.hhf.mall.gateway.config;

import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.gateway.authorization.CustomAccessDeniedHandler;
import com.hhf.mall.gateway.authorization.CustomAuthenticationEntryPoint;
import com.hhf.mall.gateway.authorization.CustomAuthorizationManager;
import com.hhf.mall.gateway.filter.IgnoreUrlsFilter;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.oauth2.server.resource.authentication.ReactiveJwtAuthenticationConverterAdapter;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

/**
 * @author hhf
 * @description: 资源服务器配置
 * @date 2023/2/8 14:22
 */
@Configuration
@EnableWebFluxSecurity
@AllArgsConstructor
public class ResourceServerConfig {
    private final CustomAuthorizationManager authorizationManager;
    private final IgnoreUrlsConfig ignoreUrlsConfig;
    private final CustomAccessDeniedHandler accessDeniedHandler;
    private final CustomAuthenticationEntryPoint authenticationEntryPoint;
    private final IgnoreUrlsFilter ignoreUrlsFilter;

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity httpSecurity) {
        httpSecurity.oauth2ResourceServer().jwt().jwtAuthenticationConverter(jwtAuthenticationConverter());
        //自定义处理JWT请求头过期或签名错误的结果
        httpSecurity.oauth2ResourceServer().authenticationEntryPoint(authenticationEntryPoint);
        //对白名单路径，直接移除JWT请求头
        httpSecurity.addFilterBefore(ignoreUrlsFilter, SecurityWebFiltersOrder.AUTHENTICATION);
        httpSecurity.authorizeExchange()
                //白名单配置
                .pathMatchers(ignoreUrlsConfig.getUrls().toArray(new String[0]))
                .permitAll()
                //鉴权管理器配置
                .anyExchange()
                .access(authorizationManager)
                .and()
                .exceptionHandling()
                //未授权配置
                .accessDeniedHandler(accessDeniedHandler)
                //未认证配置
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .csrf()
                .disable();
        return httpSecurity.build();
    }

    @Bean
    public Converter<Jwt, ? extends Mono<? extends AbstractAuthenticationToken>> jwtAuthenticationConverter() {
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix(AuthConst.AUTHORITY_PREFIX);
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName(AuthConst.AUTHORITIES_CLAIM_NAME);
        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        return new ReactiveJwtAuthenticationConverterAdapter(jwtAuthenticationConverter);
    }
}
