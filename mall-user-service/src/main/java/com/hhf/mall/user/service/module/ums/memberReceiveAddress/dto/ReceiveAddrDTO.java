package com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 15:48
 */
@Data
public class ReceiveAddrDTO {
    private Long id;
    private Long memberId;
    private String name;
    private String phone;
    private Integer defaultStatus;
    private String defaultStatusText;
    private String province;
    private String city;
    private String region;
    private String detailAddress;
    private String memberUsername;
}
