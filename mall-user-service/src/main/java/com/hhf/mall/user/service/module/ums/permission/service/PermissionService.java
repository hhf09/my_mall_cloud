package com.hhf.mall.user.service.module.ums.permission.service;

import cn.hutool.core.util.StrUtil;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermissionExample;
import com.hhf.mall.user.service.module.ums.permission.mapper.UmsPermissionMapper;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelationExample;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.service.RolePermissionRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-02 23:46
 */
@Service
public class PermissionService extends BaseService<UmsPermission, Long, UmsPermissionExample> {
    @Autowired
    private UmsPermissionMapper permissionMapper;

    @Autowired
    private RolePermissionRelationService rolePermissionRelationService;

    @Override
    protected void before(UmsPermission entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(UmsPermission entity, ActionType actionType) {
        if (actionType == ActionType.DELETE) {
            UmsRolePermissionRelationExample rolePermissionRelationExample = new UmsRolePermissionRelationExample();
            rolePermissionRelationExample.createCriteria().andPermissionIdEqualTo(entity.getId());
            rolePermissionRelationService.deleteByExample(rolePermissionRelationExample);
        }
    }

    @Override
    protected BaseMapper<UmsPermission, Long, UmsPermissionExample> getMapper() {
        return permissionMapper;
    }

    public List<UmsPermission> list(ListParam listParam) {
        UmsPermissionExample example = new UmsPermissionExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return permissionMapper.selectByExample(example);
    }

    public List<UmsPermission> getMenuListByRoleId(Long roleId) {
        return permissionMapper.getMenuListByRoleId(roleId);
    }

    public List<String> getPermissionValueListByUserId(Long userId) {
        return permissionMapper.getPermissionValueListByUserId(userId);
    }

    public void selectOrderRule(UmsPermissionExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(UmsPermissionExample example, ListParam listParam) {
        UmsPermissionExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "status":
                    criteria.andStatusEqualTo(Integer.valueOf(value));
                    break;
                case "value":
                    criteria.andValueEqualTo(value);
                    break;
            }
        }
    }

}
