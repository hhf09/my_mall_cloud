package com.hhf.mall.marketing.service.module.sms.recommendProduct.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description: 
 * @date 2022/12/28 14:58
 */
@Data
public class RecommendProductDTO {
    private Long id;
    private Long productId;
    private Integer recommendStatus;
    private String recommendStatusText;
    private Integer sort;
    private String productName;
    private String brandName;
    private BigDecimal price;
    private String productPic;
    private String subTitle;
}
