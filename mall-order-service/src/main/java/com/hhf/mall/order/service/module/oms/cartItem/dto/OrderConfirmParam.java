package com.hhf.mall.order.service.module.oms.cartItem.dto;

import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-02-04 18:17
 */
@Data
public class OrderConfirmParam {
    //用户id
    private Long memberId;

    //购物车物品id列表
    private List<Long> cartItemIdList;
}
