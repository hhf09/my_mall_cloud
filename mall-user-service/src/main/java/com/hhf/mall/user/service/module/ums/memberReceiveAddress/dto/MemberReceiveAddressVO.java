package com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 10:47
 */
@Data
public class MemberReceiveAddressVO {
    private Long id;
    private Long memberId;
    private String name;
    private String phone;
    private Integer defaultStatus;
    private String defaultStatusText;
    private String province;
    private String city;
    private String region;
    private String detailAddress;
    private String memberUsername;
}
