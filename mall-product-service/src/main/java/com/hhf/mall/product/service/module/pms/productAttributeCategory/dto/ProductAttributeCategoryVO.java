package com.hhf.mall.product.service.module.pms.productAttributeCategory.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/12 9:23
 */
@Data
public class ProductAttributeCategoryVO {
    private Long id;
    private String name;
    private Integer attributeCount;
    private Integer paramCount;
}
