package com.hhf.mall.marketing.service.module.sms.newProduct.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.marketing.service.module.sms.newProduct.dto.NewProductVO;
import com.hhf.mall.marketing.service.module.sms.newProduct.dto.NewProductVOMapper;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.SmsHomeNewProduct;
import com.hhf.mall.marketing.service.module.sms.newProduct.service.NewProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 新品推荐
 * @date 2022-01-22 23:19
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/newProduct")
@Validated
public class NewProductController {
    @Autowired
    private NewProductService newProductService;
    @Autowired
    private NewProductVOMapper newProductVOMapper;

    /**
     * 分页查找
     * @param recommendStatus
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_NEW_RECOMMEND)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"recommendStatus"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<SmsHomeNewProduct> list = newProductService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SmsHomeNewProduct> pageInfo = new PageInfo<>(list);
            List<NewProductVO> voList = newProductVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 修改推荐状态
     * @param id
     * @param recommendStatus
     * @return
     */
    @ApiLog(note = "更新推荐状态")
    @RequirePermissions(PermissionConst.ADMIN_NEW_RECOMMEND)
    @PostMapping("/updateRecommendStatus")
    public ResponseEntity<ApiResponse> updateRecommendStatus(@RequestParam Long id, @RequestParam Integer recommendStatus) {
        SmsHomeNewProduct newProduct = newProductService.selectByPrimaryKey(id);
        if (newProduct == null) {
            throw new CodeException("新品推荐" + id + "数据不存在");
        }
        newProduct.setRecommendStatus(recommendStatus);
        newProductService.updateByPrimaryKeySelective(newProduct);
        return ApiResponse.success();
    }

    /**
     * 修改排序
     * @param id
     * @param sort
     * @return
     */
    @ApiLog(note = "更新排序")
    @RequirePermissions(PermissionConst.ADMIN_NEW_RECOMMEND)
    @PostMapping("/updateSort")
    public ResponseEntity<ApiResponse> updateSort(@RequestParam Long id, @RequestParam Integer sort) {
        SmsHomeNewProduct newProduct = newProductService.selectByPrimaryKey(id);
        if (newProduct == null) {
            throw new CodeException("新品推荐" + id + "数据不存在");
        }
        newProduct.setSort(sort);
        newProductService.updateByPrimaryKeySelective(newProduct);
        return ApiResponse.success();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiLog(note = "删除新品推荐")
    @RequirePermissions(PermissionConst.ADMIN_NEW_RECOMMEND)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        newProductService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }

    /**
     * 批量添加
     * @param newProductList
     * @return
     */
    @ApiLog(note = "添加新品推荐")
    @RequirePermissions(PermissionConst.ADMIN_NEW_RECOMMEND)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody List<SmsHomeNewProduct> newProductList) {
        for (SmsHomeNewProduct newProduct : newProductList) {
            newProduct.setDefault();
            newProductService.add(newProduct);
        }
        return ApiResponse.success();
    }
}
