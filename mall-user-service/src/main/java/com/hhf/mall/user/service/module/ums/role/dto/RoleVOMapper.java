package com.hhf.mall.user.service.module.ums.role.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.ums.role.entity.RoleStatus;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 9:54
 */
@Component
public class RoleVOMapper extends BaseDTOMapper<UmsRole, RoleVO> {
    @Override
    public RoleVO mapToDTO(UmsRole entity) {
        RoleVO vo = new RoleVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(RoleStatus.instanceOf(entity.getStatus()).getText());
        return vo;
    }

    @Override
    public List<RoleVO> mapToDTOList(List<UmsRole> entityList) {
        List<RoleVO> voList = new ArrayList<>();
        for (UmsRole entity : entityList) {
            RoleVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
