package com.hhf.mall.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hhf
 * @description: 校验角色自定义注解
 * @date 2023/2/13 15:56
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequireRoles {
    /**
     * 角色值列表
     * @return
     */
    String[] value();
}
