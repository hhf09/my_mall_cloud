package com.hhf.mall.order.service.aop;

import com.hhf.mall.common.aop.BaseAuthenticationHandler;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.text.ParseException;

/**
 * @author hhf
 * @description: 权限校验切面
 * @date 2023/2/13 17:08
 */
@Aspect
@Component
@Order(1)
public class AuthenticationAspect extends BaseAuthenticationHandler {
    /**
     * 权限校验切点表达式
     */
    @Pointcut("execution(public * com.hhf.mall.order.service.module.*.*.controller.*.*(..))")
    public void authPointcut() {
    }

    /**
     * 前置通知
     * @param joinPoint
     * @throws ParseException
     */
    @Before(value = "authPointcut()")
    @Override
    public void handle(JoinPoint joinPoint) throws ParseException {
        super.handle(joinPoint);
    }
}
