package com.hhf.mall.user.service.module.ums.role.entity;

/**
 * @author hhf
 * @description: 角色状态枚举
 * @date 2022/12/28 10:50
 */
public enum RoleStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    RoleStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static RoleStatus instanceOf(int key) {
        for (RoleStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
