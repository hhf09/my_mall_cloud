package com.hhf.mall.marketing.service.module.sms.recommendProduct.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.product.ProductDTO;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.RecommendProductRecommendStatus;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.SmsHomeRecommendProduct;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/12/28 14:59
 */
@Component
public class RecommendProductDTOMapper extends BaseDTOMapper<SmsHomeRecommendProduct, RecommendProductDTO> {
    @Autowired
    private ProductClient productClient;

    public RecommendProductDTO mapToDTO(SmsHomeRecommendProduct entity) {
        RecommendProductDTO dto = new RecommendProductDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setRecommendStatusText(RecommendProductRecommendStatus.instanceOf(entity.getRecommendStatus()).getText());
        ResponseEntity<ApiResponse> responseEntity = productClient.getById(entity.getProductId());
        ApiResponse response = responseEntity.getBody();
        ProductDTO productDTO = response.toObject(ProductDTO.class);
        dto.setProductName(productDTO.getName());
        dto.setBrandName(productDTO.getBrandName());
        dto.setPrice(productDTO.getPrice());
        dto.setProductPic(productDTO.getPic());
        dto.setSubTitle(productDTO.getSubTitle());
        return dto;
    }

    public List<RecommendProductDTO> mapToDTOList(List<SmsHomeRecommendProduct> entityList) {
        List<RecommendProductDTO> dtoList = new ArrayList<>();
        for (SmsHomeRecommendProduct entity : entityList) {
            RecommendProductDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
