package com.hhf.mall.common.constant;

/**
 * @author hhf
 * @description: 角色标志常量
 * @date 2023/2/15 18:57
 */
public interface RoleConst {
    String prefix = "R_";
    /**
     * 后台超级管理员
     */
    String ADMIN = prefix + "ADMIN";
    /**
     * 后台普通管理员
     * 比后台超级管理员少了后台用户、角色、权限管理
     */
    String MANAGER = prefix + "MANAGER";
    /**
     * 前台用户（已登录）
     */
    String USER = prefix + "USER";
    /**
     * 前台游客（未登录）
     */
    String GUEST = prefix + "GUEST";

}
