package com.hhf.mall.user.service.module.ums.admin.dto;

import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 用户详细信息
 * @date 2023/2/16 14:32
 */
@Data
public class AdminDTOWithDetail {
    private Long userId;
    private String userName;
    private String userIcon;
    private List<String> roles;
    private List<String> permissions;
}
