package com.hhf.mall.product.service.module.pms.commentReply.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsComment;
import com.hhf.mall.product.service.module.pms.comment.service.CommentService;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReply;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReplyExample;
import com.hhf.mall.product.service.module.pms.commentReply.mapper.PmsCommentReplyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-24 23:43
 */
@Service
public class CommentReplyService extends BaseService<PmsCommentReply, Long, PmsCommentReplyExample> {
    @Autowired
    private PmsCommentReplyMapper replyMapper;

    @Autowired
    private CommentService commentService;

    @Override
    protected void before(PmsCommentReply entity, ActionType actionType) {

    }

    @Override
    protected void after(PmsCommentReply entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            //修改回复数量
            PmsComment comment = commentService.selectByPrimaryKey(entity.getCommentId());
            comment.setReplyCount(comment.getReplyCount() + 1);
            commentService.updateByPrimaryKeySelective(comment);
        }
        if (actionType == ActionType.DELETE) {
            PmsComment comment = commentService.selectByPrimaryKey(entity.getCommentId());
            comment.setReplyCount(comment.getReplyCount() - 1);
            commentService.updateByPrimaryKeySelective(comment);
        }
    }

    @Override
    protected BaseMapper<PmsCommentReply, Long, PmsCommentReplyExample> getMapper() {
        return replyMapper;
    }

    public List<PmsCommentReply> list(ListParam listParam) {
        PmsCommentReplyExample example = new PmsCommentReplyExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return replyMapper.selectByExample(example);
    }

    public List<PmsCommentReply> listByMemberId(ListParam listParam) {
        PmsCommentReplyExample example = new PmsCommentReplyExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return replyMapper.selectByExample(example);
    }

    public int countByMemberId(Long memberId) {
        PmsCommentReplyExample example = new PmsCommentReplyExample();
        ListParam listParam = new ListParam().add("memberId", String.valueOf(memberId));
        selectCondition(example, listParam);
        return replyMapper.countByExample(example);
    }

    public void selectOrderRule(PmsCommentReplyExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "id desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsCommentReplyExample example, ListParam listParam) {
        PmsCommentReplyExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "memberId":
                    criteria.andMemberIdEqualTo(Long.valueOf(value));
                    break;
                case "commentId":
                    criteria.andCommentIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
