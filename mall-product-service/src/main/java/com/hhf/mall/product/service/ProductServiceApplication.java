package com.hhf.mall.product.service;

import com.hhf.mall.common.Const;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hhf
 * @description 商品服务
 * @date 2022-07-05 23:51
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = Const.PRODUCT_SERVICE_MODULE_PACKAGE + Const.MAPPER_SCAN_PACKAGE)
@EnableFeignClients(basePackages = Const.FEIGN_PACKAGE)
public class ProductServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProductServiceApplication.class, args);
    }
}
