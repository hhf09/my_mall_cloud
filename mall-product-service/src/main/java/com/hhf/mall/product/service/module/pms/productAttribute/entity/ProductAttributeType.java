package com.hhf.mall.product.service.module.pms.productAttribute.entity;

/**
 * @author hhf
 * @description: 商品属性类型枚举
 * @date 2022/12/28 10:50
 */
public enum ProductAttributeType {
    ATTRIBUTE(0, "规格"),
    PARAM(1, "参数");

    private final int key;
    private final String text;

    ProductAttributeType(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static ProductAttributeType instanceOf(int key) {
        for (ProductAttributeType e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
