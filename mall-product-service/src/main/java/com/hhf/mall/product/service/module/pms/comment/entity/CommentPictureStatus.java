package com.hhf.mall.product.service.module.pms.comment.entity;

/**
 * @author hhf
 * @description: 是否有图枚举
 * @date 2022/12/28 10:50
 */
public enum CommentPictureStatus {
    NO(0, "否"),
    YES(1, "是");

    private final int key;
    private final String text;

    CommentPictureStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }
}
