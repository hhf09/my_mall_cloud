package com.hhf.mall.basic.service.module.cms.help.controller;

import com.hhf.mall.basic.service.module.cms.help.dto.HelpDTO;
import com.hhf.mall.basic.service.module.cms.help.dto.HelpDTOMapper;
import com.hhf.mall.basic.service.module.cms.help.entity.CmsHelp;
import com.hhf.mall.basic.service.module.cms.help.service.HelpService;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 帮助
 * @date 2022-02-06 17:46
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/help")
@Validated
public class HelpWebController {
    @Autowired
    private HelpService helpService;
    @Autowired
    private HelpDTOMapper helpDTOMapper;

    /**
     * 查找一个分类下的帮助项
     * @param categoryId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_HELP)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"categoryId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<CmsHelp> list = helpService.list(listParam);
        List<HelpDTO> dtoList = helpDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }
}
