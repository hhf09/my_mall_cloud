package com.hhf.mall.product.service.module.pms.feightTemplate.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplate;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplateExample;
import com.hhf.mall.product.service.module.pms.feightTemplate.mapper.PmsFeightTemplateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:47
 */
@Service
public class FeightTemplateService extends BaseService<PmsFeightTemplate, Long, PmsFeightTemplateExample> {
    @Autowired
    private PmsFeightTemplateMapper feightTemplateMapper;

    @Override
    protected void before(PmsFeightTemplate entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(PmsFeightTemplate entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsFeightTemplate, Long, PmsFeightTemplateExample> getMapper() {
        return feightTemplateMapper;
    }

    public void selectOrderRule(PmsFeightTemplateExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsFeightTemplateExample example, ListParam listParam) {
        PmsFeightTemplateExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "packingType":
                    criteria.andPackingTypeEqualTo(value);
                    break;
                case "weight":
                    criteria.andWeightEqualTo(value);
                    break;
                case "isFragile":
                    criteria.andIsFragileEqualTo(value);
                    break;
            }
        }
    }
}
