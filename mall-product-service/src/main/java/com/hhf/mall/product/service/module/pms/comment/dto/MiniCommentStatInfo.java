package com.hhf.mall.product.service.module.pms.comment.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 商品评价统计信息
 * @date 2022-02-03 17:40
 */
@Data
public class MiniCommentStatInfo {
    //评价总数
    private Integer allCount;

    //好评数
    private Integer goodCount;

    //中评数
    private Integer mediumCount;

    //差评数
    private Integer badCount;

    //有图数
    private Integer picCount;

    //好评占比
    private double goodRate;
}
