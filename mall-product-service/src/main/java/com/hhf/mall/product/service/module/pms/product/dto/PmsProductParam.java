package com.hhf.mall.product.service.module.pms.product.dto;

import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.productAttributeValue.entity.PmsProductAttributeValue;
import com.hhf.mall.product.service.module.pms.skuStock.dto.ProductSkuDTO;
import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-14 0:04
 */
@Data
public class PmsProductParam extends PmsProduct {
    private List<ProductSkuDTO> skuStockList;
    private List<PmsProductAttributeValue> productAttributeValueList;
    private String packingType;
    private String weightType;
    private String isFragile;
}
