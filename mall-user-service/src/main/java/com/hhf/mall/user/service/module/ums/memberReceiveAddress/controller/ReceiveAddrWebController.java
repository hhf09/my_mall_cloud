package com.hhf.mall.user.service.module.ums.memberReceiveAddress.controller;

import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto.ReceiveAddrDTO;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto.ReceiveAddrDTOMapper;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddress;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.service.ReceiveAddrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 收货地址
 * @date 2022-02-04 20:36
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/receiveAddress")
@Validated
public class ReceiveAddrWebController {
    @Autowired
    private ReceiveAddrService addrService;
    @Autowired
    private ReceiveAddrDTOMapper receiveAddrDTOMapper;

    /**
     * 查找地址列表
     * @param memberId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_MEMBER_ADDRESS)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<UmsMemberReceiveAddress> list = addrService.list(listParam);
        List<ReceiveAddrDTO> dtoList = receiveAddrDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }

    /**
     * 获取地址信息
     * @param id
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_MEMBER_ADDRESS)
    @GetMapping("/get")
    public ResponseEntity<ApiResponse> get(@RequestParam Long id) {
        UmsMemberReceiveAddress address = addrService.selectByPrimaryKey(id);
        ReceiveAddrDTO dto = receiveAddrDTOMapper.mapToDTO(address);
        return ApiResponse.success(dto);
    }

    @ApiLog(note = "添加收货地址")
    @RequirePermissions(PermissionConst.WEB_MEMBER_ADDRESS)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@RequestBody UmsMemberReceiveAddress address) {
        addrService.add(address);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新收货地址")
    @RequirePermissions(PermissionConst.WEB_MEMBER_ADDRESS)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@RequestBody UmsMemberReceiveAddress address) {
        addrService.updateByPrimaryKeySelective(address);
        return ApiResponse.success();
    }

    @ApiLog(note = "删除收货地址")
    @RequirePermissions(PermissionConst.WEB_MEMBER_ADDRESS)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        addrService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }


}
