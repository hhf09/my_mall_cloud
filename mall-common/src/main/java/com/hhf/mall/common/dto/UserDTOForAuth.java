package com.hhf.mall.common.dto;

import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 用于认证的用户DTO
 * @date 2023/2/7 11:00
 */
@Data
public class UserDTOForAuth {
    /**
     * 用户id
     */
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 是否启用：0->禁用；1->启用
     */
    private Integer status;
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 权限列表
     */
    private List<String> authorities;
    /**
     * 角色列表
     */
    private List<String> roles;
}
