package com.hhf.mall.order.service.module.oms.orderReturnApply.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApply;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.ReturnApplyStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 13:55
 */
@Component
public class OrderReturnApplyVOMapper extends BaseDTOMapper<OmsOrderReturnApply, OrderReturnApplyVO> {
    @Override
    public OrderReturnApplyVO mapToDTO(OmsOrderReturnApply entity) {
        OrderReturnApplyVO vo = new OrderReturnApplyVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(ReturnApplyStatus.instanceOf(entity.getStatus()).getText());
        return vo;
    }

    @Override
    public List<OrderReturnApplyVO> mapToDTOList(List<OmsOrderReturnApply> entityList) {
        List<OrderReturnApplyVO> voList = new ArrayList<>();
        for (OmsOrderReturnApply entity : entityList) {
            OrderReturnApplyVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
