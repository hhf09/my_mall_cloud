package com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.entity.PmsProductCategoryAttributeRelation;
import com.hhf.mall.product.service.module.pms.productCategoryAttributeRelation.entity.PmsProductCategoryAttributeRelationExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsProductCategoryAttributeRelationMapper extends BaseMapper<PmsProductCategoryAttributeRelation, Long, PmsProductCategoryAttributeRelationExample> {
    int countByExample(PmsProductCategoryAttributeRelationExample example);

    int deleteByExample(PmsProductCategoryAttributeRelationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsProductCategoryAttributeRelation record);

    int insertSelective(PmsProductCategoryAttributeRelation record);

    List<PmsProductCategoryAttributeRelation> selectByExample(PmsProductCategoryAttributeRelationExample example);

    PmsProductCategoryAttributeRelation selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsProductCategoryAttributeRelation record, @Param("example") PmsProductCategoryAttributeRelationExample example);

    int updateByExample(@Param("record") PmsProductCategoryAttributeRelation record, @Param("example") PmsProductCategoryAttributeRelationExample example);

    int updateByPrimaryKeySelective(PmsProductCategoryAttributeRelation record);

    int updateByPrimaryKey(PmsProductCategoryAttributeRelation record);

    //自定义start

    /**
     * 批量添加商品分类-商品属性关联
     * @param relationList
     * @return
     */
    void batchInsert(List<PmsProductCategoryAttributeRelation> relationList);
}