package com.hhf.mall.user.service.module.ums.member.entity;

/**
 * @author hhf
 * @description: 用户性别枚举
 * @date 2022/12/28 10:50
 */
public enum MemberGender {
    UNKNOWN(0, "未知"),
    MALE(1, "男"),
    FEMALE(2, "女");

    private final int key;
    private final String text;

    MemberGender(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static MemberGender instanceOf(int key) {
        for (MemberGender e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
