package com.hhf.mall.order.service.module.oms.orderReturnReason.controller;

import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.order.service.module.oms.orderReturnReason.dto.OrderReturnReasonDTO;
import com.hhf.mall.order.service.module.oms.orderReturnReason.dto.OrderReturnReasonDTOMapper;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReason;
import com.hhf.mall.order.service.module.oms.orderReturnReason.service.ReturnReasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 退货原因
 * @date 2022-02-09 15:07
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/returnReason")
@Validated
public class ReturnReasonWebController {
    @Autowired
    private ReturnReasonService returnReasonService;
    @Autowired
    private OrderReturnReasonDTOMapper orderReturnReasonDTOMapper;

    /**
     * 查找
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_ORDER_REFUND_REASON)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<OmsOrderReturnReason> list = returnReasonService.list(listParam);
        List<OrderReturnReasonDTO> dtoList = orderReturnReasonDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }
}
