package com.hhf.mall.product.service.module.pms.comment.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.comment.dto.CommentVO;
import com.hhf.mall.product.service.module.pms.comment.dto.CommentVOMapper;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsComment;
import com.hhf.mall.product.service.module.pms.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 评论
 * @date 2022-01-24 22:54
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/comment")
@Validated
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentVOMapper commentVOMapper;

    /**
     * @param productName
     * @param memberNickname
     * @param star
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_COMMENT)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(@RequestParam(value = "productName", required = false) String productName,
                                            @RequestParam(value = "memberNickname", required = false) String memberNickname,
                                            @RequestParam(value = "star", required = false) Integer star,
                                            HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsComment> list = commentService.list(productName, memberNickname, star);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<PmsComment> pageInfo = new PageInfo<>(list);
            List<CommentVO> voList = commentVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @ApiLog(note = "删除评论")
    @PostMapping("/delete")
    @RequirePermissions(PermissionConst.ADMIN_COMMENT)
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        commentService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }

    @GetMapping("/get")
    @RequirePermissions(PermissionConst.ADMIN_COMMENT)
    public ResponseEntity<ApiResponse> get(@RequestParam Long id) {
        PmsComment comment = commentService.selectByPrimaryKey(id);
        if (comment == null) {
            throw new CodeException("评论" + id + "数据不存在");
        }
        CommentVO vo = commentVOMapper.mapToDTO(comment);
        return ApiResponse.success(vo);
    }
}
