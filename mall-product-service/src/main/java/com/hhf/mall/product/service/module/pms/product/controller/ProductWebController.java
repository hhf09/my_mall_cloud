package com.hhf.mall.product.service.module.pms.product.controller;

import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.brand.dto.BrandDTO;
import com.hhf.mall.product.service.module.pms.brand.dto.BrandDTOMapper;
import com.hhf.mall.product.service.module.pms.brand.entity.PmsBrand;
import com.hhf.mall.product.service.module.pms.brand.service.BrandService;
import com.hhf.mall.product.service.module.pms.product.dto.PmsProductResult;
import com.hhf.mall.product.service.module.pms.product.dto.ProductDTO;
import com.hhf.mall.product.service.module.pms.product.dto.ProductDTOMapper;
import com.hhf.mall.product.service.module.pms.product.dto.ProductDTOWithBrand;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 商品
 * @date 2022-02-02 21:28
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/product")
@Validated
public class ProductWebController {
    @Autowired
    private ProductService productService;

    @Autowired
    private BrandService brandService;
    @Autowired
    private ProductDTOMapper productDTOMapper;
    @Autowired
    private BrandDTOMapper brandDTOMapper;

    /**
     * 获取商品列表
     * @param keywordForMini
     * @param priceGe
     * @param priceLe
     * @param brandId
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_PRODUCT)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"keywordForMini", "priceGe", "priceLe", "brandId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsProduct> productList = productService.list(listParam);
        List<ProductDTO> productDTOList = productDTOMapper.mapToDTOList(productList);
        List<PmsBrand> brandList = brandService.listByKeyword(listParam.getParamValue("keywordForMini"));
        List<BrandDTO> brandDTOList = brandDTOMapper.mapToDTOList(brandList);
        ProductDTOWithBrand dto = new ProductDTOWithBrand();
        dto.setList(productDTOList);
        dto.setBrandList(brandDTOList);
        return ApiResponse.success(dto);
    }

    /**
     * 获取商品信息
     * @param id
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_PRODUCT)
    @GetMapping("/getInfo")
    public ResponseEntity<ApiResponse> getInfo(@RequestParam Long id) {
        PmsProductResult dto = productService.updateInfo(id);
        return ApiResponse.success(dto);
    }
}
