package com.hhf.mall.marketing.service.module.sms.advertise.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 11:40
 */
@Data
public class AdvertiseVO {
    private Long id;
    private String name;
    private String pic;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date startTime;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date endTime;
    private Integer status;
    private String statusText;
    private String note;
    private Integer sort;
    private Long productId;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private String brandName;
    private String productName;
    private String productSn;
}
