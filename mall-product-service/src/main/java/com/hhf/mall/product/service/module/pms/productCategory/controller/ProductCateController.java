package com.hhf.mall.product.service.module.pms.productCategory.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.productCategory.dto.PmsProductCateWithChildrenItem;
import com.hhf.mall.product.service.module.pms.productCategory.dto.ProductCateSaleChartDTO;
import com.hhf.mall.product.service.module.pms.productCategory.dto.ProductCategoryVO;
import com.hhf.mall.product.service.module.pms.productCategory.dto.ProductCategoryVOMapper;
import com.hhf.mall.product.service.module.pms.productCategory.entity.PmsProductCategory;
import com.hhf.mall.product.service.module.pms.productCategory.entity.ProductCategoryLevelType;
import com.hhf.mall.product.service.module.pms.productCategory.entity.ProductCategoryStatus;
import com.hhf.mall.product.service.module.pms.productCategory.service.ProductCateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author hhf
 * @description 商品分类
 * @date 2022-01-09 0:07
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/productCategory")
@Validated
public class ProductCateController {
    @Autowired
    private ProductCateService productCateService;
    @Autowired
    private ProductCategoryVOMapper productCategoryVOMapper;

    /**
     * 分级别查找
     * @param parentId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_CATEGORY)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"parentId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("showStatus", String.valueOf(ProductCategoryStatus.STATUS_ENABLE.getKey()));
        List<PmsProductCategory> list = productCateService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<PmsProductCategory> pageInfo = new PageInfo<>(list);
            List<ProductCategoryVO> voList = productCategoryVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @ApiLog(note = "添加商品分类")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_CATEGORY)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody PmsProductCategory productCategory) {
        if (productCategory.getParentId() == 0) {
            productCategory.setLevel(ProductCategoryLevelType.LEVEL_ONE.getKey());
        } else {
            productCategory.setLevel(ProductCategoryLevelType.LEVEL_TWO.getKey());
        }
        productCategory.setDefault();
        productCateService.add(productCategory);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新商品分类")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_CATEGORY)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody PmsProductCategory productCategory) {
        if (productCategory.getParentId() == 0) {
            productCategory.setLevel(ProductCategoryLevelType.LEVEL_ONE.getKey());
        } else {
            productCategory.setLevel(ProductCategoryLevelType.LEVEL_TWO.getKey());
        }
        productCateService.updateByPrimaryKeySelective(productCategory);
        return ApiResponse.success();
    }

    @ApiLog(note = "删除商品分类")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_CATEGORY)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long productCateId) {
        productCateService.deleteByPrimaryKey(productCateId);
        return ApiResponse.success();
    }

    /**
     * 查询所有一级分类及子分类
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_CATEGORY)
    @GetMapping("/listWithChildren")
    public ResponseEntity<ApiResponse> listWithChildren() {
        List<PmsProductCateWithChildrenItem> dtoList = productCateService.listWithChildren();
        return ApiResponse.success(dtoList);
    }

    @GetMapping("/saleChart")
    public ResponseEntity<ApiResponse> getSaleChart() {
        List<ProductCateSaleChartDTO> dtoList = productCateService.getSaleChart();
        return ApiResponse.success(dtoList);
    }
}
