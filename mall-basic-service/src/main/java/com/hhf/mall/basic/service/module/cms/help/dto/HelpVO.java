package com.hhf.mall.basic.service.module.cms.help.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 19:29
 */
@Data
public class HelpVO {
    private Long id;
    private Long categoryId;
    private String categoryName;
    private String title;
    private Integer status;
    private String statusText;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private String content;
}
