package com.hhf.mall.product.service.module.pms.product.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:15
 */
@Data
public class ProductVO {
    private Long id;
    private Long brandId;
    private Long productCategoryId;
    private Long feightTemplateId;
    private Long productAttributeCategoryId;
    private String name;
    private String pic;
    private String productSn;
    private Integer deleteStatus;
    private Integer publishStatus;
    private Integer newStatus;
    private Integer recommandStatus;
    private Integer sort;
    private Integer sale;
    private BigDecimal price;
    private String subTitle;
    private Integer stock;
    private Integer lowStock;
    private String unit;
    private BigDecimal weight;
    private String serviceIds;
    private String keywords;
    private String note;
    private String pics;
    private String detailTitle;
    private String brandName;
    private String productCategoryName;
    private String description;
    private String detailDesc;
    private String detailMobileHtml;
}
