package com.hhf.mall.order.service;

import com.hhf.mall.common.Const;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hhf
 * @description 订单服务
 * @date 2022-07-05 23:53
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = Const.ORDER_SERVICE_MODULE_PACKAGE + Const.MAPPER_SCAN_PACKAGE)
@EnableFeignClients(basePackages = Const.FEIGN_PACKAGE)
public class OrderServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderServiceApplication.class, args);
    }
}
