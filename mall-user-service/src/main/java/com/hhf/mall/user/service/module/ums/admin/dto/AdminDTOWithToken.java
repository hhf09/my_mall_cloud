package com.hhf.mall.user.service.module.ums.admin.dto;

import com.hhf.mall.dto.auth.TokenDTO;
import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/10 16:33
 */
@Data
public class AdminDTOWithToken {
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 访问令牌信息
     */
    private TokenDTO token;
}
