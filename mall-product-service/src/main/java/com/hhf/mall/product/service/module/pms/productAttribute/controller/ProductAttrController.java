package com.hhf.mall.product.service.module.pms.productAttribute.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.productAttribute.dto.ProductAttrInfo;
import com.hhf.mall.product.service.module.pms.productAttribute.dto.ProductAttributeVO;
import com.hhf.mall.product.service.module.pms.productAttribute.dto.ProductAttributeVOMapper;
import com.hhf.mall.product.service.module.pms.productAttribute.entity.PmsProductAttribute;
import com.hhf.mall.product.service.module.pms.productAttribute.service.ProductAttrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author hhf
 * @description 商品属性
 * @date 2022-01-09 20:43
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/productAttribute")
@Validated
public class ProductAttrController {
    @Autowired
    private ProductAttrService productAttrService;
    @Autowired
    private ProductAttributeVOMapper productAttributeVOMapper;

    /**
     * 根据商品分类的id获取商品属性及属性分类
     * @param productCategoryId
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE)
    @GetMapping("/attrInfo")
    public ResponseEntity<ApiResponse> getAttrInfo(@RequestParam Long productCategoryId) {
        List<ProductAttrInfo> dtoList = productAttrService.getProductAttrInfo(productCategoryId);
        return ApiResponse.success(dtoList);
    }

    /**
     *
     * @param productAttributeCateId
     * @param type
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"productAttributeCateId", "type"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsProductAttribute> list = productAttrService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<PmsProductAttribute> pageInfo = new PageInfo<>(list);
            List<ProductAttributeVO> voList = productAttributeVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @ApiLog(note = "添加商品属性")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody PmsProductAttribute productAttribute) {
        productAttribute.setDefault();
        productAttrService.add(productAttribute);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新商品属性")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody PmsProductAttribute productAttribute) {
        productAttrService.updateByPrimaryKeySelective(productAttribute);
        return ApiResponse.success();
    }

    @ApiLog(note = "删除商品属性")
    @RequirePermissions(PermissionConst.ADMIN_PRODUCT_ATTRIBUTE)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        productAttrService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }

}