package com.hhf.mall.basic.service.module.cms.help.entity;

/**
 * @author hhf
 * @description: 帮助状态枚举
 * @date 2022/12/28 10:50
 */
public enum HelpStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    HelpStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static HelpStatus instanceOf(int key) {
        for (HelpStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
