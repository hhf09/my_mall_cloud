package com.hhf.mall.product.service.module.pms.product.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2022-04-14 0:04
 */
@Data
public class ProductAttrItem {
    private String key;
    private String value;
}
