package com.hhf.mall.gateway;

import com.hhf.mall.common.Const;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hhf
 * @description 网关
 * @date 2022-08-13 22:15
 */
//排除数据源配置  参考https://blog.csdn.net/syc000666/article/details/117996588
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = Const.FEIGN_PACKAGE)
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
