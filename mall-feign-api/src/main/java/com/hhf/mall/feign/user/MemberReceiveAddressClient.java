package com.hhf.mall.feign.user;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * @author hhf
 * @description: 收货地址feign接口
 * @date 2022/7/20 15:43
 */
@FeignClient(value = Const.USER_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "MemberReceiveAddressClient")
public interface MemberReceiveAddressClient {
    String URL_PREFIX = Const.USER_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/memberReceiveAddress";

    /**
     * 通过id查找
     * @param id
     * @return
     */
    @GetMapping(URL_PREFIX + "/getById")
    ResponseEntity<ApiResponse> getById(@RequestParam Long id);

    /**
     * 通过memberId查找
     * @param memberId
     * @return
     */
    @GetMapping(URL_PREFIX + "/getByMemberId")
    ResponseEntity<ApiResponse> getByMemberId(@RequestParam Long memberId);
}
