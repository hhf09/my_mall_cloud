package com.hhf.mall.product.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.feign.product.CommentClient;
import com.hhf.mall.product.service.module.pms.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 16:29
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/comment")
public class CommentClientImpl implements CommentClient {
    @Autowired
    private CommentService commentService;

    @Override
    @GetMapping("/countByMemberId")
    public ResponseEntity<ApiResponse> countByMemberId(Long memberId) {
        int count = commentService.countByMemberId(memberId);
        return ApiResponse.success(count);
    }
}
