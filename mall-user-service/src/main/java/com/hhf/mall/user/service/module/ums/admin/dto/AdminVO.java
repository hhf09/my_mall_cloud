package com.hhf.mall.user.service.module.ums.admin.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 9:23
 */
@Data
public class AdminVO {
    private Long id;
    private String username;
    private String icon;
    private String phone;
    private String email;
    private String nickname;
    private String note;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date loginTime;
    private Integer status;
    private String statusText;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date updateTime;
}
