package com.hhf.mall.common.service;

/**
 * @author hhf
 * @description: 接口操作类型
 * @date 2022/12/30 13:41
 */
public enum ActionType {
    /**
     * 增
     */
    ADD,
    /**
     * 改
     */
    UPDATE,
    /**
     * 删
     */
    DELETE;

    ActionType() {
    }
}
