package com.hhf.mall.product.service.module.pms.feightTemplate.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplate;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PmsFeightTemplateMapper extends BaseMapper<PmsFeightTemplate, Long, PmsFeightTemplateExample> {
    int countByExample(PmsFeightTemplateExample example);

    int deleteByExample(PmsFeightTemplateExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsFeightTemplate record);

    int insertSelective(PmsFeightTemplate record);

    List<PmsFeightTemplate> selectByExample(PmsFeightTemplateExample example);

    PmsFeightTemplate selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsFeightTemplate record, @Param("example") PmsFeightTemplateExample example);

    int updateByExample(@Param("record") PmsFeightTemplate record, @Param("example") PmsFeightTemplateExample example);

    int updateByPrimaryKeySelective(PmsFeightTemplate record);

    int updateByPrimaryKey(PmsFeightTemplate record);
}