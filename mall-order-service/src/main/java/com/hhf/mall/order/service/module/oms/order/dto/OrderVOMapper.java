package com.hhf.mall.order.service.module.oms.order.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.order.service.module.oms.order.entity.OmsOrder;
import com.hhf.mall.order.service.module.oms.order.entity.OrderConfirmStatus;
import com.hhf.mall.order.service.module.oms.order.entity.OrderPayType;
import com.hhf.mall.order.service.module.oms.order.entity.OrderStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 13:55
 */
@Component
public class OrderVOMapper extends BaseDTOMapper<OmsOrder, OrderVO> {
    @Override
    public OrderVO mapToDTO(OmsOrder entity) {
        OrderVO vo = new OrderVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(OrderStatus.instanceOf(entity.getStatus()).getText());
        vo.setConfirmStatusText(OrderConfirmStatus.instanceOf(entity.getConfirmStatus()).getText());
        vo.setPayTypeText(OrderPayType.instanceOf(entity.getPayType()).getText());
        return vo;
    }

    @Override
    public List<OrderVO> mapToDTOList(List<OmsOrder> entityList) {
        List<OrderVO> voList = new ArrayList<>();
        for (OmsOrder entity : entityList) {
            OrderVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
