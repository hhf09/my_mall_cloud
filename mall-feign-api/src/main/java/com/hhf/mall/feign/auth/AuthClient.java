package com.hhf.mall.feign.auth;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hhf
 * @description: 认证feign接口
 * @date 2023/2/8 19:43
 */
@FeignClient(value = Const.AUTH_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "AuthClient")
public interface AuthClient {
    String URL_PREFIX = Const.AUTH_SERVICE_CONTEXT_PATH + "/oauth";

    @PostMapping(URL_PREFIX + "/token")
    ResponseEntity<ApiResponse> token(@RequestParam String grant_type,
                                      @RequestParam String client_id,
                                      @RequestParam String client_secret,
                                      @RequestParam(required = false) String refresh_token,
                                      @RequestParam(required = false) String username,
                                      @RequestParam(required = false) String password) throws HttpRequestMethodNotSupportedException;

}
