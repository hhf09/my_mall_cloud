package com.hhf.mall.product.service.module.pms.commentReply.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.product.service.module.pms.commentReply.dto.CommentReplyVO;
import com.hhf.mall.product.service.module.pms.commentReply.dto.CommentReplyVOMapper;
import com.hhf.mall.product.service.module.pms.commentReply.entity.PmsCommentReply;
import com.hhf.mall.product.service.module.pms.commentReply.service.CommentReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author hhf
 * @description: 评论回复
 * @date 2022/12/29 9:17
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/reply")
@Validated
public class CommentReplyController {
    @Autowired
    private CommentReplyService commentReplyService;
    @Autowired
    private CommentReplyVOMapper commentReplyVOMapper;

    /**
     *
     * @param memberId
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_COMMENT_REPLY)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"memberId"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsCommentReply> list = commentReplyService.list(listParam);
        List<CommentReplyVO> voList = commentReplyVOMapper.mapToDTOList(list);
        return ApiResponse.success(voList);
    }

    @ApiLog(note = "添加回复")
    @RequirePermissions(PermissionConst.ADMIN_COMMENT_REPLY)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody PmsCommentReply reply, HttpServletRequest request) {
        reply.setDefault();
        String userString = request.getHeader(AuthConst.USERINFO_REQUEST_HEADER);
        if (StrUtil.isEmpty(userString)) {
            throw new CodeException("请求头的用户信息不存在");
        }
        UserDTOForAuth dto = JSON.parseObject(userString, UserDTOForAuth.class);
        reply.setMemberId(dto.getId());
        commentReplyService.add(reply);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新回复")
    @RequirePermissions(PermissionConst.ADMIN_COMMENT_REPLY)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody PmsCommentReply reply) {
        commentReplyService.updateByPrimaryKeySelective(reply);
        return ApiResponse.success();
    }

    @ApiLog(note = "删除回复")
    @RequirePermissions(PermissionConst.ADMIN_COMMENT_REPLY)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        commentReplyService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }
}
