package com.hhf.mall.feign.product;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import com.hhf.mall.dto.product.ProductDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hhf
 * @description: 商品feign接口
 * @date 2022/7/20 17:13
 */
@FeignClient(value = Const.PRODUCT_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "ProductClient")
public interface ProductClient {
    String URL_PREFIX = Const.PRODUCT_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/product";

    /**
     * 通过id查找
     *
     * @param id
     * @return
     */
    @GetMapping(URL_PREFIX + "/getById")
    ResponseEntity<ApiResponse> getById(@RequestParam Long id);

    /**
     * 通过id修改
     *
     * @param productDTO
     * @return
     */
    @PostMapping(URL_PREFIX + "/updateById")
    ResponseEntity<ApiResponse> updateById(@RequestBody ProductDTO productDTO);
}
