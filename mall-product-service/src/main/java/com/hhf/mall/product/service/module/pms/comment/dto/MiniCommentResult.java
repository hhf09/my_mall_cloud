package com.hhf.mall.product.service.module.pms.comment.dto;

import com.hhf.mall.product.service.module.pms.comment.entity.PmsComment;
import com.hhf.mall.product.service.module.pms.commentReply.dto.CommentReplyDTO;
import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-02-03 20:51
 */
@Data
public class MiniCommentResult extends PmsComment {
    private List<CommentReplyDTO> replyList;
}
