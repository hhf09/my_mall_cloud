package com.hhf.mall.gateway.authorization;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.feign.user.PermissionClient;
import com.hhf.mall.gateway.config.IgnoreUrlsConfig;
import com.nimbusds.jose.JWSObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.ReactiveAuthorizationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.authorization.AuthorizationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 自定义鉴权管理器
 * @date 2023/2/8 10:47
 */
@Component
public class CustomAuthorizationManager implements ReactiveAuthorizationManager<AuthorizationContext> {
    @Autowired
    private IgnoreUrlsConfig ignoreUrlsConfig;
    @Autowired
    private PermissionClient permissionClient;

    @Override
    public Mono<AuthorizationDecision> check(Mono<Authentication> mono, AuthorizationContext authorizationContext) {
        ServerHttpRequest request = authorizationContext.getExchange().getRequest();
        URI uri = request.getURI();
        String uriPath = uri.getPath();
        AntPathMatcher pathMatcher = new AntPathMatcher();
        List<String> ignoreUrls = ignoreUrlsConfig.getUrls();
        for (String ignoreUrl : ignoreUrls) {
            if (pathMatcher.match(ignoreUrl, uriPath)) {
                // 白名单路径放行
                return Mono.just(new AuthorizationDecision(true));
            }
        }
        if (request.getMethod() == HttpMethod.OPTIONS) {
            //对应跨域的预检请求直接放行
            return Mono.just(new AuthorizationDecision(true));
        }
        String token = request.getHeaders().getFirst(AuthConst.JWT_REQUEST_HEADER);
        if (StrUtil.isEmpty(token)) {
            //token不存在
            return Mono.just(new AuthorizationDecision(false));
        }
        token = token.replace(AuthConst.TOKEN_HEADER + " ", "");
        try {
            JWSObject jwsObject = JWSObject.parse(token);
            String userString = jwsObject.getPayload().toString();
            UserDTOForAuth dto = JSON.parseObject(userString, UserDTOForAuth.class);
            if (AuthConst.ADMIN_CLIENT_ID.equals(dto.getClientId()) && pathMatcher.match(AuthConst.MEMBER_URL_PATTERN, uriPath)) {
                //后台用户只能访问后台接口
                return Mono.just(new AuthorizationDecision(false));
            }
            if (AuthConst.MEMBER_CLIENT_ID.equals(dto.getClientId()) && !pathMatcher.match(AuthConst.MEMBER_URL_PATTERN, uriPath)) {
                //前台用户只能访问前台接口
                return Mono.just(new AuthorizationDecision(false));
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return Mono.just(new AuthorizationDecision(false));
        }
        //查找全部权限标志
        ResponseEntity<ApiResponse> responseEntity = permissionClient.listValues();
        ApiResponse response = responseEntity.getBody();
        List<String> permissionValueList = response.toList(String.class);
        if (CollUtil.isEmpty(permissionValueList)) {
            throw new CodeException("权限标志为空");
        }
        List<String> authorities = new ArrayList<>();
        for (String value : permissionValueList) {
            authorities.add(AuthConst.AUTHORITY_PREFIX + value);
        }
        return mono
                .filter(Authentication::isAuthenticated)
                .flatMapIterable(Authentication::getAuthorities)
                .map(GrantedAuthority::getAuthority)
                .any(authorities::contains)
                .map(AuthorizationDecision::new)
                .defaultIfEmpty(new AuthorizationDecision(false));
    }
}
