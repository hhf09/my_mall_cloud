package com.hhf.mall.product.service.module.pms.comment.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsComment;
import com.hhf.mall.product.service.module.pms.comment.entity.PmsCommentExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PmsCommentMapper extends BaseMapper<PmsComment, Long, PmsCommentExample> {
    int countByExample(PmsCommentExample example);

    int deleteByExample(PmsCommentExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PmsComment record);

    int insertSelective(PmsComment record);

    List<PmsComment> selectByExampleWithBLOBs(PmsCommentExample example);

    List<PmsComment> selectByExample(PmsCommentExample example);

    PmsComment selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PmsComment record, @Param("example") PmsCommentExample example);

    int updateByExampleWithBLOBs(@Param("record") PmsComment record, @Param("example") PmsCommentExample example);

    int updateByExample(@Param("record") PmsComment record, @Param("example") PmsCommentExample example);

    int updateByPrimaryKeySelective(PmsComment record);

    int updateByPrimaryKeyWithBLOBs(PmsComment record);

    int updateByPrimaryKey(PmsComment record);

    //自定义start

    /**
     * 根据商品id统计评论
     * @param productId
     * @return
     */
    int countAll(Long productId);

    /**
     * 根据商品id和评价程度统计评论
     * @param map
     * @return
     */
    int countByEvaluationStatus(Map<String, Object> map);

    /**
     * 根据商品id统计有图片的评论
     * @param productId
     * @return
     */
    int countPic(Long productId);

    /**
     * 后台查询
     * @param map
     * @return
     */
    List<PmsComment> listForAdmin(Map<String, Object> map);

    /**
     * 前台查询
     * @param map
     * @return
     */
    List<PmsComment> listForWeb(Map<String, Object> map);
}