package com.hhf.mall.marketing.service.module.sms.newProduct.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author hhf
 * @description: 
 * @date 2022-04-05 21:08
 */
@Data
public class NewProductVO {
    private Long id;
    private Long productId;
    private Integer recommendStatus;
    private String recommendStatusText;
    private Integer sort;
    private String productName;
    private String brandName;
    private BigDecimal price;
    private String productPic;
    private String subTitle;
}
