package com.hhf.mall.common.exception;

import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.controller.CustomPageInfo;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * 接口响应信息
 */
@Data
public class ApiResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 错误码
     */
    private int code;
    /**
     * 提示信息
     */
    private String message;
    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 返回数据
     */
    private Object data;
    /**
     * 分页信息
     */
    private CustomPageInfo pageInfo;

    /**
     * 成功情况返回
     *
     * @param data
     * @return
     */
    public static ResponseEntity<ApiResponse> success(Object data) {
        ApiResponse response = new ApiResponse();
        response.setCode(ApiCode.SUCCESS.getCode());
        response.setMessage(ApiCode.SUCCESS.getMessage());
        response.setData(data);
        response.setSuccess(true);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    /**
     * 成功情况返回
     *
     * @return
     */
    public static ResponseEntity<ApiResponse> success() {
        ApiResponse response = new ApiResponse();
        response.setCode(ApiCode.SUCCESS.getCode());
        response.setMessage(ApiCode.SUCCESS.getMessage());
        response.setSuccess(true);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    /**
     * 成功情况返回（分页）
     * @param data
     * @param pageInfo
     * @return
     */
    public static ResponseEntity<ApiResponse> success(Object data, CustomPageInfo pageInfo) {
        ApiResponse response = new ApiResponse();
        response.setCode(ApiCode.SUCCESS.getCode());
        response.setMessage(ApiCode.SUCCESS.getMessage());
        response.setData(data);
        response.setPageInfo(pageInfo);
        response.setSuccess(true);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    /**
     * 错误情况返回
     * @param code
     * @param message
     * @return
     */
    public static ResponseEntity<ApiResponse> error(int code, String message) {
        ApiResponse response = new ApiResponse();
        response.setCode(code);
        response.setMessage(message);
        response.setSuccess(false);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    /**
     * 错误情况返回
     * @param message
     * @return
     */
    public static ResponseEntity<ApiResponse> error(String message) {
        return error(-1, message);
    }

    /**
     * 判断是否成功且存在返回数据
     * @return
     */
    public boolean hasData() {
        return isSuccess() && getData() != null;
    }

    /**
     * 转换为对象
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T toObject(Class<T> clazz) {
        return hasData() ? JSON.parseObject(JSON.toJSONString(getData()), clazz) : null;
    }

    /**
     * 转换为对象列表
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> List<T> toList(Class<T> clazz) {
        return hasData() ? JSON.parseArray(JSON.toJSONString(getData()), clazz) : null;
    }
}
