package com.hhf.mall.user.service.feign;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.user.ApiLogDTO;
import com.hhf.mall.feign.user.ApiLogClient;
import com.hhf.mall.user.service.module.sys.apiLog.dto.ApiLogVO;
import com.hhf.mall.user.service.module.sys.apiLog.dto.ApiLogVOMapper;
import com.hhf.mall.user.service.module.sys.apiLog.entity.SysApiLog;
import com.hhf.mall.user.service.module.sys.apiLog.service.ApiLogService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023-01-27 21:06
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/apiLog")
public class ApiLogClientImpl implements ApiLogClient {
    @Autowired
    private ApiLogService apiLogService;
    @Autowired
    private ApiLogVOMapper apiLogVOMapper;

    @Override
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(ApiLogDTO dto) {
        SysApiLog entity = new SysApiLog();
        entity.setDefault();
        BeanUtils.copyProperties(dto, entity);
        apiLogService.add(entity);
        return ApiResponse.success();
    }

    @Override
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(ListParam listParam) {
        List<SysApiLog> list = apiLogService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SysApiLog> pageInfo = new PageInfo<>(list);
            List<ApiLogVO> voList = apiLogVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

}
