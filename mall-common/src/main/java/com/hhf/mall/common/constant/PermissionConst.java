package com.hhf.mall.common.constant;

/**
 * @author hhf
 * @description: 权限标志常量
 * @date 2023/2/15 19:08
 */
public interface PermissionConst {
    /**
     * 前缀
     */
    String prefix = "P_";
    /**
     * 后台
     */
    String admin = "ADMIN_";
    /**
     * 前台
     */
    String web = "WEB_";
    //-----------------------------
    //后台权限
    //-----------------------------
    /**
     * 品牌管理
     */
    String ADMIN_BRAND = prefix + admin + "BRAND";
    /**
     * 商品分类
     */
    String ADMIN_PRODUCT_CATEGORY = prefix + admin + "PRODUCT_CATEGORY";
    /**
     * 商品管理
     */
    String ADMIN_PRODUCT = prefix + admin + "PRODUCT";
    /**
     * 商品属性
     */
    String ADMIN_PRODUCT_ATTRIBUTE = prefix + admin + "PRODUCT_ATTRIBUTE";
    /**
     * 商品属性分类
     */
    String ADMIN_PRODUCT_ATTRIBUTE_CATEGORY = prefix + admin + "PRODUCT_ATTRIBUTE_CATEGORY";
    /**
     * 评论管理
     */
    String ADMIN_COMMENT = prefix + admin + "COMMENT";
    /**
     * 评论回复管理
     */
    String ADMIN_COMMENT_REPLY = prefix + admin + "COMMENT_REPLY";
    /**
     * 订单管理
     */
    String ADMIN_ORDER = prefix + admin + "ORDER";
    /**
     * 退货管理
     */
    String ADMIN_ORDER_REFUND = prefix + admin + "ORDER_REFUND";
    /**
     * 退货原因
     */
    String ADMIN_ORDER_REFUND_REASON = prefix + admin + "ORDER_REFUND_REASON";
    /**
     * 广告管理
     */
    String ADMIN_ADVERTISE = prefix + admin + "ADVERTISE";
    /**
     * 新品推荐
     */
    String ADMIN_NEW_RECOMMEND = prefix + admin + "NEW_RECOMMEND";
    /**
     * 人气推荐
     */
    String ADMIN_POPULAR_RECOMMEND = prefix + admin + "POPULAR_RECOMMEND";
    /**
     * 帮助分类
     */
    String ADMIN_HELP_CATEGORY = prefix + admin + "HELP_CATEGORY";
    /**
     * 帮助管理
     */
    String ADMIN_HELP = prefix + admin + "HELP";
    /**
     * 后台用户管理
     */
    String ADMIN_MANAGER = prefix + admin + "MANAGER";
    /**
     * 角色管理
     */
    String ADMIN_ROLE = prefix + admin + "ROLE";
    /**
     * 权限管理
     */
    String ADMIN_PERMISSION = prefix + admin + "PERMISSION";
    /**
     * 前台用户管理
     */
    String ADMIN_MEMBER = prefix + admin + "MEMBER";
    /**
     * 收货地址
     */
    String ADMIN_MEMBER_ADDRESS = prefix + admin + "MEMBER_ADDRESS";
    /**
     * 操作日志
     */
    String ADMIN_API_LOG = prefix + admin + "API_LOG";
    /**
     * 异常日志
     */
    String ADMIN_ERROR_LOG = prefix + admin + "ERROR_LOG";

    //-----------------------------
    //前台权限
    //-----------------------------
    /**
     * 用户
     */
    String WEB_MEMBER = prefix + web + "MEMBER";
    /**
     * 收货地址
     */
    String WEB_MEMBER_ADDRESS = prefix + web + "MEMBER_ADDRESS";
    /**
     * 帮助
     */
    String WEB_HELP = prefix + web + "HELP";
    /**
     * 帮助分类
     */
    String WEB_HELP_CATEGORY = prefix + web + "HELP_CATEGORY";
    /**
     * 广告
     */
    String WEB_ADVERTISE = prefix + web + "ADVERTISE";
    /**
     * 新品推荐
     */
    String WEB_NEW_RECOMMEND = prefix + web + "NEW_RECOMMEND";
    /**
     * 人气推荐
     */
    String WEB_POPULAR_RECOMMEND = prefix + web + "POPULAR_RECOMMEND";
    /**
     * 购物车
     */
    String WEB_CART_ITEM = prefix + web + "CART_ITEM";
    /**
     * 订单
     */
    String WEB_ORDER = prefix + web + "ORDER";
    /**
     * 退货
     */
    String WEB_ORDER_REFUND = prefix + web + "ORDER_REFUND";
    /**
     * 退货原因
     */
    String WEB_ORDER_REFUND_REASON = prefix + web + "ORDER_REFUND_REASON";
    /**
     * 商品分类
     */
    String WEB_PRODUCT_CATEGORY = prefix + web + "PRODUCT_CATEGORY";
    /**
     * 商品
     */
    String WEB_PRODUCT = prefix + web + "PRODUCT";
    /**
     * 评论
     */
    String WEB_COMMENT = prefix + web + "COMMENT";
    /**
     * 评论回复
     */
    String WEB_COMMENT_REPLY = prefix + web + "COMMENT_REPLY";

}
