package com.hhf.mall.user.service.aop;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hhf.mall.common.Const;
import com.hhf.mall.common.aop.BaseLogAspect;
import com.hhf.mall.common.constant.UserType;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.user.service.module.sys.apiLog.entity.SysApiLog;
import com.hhf.mall.user.service.module.sys.apiLog.service.ApiLogService;
import com.hhf.mall.user.service.module.sys.errorLog.entity.SysErrorLog;
import com.hhf.mall.user.service.module.sys.errorLog.service.ErrorLogService;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminDTOWithToken;
import com.hhf.mall.user.service.module.ums.member.dto.MemberDTOWithToken;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

/**
 * @author hhf
 * @description 日志处理切面
 * @date 2022-01-29 15:32
 */
@Aspect
@Component
@Order(1)
public class LogAspect extends BaseLogAspect {

    @Autowired
    private ApiLogService apiLogService;
    @Autowired
    private ErrorLogService errorLogService;

    /**
     * 后台用户登录切点表达式
     */
    @Pointcut("execution(public * " + Const.USER_SERVICE_MODULE_PACKAGE + "ums.admin.controller.AdminController.login(..))")
    public void adminLoginPointcut() {
    }

    /**
     * 前台用户登录切点表达式
     */
    @Pointcut("execution(public * " + Const.USER_SERVICE_MODULE_PACKAGE + "ums.member.controller.MemberWebController.login(..))")
    public void memberLoginPointcut() {

    }

    /**
     * 新增切点表达式
     */
    @Pointcut("execution(public * " + Const.USER_SERVICE_MODULE_PACKAGE + "*.*.controller.*.add*(..))")
    public void addPointcut() {
    }

    /**
     * 更新切点表达式
     */
    @Pointcut("execution(public * " + Const.USER_SERVICE_MODULE_PACKAGE + "*.*.controller.*.update*(..))")
    public void updatePointcut() {
    }

    /**
     * 删除切点表达式
     */
    @Pointcut("execution(public * " + Const.USER_SERVICE_MODULE_PACKAGE + "*.*.controller.*.delete*(..))")
    public void deletePointcut() {
    }

    /**
     * 异常切点表达式
     */
    @Pointcut("execution(public * " + Const.USER_SERVICE_MODULE_PACKAGE + "*.*.*.*.*(..))")
    public void exceptionPointcut() {
    }

    /**
     * 后台登录后置通知
     */
    @AfterReturning(value = "adminLoginPointcut()", returning = "ret")
    public void doAfterAdminLogin(JoinPoint joinPoint, Object ret) throws Throwable {
        doLoginAfterReturning(joinPoint, ret, "admin");
    }

    /**
     * 前台登录后置通知
     */
    @AfterReturning(value = "memberLoginPointcut()", returning = "ret")
    public void doAfterMemberLogin(JoinPoint joinPoint, Object ret) throws Throwable {
        doLoginAfterReturning(joinPoint, ret, "member");
    }

    public void doLoginAfterReturning(JoinPoint joinPoint, Object ret, String type) throws Throwable {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        //获取请求参数
        Object parameter = super.getParameter(method, joinPoint.getArgs());

        //获取adminId
        String retString = JSON.toJSONString(ret);
        JSONObject jsonObject = JSON.parseObject(retString);
        String body = jsonObject.get("body").toString();
        ApiResponse response = JSON.parseObject(body, ApiResponse.class);
        Long userId;
        String token;
        if ("admin".equals(type)) {
            AdminDTOWithToken dto = response.toObject(AdminDTOWithToken.class);
            userId = dto.getUserId();
            token = dto.getToken().getToken();
        } else {
            MemberDTOWithToken dto = response.toObject(MemberDTOWithToken.class);
            userId = dto.getUserId();
            token = dto.getToken().getToken();
        }

        //获取当前请求对象
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String userAgent = super.getUserAgent(request.getHeader(Const.USER_AGENT_HEADER));
        String note = super.getApiNote(method);

        SysApiLog log = new SysApiLog();
        log.setDefault();
        log.setUserType(UserType.ADMIN.getKey());
        log.setServiceSign(Const.USER_SERVICE_NAME);
        log.setUserId(userId);
        log.setIp(request.getRemoteAddr());
        log.setReqMethod(request.getRequestURI());
        log.setToken(token);
        log.setNote(note);
        log.setUserAgent(userAgent);
        log.setReqParams(JSON.toJSONString(parameter));
        apiLogService.add(log);
    }

    /**
     * 异常后置通知
     *
     * @param joinPoint
     * @param throwing
     */
    @AfterThrowing(value = "exceptionPointcut()", throwing = "throwing")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable throwing) {
        Map<String, String> map = super.handleAfterThrowing(joinPoint, throwing);
        SysErrorLog log = new SysErrorLog();
        log.setDefault();
        log.setServiceSign(Const.USER_SERVICE_NAME);
        log.setUserId(Long.valueOf(map.get("userId")));
        log.setIp(map.get("ip"));
        log.setReqMethod(map.get("uri"));
        log.setToken(map.get("token"));
        log.setNote(map.get("note"));
        log.setUserAgent(map.get("userAgent"));
        log.setReqParams(map.get("params"));
        log.setUserType(Integer.valueOf(map.get("userType")));
        errorLogService.add(log);
    }

    /**
     * 新增后置通知
     */
    @AfterReturning("addPointcut()")
    public void doAfterAdd(JoinPoint joinPoint) throws Throwable {
        doAfterReturning(joinPoint);
    }

    /**
     * 更新后置通知
     */
    @AfterReturning("updatePointcut()")
    public void doAfterUpdate(JoinPoint joinPoint) throws Throwable {
        doAfterReturning(joinPoint);
    }

    /**
     * 删除后置通知
     */
    @AfterReturning("deletePointcut()")
    public void doAfterDelete(JoinPoint joinPoint) throws Throwable {
        doAfterReturning(joinPoint);
    }

    public void doAfterReturning(JoinPoint joinPoint) throws Throwable {
        Map<String, String> map = super.handleAfterReturning(joinPoint);
        SysApiLog log = new SysApiLog();
        log.setCreateTime(new Date());
        log.setServiceSign(Const.USER_SERVICE_NAME);
        log.setUserId(Long.valueOf(map.get("userId")));
        log.setIp(map.get("ip"));
        log.setReqMethod(map.get("uri"));
        log.setToken(map.get("token"));
        log.setNote(map.get("note"));
        log.setUserAgent(map.get("userAgent"));
        log.setReqParams(map.get("params"));
        log.setUserType(Integer.valueOf(map.get("userType")));
        apiLogService.add(log);
    }
}

