package com.hhf.eureka_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author hhf
 * @description eureka服务端
 * @date 2022-06-29 23:52
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer8761Application {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer8761Application.class, args);
    }
}
