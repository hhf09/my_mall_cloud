package com.hhf.mall.user.service.module.ums.memberReceiveAddress.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddress;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddressExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UmsMemberReceiveAddressMapper extends BaseMapper<UmsMemberReceiveAddress, Long, UmsMemberReceiveAddressExample> {
    int countByExample(UmsMemberReceiveAddressExample example);

    int deleteByExample(UmsMemberReceiveAddressExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UmsMemberReceiveAddress record);

    int insertSelective(UmsMemberReceiveAddress record);

    List<UmsMemberReceiveAddress> selectByExample(UmsMemberReceiveAddressExample example);

    UmsMemberReceiveAddress selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UmsMemberReceiveAddress record, @Param("example") UmsMemberReceiveAddressExample example);

    int updateByExample(@Param("record") UmsMemberReceiveAddress record, @Param("example") UmsMemberReceiveAddressExample example);

    int updateByPrimaryKeySelective(UmsMemberReceiveAddress record);

    int updateByPrimaryKey(UmsMemberReceiveAddress record);
}