package com.hhf.mall.user.service.module.ums.member.dto;

import com.hhf.mall.dto.auth.TokenDTO;
import lombok.Data;

/**
 * @author hhf
 * @description:
 * @date 2023/2/16 16:05
 */
@Data
public class MemberDTOWithToken {
    private Long userId;
    private String type;
    private TokenDTO token;
}
