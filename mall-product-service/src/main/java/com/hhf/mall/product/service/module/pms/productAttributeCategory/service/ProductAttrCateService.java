package com.hhf.mall.product.service.module.pms.productAttributeCategory.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.productAttribute.entity.PmsProductAttributeExample;
import com.hhf.mall.product.service.module.pms.productAttribute.entity.ProductAttributeType;
import com.hhf.mall.product.service.module.pms.productAttribute.service.ProductAttrService;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.dto.ProductAttributeCateItem;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.entity.PmsProductAttributeCategory;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.entity.PmsProductAttributeCategoryExample;
import com.hhf.mall.product.service.module.pms.productAttributeCategory.mapper.PmsProductAttributeCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author hhf
 * @description: 
 * @date 2022-01-09 19:46
 */
@Service
public class ProductAttrCateService extends BaseService<PmsProductAttributeCategory, Long, PmsProductAttributeCategoryExample> {
    @Autowired
    private PmsProductAttributeCategoryMapper productAttributeCategoryMapper;

    @Autowired
    private ProductAttrService productAttrService;

    @Override
    protected void before(PmsProductAttributeCategory entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(PmsProductAttributeCategory entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsProductAttributeCategory, Long, PmsProductAttributeCategoryExample> getMapper() {
        return productAttributeCategoryMapper;
    }

    public List<ProductAttributeCateItem> getListWithAttr() {
        return productAttributeCategoryMapper.getListWithAttr();
    }


    public List<PmsProductAttributeCategory> list(ListParam listParam) {
        PmsProductAttributeCategoryExample example = new PmsProductAttributeCategoryExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        List<PmsProductAttributeCategory> productAttributeCateList = productAttributeCategoryMapper.selectByExample(example);
        for (PmsProductAttributeCategory productAttributeCategory : productAttributeCateList) {
            PmsProductAttributeExample example2 = new PmsProductAttributeExample();
            ListParam listParam2 = new ListParam().add("productAttributeCategoryId", String.valueOf(productAttributeCategory.getId()))
                    .add("type", String.valueOf(ProductAttributeType.ATTRIBUTE.getKey()));
            productAttrService.selectCondition(example2, listParam2);
            int attrCount = productAttrService.countByExample(example2);
            productAttributeCategory.setAttributeCount(attrCount);

            PmsProductAttributeExample example3 = new PmsProductAttributeExample();
            ListParam listParam3 = new ListParam().add("productAttributeCategoryId", String.valueOf(productAttributeCategory.getId()))
                    .add("type", String.valueOf(ProductAttributeType.PARAM.getKey()));
            productAttrService.selectCondition(example3, listParam3);
            int paramCount = productAttrService.countByExample(example3);
            productAttributeCategory.setParamCount(paramCount);
        }
        return productAttributeCateList;
    }

    public void selectOrderRule(PmsProductAttributeCategoryExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsProductAttributeCategoryExample example, ListParam listParam) {
        PmsProductAttributeCategoryExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "":
                    break;
            }
        }
    }

}
