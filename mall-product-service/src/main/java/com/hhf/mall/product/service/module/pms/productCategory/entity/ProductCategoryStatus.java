package com.hhf.mall.product.service.module.pms.productCategory.entity;

/**
 * @author hhf
 * @description: 商品分类状态枚举
 * @date 2022/12/28 10:50
 */
public enum ProductCategoryStatus {
    STATUS_DISABLE(0, "禁用"),
    STATUS_ENABLE(1, "启用");

    private final int key;
    private final String text;

    ProductCategoryStatus(int key, String text) {
        this.key = key;
        this.text = text;
    }

    public int getKey() {
        return key;
    }

    public String getText() {
        return text;
    }

    public static ProductCategoryStatus instanceOf(int key) {
        for (ProductCategoryStatus e : values()) {
            if (e.getKey() == key) {
                return e;
            }
        }
        throw new IndexOutOfBoundsException(String.format("enum.key:%d not found", key));
    }
}
