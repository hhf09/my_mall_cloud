package com.hhf.mall.order.service.module.oms.cartItem.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.order.service.module.oms.cartItem.entity.OmsCartItem;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 13:40
 */
@Component
public class CartItemDTOMapper extends BaseDTOMapper<OmsCartItem, CartItemDTO> {
    @Override
    public CartItemDTO mapToDTO(OmsCartItem entity) {
        CartItemDTO dto = new CartItemDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<CartItemDTO> mapToDTOList(List<OmsCartItem> entityList) {
        List<CartItemDTO> dtoList = new ArrayList<>();
        for (OmsCartItem entity : entityList) {
            CartItemDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
