package com.hhf.mall.marketing.service.module.sms.recommendProduct.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.dto.RecommendProductDTO;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.dto.RecommendProductDTOMapper;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.RecommendProductRecommendStatus;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.SmsHomeRecommendProduct;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.service.RecommendProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/12/29 11:26
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/recommendProduct")
@Validated
public class RecommendProductWebController {
    @Autowired
    private RecommendProductService recommendProductService;
    @Autowired
    private RecommendProductDTOMapper recommendProductDTOMapper;

    /**
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_POPULAR_RECOMMEND)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("recommendStatus", String.valueOf(RecommendProductRecommendStatus.YES.getKey()));
        List<SmsHomeRecommendProduct> list = recommendProductService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SmsHomeRecommendProduct> pageInfo = new PageInfo<>(list);
            List<RecommendProductDTO> dtoList = recommendProductDTOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(dtoList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }
}
