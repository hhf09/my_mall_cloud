package com.hhf.mall.user.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.feign.user.PermissionClient;
import com.hhf.mall.user.service.module.ums.permission.entity.PermissionStatus;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import com.hhf.mall.user.service.module.ums.permission.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/15 10:39
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/permission")
public class PermissionClientImpl implements PermissionClient {
    @Autowired
    private PermissionService permissionService;

    @Override
    @GetMapping("/listValues")
    public ResponseEntity<ApiResponse> listValues() {
        ListParam listParam = new ListParam().add("status", String.valueOf(PermissionStatus.STATUS_ENABLE.getKey()));
        List<UmsPermission> permissionList = permissionService.list(listParam);
        List<String> list = permissionList.stream().map(UmsPermission::getValue).collect(Collectors.toList());
        return ApiResponse.success(list);
    }
}
