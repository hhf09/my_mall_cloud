package com.hhf.mall.basic.service.module.cms.help.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.basic.service.module.cms.help.dto.HelpVO;
import com.hhf.mall.basic.service.module.cms.help.dto.HelpVOMapper;
import com.hhf.mall.basic.service.module.cms.help.entity.CmsHelp;
import com.hhf.mall.basic.service.module.cms.help.service.HelpService;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description 帮助
 * @date 2022-01-29 14:39
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/help")
@Validated
public class HelpController {
    @Autowired
    private HelpService helpService;
    @Autowired
    private HelpVOMapper helpVOMapper;

    /**
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_HELP)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<CmsHelp> list = helpService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<CmsHelp> pageInfo = new PageInfo<>(list);
            List<HelpVO> voList = helpVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    @ApiLog(note = "添加帮助")
    @RequirePermissions(PermissionConst.ADMIN_HELP)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody CmsHelp help) {
        help.setDefault();
        helpService.add(help);
        return ApiResponse.success();
    }

    @ApiLog(note = "更新帮助")
    @RequirePermissions(PermissionConst.ADMIN_HELP)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody CmsHelp help) {
        help.setCreateTime(new Date());
        helpService.updateByPrimaryKeySelective(help);
        return ApiResponse.success();
    }

    @ApiLog(note = "删除帮助")
    @RequirePermissions(PermissionConst.ADMIN_HELP)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        helpService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }
}
