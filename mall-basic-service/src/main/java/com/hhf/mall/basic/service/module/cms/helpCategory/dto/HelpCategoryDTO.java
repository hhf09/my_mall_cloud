package com.hhf.mall.basic.service.module.cms.helpCategory.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 11:27
 */
@Data
public class HelpCategoryDTO {
    private Long id;
    private String name;
    private Integer status;
    private String statusText;
    private Integer sort;
}
