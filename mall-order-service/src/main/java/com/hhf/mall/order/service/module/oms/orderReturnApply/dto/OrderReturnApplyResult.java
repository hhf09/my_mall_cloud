package com.hhf.mall.order.service.module.oms.orderReturnApply.dto;

import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApply;
import lombok.Data;

/**
 * @author hhf
 * @description 退货申请结果
 * @date 2022-03-06 11:36
 */
@Data
public class OrderReturnApplyResult extends OmsOrderReturnApply {
    //退货原因
    private String reasonName;
}
