package com.hhf.mall.product.service.module.pms.product.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:23
 */
@Component
public class ProductVOMapper extends BaseDTOMapper<PmsProduct, ProductVO> {
    @Override
    public ProductVO mapToDTO(PmsProduct entity) {
        ProductVO vo = new ProductVO();
        BeanUtils.copyProperties(entity, vo);
        return vo;
    }

    @Override
    public List<ProductVO> mapToDTOList(List<PmsProduct> entityList) {
        List<ProductVO> voList = new ArrayList<>();
        for (PmsProduct entity : entityList) {
            ProductVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
