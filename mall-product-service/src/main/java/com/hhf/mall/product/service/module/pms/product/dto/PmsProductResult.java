package com.hhf.mall.product.service.module.pms.product.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-14 0:06
 */
@Data
public class PmsProductResult extends PmsProductParam {
    private Long cateParentId;
}
