package com.hhf.mall.product.service.module.pms.brand.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.product.service.module.pms.brand.dto.BrandVO;
import com.hhf.mall.product.service.module.pms.brand.dto.BrandVOMapper;
import com.hhf.mall.product.service.module.pms.brand.entity.PmsBrand;
import com.hhf.mall.product.service.module.pms.brand.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author hhf
 * @description 品牌
 * @date 2022-01-04 23:57
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/brand")
@Validated
public class BrandController {
    @Autowired
    private BrandService brandService;
    @Autowired
    private BrandVOMapper brandVOMapper;

    /**
     * 分页查找
     *
     * @param keyword
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_BRAND)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{ListParam.KEYWORD};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<PmsBrand> list = brandService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<PmsBrand> pageInfo = new PageInfo<>(list);
            List<BrandVO> voList = brandVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 添加
     *
     * @param brand
     * @return
     */
    @ApiLog(note = "添加品牌")
    @PostMapping("/add")
    @RequirePermissions(PermissionConst.ADMIN_BRAND)
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody PmsBrand brand) {
        brand.setDefault();
        brandService.add(brand);
        return ApiResponse.success();
    }

    /**
     * 修改
     *
     * @param brand
     * @return
     */
    @ApiLog(note = "更新品牌")
    @PostMapping("/update")
    @RequirePermissions(PermissionConst.ADMIN_BRAND)
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody PmsBrand brand) {
        brandService.updateByPrimaryKeySelective(brand);
        return ApiResponse.success();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiLog(note = "删除品牌")
    @PostMapping("/delete")
    @RequirePermissions(PermissionConst.ADMIN_BRAND)
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        brandService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }
}
