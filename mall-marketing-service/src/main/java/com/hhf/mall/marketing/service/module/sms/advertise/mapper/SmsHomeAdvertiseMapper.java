package com.hhf.mall.marketing.service.module.sms.advertise.mapper;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertise;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertiseExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsHomeAdvertiseMapper extends BaseMapper<SmsHomeAdvertise, Long, SmsHomeAdvertiseExample> {
    int countByExample(SmsHomeAdvertiseExample example);

    int deleteByExample(SmsHomeAdvertiseExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SmsHomeAdvertise record);

    int insertSelective(SmsHomeAdvertise record);

    List<SmsHomeAdvertise> selectByExample(SmsHomeAdvertiseExample example);

    SmsHomeAdvertise selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SmsHomeAdvertise record, @Param("example") SmsHomeAdvertiseExample example);

    int updateByExample(@Param("record") SmsHomeAdvertise record, @Param("example") SmsHomeAdvertiseExample example);

    int updateByPrimaryKeySelective(SmsHomeAdvertise record);

    int updateByPrimaryKey(SmsHomeAdvertise record);
}