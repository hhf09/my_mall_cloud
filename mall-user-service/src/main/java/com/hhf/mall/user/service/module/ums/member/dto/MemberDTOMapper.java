package com.hhf.mall.user.service.module.ums.member.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 9:50
 */
@Component
public class MemberDTOMapper extends BaseDTOMapper<UmsMember, MemberDTO> {
    @Override
    public MemberDTO mapToDTO(UmsMember entity) {
        MemberDTO dto = new MemberDTO();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    @Override
    public List<MemberDTO> mapToDTOList(List<UmsMember> entityList) {
        List<MemberDTO> dtoList = new ArrayList<>();
        for (UmsMember entity : entityList) {
            MemberDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
