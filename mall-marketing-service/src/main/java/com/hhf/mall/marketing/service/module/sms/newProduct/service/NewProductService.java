package com.hhf.mall.marketing.service.module.sms.newProduct.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.SmsHomeNewProduct;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.SmsHomeNewProductExample;
import com.hhf.mall.marketing.service.module.sms.newProduct.mapper.SmsHomeNewProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-22 23:19
 */
@Service
public class NewProductService extends BaseService<SmsHomeNewProduct, Long, SmsHomeNewProductExample> {
    @Autowired
    private SmsHomeNewProductMapper newProductMapper;
    @Autowired
    private ProductClient productClient;

    @Override
    protected void before(SmsHomeNewProduct entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(SmsHomeNewProduct entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<SmsHomeNewProduct, Long, SmsHomeNewProductExample> getMapper() {
        return newProductMapper;
    }

    public List<SmsHomeNewProduct> list(ListParam listParam) {
        SmsHomeNewProductExample example = new SmsHomeNewProductExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return newProductMapper.selectByExample(example);
    }

    public void selectOrderRule(SmsHomeNewProductExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "sort desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(SmsHomeNewProductExample example, ListParam listParam) {
        SmsHomeNewProductExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "recommendStatus":
                    criteria.andRecommendStatusEqualTo(Integer.valueOf(value));
                    break;
            }
        }
    }
}
