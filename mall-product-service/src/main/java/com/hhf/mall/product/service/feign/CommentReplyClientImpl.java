package com.hhf.mall.product.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.feign.product.CommentReplyClient;
import com.hhf.mall.product.service.module.pms.commentReply.service.CommentReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 16:47
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/commentReply")
public class CommentReplyClientImpl implements CommentReplyClient {
    @Autowired
    private CommentReplyService commentReplyService;

    @Override
    @GetMapping("/countByMemberId")
    public ResponseEntity<ApiResponse> countByMemberId(Long memberId) {
        int count = commentReplyService.countByMemberId(memberId);
        return ApiResponse.success(count);
    }
}
