package com.hhf.mall.marketing.service.module.sms.advertise.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.marketing.service.module.sms.advertise.dto.AdvertiseDTO;
import com.hhf.mall.marketing.service.module.sms.advertise.dto.AdvertiseDTOMapper;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.AdvertiseStatus;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertise;
import com.hhf.mall.marketing.service.module.sms.advertise.service.AdvertiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description 广告
 * @date 2022-02-07 16:21
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/advertise")
@Validated
public class AdvertiseWebController {
    @Autowired
    private AdvertiseService advertiseService;
    @Autowired
    private AdvertiseDTOMapper advertiseDTOMapper;

    /**
     * @param nameLike
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_ADVERTISE)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"nameLike"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("status", String.valueOf(AdvertiseStatus.STATUS_ENABLE.getKey()));
        List<SmsHomeAdvertise> list = advertiseService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SmsHomeAdvertise> pageInfo = new PageInfo<>(list);
            List<AdvertiseDTO> dtoList = advertiseDTOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(dtoList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }
}
