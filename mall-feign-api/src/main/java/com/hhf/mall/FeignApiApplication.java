package com.hhf.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author hhf
 * @description: 
 * @date 2022-07-13 22:04
 */
@SpringBootApplication
public class FeignApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(FeignApiApplication.class, args);
    }
}
