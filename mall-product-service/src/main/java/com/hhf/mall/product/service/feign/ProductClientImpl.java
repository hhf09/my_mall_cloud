package com.hhf.mall.product.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.product.service.module.pms.product.dto.ProductDTO;
import com.hhf.mall.product.service.module.pms.product.dto.ProductDTOMapper;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.product.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 17:16
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/product")
public class ProductClientImpl implements ProductClient {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductDTOMapper productDTOMapper;

    @Override
    @GetMapping("/getById")
    public ResponseEntity<ApiResponse> getById(Long id) {
        PmsProduct product = productService.selectByPrimaryKey(id);
        if (product == null) {
            throw new CodeException("商品" + id + "数据不存在");
        }
        ProductDTO dto = productDTOMapper.mapToDTO(product);
        return ApiResponse.success(dto);
    }

    @Override
    @PostMapping("/updateById")
    public ResponseEntity<ApiResponse> updateById(com.hhf.mall.dto.product.ProductDTO productDTO) {
        PmsProduct product = new PmsProduct();
        BeanUtils.copyProperties(productDTO, product);
        productService.updateByPrimaryKeySelective(product);
        return ApiResponse.success();
    }
}
