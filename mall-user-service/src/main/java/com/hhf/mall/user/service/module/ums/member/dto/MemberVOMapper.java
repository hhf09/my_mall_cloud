package com.hhf.mall.user.service.module.ums.member.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.ums.member.entity.MemberGender;
import com.hhf.mall.user.service.module.ums.member.entity.MemberStatus;
import com.hhf.mall.user.service.module.ums.member.entity.UmsMember;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 14:06
 */
@Component
public class MemberVOMapper extends BaseDTOMapper<UmsMember, MemberVO> {
    @Override
    public MemberVO mapToDTO(UmsMember entity) {
        MemberVO vo = new MemberVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(MemberStatus.instanceOf(entity.getStatus()).getText());
        vo.setGenderText(MemberGender.instanceOf(entity.getGender()).getText());
        return vo;
    }

    @Override
    public List<MemberVO> mapToDTOList(List<UmsMember> entityList) {
        List<MemberVO> voList = new ArrayList<>();
        for (UmsMember entity : entityList) {
            MemberVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
