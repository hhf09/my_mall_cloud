package com.hhf.mall.user.service.module.ums.permission.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.ums.permission.entity.PermissionStatus;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 10:06
 */
@Component
public class PermissionVOMapper extends BaseDTOMapper<UmsPermission, PermissionVO> {
    @Override
    public PermissionVO mapToDTO(UmsPermission entity) {
        PermissionVO vo = new PermissionVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(PermissionStatus.instanceOf(entity.getStatus()).getText());
        return vo;
    }

    @Override
    public List<PermissionVO> mapToDTOList(List<UmsPermission> entityList) {
        List<PermissionVO> voList = new ArrayList<>();
        for (UmsPermission entity : entityList) {
            PermissionVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
