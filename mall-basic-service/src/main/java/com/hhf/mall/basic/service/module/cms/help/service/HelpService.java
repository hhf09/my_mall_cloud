package com.hhf.mall.basic.service.module.cms.help.service;

import com.hhf.mall.basic.service.module.cms.help.entity.CmsHelp;
import com.hhf.mall.basic.service.module.cms.help.entity.CmsHelpExample;
import com.hhf.mall.basic.service.module.cms.help.mapper.CmsHelpMapper;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategory;
import com.hhf.mall.basic.service.module.cms.helpCategory.service.HelpCateService;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-29 14:39
 */
@Service
public class HelpService extends BaseService<CmsHelp, Long, CmsHelpExample> {
    @Autowired
    private CmsHelpMapper helpMapper;

    @Autowired
    private HelpCateService helpCateService;

    @Override
    protected void before(CmsHelp entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            Long categoryId = entity.getCategoryId();
            CmsHelpCategory helpCategory = helpCateService.selectByPrimaryKey(categoryId);
            helpCategory.setHelpCount(helpCategory.getHelpCount() + 1);
            helpCateService.updateByPrimaryKeySelective(helpCategory);
        }
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(CmsHelp entity, ActionType actionType) {
        if (actionType == ActionType.DELETE) {
            Long categoryId = entity.getCategoryId();
            CmsHelpCategory helpCategory = helpCateService.selectByPrimaryKey(categoryId);
            helpCategory.setHelpCount(helpCategory.getHelpCount() - 1);
            helpCateService.updateByPrimaryKeySelective(helpCategory);
        }
    }

    @Override
    protected BaseMapper<CmsHelp, Long, CmsHelpExample> getMapper() {
        return helpMapper;
    }

    public List<CmsHelp> list(ListParam listParam) {
        CmsHelpExample example = new CmsHelpExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return helpMapper.selectByExampleWithBLOBs(example);
    }

    public void selectOrderRule(CmsHelpExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(CmsHelpExample example, ListParam listParam) {
        CmsHelpExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "categoryId":
                    criteria.andCategoryIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
