package com.hhf.mall.user.service.module.sys.apiLog.mapper;

import java.util.List;

import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.user.service.module.sys.apiLog.entity.SysApiLog;
import com.hhf.mall.user.service.module.sys.apiLog.entity.SysApiLogExample;
import org.apache.ibatis.annotations.Param;

public interface SysApiLogMapper extends BaseMapper<SysApiLog, Long, SysApiLogExample> {
    int countByExample(SysApiLogExample example);

    int deleteByExample(SysApiLogExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SysApiLog record);

    int insertSelective(SysApiLog record);

    List<SysApiLog> selectByExampleWithBLOBs(SysApiLogExample example);

    List<SysApiLog> selectByExample(SysApiLogExample example);

    SysApiLog selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SysApiLog record, @Param("example") SysApiLogExample example);

    int updateByExampleWithBLOBs(@Param("record") SysApiLog record, @Param("example") SysApiLogExample example);

    int updateByExample(@Param("record") SysApiLog record, @Param("example") SysApiLogExample example);

    int updateByPrimaryKeySelective(SysApiLog record);

    int updateByPrimaryKeyWithBLOBs(SysApiLog record);

    int updateByPrimaryKey(SysApiLog record);
}