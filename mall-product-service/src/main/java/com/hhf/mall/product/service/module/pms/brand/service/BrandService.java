package com.hhf.mall.product.service.module.pms.brand.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.brand.entity.PmsBrand;
import com.hhf.mall.product.service.module.pms.brand.entity.PmsBrandExample;
import com.hhf.mall.product.service.module.pms.brand.mapper.PmsBrandMapper;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProductExample;
import com.hhf.mall.product.service.module.pms.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-04 23:58
 */
@Service
public class BrandService extends BaseService<PmsBrand, Long, PmsBrandExample> {
    @Autowired
    private PmsBrandMapper brandMapper;

    @Autowired
    private ProductService productService;

    @Override
    protected void before(PmsBrand entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(PmsBrand entity, ActionType actionType) {
        if (actionType == ActionType.UPDATE) {
            PmsProductExample productExample = new PmsProductExample();
            PmsProductExample.Criteria criteria = productExample.createCriteria();
            criteria.andBrandIdEqualTo(entity.getId());
            PmsProduct product = new PmsProduct();
            product.setBrandName(entity.getName());
            productService.updateByExampleSelective(product, productExample);
        }
    }

    @Override
    protected BaseMapper<PmsBrand, Long, PmsBrandExample> getMapper() {
        return brandMapper;
    }

    public List<PmsBrand> list(ListParam listParam) {
        PmsBrandExample example = new PmsBrandExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return brandMapper.selectByExampleWithBLOBs(example);
    }

    public List<PmsBrand> listByKeyword(String keyword) {
        return brandMapper.listByKeyword(keyword);
    }

    public void selectOrderRule(PmsBrandExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsBrandExample example, ListParam listParam) {
        PmsBrandExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case ListParam.KEYWORD:
                    criteria.andNameLike("%" + value + "%");
                    break;
            }
        }
    }
}
