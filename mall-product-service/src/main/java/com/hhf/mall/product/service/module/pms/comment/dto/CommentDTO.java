package com.hhf.mall.product.service.module.pms.comment.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.hhf.mall.common.Const;
import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2022-07-13 21:33
 */
@Data
public class CommentDTO {
    private Long id;
    private Long skuStockId;
    private Long memberId;
    private Integer star;
    @JSONField(format = Const.YYYY_MM_DD_HH_MM_SS)
    private Date createTime;
    private Integer showStatus;
    private String showStatusText;
    private String pics;
    private Integer replyCount;
    private Integer evaluationStatus;
    private String evaluationStatusText;
    private Integer pictureStatus;
    private String pictureStatusText;
    private String content;
    private String productName;
    private String productAttr;
    private String memberNickname;
    private String memberIcon;
}
