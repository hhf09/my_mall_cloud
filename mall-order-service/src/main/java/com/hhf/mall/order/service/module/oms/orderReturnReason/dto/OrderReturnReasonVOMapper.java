package com.hhf.mall.order.service.module.oms.orderReturnReason.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReason;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.ReturnReasonStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 15:34
 */
@Component
public class OrderReturnReasonVOMapper extends BaseDTOMapper<OmsOrderReturnReason, OrderReturnReasonVO> {
    @Override
    public OrderReturnReasonVO mapToDTO(OmsOrderReturnReason entity) {
        OrderReturnReasonVO vo = new OrderReturnReasonVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(ReturnReasonStatus.instanceOf(entity.getStatus()).getText());
        return vo;
    }

    @Override
    public List<OrderReturnReasonVO> mapToDTOList(List<OmsOrderReturnReason> entityList) {
        List<OrderReturnReasonVO> voList = new ArrayList<>();
        for (OmsOrderReturnReason entity : entityList) {
            OrderReturnReasonVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
