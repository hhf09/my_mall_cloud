package com.hhf.eureka_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author hhf
 * @description eureka服务端
 * @date 2022-06-29 23:31
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer8861Application {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer8861Application.class, args);
    }
}
