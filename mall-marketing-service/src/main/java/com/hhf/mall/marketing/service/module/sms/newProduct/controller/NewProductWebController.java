package com.hhf.mall.marketing.service.module.sms.newProduct.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.marketing.service.module.sms.newProduct.dto.NewProductDTO;
import com.hhf.mall.marketing.service.module.sms.newProduct.dto.NewProductDTOMapper;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.NewProductRecommendStatus;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.SmsHomeNewProduct;
import com.hhf.mall.marketing.service.module.sms.newProduct.service.NewProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/12/29 11:20
 */
@RestController
@RequestMapping(ApiPrefix.WECHAT_WEB + "/newProduct")
@Validated
public class NewProductWebController {
    @Autowired
    private NewProductService newProductService;
    @Autowired
    private NewProductDTOMapper newProductDTOMapper;

    /**
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.WEB_NEW_RECOMMEND)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{"nameLike"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        listParam.add("recommendStatus", String.valueOf(NewProductRecommendStatus.YES.getKey()));
        List<SmsHomeNewProduct> list = newProductService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SmsHomeNewProduct> pageInfo = new PageInfo<>(list);
            List<NewProductDTO> dtoList = newProductDTOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(dtoList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }
}
