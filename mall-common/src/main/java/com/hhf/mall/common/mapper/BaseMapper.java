package com.hhf.mall.common.mapper;

import java.util.List;

/**
 * @author hhf
 * @description: 底层mapper
 * @date 2022/12/30 13:56
 */
public interface BaseMapper<E, PK, EX> {
    /**
     * 新增
     * @param entity
     * @return
     */
    int insert(E entity);

    /**
     * 根据主键更新
     * @param entity
     * @return
     */
    int updateByPrimaryKeySelective(E entity);

    /**
     * 根据条件更新
     * @param entity
     * @param example
     * @return
     */
    int updateByExampleSelective(E entity, EX example);

    /**
     * 根据主键删除
     * @param pk
     * @return
     */
    int deleteByPrimaryKey(PK pk);

    /**
     * 根据条件删除
     * @param example
     * @return
     */
    int deleteByExample(EX example);

    /**
     * 根据主键查询
     * @param pk
     * @return
     */
    E selectByPrimaryKey(PK pk);

    /**
     * 根据条件查询
     * @param example
     * @return
     */
    List<E> selectByExample(EX example);

    /**
     * 根据条件计数
     * @param example
     * @return
     */
    int countByExample(EX example);

    /**
     * 根据条件查询
     * @param example
     * @return
     */
    List<E> selectByExampleWithBLOBs(EX example);
}
