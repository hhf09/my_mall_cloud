package com.hhf.mall.user.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.feign.user.MemberReceiveAddressClient;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto.ReceiveAddrDTO;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto.ReceiveAddrDTOMapper;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddress;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.service.ReceiveAddrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 15:45
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/memberReceiveAddress")
public class MemberReceiveAddressClientImpl implements MemberReceiveAddressClient {
    @Autowired
    private ReceiveAddrService receiveAddrService;
    @Autowired
    private ReceiveAddrDTOMapper receiveAddrDTOMapper;

    @Override
    @GetMapping("/getById")
    public ResponseEntity<ApiResponse> getById(Long id) {
        UmsMemberReceiveAddress receiveAddress = receiveAddrService.selectByPrimaryKey(id);
        if (receiveAddress == null) {
            throw new CodeException("收货地址" + id + "数据不存在");
        }
        ReceiveAddrDTO dto = receiveAddrDTOMapper.mapToDTO(receiveAddress);
        return ApiResponse.success(dto);
    }

    @Override
    @GetMapping("/getByMemberId")
    public ResponseEntity<ApiResponse> getByMemberId(Long memberId) {
        ListParam listParam = new ListParam().add("memberId", String.valueOf(memberId));
        List<UmsMemberReceiveAddress> list = receiveAddrService.list(listParam);
        List<ReceiveAddrDTO> dtoList = receiveAddrDTOMapper.mapToDTOList(list);
        return ApiResponse.success(dtoList);
    }
}
