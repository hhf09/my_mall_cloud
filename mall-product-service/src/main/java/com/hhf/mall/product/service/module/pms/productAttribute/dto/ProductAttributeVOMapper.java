package com.hhf.mall.product.service.module.pms.productAttribute.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.product.service.module.pms.productAttribute.entity.PmsProductAttribute;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/12 9:29
 */
@Component
public class ProductAttributeVOMapper extends BaseDTOMapper<PmsProductAttribute, ProductAttributeVO> {
    @Override
    public ProductAttributeVO mapToDTO(PmsProductAttribute entity) {
        ProductAttributeVO vo = new ProductAttributeVO();
        BeanUtils.copyProperties(entity, vo);
        return vo;
    }

    @Override
    public List<ProductAttributeVO> mapToDTOList(List<PmsProductAttribute> entityList) {
        List<ProductAttributeVO> voList = new ArrayList<>();
        for (PmsProductAttribute entity : entityList) {
            ProductAttributeVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
