package com.hhf.mall.dto.auth;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hhf
 * @description: token封装
 * @date 2023/2/7 17:10
 */
@Data
@EqualsAndHashCode
@Builder
public class TokenDTO {
    /**
     * 登录令牌
     */
    private String token;
    /**
     * 刷新令牌
     */
    private String tokenHead;
    /**
     * 登录令牌头部
     */
    private String refreshToken;
    /**
     * 有效时间，单位秒
     */
    private int expiresIn;

    public TokenDTO() {
    }

    public TokenDTO(String token, String tokenHead, String refreshToken, int expiresIn) {
        this.token = token;
        this.tokenHead = tokenHead;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
    }
}
