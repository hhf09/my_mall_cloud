package com.hhf.mall.auth.service.exception;

import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.BaseExceptionHandler;
import com.hhf.mall.common.exception.CodeException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;

/**
 * @author hhf
 * @description 全局异常处理
 * @date 2023-01-27 18:27
 */
@ControllerAdvice(annotations = {RestController.class, Controller.class, Service.class})
@ResponseBody
public class ExceptionHandlerImpl extends BaseExceptionHandler {
    @Override
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return super.handleMethodArgumentNotValidException(e);
    }

    @Override
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiResponse> handleConstraintViolationException(ConstraintViolationException e) {
        return super.handleConstraintViolationException(e);
    }

    @Override
    @ExceptionHandler(CodeException.class)
    public ResponseEntity<ApiResponse> handleCodeException(CodeException e) {
        return super.handleCodeException(e);
    }

    @ExceptionHandler(AccountStatusException.class)
    public ResponseEntity<ApiResponse> handleAccountStatusException(AccountStatusException e) {
        return ApiResponse.error(e.getMessage());
    }

    @ExceptionHandler(OAuth2Exception.class)
    public ResponseEntity<ApiResponse> handleOAuth2Exception(OAuth2Exception e) {
        return ApiResponse.error(e.getMessage());
    }
}
