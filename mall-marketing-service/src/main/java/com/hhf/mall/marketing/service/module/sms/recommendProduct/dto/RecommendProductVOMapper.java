package com.hhf.mall.marketing.service.module.sms.recommendProduct.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.dto.product.ProductDTO;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.RecommendProductRecommendStatus;
import com.hhf.mall.marketing.service.module.sms.recommendProduct.entity.SmsHomeRecommendProduct;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/12/28 14:59
 */
@Component
public class RecommendProductVOMapper extends BaseDTOMapper<SmsHomeRecommendProduct, RecommendProductVO> {
    @Autowired
    private ProductClient productClient;

    public RecommendProductVO mapToDTO(SmsHomeRecommendProduct entity) {
        RecommendProductVO vo = new RecommendProductVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setRecommendStatusText(RecommendProductRecommendStatus.instanceOf(entity.getRecommendStatus()).getText());
        ResponseEntity<ApiResponse> responseEntity = productClient.getById(entity.getProductId());
        ApiResponse response = responseEntity.getBody();
        ProductDTO productDTO = response.toObject(ProductDTO.class);
        vo.setProductName(productDTO.getName());
        vo.setBrandName(productDTO.getBrandName());
        vo.setPrice(productDTO.getPrice());
        vo.setProductPic(productDTO.getPic());
        vo.setSubTitle(productDTO.getSubTitle());
        return vo;
    }

    public List<RecommendProductVO> mapToDTOList(List<SmsHomeRecommendProduct> entityList) {
        List<RecommendProductVO> voList = new ArrayList<>();
        for (SmsHomeRecommendProduct entity : entityList) {
            RecommendProductVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
