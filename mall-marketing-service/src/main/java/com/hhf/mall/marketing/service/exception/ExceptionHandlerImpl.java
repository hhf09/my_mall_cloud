package com.hhf.mall.marketing.service.exception;

import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.BaseExceptionHandler;
import com.hhf.mall.common.exception.CodeException;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolationException;

/**
 * @author hhf
 * @description 全局异常处理
 * @date 2023-01-27 18:27
 */
@ControllerAdvice(annotations = {RestController.class, Controller.class, Service.class, Aspect.class})
@ResponseBody
public class ExceptionHandlerImpl extends BaseExceptionHandler {
    @Override
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return super.handleMethodArgumentNotValidException(e);
    }

    @Override
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiResponse> handleConstraintViolationException(ConstraintViolationException e) {
        return super.handleConstraintViolationException(e);
    }

    @Override
    @ExceptionHandler(CodeException.class)
    public ResponseEntity<ApiResponse> handleCodeException(CodeException e) {
        return super.handleCodeException(e);
    }
}
