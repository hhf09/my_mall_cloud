package com.hhf.mall.dto.user;

import lombok.Data;

import java.util.Date;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/14 9:44
 */
@Data
public class MemberDTO {
    private Long id;

    /**
     * 用户名
     *
     * @mbggenerated
     */
    private String username;

    /**
     * 昵称
     *
     * @mbggenerated
     */
    private String nickname;

    /**
     * 手机号码
     *
     * @mbggenerated
     */
    private String phone;

    /**
     * 启用状态:0->禁用；1->启用
     *
     * @mbggenerated
     */
    private Integer status;

    /**
     * 创建时间
     *
     * @mbggenerated
     */
    private Date createTime;

    /**
     * 头像
     *
     * @mbggenerated
     */
    private String icon;

    /**
     * 性别：0->未知；1->男；2->女
     *
     * @mbggenerated
     */
    private Integer gender;

    /**
     * 用户在当前小程序的唯一标识
     *
     * @mbggenerated
     */
    private String openid;
}
