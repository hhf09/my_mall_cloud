package com.hhf.mall.user.service.module.ums.member.dto;

import lombok.Data;

/**
 * @author hhf
 * @description 绑定手机号
 * @date 2022-04-03 15:40
 */
@Data
public class BindPhoneDTO {
    /**
     * 用户id
     */
    private Long id;
    //手机号
    private String phone;
    //验证码
    private String validateCode;
}
