package com.hhf.mall.product.service.module.pms.productAttribute.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/12 9:27
 */
@Data
public class ProductAttributeVO {
    private Long id;
    private Long productAttributeCategoryId;
    private String name;
    private Integer inputType;
    private String inputList;
    private Integer sort;
    private Integer handAddStatus;
    private Integer type;
}
