package com.hhf.mall.user.service.module.ums.admin.dto;

import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdmin;
import com.hhf.mall.user.service.module.ums.permission.service.PermissionService;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import com.hhf.mall.user.service.module.ums.role.service.RoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/7 11:12
 */
@Component
public class AdminDTOMapperForAuth extends BaseDTOMapper<UmsAdmin, UserDTOForAuth> {
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RoleService roleService;

    @Override
    public UserDTOForAuth mapToDTO(UmsAdmin entity) {
        UserDTOForAuth dto = new UserDTOForAuth();
        BeanUtils.copyProperties(entity, dto);
        //查询用户拥有的权限
        List<String> permissionValues = permissionService.getPermissionValueListByUserId(entity.getId());
        dto.setAuthorities(permissionValues);
        List<UmsRole> roleList = roleService.getRoleListByUserId(entity.getId());
        List<String> roleValues = roleList.stream().map(UmsRole::getValue).collect(Collectors.toList());
        dto.setRoles(roleValues);
        dto.setClientId(AuthConst.ADMIN_CLIENT_ID);
        return dto;
    }

    @Override
    public List<UserDTOForAuth> mapToDTOList(List<UmsAdmin> entityList) {
        return null;
    }
}
