package com.hhf.mall.marketing.service.module.sms.newProduct.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.NewProductRecommendStatus;
import com.hhf.mall.marketing.service.module.sms.newProduct.entity.SmsHomeNewProduct;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description:
 * @date 2022/12/28 14:32
 */
@Component
public class NewProductDTOMapper extends BaseDTOMapper<SmsHomeNewProduct, NewProductDTO> {
    @Autowired
    private ProductClient productClient;

    public NewProductDTO mapToDTO(SmsHomeNewProduct entity) {
        NewProductDTO dto = new NewProductDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setRecommendStatusText(NewProductRecommendStatus.instanceOf(entity.getRecommendStatus()).getText());
        return dto;
    }

    public List<NewProductDTO> mapToDTOList(List<SmsHomeNewProduct> entityList) {
        List<NewProductDTO> dtoList = new ArrayList<>();
        for (SmsHomeNewProduct entity : entityList) {
            NewProductDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
