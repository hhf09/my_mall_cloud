package com.hhf.mall.user.service.module.ums.rolePermissionRelation.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelation;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelationExample;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.mapper.UmsRolePermissionRelationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/3 11:35
 */
@Service
public class RolePermissionRelationService extends BaseService<UmsRolePermissionRelation, Long, UmsRolePermissionRelationExample> {
    @Autowired
    private UmsRolePermissionRelationMapper rolePermissionRelationMapper;

    @Override
    protected void before(UmsRolePermissionRelation entity, ActionType actionType) {
        super.before(entity, actionType);
    }

    @Override
    protected void after(UmsRolePermissionRelation entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<UmsRolePermissionRelation, Long, UmsRolePermissionRelationExample> getMapper() {
        return rolePermissionRelationMapper;
    }

    public void insertAll(List<UmsRolePermissionRelation> list) {
        rolePermissionRelationMapper.insertAll(list);
    }

    public void selectOrderRule(UmsRolePermissionRelationExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(UmsRolePermissionRelationExample example, ListParam listParam) {
        UmsRolePermissionRelationExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "roleId":
                    criteria.andRoleIdEqualTo(Long.valueOf(value));
                    break;
            }
        }
    }
}
