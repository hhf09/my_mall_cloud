package com.hhf.mall.user.service.feign;

import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import cn.hutool.core.collection.CollUtil;
import com.hhf.mall.feign.user.AdminClient;
import com.hhf.mall.user.service.module.ums.admin.dto.AdminDTOMapperForAuth;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdmin;
import com.hhf.mall.user.service.module.ums.admin.entity.UmsAdminExample;
import com.hhf.mall.user.service.module.ums.admin.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/7 11:43
 */
@RestController
@RequestMapping(ApiPrefix.FEIGN + "/admin")
public class AdminClientImpl implements AdminClient {
    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminDTOMapperForAuth adminDTOMapperForAuth;

    @Override
    @GetMapping("/loadUserByUsername")
    public ResponseEntity<ApiResponse> loadUserByUsername(String username) {
        UmsAdminExample example = new UmsAdminExample();
        ListParam listParam = new ListParam().add("username", username);
        adminService.selectCondition(example, listParam);
        List<UmsAdmin> list = adminService.selectByExample(example);
        if (CollUtil.isEmpty(list)) {
            throw new CodeException("当前用户不存在：username=" + username);
        }
        UmsAdmin admin = list.get(0);
        UserDTOForAuth dto = adminDTOMapperForAuth.mapToDTO(admin);
        return ApiResponse.success(dto);
    }
}
