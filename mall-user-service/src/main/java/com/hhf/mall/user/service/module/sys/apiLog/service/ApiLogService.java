package com.hhf.mall.user.service.module.sys.apiLog.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.user.service.module.sys.apiLog.entity.SysApiLog;
import com.hhf.mall.user.service.module.sys.apiLog.entity.SysApiLogExample;
import com.hhf.mall.user.service.module.sys.apiLog.mapper.SysApiLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2023-01-26 20:40
 */
@Service
public class ApiLogService extends BaseService<SysApiLog, Long, SysApiLogExample> {
    @Autowired
    private SysApiLogMapper apiLogMapper;

    @Override
    protected void before(SysApiLog entity, ActionType actionType) {
        if (actionType == ActionType.ADD) {
            entity.setCreateTime(new Date());
        }
    }

    @Override
    protected void after(SysApiLog entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<SysApiLog, Long, SysApiLogExample> getMapper() {
        return apiLogMapper;
    }

    public List<SysApiLog> list(ListParam listParam) {
        SysApiLogExample example = new SysApiLogExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return apiLogMapper.selectByExample(example);
    }

    public void selectOrderRule(SysApiLogExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "id desc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(SysApiLogExample example, ListParam listParam) {
        SysApiLogExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "createTimeBt":
                    String[] strings = value.split(",");
                    Date start = DateUtil.parse(strings[0]);
                    Date end = DateUtil.parse(strings[1]);
                    criteria.andCreateTimeBetween(start, end);
                    break;
                case "serviceSign":
                    criteria.andServiceSignEqualTo(value);
                    break;
            }
        }
    }
}
