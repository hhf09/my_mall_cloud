package com.hhf.mall.marketing.service.module.sms.advertise.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.annotation.RequirePermissions;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.PermissionConst;
import com.hhf.mall.common.controller.CustomPageInfo;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.marketing.service.module.sms.advertise.dto.AdvertiseVO;
import com.hhf.mall.marketing.service.module.sms.advertise.dto.AdvertiseVOMapper;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertise;
import com.hhf.mall.marketing.service.module.sms.advertise.service.AdvertiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author hhf
 * @description 广告
 * @date 2022-01-23 23:08
 */
@RestController
@RequestMapping(ApiPrefix.MANAGE + "/advertise")
@Validated
public class AdvertiseController {
    @Autowired
    private AdvertiseService advertiseService;
    @Autowired
    private AdvertiseVOMapper advertiseVOMapper;

    /**
     * 分页查找
     * @param keyword
     * @param status
     * @param pageNum
     * @param pageSize
     * @param orderType
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ADVERTISE)
    @GetMapping("/list")
    public ResponseEntity<ApiResponse> list(HttpServletRequest request) {
        String[] paramKeys = new String[]{ListParam.KEYWORD, "status"};
        ListParam listParam = new ListParam().create(request, paramKeys);
        List<SmsHomeAdvertise> list = advertiseService.list(listParam);
        try {
            PageHelper.startPage(Integer.parseInt(listParam.getParamValue(ListParam.PAGENUM)), Integer.parseInt(listParam.getParamValue(ListParam.PAGESIZE)));
            PageInfo<SmsHomeAdvertise> pageInfo = new PageInfo<>(list);
            List<AdvertiseVO> voList = advertiseVOMapper.mapToDTOList(pageInfo.getList());
            return ApiResponse.success(voList, CustomPageInfo.build(pageInfo.getTotal(), pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getPages()));
        } finally {
            PageHelper.clearPage();
        }
    }

    /**
     * 添加
     *
     * @return
     */
    @ApiLog(note = "添加广告")
    @RequirePermissions(PermissionConst.ADMIN_ADVERTISE)
    @PostMapping("/add")
    public ResponseEntity<ApiResponse> add(@Valid @RequestBody SmsHomeAdvertise advertise) {
        advertise.setDefault();
        advertiseService.add(advertise);
        return ApiResponse.success();
    }

    /**
     * 修改
     *
     * @return
     */
    @ApiLog(note = "更新广告")
    @RequirePermissions(PermissionConst.ADMIN_ADVERTISE)
    @PostMapping("/update")
    public ResponseEntity<ApiResponse> update(@Valid @RequestBody SmsHomeAdvertise advertise) {
        advertiseService.updateByPrimaryKeySelective(advertise);
        return ApiResponse.success();
    }

    /**
     * @return
     */
    @RequirePermissions(PermissionConst.ADMIN_ADVERTISE)
    @GetMapping("/get")
    public ResponseEntity<ApiResponse> get(@RequestParam Long id) {
        SmsHomeAdvertise advertise = advertiseService.selectByPrimaryKey(id);
        AdvertiseVO vo = advertiseVOMapper.mapToDTO(advertise);
        if (advertise == null) {
            throw new CodeException("广告" + id + "数据不存在");
        }
        return ApiResponse.success(vo);
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ApiLog(note = "删除广告")
    @RequirePermissions(PermissionConst.ADMIN_ADVERTISE)
    @PostMapping("/delete")
    public ResponseEntity<ApiResponse> delete(@RequestParam Long id) {
        advertiseService.deleteByPrimaryKey(id);
        return ApiResponse.success();
    }

}
