package com.hhf.mall.user.service.module.ums.memberReceiveAddress.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.MemberReceiveAddressDefaultStatus;
import com.hhf.mall.user.service.module.ums.memberReceiveAddress.entity.UmsMemberReceiveAddress;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022/7/20 15:49
 */
@Component
public class ReceiveAddrDTOMapper extends BaseDTOMapper<UmsMemberReceiveAddress, ReceiveAddrDTO> {
    @Override
    public ReceiveAddrDTO mapToDTO(UmsMemberReceiveAddress entity) {
        ReceiveAddrDTO dto = new ReceiveAddrDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setDefaultStatusText(MemberReceiveAddressDefaultStatus.instanceOf(entity.getDefaultStatus()).getText());
        return dto;
    }

    @Override
    public List<ReceiveAddrDTO> mapToDTOList(List<UmsMemberReceiveAddress> entityList) {
        List<ReceiveAddrDTO> dtoList = new ArrayList<>();
        for (UmsMemberReceiveAddress entity : entityList) {
            ReceiveAddrDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
