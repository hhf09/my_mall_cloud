package com.hhf.mall.common.aop;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.Const;
import com.hhf.mall.common.annotation.ApiLog;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.constant.AuthConst;
import com.hhf.mall.common.constant.UserType;
import com.hhf.mall.common.dto.UserDTOForAuth;
import com.hhf.mall.common.exception.CodeException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hhf
 * @description 底层日志处理切面
 * @date 2023/2/21 17:25
 */
public class BaseLogAspect {

    /**
     * 处理抛出异常
     * @param joinPoint
     * @param throwing
     * @return
     */
    public Map<String, String> handleAfterThrowing(JoinPoint joinPoint, Throwable throwing) {
        throwing.printStackTrace();
        Map<String, String> map = handlePublicParam(joinPoint);
        map.put("note", map.get("note") + ":" + throwing);
        return map;
    }

    /**
     * 处理增删改接口返回后
     * @param joinPoint
     * @return
     */
    public Map<String, String> handleAfterReturning(JoinPoint joinPoint) {
        return handlePublicParam(joinPoint);
    }

    private Map<String, String> handlePublicParam(JoinPoint joinPoint) {
        Map<String, String> map = new HashMap<>();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Long userId = getUserId(request);
        String token = request.getHeader(AuthConst.JWT_REQUEST_HEADER);
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        Object parameter = getParameter(method, joinPoint.getArgs());
        String userAgent = getUserAgent(request.getHeader(Const.USER_AGENT_HEADER));
        String note = getApiNote(method);
        int userType = getUserType(request.getRequestURI());

        map.put("userId", String.valueOf(userId));
        map.put("userType", String.valueOf(userType));
        map.put("token", token);
        map.put("ip", request.getRemoteAddr());
        map.put("uri", request.getRequestURI());
        map.put("note", note);
        map.put("userAgent", userAgent);
        map.put("params", JSON.toJSONString(parameter));
        return map;
    }

    /**
     * 根据方法和传入的参数获取请求参数
     */
    public Object getParameter(Method method, Object[] args) {
        List<Object> argList = new ArrayList<>();
        Parameter[] parameters = method.getParameters();
        for (int i = 0; i < parameters.length; i++) {
            //将RequestBody注解修饰的参数作为请求参数
            RequestBody requestBody = parameters[i].getAnnotation(RequestBody.class);
            if (requestBody != null) {
                argList.add(args[i]);
            }
            //将RequestParam注解修饰的参数作为请求参数
            RequestParam requestParam = parameters[i].getAnnotation(RequestParam.class);
            if (requestParam != null) {
                Map<String, Object> map = new HashMap<>();
                String key = parameters[i].getName();
                if (!StrUtil.isEmpty(requestParam.value())) {
                    key = requestParam.value();
                }
                map.put(key, args[i]);
                argList.add(map);
            }
        }
        if (argList.size() == 0) {
            return null;
        } else if (argList.size() == 1) {
            return argList.get(0);
        } else {
            return argList;
        }
    }

    /**
     * 获取接口备注
     * @param method
     * @return
     */
    public String getApiNote(Method method) {
        ApiLog apiLog = method.getAnnotation(ApiLog.class);
        return apiLog != null ? apiLog.note() : null;
    }

    /**
     * 获取浏览器信息
     * 参考https://blog.csdn.net/weixin_37264997/article/details/115716573
     * @param userAgentInfo
     * @return
     */
    public String getUserAgent(String userAgentInfo) {
        UserAgent userAgent = UserAgentUtil.parse(userAgentInfo);
        return userAgent.getBrowser().toString() + userAgent.getVersion();
    }

    /**
     * 获取用户类型
     * @param requestUri
     * @return
     */
    public int getUserType(String requestUri) {
        return requestUri.contains(ApiPrefix.WECHAT_WEB) ? UserType.MEMBER.getKey() : UserType.ADMIN.getKey();
    }

    /**
     * 从解析的token用户信息获取用户id
     * @param request
     * @return
     */
    public Long getUserId(HttpServletRequest request) {
        String userString = request.getHeader(AuthConst.USERINFO_REQUEST_HEADER);
        if (StrUtil.isEmpty(userString)) {
            throw new CodeException("请求头的用户信息不存在");
        }
        UserDTOForAuth dto = JSON.parseObject(userString, UserDTOForAuth.class);
        return dto.getId();
    }
}
