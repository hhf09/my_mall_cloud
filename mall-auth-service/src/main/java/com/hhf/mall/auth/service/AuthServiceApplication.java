package com.hhf.mall.auth.service;

import com.hhf.mall.common.Const;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hhf
 * @description 认证服务
 * @date 2023-02-06 23:24
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = Const.FEIGN_PACKAGE)
public class AuthServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthServiceApplication.class, args);
    }
}
