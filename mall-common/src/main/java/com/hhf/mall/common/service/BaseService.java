package com.hhf.mall.common.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.mapper.BaseMapper;

import java.util.List;

/**
 * @author hhf
 * @description: 底层service
 * @date 2022/12/30 11:26
 */
public abstract class BaseService<E, PK, EX> {
    /**
     * 操作数据库前执行
     * @param entity
     * @param actionType
     */
    protected void before(E entity, ActionType actionType) {

    }

    /**
     * 操作数据库后执行
     * @param entity
     * @param actionType
     */
    protected void after(E entity, ActionType actionType) {

    }

    protected abstract BaseMapper<E, PK, EX> getMapper();

    /**
     * 新增
     * @param entity
     */
    public void add(E entity) {
        before(entity, ActionType.ADD);
        getMapper().insert(entity);
        after(entity, ActionType.ADD);
    }

    /**
     * 根据主键更新
     * @param entity
     */
    public void updateByPrimaryKeySelective(E entity) {
        before(entity, ActionType.UPDATE);
        getMapper().updateByPrimaryKeySelective(entity);
        after(entity, ActionType.UPDATE);
    }

    /**
     * 根据主键删除
     * @param pk
     */
    public void deleteByPrimaryKey(PK pk) {
        E entity = getMapper().selectByPrimaryKey(pk);
        if (entity != null) {
            before(entity, ActionType.DELETE);
            getMapper().deleteByPrimaryKey(pk);
            after(entity, ActionType.DELETE);
        }
    }

    /**
     * 根据主键查询
     * @param pk
     */
    public E selectByPrimaryKey(PK pk) {
        return getMapper().selectByPrimaryKey(pk);
    }

    /**
     * 根据条件查询
     * @param example
     */
    public List<E> selectByExample(EX example) {
        return getMapper().selectByExample(example);
    }

    /**
     * 根据条件删除
     * @param example
     */
    public void deleteByExample(EX example) {
        List<E> entityList = selectByExample(example);
        entityList.forEach(entity -> before(entity, ActionType.DELETE));
        getMapper().deleteByExample(example);
        entityList.forEach(entity -> after(entity, ActionType.DELETE));
    }

    /**
     * 根据条件更新
     * @param example
     */
    public void updateByExampleSelective(E entity, EX example) {
        before(entity, ActionType.UPDATE);
        getMapper().updateByExampleSelective(entity, example);
        after(entity, ActionType.UPDATE);
    }

    /**
     * 根据条件计数
     * @param example
     */
    public int countByExample(EX example) {
        return getMapper().countByExample(example);
    }

    /**
     * 根据条件查询
     * @param example
     * @return
     */
    public List<E> selectByExampleWithBLOBs(EX example) {
        return getMapper().selectByExampleWithBLOBs(example);
    }

    /**
     * 设置排序规则
     * @param example
     * @param listParam
     */
    public void selectOrderRule(EX example, ListParam listParam) {

    }

    /**
     * 设置筛选条件
     * @param example
     * @param listParam
     */
    public void selectCondition(EX example, ListParam listParam) {

    }
}
