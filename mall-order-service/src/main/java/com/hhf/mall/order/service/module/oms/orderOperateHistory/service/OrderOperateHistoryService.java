package com.hhf.mall.order.service.module.oms.orderOperateHistory.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.entity.OmsOrderOperateHistory;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.entity.OmsOrderOperateHistoryExample;
import com.hhf.mall.order.service.module.oms.orderOperateHistory.mapper.OmsOrderOperateHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/3 14:23
 */
@Service
public class OrderOperateHistoryService extends BaseService<OmsOrderOperateHistory, Long, OmsOrderOperateHistoryExample> {
    @Autowired
    private OmsOrderOperateHistoryMapper orderOperateHistoryMapper;

    @Override
    protected void before(OmsOrderOperateHistory entity, ActionType actionType) {
    }

    @Override
    protected void after(OmsOrderOperateHistory entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<OmsOrderOperateHistory, Long, OmsOrderOperateHistoryExample> getMapper() {
        return orderOperateHistoryMapper;
    }

    public void selectOrderRule(OmsOrderOperateHistoryExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(OmsOrderOperateHistoryExample example, ListParam listParam) {
        OmsOrderOperateHistoryExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "":
                    break;
            }
        }
    }
}
