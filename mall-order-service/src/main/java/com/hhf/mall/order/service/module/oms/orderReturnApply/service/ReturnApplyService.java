package com.hhf.mall.order.service.module.oms.orderReturnApply.service;

import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.feign.user.MemberClient;
import com.hhf.mall.order.service.module.oms.orderItem.service.OrderItemService;
import com.hhf.mall.order.service.module.oms.orderReturnApply.dto.OrderReturnApplyResult;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApply;
import com.hhf.mall.order.service.module.oms.orderReturnApply.entity.OmsOrderReturnApplyExample;
import com.hhf.mall.order.service.module.oms.orderReturnApply.mapper.OmsOrderReturnApplyMapper;
import com.hhf.mall.order.service.module.oms.orderReturnReason.entity.OmsOrderReturnReason;
import com.hhf.mall.order.service.module.oms.orderReturnReason.service.ReturnReasonService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description
 * @date 2022-01-22 16:39
 */
@Service
public class ReturnApplyService extends BaseService<OmsOrderReturnApply, Long, OmsOrderReturnApplyExample> {
    @Autowired
    private OmsOrderReturnApplyMapper returnApplyMapper;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private ReturnReasonService returnReasonService;
    @Autowired
    private MemberClient memberClient;

    @Override
    protected void before(OmsOrderReturnApply entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(OmsOrderReturnApply entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<OmsOrderReturnApply, Long, OmsOrderReturnApplyExample> getMapper() {
        return returnApplyMapper;
    }

    public List<OmsOrderReturnApply> list(ListParam listParam) {
        OmsOrderReturnApplyExample example = new OmsOrderReturnApplyExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return returnApplyMapper.selectByExample(example);
    }

    public OrderReturnApplyResult detail(Long returnApplyId) {
        OmsOrderReturnApply returnApply = returnApplyMapper.selectByPrimaryKey(returnApplyId);
        OrderReturnApplyResult returnApplyResult = new OrderReturnApplyResult();
        BeanUtils.copyProperties(returnApply, returnApplyResult);
        OmsOrderReturnReason returnReason = returnReasonService.selectByPrimaryKey(returnApply.getReasonId());
        returnApplyResult.setReasonName(returnReason.getName());
        return returnApplyResult;
    }

    public void selectOrderRule(OmsOrderReturnApplyExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(OmsOrderReturnApplyExample example, ListParam listParam) {
        OmsOrderReturnApplyExample.Criteria criteria = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case "orderSn":
                    criteria.andOrderSnEqualTo(value);
                    break;
                case "status":
                    criteria.andStatusEqualTo(Integer.valueOf(value));
                    break;
                case "createTimeBt":
                    String[] strings = value.split(",");
                    Date start = DateUtil.parse(strings[0]);
                    Date end = DateUtil.parse(strings[1]);
                    criteria.andCreateTimeBetween(start, end);
                    break;
            }
        }
    }
}
