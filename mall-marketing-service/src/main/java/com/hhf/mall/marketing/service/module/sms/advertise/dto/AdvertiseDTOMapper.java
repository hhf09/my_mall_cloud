package com.hhf.mall.marketing.service.module.sms.advertise.dto;

import com.hhf.mall.common.dto.BaseDTOMapper;
import com.hhf.mall.feign.product.ProductClient;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.AdvertiseStatus;
import com.hhf.mall.marketing.service.module.sms.advertise.entity.SmsHomeAdvertise;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/9 9:35
 */
@Component
public class AdvertiseDTOMapper extends BaseDTOMapper<SmsHomeAdvertise, AdvertiseDTO> {
    @Autowired
    private ProductClient productClient;

    @Override
    public AdvertiseDTO mapToDTO(SmsHomeAdvertise entity) {
        AdvertiseDTO dto = new AdvertiseDTO();
        BeanUtils.copyProperties(entity, dto);
        dto.setStatusText(AdvertiseStatus.instanceOf(entity.getStatus()).getText());
        return dto;
    }

    @Override
    public List<AdvertiseDTO> mapToDTOList(List<SmsHomeAdvertise> entityList) {
        List<AdvertiseDTO> dtoList = new ArrayList<>();
        for (SmsHomeAdvertise entity : entityList) {
            AdvertiseDTO dto = mapToDTO(entity);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
