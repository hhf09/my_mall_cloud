package com.hhf.mall.product.service.module.pms.product.dto;

import com.hhf.mall.product.service.module.pms.brand.dto.BrandDTO;
import lombok.Data;

import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/2/13 9:46
 */
@Data
public class ProductDTOWithBrand {
    private List<ProductDTO> list;
    private List<BrandDTO> brandList;
}
