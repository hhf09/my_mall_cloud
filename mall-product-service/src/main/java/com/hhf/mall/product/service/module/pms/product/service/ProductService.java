package com.hhf.mall.product.service.module.pms.product.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.hhf.mall.common.Const;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.controller.Param;
import com.hhf.mall.common.mapper.BaseMapper;
import com.hhf.mall.common.service.ActionType;
import com.hhf.mall.common.service.BaseService;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplate;
import com.hhf.mall.product.service.module.pms.feightTemplate.entity.PmsFeightTemplateExample;
import com.hhf.mall.product.service.module.pms.feightTemplate.service.FeightTemplateService;
import com.hhf.mall.product.service.module.pms.product.dto.PmsProductParam;
import com.hhf.mall.product.service.module.pms.product.dto.PmsProductResult;
import com.hhf.mall.product.service.module.pms.product.dto.ProductAttrItem;
import com.hhf.mall.product.service.module.pms.product.dto.ProductSaleChartDTO;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProduct;
import com.hhf.mall.product.service.module.pms.product.entity.PmsProductExample;
import com.hhf.mall.product.service.module.pms.product.mapper.PmsProductMapper;
import com.hhf.mall.product.service.module.pms.productAttributeValue.service.ProductAttributeValueService;
import com.hhf.mall.product.service.module.pms.productAttributeValue.entity.PmsProductAttributeValue;
import com.hhf.mall.product.service.module.pms.productAttributeValue.entity.PmsProductAttributeValueExample;
import com.hhf.mall.product.service.module.pms.skuStock.dto.ProductSkuDTO;
import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStock;
import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStockExample;
import com.hhf.mall.product.service.module.pms.skuStock.service.SkuStockService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2022-01-10 23:27
 */
@Service
public class ProductService extends BaseService<PmsProduct, Long, PmsProductExample> {
    @Autowired
    private PmsProductMapper productMapper;

    @Autowired
    private FeightTemplateService feightTemplateService;

    @Autowired
    private SkuStockService skuStockService;

    @Autowired
    private ProductAttributeValueService productAttributeValueService;

    @Override
    protected void before(PmsProduct entity, ActionType actionType) {
        if (actionType == ActionType.ADD || actionType == ActionType.UPDATE) {
            entity.setUpdateTime(new Date());
        }
    }

    @Override
    protected void after(PmsProduct entity, ActionType actionType) {
        super.after(entity, actionType);
    }

    @Override
    protected BaseMapper<PmsProduct, Long, PmsProductExample> getMapper() {
        return productMapper;
    }

    public List<PmsProduct> list(ListParam listParam) {
        PmsProductExample example = new PmsProductExample();
        selectCondition(example, listParam);
        selectOrderRule(example, listParam);
        return productMapper.selectByExampleWithBLOBs(example);
    }


    public PmsProductResult updateInfo(Long productId) {
        PmsProductResult productResult = productMapper.getUpdateInfo(productId);
        if (productResult.getFeightTemplateId() != 0) {
            PmsFeightTemplate feightTemplate = feightTemplateService.selectByPrimaryKey(productResult.getFeightTemplateId());
            productResult.setPackingType(feightTemplate.getPackingType());
            productResult.setWeightType(feightTemplate.getWeight());
            productResult.setIsFragile(feightTemplate.getIsFragile());
        }

        return productResult;
    }

    public void add(PmsProductParam productParam) {
        Long templateId = handleFeightTemplate(productParam.getPackingType(), productParam.getWeightType(), productParam.getIsFragile());
        String serviceIds = productParam.getServiceIds();
        //不包运费
        if (StrUtil.isNotEmpty(serviceIds) && !serviceIds.contains("3")) {
            templateId = handleFeightTemplate(productParam.getPackingType(), productParam.getWeightType(), productParam.getIsFragile());
        }

        PmsProduct product = new PmsProduct();
        BeanUtils.copyProperties(productParam, product);
        product.setFeightTemplateId(templateId);
        String productSn = "prod_" + DateUtil.format(new Date(), Const.YYYYMMDDHHMMSS);
        product.setProductSn(productSn);
        product.setDefault();
        add(product);

        handleSkuStockCode(productParam.getSkuStockList(), product.getId());
        handleSkuStock(productParam.getSkuStockList(), product.getId(), false);
        handleAttributeValue(productParam.getProductAttributeValueList(), product.getId(), false);
    }

    public void update(Long productId, PmsProductParam productParam) {
        Long templateId = 0L;
        String serviceIds = productParam.getServiceIds();
        //不包运费
        if (StrUtil.isNotEmpty(serviceIds) && !serviceIds.contains("3")) {
            templateId = handleFeightTemplate(productParam.getPackingType(), productParam.getWeightType(), productParam.getIsFragile());
        }

        PmsProduct product = new PmsProduct();
        BeanUtils.copyProperties(productParam, product);
        product.setFeightTemplateId(templateId);
        product.setId(productId);
        updateByPrimaryKeySelective(product);

        handleSkuStock(productParam.getSkuStockList(), product.getId(), true);
        handleAttributeValue(productParam.getProductAttributeValueList(), product.getId(), true);
    }

    private Long handleFeightTemplate(String packingType, String weight, String isFragile) {
        if (!StrUtil.isNotEmpty(packingType) && !StrUtil.isNotEmpty(weight) && !StrUtil.isNotEmpty(isFragile)) {
            return 0L;
        }
        //查找运费类型
        PmsFeightTemplateExample example = new PmsFeightTemplateExample();
        ListParam listParam = new ListParam().add("packingType", packingType).add("weight", weight).add("isFragile", isFragile);
        feightTemplateService.selectCondition(example, listParam);
        PmsFeightTemplate template = feightTemplateService.selectByExample(example).get(0);
        return template.getId();
    }

    private void handleSkuStockCode(List<ProductSkuDTO> skuStockList, Long productId) {
        if (!CollUtil.isNotEmpty(skuStockList)) {
            return;
        }
        for (int i = 0; i < skuStockList.size(); i++) {
            ProductSkuDTO skuStock = skuStockList.get(i);
            if (!StrUtil.isNotEmpty(skuStock.getSkuCode())) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                StringBuilder sb = new StringBuilder();
                //日期
                sb.append(sdf.format(new Date()));
                //四位商品id
                sb.append(String.format("%04d", productId));
                //三位索引id
                sb.append(String.format("%03d", i + 1));
                skuStock.setSkuCode(sb.toString());
            }
        }
    }

    private void handleSkuStock(List<ProductSkuDTO> skuStockList, Long productId, boolean isEdit) {
        if (!CollUtil.isNotEmpty(skuStockList)) {
            return;
        }

        if (isEdit) {
            PmsSkuStockExample example = new PmsSkuStockExample();
            ListParam listParam = new ListParam().add("productId", String.valueOf(productId));
            skuStockService.selectCondition(example, listParam);
            skuStockService.deleteByExample(example);
        }

        for (ProductSkuDTO productSkuDto : skuStockList) {
            PmsSkuStock skuStock = new PmsSkuStock();
            BeanUtils.copyProperties(productSkuDto, skuStock);

            skuStock.setProductId(productId);
            String spData = productSkuDto.getSpData();
            JSONArray jsonArray = JSON.parseArray(spData);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < jsonArray.size(); i++) {
                ProductAttrItem productAttrItem = jsonArray.getObject(i, ProductAttrItem.class);
                stringBuilder.append(productAttrItem.getKey() + ":" + productAttrItem.getValue());
                if ((i + 1) != jsonArray.size()) {
                    stringBuilder.append(",");
                }
            }
            skuStock.setSp(stringBuilder.toString());
            skuStockService.add(skuStock);
        }
    }

    private void handleAttributeValue(List<PmsProductAttributeValue> productAttributeValueList, Long productId, boolean isEdit) {
        if (!CollUtil.isNotEmpty(productAttributeValueList)) {
            return;
        }

        if (isEdit) {
            PmsProductAttributeValueExample example = new PmsProductAttributeValueExample();
            ListParam listParam = new ListParam().add("productId", String.valueOf(productId));
            productAttributeValueService.selectCondition(example, listParam);
            productAttributeValueService.deleteByExample(example);
        }

        for (PmsProductAttributeValue productAttributeValue : productAttributeValueList) {
            productAttributeValue.setProductId(productId);
            productAttributeValueService.add(productAttributeValue);
        }
    }

    public List<ProductSaleChartDTO> getSaleChart() {
        return productMapper.getSaleChart();
    }

    public void selectOrderRule(PmsProductExample example, ListParam listParam) {
        String orderByClause = null;
        String orderType = listParam.getParamValue(ListParam.ORDERTYPE);
        switch (orderType) {
            case "1":
                orderByClause = "";
                break;
            case "2":
                orderByClause = "sale desc";
                break;
            case "3":
                orderByClause = "price desc";
                break;
            case "4":
                orderByClause = "price asc";
                break;
        }
        if (StrUtil.isNotEmpty(orderByClause)) {
            example.setOrderByClause(orderByClause);
        }
    }

    public void selectCondition(PmsProductExample example, ListParam listParam) {
        PmsProductExample.Criteria criteria = example.createCriteria();
        PmsProductExample.Criteria criteria2 = example.createCriteria();
        PmsProductExample.Criteria criteria3 = example.createCriteria();
        PmsProductExample.Criteria criteria4 = example.createCriteria();
        List<Param> paramList = listParam.getParamList();
        for (Param param : paramList) {
            String value = param.getValue();
            switch (param.getKey()) {
                case ListParam.KEYWORD:
                    criteria.andNameLike("%" + value + "%");
                    break;
                case "productSn":
                    criteria.andProductSnEqualTo(value);
                    break;
                case "brandId":
                    criteria.andBrandIdEqualTo(Long.valueOf(value));
                    break;
                case "keywordForMini":
                    criteria.andNameLike("%" + value + "%");
                    criteria2.andKeywordsLike("%" + value + "%");
                    criteria3.andBrandNameLike("%" + value + "%");
                    criteria4.andProductCategoryNameLike("%" + value + "%");
                    example.or(criteria2);
                    example.or(criteria3);
                    example.or(criteria4);
                    break;
                case "priceGe":
                    criteria.andPriceGreaterThanOrEqualTo(new BigDecimal(value));
                    break;
                case "priceLe":
                    criteria.andPriceLessThanOrEqualTo(new BigDecimal(value));
                    break;
            }
        }
    }

}
