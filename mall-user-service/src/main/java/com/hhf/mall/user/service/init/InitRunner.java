package com.hhf.mall.user.service.init;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.hhf.mall.common.controller.ListParam;
import com.hhf.mall.common.exception.CodeException;
import com.hhf.mall.user.service.init.dto.PermissionDTO;
import com.hhf.mall.user.service.init.dto.RolePermissionDTO;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermission;
import com.hhf.mall.user.service.module.ums.permission.entity.UmsPermissionExample;
import com.hhf.mall.user.service.module.ums.permission.service.PermissionService;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRole;
import com.hhf.mall.user.service.module.ums.role.entity.UmsRoleExample;
import com.hhf.mall.user.service.module.ums.role.service.RoleService;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelation;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.entity.UmsRolePermissionRelationExample;
import com.hhf.mall.user.service.module.ums.rolePermissionRelation.service.RolePermissionRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.List;
import java.util.Objects;

/**
 * @author hhf
 * @description: 初始化运行器
 * @date 2023/2/17 17:41
 */
@Component
public class InitRunner implements CommandLineRunner {
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private RolePermissionRelationService rolePermissionRelationService;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("初始化开始");
        if (!ifInitRolePermission()) {
            initRolePermission();
        }
        System.out.println("初始化结束");
    }

    /**
     * 判断是否初始化角色权限
     * @return
     */
    private boolean ifInitRolePermission() {
        int roleCount = roleService.countByExample(new UmsRoleExample());
        int permissionCount = permissionService.countByExample(new UmsPermissionExample());
        int rpCount = rolePermissionRelationService.countByExample(new UmsRolePermissionRelationExample());
        return roleCount != 0 && permissionCount != 0 && rpCount != 0;
    }

    /**
     * 初始化角色权限
     */
    private void initRolePermission() {
        String filePath = "/init/rolepermission.json";
        String value;
        try(InputStream in = this.getClass().getResourceAsStream(filePath)) {
            value = IoUtil.read(Objects.requireNonNull(in), CharsetUtil.CHARSET_UTF_8);
        } catch (Exception e) {
            System.out.println("读取文件失败");
            throw new RuntimeException(e.getMessage());
        }
        if (StrUtil.isEmpty(value)) {
            throw new CodeException("文件内容为空");
        }
        List<RolePermissionDTO> rolePermissionDTOS = JSON.parseArray(value, RolePermissionDTO.class);
        if (CollUtil.isEmpty(rolePermissionDTOS)) {
            throw new CodeException("文件内容为空");
        }
        //先删除之前的角色权限关联
        rolePermissionRelationService.deleteByExample(new UmsRolePermissionRelationExample());
        for (RolePermissionDTO rolePermissionDTO : rolePermissionDTOS) {
            UmsRoleExample roleExample = new UmsRoleExample();
            ListParam listParam = new ListParam().add("value", rolePermissionDTO.getValue());
            roleService.selectCondition(roleExample, listParam);
            List<UmsRole> roles = roleService.selectByExample(roleExample);
            UmsRole role;
            if (CollUtil.isEmpty(roles)) {
                role = new UmsRole();
                role.setDefault();
                role.setName(rolePermissionDTO.getName());
                role.setDescription(rolePermissionDTO.getDescription());
                role.setValue(rolePermissionDTO.getValue());
                roleService.add(role);
            } else {
                role = roles.get(0);
                role.setName(rolePermissionDTO.getName());
                role.setDescription(rolePermissionDTO.getDescription());
                roleService.updateByPrimaryKeySelective(role);
            }


            List<PermissionDTO> permissionDTOS = rolePermissionDTO.getPermissions();
            if (CollUtil.isEmpty(permissionDTOS)) {
                System.out.println("权限不存在");
                continue;
            }
            for (PermissionDTO permissionDTO : permissionDTOS) {
                UmsPermissionExample permissionExample = new UmsPermissionExample();
                ListParam listParam1 = new ListParam().add("value", permissionDTO.getValue());
                permissionService.selectCondition(permissionExample, listParam1);
                List<UmsPermission> permissions = permissionService.selectByExample(permissionExample);
                UmsPermission permission;
                if (CollUtil.isEmpty(permissions)) {
                    permission = new UmsPermission();
                    permission.setDefault();
                    permission.setTitle(permissionDTO.getTitle());
                    permission.setValue(permissionDTO.getValue());
                    permissionService.add(permission);
                } else {
                    permission = permissions.get(0);
                    permission.setTitle(permissionDTO.getTitle());
                    permissionService.updateByPrimaryKeySelective(permission);
                }

                UmsRolePermissionRelation rolePermissionRelation = new UmsRolePermissionRelation();
                rolePermissionRelation.setRoleId(role.getId());
                rolePermissionRelation.setPermissionId(permission.getId());
                rolePermissionRelationService.add(rolePermissionRelation);
            }
        }
    }
}
