package com.hhf.mall.product.service.module.pms.skuStock.dto;

import com.hhf.mall.product.service.module.pms.skuStock.entity.PmsSkuStock;
import lombok.Data;


/**
 * @author hhf
 * @description: 
 * @date 2022-04-14 0:05
 */
@Data
public class ProductSkuDTO extends PmsSkuStock {
    private String spData;
}
