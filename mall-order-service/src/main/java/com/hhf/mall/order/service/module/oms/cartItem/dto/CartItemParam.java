package com.hhf.mall.order.service.module.oms.cartItem.dto;

import lombok.Data;

/**
 * @author hhf
 * @description: 
 * @date 2022-02-04 0:57
 */
@Data
public class CartItemParam {
    //用户
    private Long memberId;

    //商品
    private Long productId;

    //sku
    private Long productSkuId;

    //数量
    private Integer quantity;

    //类型，0->购物车，1->立即购买
    private Integer type;
}
