package com.hhf.mall.feign.product;

import com.hhf.mall.common.Const;
import com.hhf.mall.common.constant.ApiPrefix;
import com.hhf.mall.common.exception.ApiResponse;
import com.hhf.mall.config.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hhf
 * @description: 评论回复feign接口
 * @date 2022/7/20 16:46
 */
@FeignClient(value = Const.PRODUCT_SERVICE_NAME, configuration = FeignClientConfig.class, contextId = "CommentReplyClient")
public interface CommentReplyClient {
    String URL_PREFIX = Const.PRODUCT_SERVICE_CONTEXT_PATH + ApiPrefix.FEIGN + "/commentReply";

    /**
     * 通过memberId统计
     * @param memberId
     * @return
     */
    @GetMapping(URL_PREFIX + "/countByMemberId")
    ResponseEntity<ApiResponse> countByMemberId(@RequestParam Long memberId);
}
