package com.hhf.mall.basic.service.module.cms.helpCategory.dto;

import com.hhf.mall.basic.service.module.cms.helpCategory.entity.CmsHelpCategory;
import com.hhf.mall.basic.service.module.cms.helpCategory.entity.HelpCategoryStatus;
import com.hhf.mall.common.dto.BaseDTOMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hhf
 * @description: 
 * @date 2023/1/11 11:28
 */
@Component
public class HelpCategoryVOMapper extends BaseDTOMapper<CmsHelpCategory, HelpCategoryVO> {
    @Override
    public HelpCategoryVO mapToDTO(CmsHelpCategory entity) {
        HelpCategoryVO vo = new HelpCategoryVO();
        BeanUtils.copyProperties(entity, vo);
        vo.setStatusText(HelpCategoryStatus.instanceOf(entity.getStatus()).getText());
        return vo;
    }

    @Override
    public List<HelpCategoryVO> mapToDTOList(List<CmsHelpCategory> entityList) {
        List<HelpCategoryVO> voList = new ArrayList<>();
        for (CmsHelpCategory entity : entityList) {
            HelpCategoryVO vo = mapToDTO(entity);
            voList.add(vo);
        }
        return voList;
    }
}
